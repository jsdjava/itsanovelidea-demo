const {
  snapshotToText,
  diffToList,
} = require('./utils.js');

test('it should join together all of the inserts in the snapshot ops into one string', () => {
  const text = snapshotToText({
    data: {
      ops: [
        {
          insert: 'i ',
        },
        {
          insert: 'saw ',
        },
        {
          insert: 'the sign',
        },
      ],
    },
  });
  expect(text).toEqual('i saw the sign');
});

test('it should ignore deletes and retains in the snapshot ops', () => {
  const text = snapshotToText({
    data: {
      ops: [
        {
          retain: 9,
        },
        {
          insert: 'weezer',
        },
        {
          delete: 4,
        },
      ],
    },
  });
  expect(text).toEqual('weezer');
});

test('it should return an empty list on an empty diff', () => {
  const diffLines = diffToList({
    ops: [],
  });
  expect(diffLines).toEqual([]);
});

test('it should insert add lines for insert ops', () => {
  const diffLines = diffToList({
    ops: [{
      insert: 'josh',
    }, {
      insert: 'was',
    }, {
      insert: 'here',
    }],
  });
  expect(diffLines).toEqual([{
    add: 'josh',
  }, {
    add: 'was',
  }, {
    add: 'here',
  }]);
});

it('should insert a delete line with correct offset text given a retain and delete op', () => {
  const diffLines = diffToList({
    ops: [{
      retain: 4,
    }, {
      delete: 5,
    }],
  }, '123456789');

  // Note how the retain only serves to increase the cursor for delete ops
  expect(diffLines).toEqual([{
    delete: '56789',
  }]);
});

it('should insert multiple delete lines with correctly offset text given multiple delete ops', () => {
  const diffLines = diffToList({
    ops: [{
      delete: 4,
    }, {
      delete: 5,
    }],
  }, '123456789');

  // Note how the retain only serves to increase the cursor for delete ops
  expect(diffLines).toEqual([{
    delete: '1234',
  }, {
    delete: '56789',
  }]);
});
/*
const diffToList = (diff,snapshotText) =>{
  let cursor = 0;
  const diffOps = diff.ops;
  const diffLines = [];
  for(let curOpSpot = 0; curOpSpot < diffOps.length; curOpSpot++){
    const curOp = diffOps[curOpSpot];
    if(curOp.insert){
      diffLines.push({add:curOp.insert});
    }else if(curOp.delete){
      diffLines.push({delete:snapshotText.slice(cursor,cursor+curOp.delete)});
      cursor+= curOp.delete;
    } else{
      cursor+=curOp.retain;
    }
  }
  return diffLines;
}
*/
