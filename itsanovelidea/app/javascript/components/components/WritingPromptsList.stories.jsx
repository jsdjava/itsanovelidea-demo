import React from 'react';
import WritingPromptsList from "./WritingPromptsList.jsx";

const writingPrompts = require('./fixtures/writingPrompts.json');

export default {
  component: WritingPromptsList,
  title: 'WritingPromptsList',
};

export const writingPromptsList = () => <WritingPromptsList writingPrompts={writingPrompts}/>;

//writingPromptsEmpty

//writingPromptsNarrow
