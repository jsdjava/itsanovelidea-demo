import React from "react";
import ReactQuill from 'react-quill';
import {Flipped } from 'react-flip-toolkit';

import 'react-quill/dist/quill.bubble.css';
import 'react-quill/dist/quill.snow.css';

class StoryViewer extends React.Component{

  constructor(props){
    super(props);
  }

  render(){
    const {
      className,
      publishedStorySpot,
        imageUrl,
        modules={},
        children,
        theme="bubble"
    } = this.props;
    return <Flipped flipId={`body-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}>
    <div className = "storyViewerDiv" style={{
      willChange:"transform",
    }}>
    <Flipped inverseFlipId={`body-${publishedStorySpot}`} shouldInvert={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}>
    <div style={{
      willChange:"transform",
      height:"100%",
    }}>
    {children}
    {imageUrl ? <Flipped flipId={`image-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}><img className={className} src={imageUrl}/></Flipped> :null}
    <ReactQuill
      className = {className}
      modules={modules}
      theme={theme}
      readOnly={true}
      ref={(reactQuill)=>{
        if(reactQuill && !this.quill){
          const {story} = this.props;
          let parsedStory = {};
          try{
            parsedStory = JSON.parse(story).data;
          }catch(err){
            console.log("Couldn't parse the text for the story");
            console.log(err);
            console.log(story);
          }
          this.quill = reactQuill.getEditor();
          this.quill.setContents(parsedStory);

          // TODO Not sure if i need this or not .....
          //this.quill.enable();
        }
    }}
    />
    </div>
    </Flipped>
    </div>
    </Flipped>
  }
}

export default StoryViewer;