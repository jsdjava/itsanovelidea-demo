const {default:initStoryshots,multiSnapshotWithOptions,snapshotWithOptions} = require('@storybook/addon-storyshots');

initStoryshots({
    integrityOptions: { cwd: __dirname },
    storyKindRegex:/^(?!.*DashboardSearchBar|DashboardNavBar|.*StoryHistory|.*StoryQuill|.*WelcomeNavBar).*$/,
    test: snapshotWithOptions({
      createNodeMock: (element) => {
        return document.createElement('div');
      },
    }),
});
