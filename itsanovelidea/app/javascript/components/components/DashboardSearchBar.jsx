
import React from "react";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';

import "../stylesheets/DashboardSearchBar.css";

class DashboardSearchBar extends React.Component{
  render(){
    const {search=()=>{}} = this.props;
    return <div class="dashboardSearchDiv">
        <TextField
            label="Title"
            InputProps={{
            startAdornment: (
                <InputAdornment position="start">
                    <SearchIcon/>
                </InputAdornment>
            ),
            }}
            onChange={(e)=>{
                search({searchName:e.target.value});
            }}
        />
        <FormControlLabel 
            labelPlacement="top"
            control={<Checkbox/>} 
            label="Published"
            onChange={(e)=>{
                search({published:e.target.checked});
            }} 
        />
        </div>
  }
}

export default DashboardSearchBar;
