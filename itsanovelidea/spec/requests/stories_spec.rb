require 'rails_helper'

RSpec.describe "Stories", type: :request do
  describe "POST /stories" do
    describe "logged in" do
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      
      #TODO I don't know, this test is pretty brittle
      def check_fails(service_name)
        writing_prompt = create(:writing_prompt)
        hit_service = false
        expect(RestClient).to receive(:post) do |url|
          if url.include? service_name
            hit_service = true
            throw "couldn't connect to #{service_name} server bud"
          end
          double("postResponse",:body=>{
            idFile:"test.png",
            width:"600px",
            height:"500px",
            backgroundColor:"FFFFFFFF",
            textColor:"AAAAAAAA"
          }.to_json)
        end.at_least(:once)
        allow(RestClient).to receive(:get).and_return(double("getResponse",:body=>"photoUrl"))
        headers = { "CONTENT_TYPE" => "application/json" }
        post "/stories.json", :params => "{\"story\":{\"text\":\"mystory\",\"name\":\"myName\",\"search_hash\":\"who am i\",\"index\":10,\"writing_prompt_id\":#{writing_prompt.id}}}", :headers=>headers
        expect(response).to have_http_status(422)
        expect(Story.all.count).to eql(0)
        expect(hit_service).to eql(true)
      end

      it "fails if it can't create the document in the document ms" do
        RestClient = double
        check_fails("document")
      end
      it "fails if it can't create the chat in the chat ms" do
        RestClient = double
        check_fails("chat")
      end
      it "fails if it can't download the photo in the photo ms" do
        RestClient = double
        check_fails("photo")
      end
      it "fails if properties are invalid/missing for the story"
      it "fails if it can't get the photo url from the photo ms"
      it "creates the story" do
        RestClient = double(RestClient)
        allow(RestClient).to receive_messages(:post=>double("postResponse",:body=>{
          idFile:"test.png",
          width:"600px",
          height:"500px",
          backgroundColor:"FFFFFFFF",
          textColor:"AAAAAAAA"
        }.to_json),:get=>double("getResponse",:body=>"photoUrl"))
        writing_prompt = create(:writing_prompt)
        headers = { "CONTENT_TYPE" => "application/json" }
        post "/stories.json", :params => "{\"story\":{\"text\":\"mystory\",\"name\":\"myName\",\"search_hash\":\"who am i\",\"index\":10,\"writing_prompt_id\":#{writing_prompt.id}}}", :headers=>headers
        expect(response).to have_http_status(201)
        expect(JSON.parse(response.body)["name"]).to eql("myName")
        expect(Story.all.count).to eql(1)    
      end
    end
  end
  describe "PUT /stories/:id/rating" do
    describe "logged in" do
      user = nil
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      it "returns 404 when it can't find the story" do
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/stories/3/rating.json", :params => "{\"rating\":3}", :headers=>headers
        expect(response).to have_http_status(404)
      end
      it "creates a new UserStory if the user hasn't rated the story" do
        story,other_story = create_list(:story,2)
        create(:user_story,rating:3,story:story)
        create(:user_story,rating:4,user:user,story:other_story)
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/stories/#{story.id}/rating.json", :params => "{\"rating\":3}", :headers=>headers
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rating"]).to eql(3)
        expect(UserStory.count).to eql(3)
      end
      it "updates the UserStory if the user has rated the story" do
        story = create(:story)
        create(:user_story,rating:4,story:story,user:user)
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/stories/#{story.id}/rating.json", :params => "{\"rating\":5}", :headers=>headers
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rating"]).to eql(5)
        expect(UserStory.count).to eql(1)
      end
    end
  end
  describe "GET /stories/:id/rating" do
    describe "logged in" do
      user = nil
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      it "returns 404 when it can't find a user_story (with the rating)" do
        story = create(:story)
        get "/stories/#{story.id}/rating.json"
        expect(response).to have_http_status(404)
      end
      it "returns the rating given to the story by the user" do
        story = create(:story)
        create(:user_story,story:story,user:user,rating:2)
        get "/stories/#{story.id}/rating.json"
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rating"]).to eql(2)
      end
    end
  end
  describe "GET /stories" do
    describe "logged in" do
      user = nil
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      it "returns an empty list when the current user has no stories" do
        create_list(:story,2)
        get "/stories.json"
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)).to eql([])
      end
      it "returns a properly filtered/paged/sorted list" do
        create_list(:story,2)
        create_list(:story,3,owner:user)
        expected_stories = create_list(:story,4,owner:user,published_text:"soryukenasdf").to_a.slice(0,2).reverse
        get "/stories.json?published=true&page=1&page_size=2&sort_by_date=DESC"
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body).map{|actual_story| actual_story["id"]}).to eql(expected_stories.map{|expected_story| expected_story.id})
      end
    end
  end
end
