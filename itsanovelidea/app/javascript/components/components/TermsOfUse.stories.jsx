import React from 'react';
import TermsOfUse from "./TermsOfUse.jsx";

export default {
  component: TermsOfUse,
  title: 'TermsOfUse',
};

export const storyTermsOfUse = () => <div style={{
    height:"500px"
}}><TermsOfUse isOpen={true}/></div>;

export const storyTermsOfUseClosed = () => <div style={{
    height:"500px"
}}><TermsOfUse/></div>;
