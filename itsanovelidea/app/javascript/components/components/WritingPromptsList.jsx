import React from "react";
import {
    Table,TableBody,TableCell,TableFooter,TablePagination,TableRow,TableSortLabel,TableHead,TextField,Link
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

import MaterialLoadingOverlay from "./MaterialLoadingOverlay.jsx";
import WritingPromptCard from "./WritingPromptCard.jsx";
import {fetchWithToken} from "../helpers/api.js";

import "../stylesheets/WritingPromptsList.css";

const debounce = require('debounce'); 
const moment = require('moment');

class WritingPromptsList extends React.Component{
    
    constructor(){
        super();
        this.state = {
            writingPrompts:[],
            writingPromptsCount:0,
            page:1,
            orderDirection:'desc',
            pageSize:10,
            firstLoad:true,
        }
        this.debouncedGetFilteredWritingPrompts = debounce(this.getFilteredWritingPrompts.bind(this),1000);
    }

    async getFilteredWritingPrompts(newState={}){
        const {
            url,
            count_url,
        } = this.props;
        const {
            pageSize,
            page,
            orderProp,
            orderDirection,
            authorSearch,
            promptSearch,
        } = {
          ...this.state,
           ...newState,
           ...this.props,
        };
        try{
            this.setState({
              loaded:false,
            });
            const paramsUrl = `${authorSearch? 'user_like='+authorSearch:''}&${promptSearch? 'text='+promptSearch:''}&${orderProp}=${orderDirection}&page=${page}&page_size=${pageSize}`
            const writingPromptsResp = await fetchWithToken(`${url}?${paramsUrl}`);
            const writingPromptsCountResp = await fetchWithToken(`${count_url}?${paramsUrl}`);
            if(!writingPromptsResp.ok){
                throw writingPromptsResp;
            }
            if(!writingPromptsCountResp.ok){
                throw writingPromptsCountResp;
            }
            const writingPrompts = await writingPromptsResp.json();
            const writingPromptsCount = (await writingPromptsCountResp.json()).num_writing_prompts;
            this.setState({
                writingPrompts,
                writingPromptsCount,
                page,
                loaded:true,
                firstLoad:false,
                orderProp,
                orderDirection,
                authorSearch,
                promptSearch,
            });
        } catch(e){
            console.log("Could not get writing prompts");
            console.log(e);
        }
    }
    componentDidMount(){
        this.debouncedGetFilteredWritingPrompts();
    }

    sort(newOrderProp){
        const {orderDirection,orderProp} = this.state;
        const isDesc = orderProp === newOrderProp && orderDirection === 'desc';
        this.debouncedGetFilteredWritingPrompts({
            orderProp:newOrderProp,
            orderDirection: isDesc ? 'asc' : 'desc',
            page:1,
        });
    }

    render(){
        const {authorSearch: hideAuthorFilter,leftHalf} = this.props;

        //Passing props as well just for the storybook
        const {
            writingPrompts,
            writingPromptsCount,
            orderDirection,
            orderProp,
            page,
            pageSize,
            loaded,
            firstLoad,
        } = {...this.state,...this.props};
        
        const getCreatedAt = writingPrompt=>moment(writingPrompt.created_at).format("MM/DD/YYYY");

        return <div className="writingPromptsListDiv">
        {firstLoad ? <CircularProgress size={leftHalf ? "5vw": "10vw"} thickness={2} style={{
        position: "fixed",
        left: leftHalf ? "20%" : "45%",
        top: "45%",
        "z-index": "9001"
     }}/>:
        <MaterialLoadingOverlay loading = {!loaded}>
        <Table size="small">
        <TableHead>
            <TableRow>
                <TableCell align="left" className="textSearchCell">
                    <TextField 
                        label="Prompt"
                        onChange={e=>{
                            this.debouncedGetFilteredWritingPrompts({
                                page:1,
                                promptSearch:e.target.value,
                            });
                        }}/>
                </TableCell>
                {hideAuthorFilter ? null :
                <TableCell align="left" className="authorSearchCell">
                    <TextField label="Author"
                     onChange={e=>{
                        this.debouncedGetFilteredWritingPrompts({
                            authorSearch:e.target.value,
                            page:1,
                        });
                    }}/>
                </TableCell>
                }
                <TableCell align="left">
                    <TableSortLabel
                        active={orderProp==='sort_by_rating'}
                        direction={orderDirection}
                        onClick={()=>this.sort("sort_by_rating")}>
                    Rating
                    </TableSortLabel>
                </TableCell>
                <TableCell align="left">
                    <TableSortLabel
                        active={orderProp==='sort_by_date'}
                        direction={orderDirection}
                        onClick={()=>this.sort("sort_by_date")}>
                    Date
                    </TableSortLabel>
                </TableCell>
                <TableCell align="left">
                    <TableSortLabel
                        active={orderProp==='sort_by_published_stories'}
                        direction={orderDirection}
                        onClick={()=>this.sort("sort_by_published_stories")}>
                    Stories
                    </TableSortLabel>
                </TableCell>
                <TableCell align="left">
                    <TableSortLabel
                        active={orderProp==='sort_by_views'}
                        direction={orderDirection}
                        onClick={()=>this.sort("sort_by_views")}>
                    Views
                    </TableSortLabel>
                </TableCell>
            </TableRow>
        </TableHead>
        <div className="writingPromptCardsListDiv">
            {
                writingPrompts.map(writingPrompt=><WritingPromptCard
                    text={writingPrompt.text}
                    author={writingPrompt.owner.email}
                    rating={writingPrompt.rating}
                    views={writingPrompt.impressions_count}
                    stories={writingPrompt.num_published_stories}
                    date={getCreatedAt(writingPrompt)}
                    url={writingPrompt.url}
                />)
            }
        </div>
        <TableBody>
            {writingPrompts.map((writingPrompt,spot) => (
              <TableRow key={spot}>
                <TableCell component="th" scope="row">
                  <Link  
                    className="writingPromptLink"
                    color="inherit"
                    onClick={() => window.location.href = writingPrompt.url.replace(".json",".html")}>
                  {writingPrompt.text}
                  </Link>
                </TableCell>
                {hideAuthorFilter ? null : <TableCell align="right">{writingPrompt.owner.email}</TableCell>}
                <TableCell align="right" size="small">{writingPrompt.rating}</TableCell>
                <TableCell align="right" size="small">{getCreatedAt(writingPrompt)}</TableCell>
                <TableCell align="right" size="small">{writingPrompt.num_published_stories}</TableCell>
                <TableCell align="right" size="small">{writingPrompt.impressions_count}</TableCell>
              </TableRow>
            ))}

            {!writingPrompts.length && (
              <TableRow style={{ height: 48 * writingPrompts.length }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                colSpan={3}
                count={writingPromptsCount}
                rowsPerPageOptions={[pageSize]}
                rowsPerPage={pageSize}
                page={page-1}
                onChangePage={(event,page)=>{
                    this.debouncedGetFilteredWritingPrompts({page:page+1});
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
        </MaterialLoadingOverlay>
        }
        </div>
    }
}

export default WritingPromptsList;