#https://arkadiyt.com/2018/01/26/deploying-effs-certbot-in-aws-lambda/  - Copied from here

import boto3
import certbot.main
import datetime
import os
import raven
import subprocess

def read_and_delete_file(path):
  with open(path, 'r') as file:
    contents = file.read()
  os.remove(path)
  return contents

def get_domain_zone_id(domain_name):
  client = boto3.client('route53')
  response = client.list_hosted_zones_by_name(
    DNSName=domain_name, 
    MaxItems='1'
  )
  if not response or response['DNSName'] != domain_name:
    raise Exception("Hosted zone for domain not found")
  hosted_zone = response['HostedZones'][0]['Id']
  return hosted_zone

def wipe_subdomain(domain_zone_id,backend_domain):
  client = boto3.client('route53')
  response = client.list_resource_record_sets(
    HostedZoneId=domain_zone_id,
    StartRecordName=backend_domain,
  )['ResourceRecordSets']

  if not response or response[0]['Name'] != backend_domain:
    raise Exception("Backend domain not found")

  subdomain_props = {
    "type":response[0]['Type'],
    "resource_records":response[0]['ResourceRecords'],
    "ttl":response[0]['TTL']
  }

  client.change_resource_record_sets(
    HostedZoneId=domain_zone_id,
    ChangeBatch={
        'Changes': [
            {
                'Action': 'DELETE',
                'ResourceRecordSet': {
                    'Name': backend_domain,
                    'Type': subdomain_props['type'],
                    'TTL': subdomain_props['ttl'],
                    'ResourceRecords': subdomain_props['resource_records']
                }
            },
        ]
    }
  )
  return subdomain_props

def create_subdomain(domain_zone_id,subdomain_props,subdomain):
  client = boto3.client('route53')
  response = client.change_resource_record_sets(
    HostedZoneId=domain_zone_id,
    ChangeBatch={
        'Changes': [
            {
                'Action': 'CREATE',
                'ResourceRecordSet': {
                    'Name': subdomain,
                    'Type': subdomain_props['type'],
                    'TTL': subdomain_props['ttl'],
                    'ResourceRecords': subdomain_props['resource_records']
                }
            },
        ]
    }
  )

def provision_cert(email, domains):
  certbot.main.main([
    'certonly',                             # Obtain a cert but don't install it
    '-n',                                   # Run in non-interactive mode
    '--agree-tos',                          # Agree to the terms of service,
    '--email', email,                       # Email
    '--dns-route53',                        # Use dns challenge with route53
    '-d', domains,                          # Domains to provision certs for
    # Override directory paths so script doesn't have to be run as root
    '--config-dir', '/tmp/config-dir/',
    '--work-dir', '/tmp/work-dir/',
    '--logs-dir', '/tmp/logs-dir/',
  ])

  first_domain = domains.split(',')[0]
  path = '/tmp/config-dir/live/' + first_domain + '/'
  return {
    'certificate': read_and_delete_file(path + 'cert.pem'),
    'private_key': read_and_delete_file(path + 'privkey.pem'),
    'certificate_chain': read_and_delete_file(path + 'chain.pem')
  }

def should_provision(domains):
  min_created_date = find_existing_cert(domains)
  if min_created_date:
    now = datetime.datetime.now(datetime.timezone.utc)
    return (now - min_created_date).days >= 30
  else:
    return True


def find_existing_cert(domains):
  client = boto3.client('secretsmanager')
  secret_ids = ["certificate","private_key","certificate_chain"]
  secret_prefix = domains.split(",")[0]
  min_created_date = None
  for secret_id in secret_ids:
    try:
      resp = client.get_secret_value(
        SecretId = f"{secret_prefix}-{secret_id}"
      )
      min_created_date = resp['CreatedDate']
    except:
      return None
  return min_created_date


def upload_cert_to_secret_manager(cert,domains):
  client = boto3.client('secretsmanager')

  secret_ids = ["certificate","private_key","certificate_chain"]
  secret_prefix = domains.split(",")[0]

  for secret_id in secret_ids:
    secret_name = f"{secret_prefix}-{secret_id}"
    try:
      client.put_secret_value(
        SecretId= secret_name,
        SecretString = cert[secret_id]
      )
    except:
      client.create_secret(
        Name= secret_name,
        SecretString = cert[secret_id] 
      )
  

def handler(event, context):
  try:
    domains = os.environ['LETSENCRYPT_DOMAINS']
    subdomain = os.environ['SUBDOMAIN']
    if should_provision(domains):
      domain_zone_id = get_domain_zone_id(domains.split(",")[0])
      subdomain_props = wipe_subdomain(domain_zone_id,subdomain)
      try:
        cert = provision_cert(os.environ['LETSENCRYPT_EMAIL'], domains)
      except:
        create_subdomain(domain_zone_id,subdomai_props,subdomain)
        raise
      create_subdomain(domain_zone_id,subdomain_props,subdomain)
      upload_cert_to_secret_manager(cert,domains)
      return "Provisioned cert"
    else:
      return "Cert is up to date"
  except:
    #client = raven.Client(os.environ['SENTRY_DSN'], transport=raven.transport.http.HTTPTransport)
    #client.captureException()
    raise