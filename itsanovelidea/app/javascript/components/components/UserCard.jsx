import React from "react";

import Card from "@material/react-card";
import CardContent from '@material-ui/core/CardContent';
import Rate from "./Rate.jsx";
import Views from "./Views.jsx";
import "../stylesheets/UserCard.css";

class UserCard extends React.Component{
    constructor(){
        super();
        this.state={};
    }
    render(){
        const {
            email,
            user_url,
            stories_rating,
            total_stories_views,
            prompts_rating,
            total_writing_prompts_views,
            total_stories,
            total_writing_prompts,
            total_writing_prompts_responses,
        } = this.props;
        return <Card className="userCardDiv" raised={true}>
            <CardContent>
                <div className="headingDiv">
                    <p className="emailParagraph">
                        <a href={user_url}>{email}</a>
                    </p>
                </div>
                <div className="footerDiv">
                    <p>Stories:</p>
                    <p>Total: {total_stories}  <Rate rating={stories_rating}/> <Views views={total_stories_views}/></p>
                    <p>Prompts:</p>
                    <p>Total: {total_writing_prompts} Responses: {total_writing_prompts_responses} <Rate rating={prompts_rating}/> <Views views={total_writing_prompts_views}/></p>
                </div>
            </CardContent>
        </Card>;
    };
}

export default UserCard;