require 'rest-client'

class PhotosController < ApplicationController
  skip_before_action :authenticate_user!, only: [:files]
    
    def search
      response = RestClient.get(Rails.configuration.photo_host+request.fullpath,{
        apikey: Rails.configuration.node_api_key,
      })
      render :json=>response
    end
    
    def download
      response = RestClient.post(Rails.configuration.photo_host+request.fullpath,request.raw_post,{
        apikey: Rails.configuration.node_api_key,
      })
      render :json=>response
    end
    
    def getLink
      response = RestClient.get(Rails.configuration.photo_host+request.fullpath,{
        apikey: Rails.configuration.node_api_key,
      })
      render :json=>response
    end
    
    def files
      redirect_to "http://"+Rails.configuration.photo_host+request.fullpath
    end
end
