import React from 'react';
import TopPrompts from './TopPrompts.jsx';

export default {
  component: TopPrompts,
  title: 'TopPrompts',
};

const buildPrompt=(overrides={})=>({
    owner:{
        email:"meme@yahoo.com",
    },
    text:"very interesting prompt here",
    rating:"4",
    created_at: "08/17/1995",
    num_stories:10,
    impressions_count:3,
    ...overrides,
})

export const topPrompts = () => <TopPrompts
    highestRatedPrompt={buildPrompt({
        rating:"5",
    })}
    mostViewedPrompt={buildPrompt({
        text:"man, lots of people like this story",
        owner:{
            email:"lotsofviews@asdf.com",
        },
        impressions_count:9000,
    })}
    mostRecentPrompt={buildPrompt({
        text:"new prompt idea here man",
        owner:{
            email:"made@recently.com",
        },
    })}
    mostRepliedPrompt={buildPrompt({
        text:"lots of people wrote stories about this one",
        owner:{
            email:"replied@computer.com",
        },
    })}
/>
