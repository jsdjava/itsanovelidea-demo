import React from "react";

import Card from "@material/react-card";
import CardContent from '@material-ui/core/CardContent';
import Link from '@material-ui/core/Link';
import Rate from "./Rate.jsx";
import Views from "./Views.jsx";
import BookIcon from '@material-ui/icons/Book';
import "../stylesheets/WritingPromptCard.css";

class WritingPromptCard extends React.Component{
    constructor(){
        super();
        this.state={};
    }
    render(){
        const {
            text,
            author,
            rating,
            date,
            stories,
            views,
            url,
        } = this.props;
        return <Card className="writingPromptCardDiv" raised={true}>
            <CardContent>
                <div className="headingDiv">
                    <p className="authorParagraph">{author}</p>
                    <p className="dateParagraph">{date}</p>
                </div>
                <Link  
                    className="writingPromptLink"
                    color="inherit"
                    onClick={() => window.location.href = url.replace(".json",".html")}>
                  {text}
                </Link>
                <div className="footerDiv">
                    <div className="storiesCountDiv">
                        {stories} 
                        <BookIcon/>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <Views views={views}/>
                    <Rate rating={rating}/>
                </div>
            </CardContent>
        </Card>;
    };
}

export default WritingPromptCard;