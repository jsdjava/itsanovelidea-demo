require 'rails_helper'

RSpec.describe ChatController, type: :controller do
    describe "signed in" do
        login_user
    
        before(:each) do
            RestClient = double
        end
        it "200s and gets previous messages" do
            #request.accept = "application/json"
            #get :get_previous_messages, params: {documentId: 1}
            #expect(response.status).to eq(200)
        end
        it "404s on a missing story id" do
            request.accept = "application/json"
            get :get_previous_messages, params: {documentId: 300}
            expect(response.status).to eq(404)
        end
        it "403s on accessing an unowned story" do
        end
        it "500s on an error in the itsanovelida-chat" do
        end
    end
    describe "not signed in" do   
    end
end