class RenameRatingsTable < ActiveRecord::Migration[5.1]
  def change
    rename_table :rating, :ratings
  end
end
