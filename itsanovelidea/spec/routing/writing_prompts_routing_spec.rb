require "rails_helper"

RSpec.describe WritingPromptsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/writing_prompts").to route_to("writing_prompts#index")
    end

    it "routes to #new" do
      expect(:get => "/writing_prompts/new").to route_to("writing_prompts#new")
    end

    it "routes to #show" do
      expect(:get => "/writing_prompts/1").to route_to("writing_prompts#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/writing_prompts").to route_to("writing_prompts#create")
    end
 
    it "routes to #rate" do
      expect(:put => "/writing_prompts/1/rate").to route_to("writing_prompts#rate", :id => "1")
    end

    it "routes to #count" do
      expect(:get => "/writing_prompts/count").to route_to("writing_prompts#count")
    end

  end
end
