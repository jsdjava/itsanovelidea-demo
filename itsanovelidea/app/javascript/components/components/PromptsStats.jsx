import React from "react";
import {
  Table,TableBody,TableCell,TableRow,TableHead,
} from '@material-ui/core';

import Rate from "./Rate.jsx";

import "../stylesheets/PromptsStats.css"

class PromptsStats extends React.Component{
  render(){ 
    const {
      writingPromptsCount,
      rating,
      totalResponses,
      totalViews,
    } = this.props;
    return <div className="promptsDiv">
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>
              Total Prompts
            </TableCell>
            <TableCell>
              Average Rating
            </TableCell>
            <TableCell>
              Total Responses
            </TableCell>
            <TableCell>
              Total Views
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell size="small">{writingPromptsCount}</TableCell>
            <TableCell size="small" classes={{
              root:"ratingTableCell" 
            }}><Rate rating={rating}/></TableCell>
            <TableCell size="small">{totalResponses}</TableCell>
            <TableCell size="small">{totalViews}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </div>;
    /*return <div className="promptsDiv">
      <p>Total Prompts: {writingPromptsCount}</p> 
      <p className="averageRatingParagraph">Average Rating: </p> <Rate rating={rating}/>
      <p>Total Responses: {totalResponses}</p>
      <p>Total Views: {totalViews}</p>
    </div>*/
  }
}

export default PromptsStats;