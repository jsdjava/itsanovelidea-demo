#!/bin/bash

# ECS config - should probably also being passing in the cluster name
{
  echo "ECS_CLUSTER=itsanovelidea"
  echo 'ECS_INSTANCE_ATTRIBUTES={"itsanovelidea-type": "${subdomain}"}'
} >> /etc/ecs/ecs.config

start ecs

echo "Done"