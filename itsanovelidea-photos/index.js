const { config, logger } = require('itsanovelidea-middleware');
const app = require('./app.js');

const port = config.get('photosPort');

app.listen(port, () => {
  logger.log('info', `itsanovelidea-photo listening at ${port}`);
});
