class ChangeWritingPromptsTextColumn < ActiveRecord::Migration[5.1]
  def change
    change_column :writing_prompts, :text, :text, :limit => 65535
  end
end
