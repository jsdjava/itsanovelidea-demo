class ChangeStoriesTextColumn < ActiveRecord::Migration[5.1]
  def change
    change_column :stories, :text, :text, :limit => 16777215
  end
end
