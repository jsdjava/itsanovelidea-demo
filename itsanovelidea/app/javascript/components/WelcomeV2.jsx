import React from "react";
import PropTypes from "prop-types";
   
import {fetchWithToken} from "./helpers/api.js";
import StoriesGallery from "./components/StoriesGallery.jsx";
import WritingPromptCarousel from "./components/WritingPromptCarousel.jsx";

import "./stylesheets/Welcome.css";

class WelcomeV2 extends React.Component {
  constructor(props){
    super(props);
    this.state={
      writingPrompt:{},
      writingPrompts:[],
      storiesByWritingPrompt:{},

      // TODO Is the page size a problem for the scrolling? I feel like you are screwed if the scrollbar doesn't show up,
      // but there are more pages to load
      pageSize:12,
    };
    this.getWritingPrompts = this.getWritingPrompts.bind(this);
    this.getStories = this.getStories.bind(this);
  }

  getWritingPrompts(){
    const {writingPromptsUrl} = this.props;
    const{writingPrompts} = this.state;
    fetchWithToken(`${writingPromptsUrl}?sort_by_random=true&min_published_stories=6`)
      .then(resp=>{
        if(resp.ok){
          resp.json().then(newWritingPrompts=>{
            const combinedWritingPrompts = [...writingPrompts,...newWritingPrompts];
            this.setState({
              loadedWritingPrompts:true,
              writingPrompts:combinedWritingPrompts,
              writingPrompt:combinedWritingPrompts[0] || {},
            });
            newWritingPrompts.forEach(newWritingPrompt=>this.getStories(newWritingPrompt));
          });
        }
      })
      .catch(err=>{
        console.log("Could not get writing prompts");
        console.log(err);
      });
  }

  async getStories(writingPrompt){
    const {publishedStoriesUrl} = this.props;
    const {storiesByWritingPrompt,pageSize} = this.state;
    const storiesResult = storiesByWritingPrompt[writingPrompt.id] || {
      page:0,
      stories:[],
    };
    const {page,stories} = storiesResult;
    let {num_published_stories:storiesCount} = writingPrompt;

    try{
      const resp = await fetchWithToken(`${publishedStoriesUrl}${writingPrompt.id}?page=${page+1}&page_size=${pageSize}`)
      if(resp.ok){
        const newStories = await resp.json();
        this.setState({
          storiesByWritingPrompt:{
            ...this.state.storiesByWritingPrompt,
            [writingPrompt.id]:{
              loadedStories:true,
              page:page+1,
              storiesCount,
              stories:[...stories,...newStories],
            },
          }
        });
      }
    }catch(err){
      console.log("Could not get stories or stories count");
      console.log(err);
    }
  }

  componentDidMount(){
    this.getWritingPrompts();
  }

  render () {
    const {
      showTermsOfUse,
      writingPrompts,
      writingPrompt,
      focusedStory,
      storiesByWritingPrompt,
      loadedWritingPrompts,
    } = this.state;
    const {
      stories,
      storiesCount,
      loadedStories,
    } = storiesByWritingPrompt[writingPrompt.id] || {stories:[]};
    return <div>
      <WritingPromptCarousel 
        loadedWritingPrompts={loadedWritingPrompts}
        writingPrompts={writingPrompts} 
        onChange={writingPrompt=>{
          this.setState({
            writingPrompt:writingPrompt,
          })}
        }
      />
      <StoriesGallery
        viewOnly = {true}
        loadedStories={!loadedWritingPrompts || loadedStories}
        key={writingPrompt.id}
        hasMore={storiesCount>stories.length}
        publishedStories={stories} 
        next = {()=>{this.getStories(writingPrompt)}}
        containerClassName="welcomePageDiv"
        onFocusedStoryChange={(focused)=>this.setState({focusedStory:focused})}
      />
    </div>
  }
}

WelcomeV2.propTypes = {
  writingPromptsUrl: PropTypes.string.isRequired,
  publishedStoriesUrl: PropTypes.string.isRequired,
};

export default WelcomeV2;