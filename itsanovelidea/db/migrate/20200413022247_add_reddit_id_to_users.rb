class AddRedditIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :reddit_id, :string
  end
end
