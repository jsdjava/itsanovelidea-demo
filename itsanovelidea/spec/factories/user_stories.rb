FactoryBot.define do
  factory :user_story do
    association :user
    association :story
    rating {4}
  end
end
