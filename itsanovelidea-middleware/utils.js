const request = require('request-promise');
const config = require('config');

const redis = require('./redis.js');

// Given a cookie containing the validation token, returns a user from redis.
// The user is placed in redis on login by the Rails server
const getUser = async (validationTokenKey) => {
  // redis.hget instead of hget is necessary for mocking
  // You can't just import hget directly because then it won't
  // get mocked out.
  const user = await redis.hget('auth_keys', validationTokenKey);
  if (!user) {
    throw Error('User has expired');
  }
  return JSON.parse(user);
};
// Determines if a user is allowed to edit a story by calling a route on the rails server
const hasStoryAccess = async (userId, documentId) => {
  try {
    await request(`${config.get('railsUrl')}/stories/${documentId}/users/${userId}`);
    return true;
  } catch (e) {
    return e;
    // Completely fine, we will just return false
  }
  // return false;
};

module.exports = {
  getUser,
  hasStoryAccess,
};
