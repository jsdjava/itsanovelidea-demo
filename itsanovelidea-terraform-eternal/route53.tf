resource "aws_route53_zone" "itsanovelidea-zone" {
  name = "itsanovelidea.com"
}

resource "aws_route53_record" "itsanovelidea-ns" {
  allow_overwrite = true
  name            = "itsanovelidea.com"
  ttl             = 300
  type            = "NS"
  zone_id         = "${aws_route53_zone.itsanovelidea-zone.zone_id}"

  # Pull these records from the name servers at https://us-east-1.console.aws.amazon.com/route53/home#DomainDetail:itsanovelidea.com
  records = [
    "ns-549.awsdns-04.net",
    "ns-307.awsdns-38.com",
    "ns-1872.awsdns-42.co.uk",
    "ns-1061.awsdns-04.org",
  ]
}

data "aws_route53_zone" "backend-itsanovelidea-zone" {
  name         = "backend.itsanovelidea.com."
  private_zone = false
  depends_on = [aws_service_discovery_public_dns_namespace.itsanovelidea]
}

resource "aws_route53_record" "backend-itsanovelidea-ns" {
  allow_overwrite = true
  name            = "backend.itsanovelidea.com"
  ttl             = 300
  type            = "NS"
  zone_id         = "${aws_route53_zone.itsanovelidea-zone.zone_id}"

  records = [
    "${data.aws_route53_zone.backend-itsanovelidea-zone.name_servers.0}",
    "${data.aws_route53_zone.backend-itsanovelidea-zone.name_servers.1}",
    "${data.aws_route53_zone.backend-itsanovelidea-zone.name_servers.2}",
    "${data.aws_route53_zone.backend-itsanovelidea-zone.name_servers.3}",
  ]
}

resource "aws_route53_record" "itsanovelidea-a" {
  zone_id = "${aws_route53_zone.itsanovelidea-zone.zone_id}"
  name    = "itsanovelidea.com"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.itsanovelidea-redirect.website_domain}"
    zone_id                = "${aws_s3_bucket.itsanovelidea-redirect.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www-itsanovelidea-a" {
  zone_id = "${aws_route53_zone.itsanovelidea-zone.zone_id}"
  name    = "www.itsanovelidea.com"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.itsanovelidea-redirect.website_domain}"
    zone_id                = "${aws_s3_bucket.itsanovelidea-redirect.hosted_zone_id}"
    evaluate_target_health = false
  }
}