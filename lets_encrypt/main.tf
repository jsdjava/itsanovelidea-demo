provider "aws" {
  region = "us-east-1"
}

# Z3AQBSTGFYJSTF=HOSTED_ZONE_ID, pulled from itsanovelidea-terraform-eternal, its the zone for itsanovelidea.com
# 242929592693=ACCOUNT_NUMBER, comes from AWS, used to identify all my certificates

resource "aws_iam_policy" "lets_encrypt_policy" {
  name        = "lets_encrypt_policy"
  path        = "/"
  description = "IAM policy for lets encrypt"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "route53:ListResourceRecordSets",
                "route53:ListHostedZonesByName",
                "route53:ListHostedZones",
                "logs:PutLogEvents",
                "logs:CreateLogStream",
                "logs:CreateLogGroup",
                "cloudwatch:PutMetricData",
                "secretsmanager:*"
            ],
            "Resource": "*"
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "route53:GetChange",
                "route53:ChangeResourceRecordSets"
            ],
            "Resource": [
                "arn:aws:route53:::hostedzone/Z08113571SRJ67FVKQJ0M",
                "arn:aws:route53:::change/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lets_encrypt_role_attach_policy" {
  role       = "${aws_iam_role.iam_for_lets_encrypt.name}"
  policy_arn = "${aws_iam_policy.lets_encrypt_policy.arn}"
}

resource "aws_iam_role" "iam_for_lets_encrypt" {
  name = "iam_for_lets_encrypt"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "template_file" "lets_encrypt_main_template" {
  template = "${file("${path.module}/lets_encrypt.py")}"
}

resource "local_file" "lets_encrypt_main" {
  content  = data.template_file.lets_encrypt_main_template.rendered
  filename = "${path.module}/python_deps/lets_encrypt.py"
}

data "archive_file" "lets_encrypt_zip" {
  type        = "zip"
  output_path = "${path.module}/lets_encrypt.zip"
  source_dir = "${path.module}/python_deps"
    depends_on = [
      local_file.lets_encrypt_main 
    ]
}

resource "aws_lambda_function" "lets_encrypt" {
  runtime = "python3.6"
  filename      = "lets_encrypt.zip"
  function_name = "lets_encrypt"
  handler = "lets_encrypt.handler"
  timeout = 300
  role          = "${aws_iam_role.iam_for_lets_encrypt.arn}"

  source_code_hash = data.archive_file.lets_encrypt_zip.output_base64sha256

  environment {
    variables = {
      LETSENCRYPT_DOMAINS = "itsanovelidea.com,*.backend.itsanovelidea.com"

      # That ending . is not a typo, its how aws stores the dns NS record
      SUBDOMAIN = "backend.itsanovelidea.com."
      LETSENCRYPT_EMAIL = "itsanovelidea-test@gmail.com"
    }
  }
}

# Create a timer that runs every 12 hours
resource "aws_cloudwatch_event_rule" "lets_encrypt_timer_rule" {
  name                = "lets_encrypt_timer"
  schedule_expression = "cron(0 */12 * * ? *)"
}

# Specify the lambda function to run
resource "aws_cloudwatch_event_target" "lets_encrypt_timer_target" {
  rule = "${aws_cloudwatch_event_rule.lets_encrypt_timer_rule.name}"
  arn  = "${aws_lambda_function.lets_encrypt.arn}"
}

# Give cloudwatch permission to invoke the function
resource "aws_lambda_permission" "permission" {
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lets_encrypt.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.lets_encrypt_timer_rule.arn}"
}