const {expect} = require('chai')
const paginate = require("../components/helpers/paginate.js");

describe("paginating monospaced text",()=>{
    it("should do nothing with empty text",()=>{
        expect(paginate("",1,1)).to.deep.equal([""]);
    });
    it("should count newlines as separate lines",()=>{
        const actual = paginate("\n\nxddddddd\n\nmixed personalities\n\nmad at me\ns\nu\nw",20,3);
        expect(actual).to.deep.equal([
            "\n\nxddddddd",
            "\nmixed personalities\n",
            "mad at me\ns\nu",
            "w",
        ]);
    });
    it("should replace tabs with 4 spaces",()=>{
        const actual = paginate("\t",1,2);
        expect(actual).to.deep.equal([
            " \n ",
            " \n ",
        ]);
    });
    it("should split words that are longer than a line into multiple lines",()=>{
        const actual = paginate("aaaaaa",5,3);
        expect(actual).to.deep.equal([
            "aaaaa\na",
        ]);
    });
    it("should split words that are longer than a page into multiple pages",()=>{
        const actual = paginate("aaaaaaaaaa",2,4);
        expect(actual).to.deep.equal([
            "aa\naa\naa\naa",
            "aa"
        ]);
    });
    it("should move words that go over a line width onto the next line",()=>{
        const actual = paginate("aa aaaa",4,2);
        expect(actual).to.deep.equal([
            "aa \naaaa"
        ]);
    });
});