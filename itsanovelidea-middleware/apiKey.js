const expectedApiKey = process.env.API_KEY;

// I don't want the default behavior when an api key is missing
// to be just accepting all requests that have apikey=undefined
if (!expectedApiKey) {
  throw Error('No apikey set!');
}

module.exports = (req, res, next) => {
  const apiKey = req.header('apikey');

  // Taking advantage of this section of code in express-ws to
  // see if a request is a normal REST request or a websocket request.
  /*
     // Pulled from https://github.com/HenningM/express-ws/blob/master/src/index.js
     wsServer.on('connection', (socket, request) => {
      if ('upgradeReq' in socket) {
        request = socket.upgradeReq;
      }

      request.ws = socket;
      request.wsHandled = false;
      ...
    }

  */
  // If its a websocket request, then its coming straight from the client
  // browser and I don't need an apiKey because I will be using the validation token
  // cookie
  if (apiKey !== expectedApiKey && !req.ws) {
    return res.sendStatus(401);
  }
  return next();
};
