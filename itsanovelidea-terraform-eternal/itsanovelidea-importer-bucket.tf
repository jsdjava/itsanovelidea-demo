resource "aws_s3_bucket" "itsanovelidea-importer" {
  bucket = "itsanovelidea-importer"
}

resource "aws_s3_bucket_policy" "itsanovelidea-importer" {
  bucket = "${aws_s3_bucket.itsanovelidea-importer.id}"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "itsanovelidea-importers",

  "Statement": [

    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::itsanovelidea-importer/*",
      "Condition": {
         "IpAddress": {"aws:SourceIp": "65.29.169.244"}
      }
    }
  ]
}
POLICY
}