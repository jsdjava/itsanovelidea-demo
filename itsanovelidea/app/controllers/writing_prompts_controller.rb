class WritingPromptsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show, :count]

  # GET /writing_prompts.json
  def index
    @writing_prompts = WritingPrompt.get_filtered_writing_prompts(params)
    impressionist_aggregate(@writing_prompts)
  end

  # GET /writing_prompts/count.json
  def count
    num_writing_prompts = WritingPrompt.get_num_filtered_writing_prompts(params)
    render :json=> {num_writing_prompts:num_writing_prompts}, :status=> :ok
  end

  def new
  end

  # GET /writing_prompts/1.json
  # GET /writing_prompts/1.html
  def show
    @writing_prompt = WritingPrompt.find_by(id:params[:id])
    respond_to do |format|
      format.json {  render :json => {}, :status => :not_found if @writing_prompt.nil? }
      format.html {  render :show }
    end
  end

  # POST /writing_prompts.json
  def create
    @writing_prompt = WritingPrompt.new(writing_prompt_params)
    @writing_prompt.owner = current_user
    respond_to do |format|
      if @writing_prompt.save
        format.json { render :show, status: :created, location: @writing_prompt }
      else
        format.json { render json: @writing_prompt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /writing_prompts/rate.json
  def rate
    if WritingPrompt.find_by(id:params[:id]).nil?
      render :json => {}, :status => :not_found
      return
    end
    @writing_prompt_rating = UserWritingPrompt.find_by(user:current_user,writing_prompt_id:params[:id])
    save_or_update_result = nil
    if @writing_prompt_rating
      save_or_update_result = @writing_prompt_rating.update(rating:params[:rating])
    else
      @writing_prompt_rating = UserWritingPrompt.new(user:current_user,writing_prompt_id:params[:id],rating:params[:rating])
      save_or_update_result = @writing_prompt_rating.save
    end
    respond_to do |format|
      if save_or_update_result
        format.json { render json: @writing_prompt_rating, status: :created}
      else
        format.json { render json: @writing_prompt_rating.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def writing_prompt_params
      params.permit(:text,:page,:page_size,:rating)
    end
end
