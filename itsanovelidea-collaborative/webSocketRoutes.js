/* eslint-disable no-param-reassign */
const WebSocketJSONStream = require('websocket-json-stream');

const itsanovelideaMiddleware = require('itsanovelidea-middleware');

const logger = require('./logger.js');

module.exports = (shareDb) => {
  const profilesRoute = async (ws, req) => {
    try {
      const { cookie } = req.query;
      logger.log('info', 'connection attempt to profiles socket');
      // Pull the user out of redis using the validation token in the cookie sent
      // by the browser
      logger.log('info', cookie);
      const user = await itsanovelideaMiddleware.getUser(cookie);
      logger.log('info', 'pulled user out of redis');
      // Hook it into sharedb. The document id is the user id because its a profile,
      // so there's one per user. this is checked by the sharedb access middleware
      shareDb.listen(new WebSocketJSONStream(ws)).connectSession = user.user_id;
      logger.log('info', JSON.stringify(user, null, 2));
      logger.log('info', `setup sharedb connection to be ${user.user_id}`);

      // Need to clean up sockets when they become dead
      ws.isAlive = true;
      ws.on('pong', () => (ws.isAlive = true));
    } catch (err) {
      logger.log('error', 'profile socket error');
      logger.log('error', err.toString());
    }
  };

  const storiesRoute = async (ws, req) => {
    logger.log('info', 'connection attempt to stories socket');
    const { storyId, cookie } = req.query;
    logger.log('info', cookie);
    // Pull the user out of redis using the validation token in the cookie sent
    // by the browser
    const user = await itsanovelideaMiddleware.getUser(cookie);
    // Confirm with the rails server that the user has access to the story they
    // are trying to modify
    logger.log('info', user.user_id);
    logger.log('info', storyId);
    const canAccessStory = await itsanovelideaMiddleware.hasStoryAccess(user.user_id, storyId);
    logger.log('error', canAccessStory && canAccessStory.toString());
    if (canAccessStory !== true) {
      logger.log('error', 'user is not allowed to access story');
      ws.terminate();
      return;
    }
    // Connect to sharedb, using the storyId as the connection session. this is checked by
    // the sharedb access middleware
    shareDb.listen(new WebSocketJSONStream(ws)).connectSession = storyId;

    // Need to clean up dead sockets
    ws.isAlive = true;
    ws.on('pong', () => (ws.isAlive = true));
  };

  const cleanupSocket = (ws) => {
    // I'm doing an explicit skip on isAlive undefined because theres a race condition where
    // - websocket connects
    // - setInterval triggers
    // - websocket pong event is setup
    // This means the websocket must set isAlive to true before it can be terminated. I'm not sure
    // if this still leaves a potential hole where the websocket breaks in between connecting and
    // setting ws.isAlive = true, but for now I'm going to try this
    if (ws.isAlive === undefined) {
      return;
    }
    if (!ws.isAlive) {
      logger.log('info', 'timing out dead websocket');
      ws.terminate();
      return;
    }
    ws.isAlive = false;
    // Triggers the ws.on('pong') event registered in each route, which should flip the isAlive
    // flag back to true before the 5s interval is up
    ws.ping();
  };
  return {
    profilesRoute,
    storiesRoute,
    cleanupSocket,
  };
};
