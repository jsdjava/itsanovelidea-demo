require 'rails_helper'

RSpec.describe StoriesController, type: :controller do

  describe "GET #published" do
    it "returns http success" do
      get :published
      expect(response).to have_http_status(:success)
    end
  end

end
