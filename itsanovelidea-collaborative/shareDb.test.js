/* eslint-disable global-require */

test('it should register the rich text type with ShareDb', () => {
  jest.resetModules();
  const ShareDB = require('sharedb');
  require('./shareDb.js');
  expect(ShareDB.types.register.mock.calls[0][0]).toEqual(expect.objectContaining({
    name: 'rich-text',
  }));
});

test('it should wrap shareDb in the share db access middleware', () => {
  jest.resetModules();
  const shareDbAccess = require('sharedb-access');
  require('./shareDb.js');
  expect(shareDbAccess).toHaveBeenCalled();
});

test('fetch snapshot should call a promisified version of the native sharedb fetch snapshot', async () => {
  // I need to mock new ShareDB().connect(), which happens when
  // the module is required
  jest.resetModules();
  const ShareDB = require('sharedb');
  // Note how im using a callback here, because thats what the actual sharedb function uses
  const fetchSnapshotMock = jest.fn((collection, id, version, cb) => {
    // First parameter is error, so set that to undefined
    cb(undefined, 'fake snapshot');
  });
  ShareDB.mockImplementationOnce(() => ({
    connect: jest.fn(() => ({
      fetchSnapshot: fetchSnapshotMock,
    })),
  }));
  const shareDb = require('./shareDb.js');
  const snapshot = await shareDb.fetchSnapshot('stories', 1);
  expect(fetchSnapshotMock).toHaveBeenCalled();
  // I don't want to check the last arg to fetchSnapshot because
  // its a callback. I'm already checking that its working by awaiting
  // the pify-ed fetchSnapshot call
  const [collection, id, version] = fetchSnapshotMock.mock.calls[0];
  expect([collection, id, version]).toEqual(['stories', 1, null]);
  expect(snapshot).toEqual('fake snapshot');
});

test('fetch snapshot metadata should call a promisified version of shareDb getOps', async () => {
  const getOpsMock = jest.fn((agent, collection, id, v1, v2, options, cb) => {
    cb(undefined, ['fake metadata']);
  });
  jest.resetModules();
  const ShareDB = require('sharedb');
  ShareDB.mockImplementationOnce(() => ({
    getOps: getOpsMock,
    connect: jest.fn(() => ({
      agent: 'fake agent',
    })),
  }));
  const shareDb = require('./shareDb.js');
  const metadata = await shareDb.fetchSnapshotMetadata('stories', 1, 25);
  expect(getOpsMock).toHaveBeenCalled();
  // I don't really care about checking the agent or the options.
  // Those are statically defined by ShareDB.
  const [, collection, id, v1, v2] = getOpsMock.mock.calls[0];
  expect([collection, id, v1, v2]).toEqual(['stories', 1, 25, 26]);
  expect(metadata).toEqual('fake metadata');
});

test('create document should get the document from shareClient and then insert an empty line into it', async () => {
  const createMock = jest.fn((data, type, cb) => {
    // no errors, doc was created
    cb(false, true);
  });
  jest.resetModules();
  const ShareDB = require('sharedb');
  // I need to mock out:
  // - shareDb = new ShareDb(...);
  // - shareClient = shareDb.connect(...);
  // - doc = shareClient.get(...)
  // - doc.create(... stuff i care about ...)
  ShareDB.mockImplementationOnce(() => ({
    connect: jest.fn(() => ({
      get: jest.fn(() => ({
        create: createMock,
      })),
    })),
  }));
  const shareDb = require('./shareDb.js');
  // I don't care about the result, I just want to make sure I could create a backing document
  // for the matching story id in the rails server.
  await shareDb.createDocument('stories', 20);
  expect(createMock).toHaveBeenCalled();
  const [data, type] = createMock.mock.calls[0];
  expect([data, type]).toEqual([[{ insert: '\n' }], 'rich-text']);
});

test('listen should pass everything straight to sharedb listen', () => {
  const listenMock = jest.fn();
  jest.resetModules();
  const ShareDB = require('sharedb');
  ShareDB.mockImplementationOnce(() => ({
    connect: jest.fn(),
    listen: listenMock,
  }));
  const shareDb = require('./shareDb.js');
  shareDb.listen('fake stream', 'fake req');
  expect(listenMock).toHaveBeenCalledWith('fake stream', 'fake req');
});

test('it calls back sharedb-mongo with the mongo client on a successful connection to mongo', () => {
  const registerHookMock = jest.fn();
  jest.resetModules();
  const sharedbMongo = require('sharedb-mongo');
  const mongo = require('mongodb');
  mongo.MongoClient.connect.mockImplementationOnce((connectionStr, cb) => {
    cb(false, 'mongo client');
  });
  // Yeah, it feels kind of jenky, but the flow is:
  // - we: call sharedb-mongo without a connection string, instead passing it a callback
  // - sharedb-mongo: calls our callback with a single parameter, a register hook
  // - we: connect to mongodb in our callback
  // - mongodb: calls us back with a mongo client (or an error),
  // - we: callback sharedb-mongo with the mongo client
  sharedbMongo.mockImplementationOnce((cb) => {
    cb(registerHookMock);
  });
  require('./shareDb.js');
  expect(registerHookMock).toHaveBeenCalledWith(false, 'mongo client');
});

test("it doesn't callback sharedb-mongo when the mongo client fails to connect", () => {
  const registerHookMock = jest.fn();
  jest.resetModules();
  const sharedbMongo = require('sharedb-mongo');
  const mongo = require('mongodb');
  mongo.MongoClient.connect.mockImplementationOnce((connectionStr, cb) => {
    cb('could not connect to mongo buddy');
  });
  sharedbMongo.mockImplementationOnce((cb) => {
    cb(registerHookMock);
  });
  require('./shareDb.js');
  expect(registerHookMock.mock.calls.length).toEqual(0);
});
