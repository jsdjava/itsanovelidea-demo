resource "aws_service_discovery_public_dns_namespace" "itsanovelidea" {
  name        = "backend.itsanovelidea.com"
}

resource "aws_service_discovery_service" "itsanovelidea-redis" {
  name = "redis"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "itsanovelidea-mongo" {
  name = "mongo"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "itsanovelidea-photos" {
  name = "photos"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "itsanovelidea-collaborative" {
  name = "collaborative"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "itsanovelidea-mysql" {
  name = "mysql"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "itsanovelidea-main" {
  name = "main"

  dns_config {
    namespace_id = "${aws_service_discovery_public_dns_namespace.itsanovelidea.id}"
    dns_records {
      ttl  = 300
      type = "A"
    }
  }
}