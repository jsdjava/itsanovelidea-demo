const stringHash = require("string-hash");
const {intersection,difference} = require('lodash');

const {appUrl} = require('../../../support/app.json');
const {welcomePage} = require('../../../support/pages');
const {forceVisit,expandStory} = require('../../../support/actions/shared.js');
const {
    route,
    storyTileDivs,
    writingPromptDiv,
    writingPromptLink,
    writingPromptNextButton,
    writingPromptPrevButton,
    writingPromptCarousel,
    storyTileName,
    storyTileUserLink,
    storyTileRedditLink,
    storyTileImage,
    storyTileText,
    storyTileRating,
    storyTileViews,
    storyTileLink,
    expandedStoryDiv,
    expandedStoryName,
    expandedStoryUserLink,
    expandedStoryRedditLink,
    expandedStoryText,
    expandedStoryImage,
    expandedStoryViews,
    expandedStoryLink,
    expandedStoryCloseBtn,
    termsOfUseBtn,
    termsOfUse,
    termsOfUseCloseBtn,
} = welcomePage;

describe('welcome screen', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
    });
    it('displays a writing prompt',function(){
      cy.get(writingPromptDiv).first().should('be.visible');
      cy.get(writingPromptLink).first()
        .should($writingPromptLink=>{
          expect($writingPromptLink[0].href).to.match(/http:\/\/.*\/writing_prompts\/[\d]*.html/);
          expect($writingPromptLink[0].text.length).to.be.greaterThan(20);
        });
      cy.get(writingPromptNextButton).should('exist');
      cy.get(writingPromptPrevButton).should('exist');
    });
    it('displays story tiles', function() {
      cy.get(storyTileDivs).should(($storyTileDivs)=>{
        expect($storyTileDivs.length).to.be.greaterThan(5);
      });
      cy.get(storyTileName).first().should($storyTileName=>{
        expect($storyTileName[0].textContent.length).to.be.greaterThan(5);
      });
      cy.get(storyTileUserLink).first()
        .should($storyTileUserLink=>{
          expect($storyTileUserLink[0].href).to.match(/\/users\/[\d]*/);
          expect($storyTileUserLink[0].text.length).to.be.greaterThan(5);
      });
      cy.get(storyTileRedditLink).first()
        .should($storyTileRedditLink=>{
          expect($storyTileRedditLink[0].href).to.match(/reddit.com/);
        });
      cy.get(storyTileImage).should('exist');
      cy.get(storyTileText).first()
        .should($storyTileText=>{
          expect($storyTileText[0].textContent.length).to.be.greaterThan(20);
        });
      cy.get(storyTileRating).first().children().should('have.length',5);

      // Once NaN bug is fixed, could make sure its >=0
      cy.get(storyTileViews).should('exist');
      cy.get(storyTileLink).should('exist');
    });
    it('scrolls to a new prompt and stories gallery',function(){
      let curTranslate, curStoryHashes;
      cy.get(storyTileText).then($storyTileText=>{
        expect($storyTileText.length).to.be.greaterThan(5);
        curStoryHashes = [...$storyTileText].map(x=>stringHash(x.textContent));
      });
      cy.get(writingPromptCarousel).then($writingPromptCarousel=>{
        curTranslate = $writingPromptCarousel[0].style.transform.match(/translateX\(([\d-]*)px\)/)[1];
      });
      cy.get(writingPromptNextButton).click();
      cy.get(writingPromptCarousel).should($writingPromptCarousel=>{
        const nextTranslate = $writingPromptCarousel[0].style.transform.match(/translateX\(([\d-]*)px\)/)[1];
        expect(curTranslate-nextTranslate).to.be.greaterThan(100);
      });
      cy.get(storyTileText).then($storyTileText=>{
        const nextStoryHashes = [...$storyTileText].map(x=>stringHash(x.textContent));
        expect(intersection(curStoryHashes,nextStoryHashes)).to.be.empty
      });
    });
    it('scrolls back to the old prompt and story gallery',function(){
      let curTranslate, curStoryHashes;
      cy.get(writingPromptCarousel).then($writingPromptCarousel=>{
        curTranslate = $writingPromptCarousel[0].style.transform.match(/translateX\(([\d-]*)px\)/)[1];
      });
      cy.get(storyTileText).then($storyTileText=>{
        expect($storyTileText.length).to.be.greaterThan(5);
        curStoryHashes = [...$storyTileText].map(x=>stringHash(x.textContent));
      });
      cy.get(writingPromptNextButton).click();
      cy.get(storyTileText).should('exist');
      cy.get(writingPromptPrevButton).click();
      cy.get(storyTileText).then($storyTileText=>{
        const nextStoryHashes = [...$storyTileText].map(x=>stringHash(x.textContent));
        expect(difference(curStoryHashes,nextStoryHashes)).to.be.empty
      });
      cy.get(writingPromptCarousel).then($writingPromptCarousel=>{
        const nextTranslate = $writingPromptCarousel[0].style.transform.match(/translateX\(([\d-]*)px\)/)[1];
        expect(curTranslate).to.be.equal(nextTranslate);
      });
    });
    it('opens the terms of use',()=>{
      cy.get(termsOfUseBtn).click();
      cy.get(termsOfUse).should('be.visible');
    });
    it('closes the terms of use',()=>{
      cy.get(termsOfUseBtn).click();
      cy.get(termsOfUseCloseBtn).click();
      cy.get(termsOfUse).should('not.exist');
    });
    it('expands a story',()=>{
      const storyProps = [{
        oldEle:storyTileName,
        newEle:expandedStoryName,
        prop:'textContent',
        name:'name',
      },{
        oldEle:storyTileUserLink,
        newEle:expandedStoryUserLink,
        prop:'href',
        name:'userLink',
      },{
        oldEle:storyTileUserLink,
        newEle:expandedStoryUserLink,
        prop:'text',
        name:'user'
      },{
        oldEle:storyTileRedditLink,
        newEle:expandedStoryRedditLink,
        prop:'href',
        name:'redditLink',
      },{
        oldEle:storyTileText,
        newEle:expandedStoryText,
        prop:'textContent',
        name:'text',
      }]
      // Id like to do a little comparison between the expanded story and the normal story
      // to make sure all the props are there and match up. storeProps is a helper fn
      // to take an element string (like .writingPromptDiv) and a property from that element
      // (like href) and store it in normalStoryProps (calling it by name)
      const normalStoryProps = storyProps.reduce((acc,{oldEle,prop,name})=>{
        cy.get(oldEle).first().then(x=>acc[name]=x[0][prop]);
        return acc;
      },{});
      expandStory();
      cy.get(expandedStoryDiv).should('exist');
      const expandedStoryProps = storyProps.reduce((acc,{newEle,prop,name})=>{
        cy.get(newEle).first().then(x=>acc[name]=x[0][prop]);
        return acc;
      },{});
      expect(normalStoryProps).to.deep.equal(expandedStoryProps);
      cy.get(expandedStoryImage).should('exist');
      cy.get(expandedStoryViews).should('exist');
      cy.get(expandedStoryLink).should('exist');
    });
    it('closes a story',()=>{
      cy.get(storyTileDivs)
        .first()
        .should('be.visible')
        .click({force:true});
      cy.get(expandedStoryDiv).should('exist');
      cy.get(expandedStoryCloseBtn).click({
        force:true
      });
      cy.get(expandedStoryDiv).should('not.exist');
    });
  });