import React from 'react';
import ProfileBuilder from "./ProfileBuilder.jsx";

export default {
  component: ProfileBuilder,
  title: 'ProfileBuilder',
};

export const profileBuilder = () => <ProfileBuilder/>;
