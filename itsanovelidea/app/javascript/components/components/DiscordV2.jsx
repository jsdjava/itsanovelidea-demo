import React from 'react';
import Script from '@gumgum/react-script-tag';

class DiscordV2 extends React.Component {
    
    _onMyScriptLoad = () => {
        new Crate({
            server: '608805423100985366',
            channel: '615050489158959125',
            shard: 'https://e.widgetbot.io'
        })
    };

    render() {
        return (
            <div>
                <Script
                    src="https://cdn.jsdelivr.net/npm/@widgetbot/crate@3"
                    type="text/javascript"
                    onLoad={ this._onMyScriptLoad }
                    async
                    defer
                />
            </div>
        );
    }
}

export default DiscordV2;
