const https = require('https');
const fs = require('fs');
const expressWsFn = require('express-ws');

const { key, cert } = process.env.NODE_ENV === 'default' ? {
  cert: fs.readFileSync('/etc/ssl/www.itsanovelidea.com.crt'),
  key: fs.readFileSync('/etc/ssl/www.itsanovelidea.com.key'),
} : require('https-pem');
const { config } = require('itsanovelidea-middleware');

const shareDb = require('./shareDb.js');
const {
  profilesRoute,
  storiesRoute,
  cleanupSocket,
} = require('./webSocketRoutes.js')(shareDb);

const logger = require('./logger.js');


const app = require('./app.js');

const port = config.get('collaborativePort');

const server = https.createServer({
  key,
  cert,
}, app);

const expressWs = expressWsFn(app, server);

// Kind of stupid, but if you use the same REST route as a websocket route, (like /profiles),
// then express-ws will route you to the wrong one. To get around this, I'm just throwing
// /ws/ at the start of any websocket routes.
app.ws('/ws/profiles', profilesRoute);

app.ws('/ws/stories', storiesRoute);

app.use((err) => {
  // eslint-disable-next-line
  logger.log('error',"Probably some kind of websocket error:");
  // eslint-disable-next-line
  logger.log('error',err.toString());
});

server.listen(port, () => {
  // eslint-disable-next-line
  logger.log('error',`Node listening on ${port}`);
});

// Clean up dead sockets
setInterval(() => expressWs.getWss().clients.forEach(cleanupSocket), 5000);
