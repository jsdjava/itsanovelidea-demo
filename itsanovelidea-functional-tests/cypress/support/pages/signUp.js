module.exports = {
    route:"users/sign_up",
    emailInput: "[data-qa='emailInput'] input",
    passwordInput: "[data-qa='passwordInput'] input",
    confirmedPasswordInput: "[data-qa='confirmedPasswordInput'] input",
    signUpBtn: "[data-qa='signUpBtn']",
    successAlert: ".Toastify__toast--success",
    errorAlert: ".Toastify__toast--error",
};