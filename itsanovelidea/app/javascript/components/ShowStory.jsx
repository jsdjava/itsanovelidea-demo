import React from "react";
import PropTypes from "prop-types";

import FocusedStory from './components/FocusedStory';

class ShowStory extends React.Component {

  constructor(){    
    super();
  }

  render () {
    const {
        photoBackgroundColor,
        photoUrl,
        impressions_count,
        owner,
        name,
        text,
        rateUrl,
        url,
        writingPromptUrl,
        redditLink,
        writingPromptText,
    } = this.props;
    //TODO Define this !
    //refreshStory:()=>{}
    return <FocusedStory viewOnly={true} hideX = {true}close={()=>{
        window.location.href = writingPromptUrl.split(".")[0];
    }}
    publishedStory={{
        photoBackgroundColor,
        photoUrl,
        impressions_count,
        owner,
        name,
        text,
        rateUrl,
        url,
        redditLink,
        writingPromptText,
        writingPromptUrl,
    }}/>;
  }
}

ShowStory.propTypes = {
    photoBackgroundColor:PropTypes.string.isRequired,
    photoUrl:PropTypes.string.isRequired,
    impressions_count:PropTypes.string.isRequired,
    owner:PropTypes.object.isRequired,
    name:PropTypes.string.isRequired,
    text:PropTypes.string.isRequired,
    rateUrl:PropTypes.string.isRequired,
    url:PropTypes.string.isRequired,
    writingPromptUrl: PropTypes.object.isRequired,
};

export default ShowStory;
