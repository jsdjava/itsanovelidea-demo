import React from 'react';
import DashboardSearchBar from "./DashboardSearchBar";

export default {
  component: DashboardSearchBar,
  title: 'DashboardSearchBar',
};

export const dashboardSearchBar = ()=> <div style={{width:"900px"}}><DashboardSearchBar/></div>;

export const dashboardSearchBarNarrow = () => <div style={{width:"300px"}}><DashboardSearchBar/></div>;
