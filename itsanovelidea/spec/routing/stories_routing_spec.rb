require "rails_helper"

RSpec.describe StoriesController, type: :routing do
  describe "routing" do
    it "routes to #create" do
      expect(:post => "/stories").to route_to("stories#create")
    end
    it "routes to #publish" do
      expect(:post =>"/stories/1/publish").to route_to("stories#publish",:id=>"1")
    end
    it "routes to #published" do
      expect(:get=>"/stories/published/1").to route_to("stories#published",:writing_prompt_id=>"1")
    end
    it "routes to #published_count" do
      expect(:get=>"/stories/published/1/count").to route_to("stories#published_count",:writing_prompt_id=>"1")
    end
    it "routes to #index" do
      expect(:get=>"/stories").to route_to("stories#index")
    end
    it "routes to #rating" do
      expect(:put=>"/stories/1/rating").to route_to("stories#rating",:id=>"1")
    end
    it "routes to #rating" do
    expect(:get=>"/stories/1/rating").to route_to("stories#rating",:id=>1)
  end
  end
end
