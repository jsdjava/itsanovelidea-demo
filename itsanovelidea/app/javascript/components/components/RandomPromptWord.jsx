import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import RefreshIcon from '@material-ui/icons/Refresh';
import IconButton from '@material-ui/core/IconButton';

import {fetchWithToken} from "../helpers/api.js";

class RandomPromptWord extends React.Component{

    constructor(props){
        super(props);
        this.state = {};
        this.getRandomWord = this.getRandomWord.bind(this);
    }

    componentDidMount(){
        this.getRandomWord();
    }

    async getRandomWord(){
        const {randomWordUrl} = this.props;
        try{
            const resp = await fetchWithToken(randomWordUrl);
            if(!resp.ok){
                throw resp;
            }
            const {random_word} = (await resp.json());
            this.setState({
                randomWord: random_word,
            })
        } catch(e){
            console.log("Could not load random word");
            console.log(e);
        }
    }
    
    render(){

        // Purely here for the storybook, normal use is passing in a url to load random word,
        // not setting it as a prop
        const {randomWord} = {...this.state,...this.props};
        return <div className = "randomPromptWordDiv">
            {
                randomWord ? <div> 
                    <h3>Random Prompt Idea</h3> 
                    <IconButton aria-label="refresh">
                        <RefreshIcon onClick={this.getRandomWord}/>
                    </IconButton>
                    <p className="promptDiv"> Try creating a prompt that uses "{randomWord}" </p>
                </div> : <CircularProgress/>
            }
        </div>
    }
}

export default RandomPromptWord;