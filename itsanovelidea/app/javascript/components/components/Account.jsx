import React from "react";
import Button from '@material-ui/core/Button';
import {pick} from 'lodash';
import { ValidatorForm} from 'react-material-ui-form-validator';

import {fetchWithToken} from "../helpers/api.js"; 
import Background from "./Background.jsx";
import "../stylesheets/Account.css";
import {success,error} from "../helpers/notifications.js";  
  
class Account extends React.Component{
    constructor(){
        super();
        this.state= {};
        this.setFormValue = this.setFormValue.bind(this);
        this.submit = this.submit.bind(this);
    }

    setFormValue(key,event){
        this.setState({
            [key]:event.target.value
        })
    } 

    componentDidMount(){
        const {rules={}} = this.props;
        Object.entries(rules)
            .forEach(([ruleKey,rule])=>ValidatorForm.addValidationRule(ruleKey,(value)=>rule(value, this)));
    }

    async submit(){

        //TODO Should probably try-catch
        const {url,formKeys,commit,method="POST"} = this.props;
        const formUrl = Object.entries(pick({...this.props,...this.state},formKeys)).reduce((formUrl,[formKey,formVal])=>{
            return `${formUrl}user[${formKey}]=${formVal}&`;
        },"");
        const resp = await fetchWithToken(url,method,new URLSearchParams(
            `utf8=true&authenticity_token=${document.querySelector("meta[name='csrf-token']").content}&${formUrl}${commit}=${commit}`),{
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept":"text/html",
        });
        if(resp.redirected){
            window.location.href = resp.url;
        }
        const {errors,location,message} = await resp.json();
        if(errors && errors.length){
            error(errors[0]);
        }
        else if(resp.status===200){
            success(message);
            if(location){
                window.location.href = location;
            }
        }
    }

    render(){
        const {title,btnDataQaTag,btnTitle,render,renderAfter} = {render:()=>null,renderAfter:()=>null,...this.props};
        return (
            <div className= "accountDiv">
              <Background/>
                <div>
                    <div>
                    <ValidatorForm onSubmit = { async (event)=>{
                            event.preventDefault();
                            this.submit();
                        }}>
                        <h3 className="accountHeading">{title}</h3>
                        <div className="grey-text">
                        {
                            render(this.setFormValue,this.state)
                        }
                        </div>
                        <div className="text-center">
                        <Button variant="contained" 
                                type="submit"
                                data-qa={btnDataQaTag}>
                            {btnTitle}
                        </Button>
                        {
                            renderAfter()
                        }
                        </div>
                    </ValidatorForm>
                    </div>
                </div>
            </div>);
    };
}
export default Account;
