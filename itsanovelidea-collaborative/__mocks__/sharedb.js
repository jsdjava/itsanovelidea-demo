// this is the `shareDb` that.connect is called on to get a share client
const ShareDB = jest.fn(() => ({
  connect: jest.fn(),
  listen: jest.fn(),
}));

ShareDB.types = {
  register: jest.fn(),
};

module.exports = ShareDB;
