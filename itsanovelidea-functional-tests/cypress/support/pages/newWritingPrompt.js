module.exports = {
    createWritingPromptDiv:".createWritingPromptDiv",
    randomPromptWordBtn:".randomPromptWordDiv button",
    randomPromptWordDiv:".randomPromptWordDiv .promptDiv",
    createWritingPromptErrorText:".createWritingPromptDiv .Mui-error",
    createWritingPromptTextArea:".createWritingPromptDiv textarea[required]",
    createWritingPromptBtn:".createWritingPromptDiv [data-qa='createWritingPromptBtn']"
}