class AddWritingPromptsPublishedAtIndex < ActiveRecord::Migration[5.1]
  def change
    add_index :stories, [:id, :published_at]
  end
end
