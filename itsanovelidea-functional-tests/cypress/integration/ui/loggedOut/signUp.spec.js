const {appUrl} = require('../../../support/app.json');
const {login,forceVisit,signUp,getRandomEmail,getEmail} = require('../../../support/actions/shared.js');
const {signUpPage,dashboardPage} = require('../../../support/pages');
const {route,signUpBtn,successAlert,errorAlert} = signUpPage;
const {userHeading} = dashboardPage;

describe('signing up', function() {
    beforeEach(()=>{
        forceVisit(appUrl,route);
    });
    describe('successfully',function(){
        it('creates a valid account', function() {
            getRandomEmail().then(randomEmail=>{
                const password = "mittens";
                signUp(randomEmail,password);
                cy.get(successAlert).should('be.visible');
            
                getEmail(randomEmail).then(signUpEmail=>{
                    const confirmationLink = signUpEmail.match(/Confirm my account\n\[(.*)\]/)[1];
                    cy.visit(confirmationLink);
                });

                login(randomEmail,password);
                cy.get(userHeading)
                    .should('contain',randomEmail);
            });
        });
    });
    describe('unsuccessfully',function(){
        it("shows an error message for an already in-use account",function(){
            getRandomEmail().then(randomEmail=>{
                const password = "mittens";
                signUp(randomEmail,password);
                cy.get(successAlert).should('be.visible');           
                cy.get(signUpBtn).click();
                cy.get(errorAlert).should('be.visible');
            });
        });
    });
  })