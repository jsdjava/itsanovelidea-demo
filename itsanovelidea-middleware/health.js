const { timeout } = require('promise-timeout');
const expressHealth = require('express-nemo-route-health');

// I found myself repeating the pattern of
/*
 try{
     // do thing to make sure dependency is healthy
     return {
         healthy:true
     }
 } catch(err){
     return err;
 }
 */
// This provides a mechanism to DRY up that fairly common use case. Note that
// getHealth MUST throw an error if the component is not healthy, as any non-error
// response will simply get spread into the healthy response object.
// healthTimeout is the number of ms to give getHealth() to respond before
// timing it out and failing the health check.

const healthCheck = (getHealth = () => {}, healthTimeout = 2000) => async () => {
  try {
    // Timeout health calls that take too long - will prevent health() from hanging
    let resp = await timeout(getHealth(), healthTimeout);

    // Coerce to object so we don't spread something weird like a string into the response;
    // Will clear any response that isn't an object, which is intentional to support a default
    // use case where undefined is returned by getHealth()
    resp = typeof (resp) === 'object' ? resp : {};
    return {
      healthy: true,
      ...resp,
    };
  } catch (err) {
    // eslint-disable-next-line
    err = err || 'Undefined error thrown!';
    return err.toString();
  }
};

// Exposes a health middleware.
// Checks should be an object of the form
/*
  {
    "itsanovelidea": healthCheck(),
    "redis": healthCheck(redis.ping),
    "random": ()=>{
      if(Math.random()>0.5){
          return {
              healthy:true
          }
      }
      return {
        "error":"got unlucky"
      }
    }
  }
*/
// Note that you don't have to use healthCheck, but its recommended. If you don't use it,
// your function must return an object of the form
/*
  {
      healthy: true
  }
*/
// to indicate that it is healthy.
// Output of health(...) is:
/*
[
  {
    "name": "itsanovelidea-middleware",
    "status": {
      "healthy": true
    }
  },
  ...
]
*/

const health = (checks) => expressHealth({
  responseTemplate: (results, req, res) => {
    // Make sure every dependency is healthy
    const mergedStatus = results.every(({ status }) => status && status.healthy) ? 200 : 424;
    res.status(mergedStatus)
      .set('Content-Type', 'application/json')
    // Pretty print the json response
      .send(JSON.stringify(results, null, 2));
  },
  // Maps over
  /*
    {
        "itsanovelidea": ()=>"Healthy",
        "redis": redis.ping,
        ...
    }
    to
    [
        {
            name:"itsanovelidea",
            check: ()=>"Healthy",
        },
        {
            "name":redis,
            check:redis.ping,
        }
        ...
    ]
    which is the format that express-nemo-route-health expects
    */
  checks: Object.entries(checks).map(([k, v]) => ({
    name: k,
    check: v,
  })),
});

module.exports = {
  health,
  healthCheck,
};
