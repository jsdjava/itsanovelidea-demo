import React from "react";
import Carousel from 'react-material-ui-carousel'

import StoryCard from "./StoryCard.jsx";

class TopStories extends React.Component{
    render(){   
        function TopStory({story,title}){
          return <div>
            <p>{title}</p>
            <StoryCard photo={{
                url:story.photoUrl,
                backgroundColor:story.photoBackgroundColor,
                text:story.text,
                title:story.name,
                authorUrl:story.owner.url,
                author:story.owner.email,
                views:story.impressions_count,
                rating:story.rating,
            }}/>            
          </div>
        }; 
        const {
            highestRatedStory,
            mostViewedStory,
            mostRecentStory,
        } = this.props;
        const storyItems = [
            {
                title: 'Highest Rated Story',
                story: highestRatedStory,
            },
            {
                title: 'Most Viewed Story',
                story: mostViewedStory,
            },
            {
                title: 'Most Recent Story',
                story: mostRecentStory,
            },
        ].filter(({story})=>story);
        return <Carousel>
            { 
                storyItems.map(({title,story})=><TopStory title={title} story={story}/>)
            }
        </Carousel>;
    };
}

export default TopStories;
