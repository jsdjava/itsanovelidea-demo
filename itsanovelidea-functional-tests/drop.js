const MongoClient = require('mongodb').MongoClient;
const shell = require("shelljs");
const fs = require('fs-extra');
const mongoHost = process.env.MONGO_HOST || 'localhost:27017';
const db = "itsanovelidea";

const clearItsanovelidea = ()=>{
    shell.cd("../itsanovelidea/db");
    shell.exec("echo Dropping sql database");
    shell.rm("development.sqlite3");
    shell.rm("schema.rb");
    shell.cd("../");
    shell.exec("bundle exec rake db:migrate");
}

const clearItsanovelideaCollaborative = ()=>{
    console.log("Dropping mongo database");
    MongoClient.connect(`mongodb://${mongoHost}`,{useNewUrlParser:true},(err,mongoClient)=>{
        if(err){
          console.log("Couldn't connect to mongodb!");
          console.log(err);
          return;
        }
        const mongoClientDb = mongoClient.db(db);
        mongoClientDb.dropDatabase().then(()=>{
            console.log("Dropped mongo db");
            process.exit();
        }).catch(err=>{
            console.log(err);
        });
    });
}

const clearItsanovelideaPhotos = ()=>{
    console.log("Wiping photos");
    fs.emptyDirSync("../itsanovelidea-photos/photos");
}

clearItsanovelidea();
clearItsanovelideaPhotos();
clearItsanovelideaCollaborative();
