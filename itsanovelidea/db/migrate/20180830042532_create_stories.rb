class CreateStories < ActiveRecord::Migration[5.1]
  def change
    create_table :stories do |t|
      t.string :text
      t.decimal :minRating, :precision => 5, :scale=>3
      t.timestamps
    end
  end
end
