class AddRatingToUsers < ActiveRecord::Migration[5.1]
  def change      
    add_column :users, :rating, :decimal, :precision => 5, :scale=>3, :default=>5
  end
end
