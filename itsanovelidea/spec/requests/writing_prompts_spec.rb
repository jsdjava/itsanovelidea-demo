require 'rails_helper'

RSpec.describe "WritingPrompts", type: :request do
  describe "GET /writing_prompts" do
    it "returns nothing" do
      get "/writing_prompts.json"
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eql([])
    end
    it "filters/pages/sorts properly" do
      user = create(:user)
      create_list(:writing_prompt,2,text:"mittens was here but its the 1st page",owner:user)
      create_list(:writing_prompt,2,text:"not the right text",owner:user)
      create_list(:writing_prompt,3,text:"mittens was here but the user is wrong")
      expected_writing_prompts = create_list(:writing_prompt,2,text:"hi mittens asdfasdf",owner:user)
      get "/writing_prompts.json?text=mittens&user=#{user.id}&order=asc&page_size=2&page=2"
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map{|prompt|prompt["id"]}).to eql(expected_writing_prompts.map{|prompt|prompt.id})
    end
  end
  describe "GET /writing_prompt/:id" do
    it "returns the writing prompt" do
      expected_writing_prompt = create(:writing_prompt)
      get "/writing_prompts/#{expected_writing_prompt.id}.json"
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)["id"]).to eql(expected_writing_prompt.id)
    end
    it "404s" do
      get "/writing_prompts/5.json"
      expect(response).to have_http_status(404)
    end
  end
  describe "GET /writing_prompt" do
    it "renders the page for creating writing prompts" do
      user = create(:user)
      user.confirm
      sign_in user
      get "/writing_prompts/new"
      expect(response).to render_template :new
    end
  end
  describe "POST /writing_prompt" do
    describe "not logged in" do
      it "401s" do
        headers = { "CONTENT_TYPE" => "application/json" }
        post "/writing_prompts.json", :params => '{"text":"cha cha cha cha"}', :headers=>headers
        expect(response).to have_http_status(401)
      end
    end
    describe "logged in" do
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      it "creates the writing prompt" do
        headers = { "CONTENT_TYPE" => "application/json" }
        post "/writing_prompts.json", :params=>'{"text":"cha cha cha cha"}', :headers=>headers
        expect(response).to have_http_status(201)
        expect(JSON.parse(response.body)["text"]).to eql("cha cha cha cha")
      end
      it "400s" do
        headers = { "CONTENT_TYPE" => "application/json" }
        post "/writing_prompts.json", :params=>'{"text":"cha"}', :headers=>headers
        expect(response).to have_http_status(422)
      end
    end
  end
  describe "PUT /writing_prompt/:id/rate" do
    describe "not logged in" do
      it "401s" do
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/writing_prompts/1/rate.json", :params => '{"rating":3}', :headers=>headers
        expect(response).to have_http_status(401)
      end 
    end
    describe "logged in" do
      user = nil
      before(:each) do
        user = create(:user)
        user.confirm
        sign_in user
      end
      it "rates the writing prompt" do
        writing_prompt = create(:writing_prompt)
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/writing_prompts/#{writing_prompt.id}/rate.json", :params=>'{"rating":4}', :headers=>headers
        expect(response).to have_http_status(201)
        expect(JSON.parse(response.body)["rating"]).to eql(4)
      end
      it "updates instead of creating another writing prompt on a re-rate" do
        writing_prompt = create(:writing_prompt)
        writing_prompt_rating = create(:user_writing_prompt,user:user,writing_prompt:writing_prompt,rating:4)
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/writing_prompts/#{writing_prompt.id}/rate.json", :params=>'{"rating":2}', :headers=>headers
        expect(response).to have_http_status(201)
        expect(JSON.parse(response.body)["rating"]).to eql(2)
      end
      it "422s" do
        writing_prompt = create(:writing_prompt)
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/writing_prompts/#{writing_prompt.id}/rate.json", :params=>'{"rating":10}', :headers=>headers
        expect(response).to have_http_status(422)
      end
      it "404s" do
        headers = { "CONTENT_TYPE" => "application/json" }
        put "/writing_prompts/1/rate.json", :params=>'{"rating":10}', :headers=>headers
        expect(response).to have_http_status(404)
      end
    end
  end
  describe "GET /writing_prompt/count" do
    it "gets the total number of writing prompts matching the params" do
      writing_prompt = create(:writing_prompt,text:"let them in")
      writing_prompt = create(:writing_prompt,text:"don't let them win")
      writing_prompt = create(:writing_prompt,text:"asdfasdfasdfasdf")
      get "/writing_prompts/count.json?text=them&sort_by_published_stories=asc"
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)["num_writing_prompts"]).to eql(2)
    end
  end
end
