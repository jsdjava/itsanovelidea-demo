import React from "react";
import LoadingOverlay from 'react-loading-overlay'
import CircularProgress from '@material-ui/core/CircularProgress';

class MaterialLoadingOverlay extends React.Component{

  render(){
    const {loading,children} = this.props;
    return <LoadingOverlay
      active={loading}
      spinner={<CircularProgress size="10vw" thickness={2} style={{
        position: "fixed",
        left: "45%",
        top: "45%",
        "z-index": "9001"
     }}/>}
    >
      {children}
    </LoadingOverlay>
  }
}

export default MaterialLoadingOverlay;