import React from "react";
import PropTypes from "prop-types";
import Grid from '@material-ui/core/Grid';

import PromptsStats from "./components/PromptsStats.jsx";
import WritingPromptsList from "./components/WritingPromptsList.jsx";
import FloatingAddButton from "./components/FloatingAddButton.jsx";
import RandomPromptWord from "./components/RandomPromptWord.jsx";
import TopPrompts from "./components/TopPrompts.jsx";
import {getUser,getWritingPromptsCount} from './helpers/api';

import "./stylesheets/Dashboard.css";

class MyPrompts extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      page:1,
      pageSize:10,
      user:{},
    }
    this.getUser = this.getUser.bind(this);
    this.getWritingPromptsCount = this.getWritingPromptsCount.bind(this);
    this.getTopWritingPrompts = this.getTopWritingPrompts.bind(this);
  }

  // TODO It feels like im repeating this pattern a lot - is there a way to abstract this out so I dry things up, 
  // but I don't make a tangled chain of fn's calling fn's calling fn's with tons of params?
  async getWritingPromptsCount (){
    const {
      writingPromptsCountUrl,
      userId,
    } = this.props;
    const writingPromptsCount = await getWritingPromptsCount(writingPromptsCountUrl,userId);
    if(writingPromptsCount!==undefined){
      this.setState({
        writingPromptsCount,
      })
    }
  }

  async getUser(){
    const {userUrl} = this.props;
    const user = await getUser(userUrl);
    if(user){
      this.setState({
        user,
      });
    }
  }

  async getTopWritingPrompts(){
    const {writingPromptsUrl,userId} = this.props;
    try{
      const mostRecentResp = await fetchWithToken(`${writingPromptsUrl}?user=${userId}&sort_by_date=desc&page=1&page_size=1`);
      const mostViewedResp = await fetchWithToken(`${writingPromptsUrl}?user=${userId}&sort_by_views=desc&page=1&page_size=1`);
      const topRatedResp = await fetchWithToken(`${writingPromptsUrl}?user=${userId}&sort_by_rating=desc&page=1&page_size=1`);
      const mostStoriesResp = await fetchWithToken(`${writingPromptsUrl}?user=${userId}&sort_by_published_stories=desc&page=1&page_size=1`);
      
      //If any responses fail, just give up
      [mostRecentResp,mostViewedResp,topRatedResp,mostStoriesResp].forEach(resp=>{
        if(!resp.ok){
          throw resp;
        }
      })

      const [mostRecent] = (await mostRecentResp.json());
      const [mostViewed] = (await mostViewedResp.json());
      const [topRated] = (await topRatedResp.json());
      const [mostStories] = (await mostStoriesResp.json());

      this.setState({
        mostRecent,
        mostViewed,
        topRated,
        mostStories,
      })
    } catch(err){
      console.log("Could not load top writing prompts");
      console.log(err);
    }
  }

  componentDidMount(){
    this.getWritingPromptsCount();
    this.getUser();
    this.getTopWritingPrompts();
  }

  render () {
    const {
        writingPromptsUrl,
        writingPromptsCountUrl,
        userEmail,
        createWritingPromptUrl,
        randomWordUrl,
    } = this.props;
    const {
      user,
      writingPromptsCount,
      mostRecent={},
      mostViewed={},
      topRated={},
      mostStories={},
    } = this.state;
    const topPrompts = [
      {
        ...mostRecent,
        type:"Most Recent",
      },
      {
        ...mostViewed,
        type:"Most Viewed",
      },
      {
        ...topRated,
        type:"Top Rated",
      },
      {
        ...mostStories,
        type:"Most Stories",
      }
    ].filter(({text})=>text);
    return <Grid container>
        <FloatingAddButton onClick = {()=>{window.location.href = createWritingPromptUrl}}/>
        <Grid item sg={12} md={6}>
          <h3> My Prompts </h3>
          <div className="tabDiv">
            <WritingPromptsList url = {writingPromptsUrl} count_url = {writingPromptsCountUrl} authorSearch={userEmail}/>
          </div>
        </Grid>
        <Grid item sg={12} md={6}>
          <PromptsStats writingPromptsCount={writingPromptsCount}
            rating={user.prompts_rating}
            totalResponses={user.total_writing_prompts_responses}
            totalViews={user.total_writing_prompts_views}/>
          <TopPrompts highestRatedPrompt={user.most_popular_prompt}
            mostViewedPrompt={user.most_viewed_prompt}
            mostRecentPrompt={user.most_recent_prompt}
            mostRepliedPrompt={user.most_replied_prompt}/>
          <RandomPromptWord randomWordUrl={randomWordUrl}/>
        </Grid>
      </Grid>
  }
}

MyPrompts.propTypes = {
  url: PropTypes.string,
  userUrl: PropTypes.string,
  writingPromptsCountUrl: PropTypes.string,
  writingPromptsUrl: PropTypes.string,
  userId: PropTypes.number,
  createWritingPromptUrl: PropTypes.string,
  randomWordUrl: PropTypes.string,
};
export default MyPrompts;
