const {appUrl} = require('../../../support/app.json');
const {loginPage,newStoryPage,editStoryPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {
    route:newStoryRoute,
    storyTitleInput,
    photoSearchInput,
    photoSearchImages,
    photoSearchErrorMessage,
    createStoryBtn,
    storyTitleErrorMessage,
} = newStoryPage;

const {
    editStoryDiv,
    editStoryInput,
} = editStoryPage;

describe("create story page",()=>{
  beforeEach(()=>{
    forceVisit(appUrl,route);
    login(email,password);  
    cy.visit(`${appUrl}/${newStoryRoute}`);
    cy.get(storyTitleInput).type("cool story");
    cy.get(photoSearchInput).type("cat");
    cy.get(photoSearchImages).eq(2).click();
    cy.get(photoSearchErrorMessage).should('not.exist');
    cy.get(createStoryBtn).click();
    cy.get(editStoryDiv,{timeout:20000}).should('exist');
  });
  it("allows you to edit a story",()=>{
    cy.get(editStoryInput).type("where did it all go so wrong");
  });
  it("saves a story");
  it("allows you to publish a story");
  it("allows you to update a story");
  it("stores history");
  it("allows you to view old versions of a story");
  it("allows you to revert a story");
  it("limits the amount of text you can type in a story");
  it("formats text in a story (bolding,italics,etc)")
});