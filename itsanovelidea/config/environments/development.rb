Rails.application.configure do
  # Verifies that versions and hashed value of the package contents in the project's package.json
config.webpacker.check_yarn_integrity = false

  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.seconds.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Don't care if the mailer can't send.
  config.action_mailer.default_url_options = { host: "localhost:3001" }

  config.action_mailer.perform_caching = false

  config.action_mailer.perform_deliveries = true
  config.action_mailer.default :charset => "utf-8"
    ActionMailer::Base.smtp_settings = {
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :user_name            => "test.itsanovelidea@gmail.com",
    :password             => ENV["EMAIL_PASSWORD"],
    :authentication       => "plain",
    :enable_starttls_auto => true
   }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  #config.node_host = "localhost:6001"
  #config.node_api_key = "redacted"

  config.node_host = "collaborative.backend.itsanovelidea.com"
  config.photo_host = "photos.backend.itsanovelidea.com"

  #TODO Really need a different api key here
  #config.photo_host = "localhost:4444"
  config.discord_host = "localhost:7000"
  #config.chat_host = "localhost:10000"
  #config.chat_host_websocket = "localhost:10001"
  config.node_api_key = ENV["API_KEY"]
end
