class WritingPrompt < ApplicationRecord
  extend Filterable
  belongs_to :owner, class_name: "User"
  has_many :user_writing_prompts, dependent: :destroy
  has_many :users, through: :user_writing_prompts
  has_many :stories, dependent: :destroy
  validates :owner, :text, presence: true
  validates :text, length: { in:10..300 }
  is_impressionable :counter_cache => true, :unique=>true
  before_destroy :destroy_impressions
  attr_accessor :computed_num_published_stories, :computed_rating, :email
  
  def owner_email
    self.attributes["email"] || self.owner.email
  end

  def rating
    self.attributes["computed_rating"] || self.user_writing_prompts.average(:rating)
  end

  def num_published_stories
      self.attributes["computed_num_published_stories"] || self.stories.where.not({published_text:nil}).count
  end

  def self.ratings_view
    self.all
      .left_joins(:user_writing_prompts)
      .group(:id)
      .select('
        writing_prompts.id as id,
        COALESCE(round(avg(user_writing_prompts.rating),2),"Not Rated") as computed_rating'
      )
  end

  def self.writing_prompts_view
    inner_query = self.ratings_view
    self.all
      .joins("join users on users.id=writing_prompts.owner_id")
      .left_joins(:stories)
      .joins("inner join (#{inner_query.to_sql}) x on x.id = writing_prompts.id")
    .group(:id)
    .select('
      writing_prompts.id as id,
      writing_prompts.created_at as created_at, 
      writing_prompts.updated_at as updated_at, 
      writing_prompts.text as text, 
      writing_prompts.impressions_count as impressions_count,
      users.email as email,
      count(stories.published_at) as computed_num_published_stories,
      x.computed_rating as computed_rating'
    )
  end

  def self.get_writing_prompts_by_ids(ids)
    return [] if ids.empty?
    writing_prompts_view
      .where("writing_prompts.id IN (#{ids.join(",")})")
  end

  def self.get_filtered_writing_prompts(params,should_page=true)
    query = filter_by_min_published_stories_v2(params,writing_prompts_view)
    query = filter_by_text(params,query)
    query = filter_by_like_user(params,query)
    query = filter_by_user(params,query)
    return query unless should_page
    query = sort_by_date(params,query)
    query = sort_by_views(params,query)
    query = sort_by_published_stories_v2(params,query)
    query = sort_by_random(params,query)
    query = sort_by_rating_v2(params,query)
    page(params,query)
  end
  
  def self.filter_by_min_published_stories_v2(params, query=self.all)
      return query unless params[:min_published_stories]
      query.having("computed_num_published_stories >= ?",params[:min_published_stories].to_i)
  end
  def self.sort_by_rating_v2(params, query=self.all)
      return query unless params[:sort_by_rating]
      query.order("computed_rating #{params[:sort_by_rating]==='asc' ? 'asc' : 'desc'}")
  end

  def self.sort_by_published_stories_v2(params, query)
      return query unless params[:sort_by_published_stories]    
      query.order("computed_num_published_stories #{params[:sort_by_published_stories]==='desc' ? 'desc' : 'asc'}")
  end

  def self.get_num_filtered_writing_prompts(params)
    query = User
      .select("count(*) as count")
      .from("(#{self.get_filtered_writing_prompts(params,false).to_sql}) as x")
    query.to_a[0]["count"]
  end

private

  def self.sort_by_random(params,query = self.all)
      return query unless params[:sort_by_random]
      
      #TODO This causes issues between sqlite and mysql (RANDOM() vs RAND())
      #query.order(ENV["RAILS_ENV"]==="production" ? 'RAND()' : 'RANDOM()')
      query.order('RAND()')
  end

  def self.filter_by_text(params,query=self.all)
      return query unless params[:text]
      query.where("writing_prompts.text like ?","%#{params[:text].to_s}%")
  end

  def destroy_impressions
    Impression.where(impressionable_type:"WritingPrompt",impressionable_id:self.id).destroy_all
  end

end
