const {appUrl} = require('../../../support/app.json');
const {navbar,loginPage,writingPromptsPage,newWritingPromptPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit,assertSort} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {promptsLink} = navbar;
const {
    promptsTable,
    promptSearchInput,
    promptTexts,
    authorSearchInput,
    promptAuthors,
    sortByRatingBtn,
    sortByDateBtn,
    sortByStoriesBtn,
    sortByViewsBtn,
    promptRatings,
    promptDates,
    promptStories,
    promptViews,
    nextPageBtn,
    createPromptBtn,
} = writingPromptsPage;

const {createWritingPromptDiv} = newWritingPromptPage;

describe('writing prompts page', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
      login(email,password);
      cy.get(promptsLink).click();
    });

    it('displays a table of prompts', function() {
      cy.get(promptsTable).should('be.visible');
    });

    const assertSearch = ({searchText,searchField,searchResults})=>{
      cy.get(searchField).type(searchText);
      cy.document()
        .pipe(x=>x.querySelectorAll(searchResults),{timeout:15000})
        .should(x=>{
          const actual = [...x].map(x=>x.innerText.toLowerCase());
          expect(actual.length).to.be.above(3);
          actual.map(x=>expect(x).to.have.string(searchText))
        });
    }

    it('should filter prompts by text',function(){
      assertSearch({
        searchText:"wife",
        searchField:promptSearchInput,
        searchResults:promptTexts,
      });
    });

    it('should filter prompts by author',function(){
      assertSearch({
        searchText:"crazy",
        searchField:authorSearchInput,
        searchResults:promptAuthors,
      });
    })

    it('should sort by rating',function(){
      assertSort({
        sortBtn:sortByRatingBtn,
        sortCells:promptRatings,
        isInt:false,
      });
    });
    it('should sort by date',function(){
      assertSort({
        sortBtn:sortByDateBtn,
        sortCells:promptDates,
        isDate:true,
      });
    })

    it('should sort by stories',function(){
      assertSort({
        sortBtn:sortByStoriesBtn,
        sortCells:promptStories,
        isInt:true,
      });
    });

    it('should sort by views',function(){
      assertSort({
        sortBtn:sortByViewsBtn,
        sortCells:promptViews,
        isInt:true
      });
    });

    it("maintains compound searches and a sort",function(){
      assertSort({
        sortBtn:sortByStoriesBtn,
        sortCells:promptStories,
        isInt:true,
      });
      assertSearch({
        searchText:"wife",
        searchField:promptSearchInput,
        searchResults:promptTexts,
      });
      cy.get(authorSearchInput).type("a");
      cy.document()
        .pipe((x)=>{
        const stories = [...x.querySelectorAll(promptStories)];
        const texts = [...x.querySelectorAll(promptTexts)];
        const authors = [...x.querySelectorAll(promptAuthors)];
        return {
          stories,
          texts,
          authors,
        }
      },{timeout:15000})
      .should(({stories,texts,authors})=>{
        expect(texts.length).to.be.above(1);
        expect(authors.length).to.equal(texts.length);
        expect(stories.length).to.equal(texts.length);
        texts.map(x=>expect(x.innerText.toLowerCase()).to.include("wife"));
        authors.map(x=>expect(x.innerText.toLowerCase()).to.include("a"));
        
        const actual = stories.map(x=>parseInt(x.innerText));
        expect([...actual].sort((a,b)=>a-b).reverse()).to.eql(actual);
      });
    })

    it("pages maintaining order",function(){
      assertSort({
        sortBtn: sortByStoriesBtn,
        sortCells: promptStories,
        isInt:true,
      });
      let oldPage;
      cy.get(promptStories).then(x=>{
        oldPage = [...x].map(x=>parseInt(x.innerText));
      });
      cy.get(nextPageBtn).click();
      cy.document()
      .pipe((x)=>x.querySelectorAll(promptStories),{timeout:15000})
      .should(x=>{
        const actual = [...x].map(x=>parseInt(x.innerText));
        expect(actual[0]).to.be.below(oldPage[oldPage.length-1]);
        expect([...actual].sort((a,b)=>a-b).reverse()).to.eql(actual);
      });
    });

    it("creates a new writing prompt",()=>{
      cy.get(createPromptBtn).click();
      cy.get(createWritingPromptDiv).should('exist');
    });
  });
