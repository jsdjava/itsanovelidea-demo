import React from 'react';
import StoryTile from './StoryTile';

export default {
  component: StoryTile,
  title: 'StoryTile',
};

const getNormalText = (text)=>JSON.stringify({
  data:{
    "ops": [
      {
        "insert": text,
      },
    ]
  }
});

const text = getNormalText(`Hummingbirds are birds native to the Americas and constitute the biological family Trochilidae. They are the smallest of birds, 
most species measuring 7.5–13 cm (3–5 in) in length. The smallest extant bird species is the 5 cm (2.0 in) bee hummingbird, 
which weighs less than 2.0 g (0.07 oz).

They are known as hummingbirds because of the humming sound created by their beating wings which flap at high frequencies audible to humans.
They hover in mid-air at rapid wing-flapping rates, which vary from around 12 beats per second in the largest species, 
to in excess of 80 in some of the smallest. Of those species that have been measured in wind tunnels, their top speed exceeds 15 m/s 
(54 km/h; 34 mph) and some species can dive at speeds in excess of 22 m/s (79 km/h; 49 mph).[1][2]

Hummingbirds have the greatest mass-specific metabolic rate of any homeothermic animal.[3] 
To conserve energy when food is scarce, and nightly when not foraging, they can go into torpor, a state similar to hibernation,
 slowing metabolic rate to 1/15th of its normal rate.[4]`);

const url = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg/1024px-Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg";
const title = "Hummingbirds";
const author = "Jim Bird";
const rating = 4.5;
const views = 12000;

export const storyTile = () => <div style={{height:"300px", width:"480px"}}><StoryTile photo={{
    url,
    text,
    author,
    title,
    backgroundColor:"#AAAAAA",
    rating,
    views,
    width:"100%",
    height:"100%"
}}/></div>;

export const storyTileMaxLengthTitle = () => <div style={{height:"200px"}}><StoryTile photo={{
    url,
    text,
    title: [...new Array(30)].map(()=>'A'),
    author,
    rating,
    views,
}}/></div>;

export const storyTileMaxLengthAuthor = () => <div style={{height:"200px"}}><StoryTile photo={{
    url,
    text,
    author: [...new Array(320)].map(()=>'A'),
    title,
    rating,
    views,
}}/></div>;

export const storyTileMinLengthStory = () => <div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:getNormalText("A"),
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileWideImage = () => <div style={{height:"200px"}}><StoryTile photo={{
    url:"https://upload.wikimedia.org/wikipedia/commons/4/44/Cucurbita_2018_G1.jpg",
    text,
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileTallImage = () => <div style={{height:"200px"}}><StoryTile photo={{
    url:"https://upload.wikimedia.org/wikipedia/commons/9/92/Persicaria_maculosa.jpg",
    text,
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileNarrow = () => <div style={{height:"200px",width:"300px"}}><StoryTile photo={{
    url,
    text,
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileNoWordBreaks = () => <div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:getNormalText([...new Array(10000)].map(()=>'A').join("")),
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileAllH1s = ()=><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:JSON.stringify({
        "ops": [...new Array(1000)].map(()=>[
          {
            "insert": "GANDALF THE GREY"
          },
          {
            "attributes": {
              "header": 1
            },
            "insert": "\n"
          }
        ]).reduce((allOps,curOps)=>[...allOps,...curOps],[])
      }),
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileBoldItalicsUnderlines = ()=><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:JSON.stringify({
        "ops": [
          {
            "attributes": {
              "bold": true
            },
            "insert": "BOLD THIS"
          },
          {
            "insert": "\n"
          },
          {
            "attributes": {
              "underline": true
            },
            "insert": "underline this"
          },
          {
            "insert": "\n"
          },
          {
            "attributes": {
              "italic": true
            },
            "insert": "italicize this"
          },
          {
            "insert": "\n"
          }
        ]
      }),
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileHyperlinks = () =><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:JSON.stringify({
        "ops": [
          {
            "insert": "I am a link to "
          },
          {
            "attributes": {
              "link": "https://www.google.com"
            },
            "insert": "google.com"
          },
          {
            "insert": "\n\nI am a link to "
          },
          {
            "attributes": {
              "link": "http://notgoogle.com/"
            },
            "insert": "not google"
          },
          {
            "insert": "\n\nI am a link to "
          },
          {
            "attributes": {
              "link": "https://quilljs.com/docs/delta/"
            },
            "insert": "quill"
          },
          {
            "insert": "\n\nI am a link to "
          },
          {
            "attributes": {
              "link": "https://www.yahoo.com"
            },
            "insert": "yahoo"
          },
          {
            "insert": "\n"
          }
        ]
      }),
    author,
    title,
    rating,
    views,
}}/></div>; 

export const storyTileQuote = ()=><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:JSON.stringify({
        "ops": [
          {
            "insert": "Says lil peep in his eponymous beamer boy track\ni feel like im a no one"
          },
          {
            "attributes": {
              "blockquote": true
            },
            "insert": "\n"
          },
          {
            "insert": "am i a pitch fork reviewer now\n"
          }
        ]
      }),
    author,
    title,
    rating,
    views,
}}/></div>;

export const storyTileBulletsAndNumbers = ()=><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:getNormalText([...new Array(10000)].map(()=>'A').join("")),
    author,
    title,
    rating,
    views,
}}/></div>;


export const storyTileFullHtml = ()=><div style={{height:"200px"}}><StoryTile photo={{
    url,
    text:getNormalText([...new Array(10000)].map(()=>'A').join("")),
    author,
    title,
    rating,
    views,
}}/></div>;

