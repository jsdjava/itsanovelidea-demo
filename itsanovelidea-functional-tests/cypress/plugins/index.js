process.env.NODE_CONFIG_DIR = `${__dirname}/../config`;
const config = require('config');

module.exports = (on, cypressConfig) => {
  on("before:browser:launch", (browser = {}, args) => {
    // browser will look something like this
    // {
    //   name: 'chrome',
    //   displayName: 'Chrome',
    //   version: '63.0.3239.108',
    //   path: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    //   majorVersion: '63'
    // }

    // args are different based on the browser
    // sometimes an array, sometimes an object

    if (browser.name === "chrome") {
      args.push("--disable-site-isolation-trials");

      // whatever you return here becomes the new args
      return args;
    }
  });
  return {
      ...config.util.toObject(),
      apiKey:process.env.API_KEY,
  };
}
