import React from "react";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import {Flipped } from 'react-flip-toolkit';

import {getStoryWidth,getGalleryPadding} from "../helpers/utils.js";

import "../stylesheets/Gallery.css";


class Gallery extends React.Component{

  constructor(){
    super();
    this.state = {};
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  updateDimensions() {
    const numColumns = Math.ceil(window.innerWidth/600)
    this.setState({ numColumns });
  };

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }
  componentWillUnmount() { 
    window.removeEventListener('resize', this.updateDimensions);
  }

    render(){ 
        const {photos,renderImage,publishedStorySpot} = this.props;
        const {numColumns,hideElements} = this.state;
        const galleryPadding = getGalleryPadding(numColumns);
        const storyWidth = getStoryWidth(numColumns);
        return <GridList cellHeight={300} cols={numColumns} style={{
          overflowY:"visible",
          width:"100%",
          paddingLeft: `${galleryPadding}%`,
        }}>   
          {
            photos.map((photo,i)=>{
              let tileRows = 1;
              return publishedStorySpot==i ? null : <GridListTile className={`gridListTile-${i}`} key={i} rows = {tileRows} style={{
                width:`${storyWidth}%`,
                padding:"unset",
                height:`${304*tileRows}px`,
                backgroundColor:photo.backgroundColor,
              }} onMouseEnter={() =>{
                const tile = document.querySelector(`.gridListTile-${i}`);
                tile.style["transform-origin"] = "";
                tile.style["overflow-y"] = "visible";
                tile.style["overflow-x"]= "visible";
                tile.style["z-index"] = 9000;
                tile.style.transition = "transform 0.5s";
                tile.style.transform="scale(1.1)";
              }} 
                onMouseLeave={() => {
                  const tile = document.querySelector(`.gridListTile-${i}`)
                  tile.style["overflow-y"] = "hidden";
                  tile.style["overflow-x"]= "hidden";
                  tile.style["z-index"] = "initial";
                  tile.style.transform="";
                  tile.style.transition="";
                }}
                component={(props)=>{
                const onComplete = ()=>{
                  if(!window.clonedTile.parentNode){
                    return;
                  }
                  window.clearTimeout(window.onCompleteHook);
                  window.endTime = new Date();
                  window.clonedTile.parentNode.removeChild(window.clonedTile);
                  const baseSelector = `.gridListTile-${i}`;
                  const {style} = document.querySelector(baseSelector);
                  const tileChild = document.querySelector(`${baseSelector} .MuiGridListTile-tile`);
                  //document.querySelector(`${baseSelector} .storyContentHeaderDiv>div`).style.display="unset";
                  //document.querySelector(`${baseSelector} .storyViewerDiv>img`).style.display="unset";
                  //document.querySelector(`${baseSelector} .storyViewerDiv>.quill`).style.display="unset";
                  style.position="relative";
                  style.left = "0px";
                  style.top = "0px";
                  style.width = window.clonedTile.style.width;
                  style.height = window.clonedTile.style.height;
                  style.zIndex = "initial";
                  tileChild.style["overflow-x"] = "hidden";
                  tileChild.style["overflow-y"] = "hidden";
                  window.frozen = false;
                }
              return <Flipped flipId={`card-${i}`} onStart={()=>{
                window.frozen = true;
                window.startTime = new Date();
                const baseSelector = `.gridListTile-${i}`;
                const tile = document.querySelector(baseSelector);
                const tileChild = document.querySelector(`${baseSelector} .MuiGridListTile-tile`);
                //document.querySelector(`${baseSelector} .storyContentHeaderDiv>div`).style.display="none";
                //document.querySelector(`${baseSelector} .storyViewerDiv>img`).style.display="none";
                //document.querySelector(`${baseSelector} .storyViewerDiv>.quill`).style.display="none";
                const clonedTile = tile.cloneNode(); 
                window.clonedTile = clonedTile;
                clonedTile.removeAttribute("data-flip-id");
                clonedTile.removeAttribute("data-flip-config");
                clonedTile.removeAttribute("data-portal-key");
                clonedTile.style.transform="none"; 
                clonedTile.style["transform-origin"]="none";
                tile.parentNode.insertBefore(clonedTile,tile);
                const {x,y,width,height} = clonedTile.getBoundingClientRect();
                tile.style.position = "absolute";
                tile.style.top = `${y+window.scrollY}px`;

                // Don't even ask about the 28 lmao -  its 25 for the margin on the scroll component, 5 for the grid list LI,
                // and -2 for the grid list UL
                tile.style.left = `${x}px`;
                tile.style.width = `${width}px`;
                tile.style.height = `${height}px`;
                tile.style.zIndex = 2000;
                tileChild.style["overflow-x"]="visible";
                tileChild.style["overflow-y"]="visible";
                if(!window.disableCompleteHook){
                  window.onCompleteHook = setTimeout(onComplete,1000);
                }
              }} onComplete={onComplete} shouldFlip={(prevStorySpot,curStorySpot)=>{
                return i==(prevStorySpot || curStorySpot);
              }}>
                <li {...props} style={{
                  ...props.style,
                  willChange:"transform",
                }}>
                  <Flipped inverseFlipId={`card-${i}`} shouldInvert={(prevStorySpot,curStorySpot)=>{
                    return i==(prevStorySpot || curStorySpot);
                  }} style={{
                    willChange:"transform",
                  }}>
                    {props.children}
                  </Flipped>
                </li>
              </Flipped>}}>
              {
                renderImage({
                  hideElements,
                  imageProps:{},
                  divProps:{},
                  photo:{
                    ...photo,
                    width:"100%",
                    height:"100%",
                  },
                })
              }
            </GridListTile>})
          }
        </GridList>;
    };
}

export default Gallery;
