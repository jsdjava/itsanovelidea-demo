const supertest = require('supertest');

// I don't want to mock out all of the middlewares included in here
// as I want to test to make sure that they exist as routes
jest.unmock('itsanovelidea-middleware');

// Need to reset modules so that apikey middleware can pick up new
// key value
jest.resetModules();
process.env.API_KEY = 'TEST';

const { redis } = require('itsanovelidea-middleware');

const photo = require('./photo.js');
const bucket = require('./bucket.js');
const app = require('./app.js');

// Best I can tell, you have to do this to enable __mocks__ for requires of local files,
// but for node modules it just magically happens
jest.mock('./photo.js');
jest.mock('./bucket.js');

test('should 401 /health on a missing api key', async () => {
  const res = await supertest(app).get('/health');
  expect(res.statusCode).toEqual(401);
});

test('should return /health', async () => {
  const res = await supertest(app)
    .get('/health')
    .set('apikey', 'TEST');

  // I don't really care what this route returns as a status code as long as it is authed
  // Its likely that some of the health calls could fail, i'm not really sure
  expect(res.statusCode).not.toEqual(401);
  expect(res.body).toEqual(expect.arrayContaining([
    expect.objectContaining({
      name: 'itsanovelidea-photos',
    }),
    expect.objectContaining({
      name: 'redis',
    }),
    expect.objectContaining({
      name: 's3',
    }),
  ]));
// Increasing the test timeout because the S3 health check will take 10s to timeout
}, 15000);

test('should return /swagger', async () => {
  const res = await supertest(app)
    .get('/swagger')
  // Looks like the swagger mware redirects from /swagger to like /swagger/index or something
  // like that
    .redirects(1)
    .set('apikey', 'TEST');

  expect(res.statusCode).toEqual(200);
  // Just going to make sure that "swagger" is somewhere inside the giant html response
  expect(res.text).toMatch(/swagger/);
});

test('should return 400 from GET /photo on search missing from the query', async () => {
  const res = await supertest(app)
    .get('/photo')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(400);
});

test('should return 400 from GET /photo on numResults<3', async () => {
  const res = await supertest(app)
    .get('/photo?search=xd&numResults=2')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(400);
});

test('should return 400 from GET /photo on numResults>200', async () => {
  const res = await supertest(app)
    .get('/photo?search=xd&numResults=201')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(400);
});

test('should return 400 from GET /photo when the index of the photo is >500', async () => {
  const res = await supertest(app)
    .get('/photo?search=xd&page=51&numResults=10')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(400);
});

test('GET /photo should pass the page,numResults,search, and skipCache to photo.search()', async () => {
  await supertest(app)
    .get('/photo?page=2&numResults=20&search=cat&skipCache=true')
    .set('apikey', 'TEST');
  expect(photo.search).toBeCalledWith('cat', 2, 20, true);
});

test('GET /photo should return 500 when photo.search() throws an exception', async () => {
  photo.search.mockImplementationOnce(() => {
    throw Error("can't talk to pixabay");
  });
  const res = await supertest(app)
    .get('/photo?page=2&numResults=20&search=cat')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('GET /photo should return the result of photo()', async () => {
  photo.search.mockReturnValueOnce({
    a: 1,
  });
  const res = await supertest(app)
    .get('/photo?page=2&numResults=20&search=cat')
    .set('apikey', 'TEST');
  expect(res.body).toMatchObject({
    a: 1,
  });
});

test("POST /photo/:searchKey should 404 when the index + searchKey don't exist", async () => {
  // Taking advantage of the default photo.prepare mock returning undefined
  const res = await supertest(app)
    .post('/photo/searchKey?index=1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(404);
});

test('POST /photo/:searchKey should upload the image url in a text file to s3 and return the link to it', async () => {
  bucket.uploadTextFile.mockReturnValueOnce('https://amazon.s3.com/myPhoto.txt');
  photo.prepare.mockReturnValueOnce({
    largeImageURL: 'https://pixabay.com/cat.jpg',
    id: 'photoId',
  });
  const res = await supertest(app)
    .post('/photo/searchKey?index=1')
    .set('apikey', 'TEST');
  expect(bucket.uploadTextFile).toBeCalledWith('photoId.txt', 'https://pixabay.com/cat.jpg');
  expect(res.body).toMatchObject(expect.objectContaining({
    idUrl: 'https://amazon.s3.com/myPhoto.txt',
  }));
});


test('POST /photo/:searchKey should return 500 when photo.prepare() throws an exception', async () => {
  photo.prepare.mockImplementationOnce(() => {
    throw Error("can't get image");
  });
  const res = await supertest(app)
    .post('/photo/searchKey?index=1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('POST /photo/:searchKey should (asynchronously) upload the image to s3, and update the text file with the new link when its done', async (done) => {
  bucket.uploadTextFile.mockReturnValueOnce('https://amazon.s3.com/myPhoto.txt');
  bucket.uploadImageFromUrl.mockImplementation((actualUrl, actualText, cb) => {
    expect(actualUrl).toEqual('https://pixabay.com/cat.jpg');
    expect(actualText).toEqual('photoId.jpg');

    // This is the callback that updates the text file to switch the url from pixabay to
    // the s3 bucket
    cb('https://aws.s3.com/myPhoto.jpg');
    expect(bucket.uploadTextFile.mock.calls[1]).toEqual(['photoId.txt', 'https://aws.s3.com/myPhoto.jpg']);
    done();
  });
  photo.prepare.mockReturnValueOnce({
    largeImageURL: 'https://pixabay.com/cat.jpg',
    id: 'photoId',
  });
  await supertest(app)
    .post('/photo/searchKey?index=1')
    .set('apikey', 'TEST');
});

// I have to force kill redis because otherwise it hangs, causing jest to also hang
afterAll(async () => {
  await redis.end();
});
