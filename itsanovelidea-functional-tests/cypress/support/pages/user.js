module.exports = {
  userHeading:".myStoriesDiv h1",
  storiesTab:".MuiTab-root:nth-child(1)",
  profileTab:".MuiTab-root:nth-child(2)",
  promptsTab:".MuiTab-root:nth-child(3)",
  storyStats:{
    numStoriesCell:".storiesStatsDiv td:nth-child(1)",
    ratingCell:".storiesStatsDiv td:nth-child(2)",
    viewsCell:".storiesStatsDiv td:nth-child(3)",
  },
  storiesGallery:".storiesGalleryDiv",
  storiesCarousel:".CarouselItem",
  expandedStoryDiv: ".focusedItemBackground",
  storyDiv:".storyTile-0",
  profileBio: ".ql-editor p",
  promptStats:{
    numPromptsCell:".promptsDiv td:nth-child(1)",
    promptsRatingCell:".promptsDiv td:nth-child(2)",
    promptsViewsCell:".promptsDiv td:nth-child(3)",
    numResponsesCell:".promptsDiv td:nth-child(4)",
  },
  promptsCarousel:{

  },
  promptsTable:{

  },
};