class UserWritingPrompt < ApplicationRecord
    belongs_to :user
    belongs_to :writing_prompt
    validates :rating, :inclusion => 1..5
    validates :user, :writing_prompt, presence: true
end
