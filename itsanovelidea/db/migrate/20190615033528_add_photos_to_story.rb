class AddPhotosToStory < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :photo_id, :string
    add_column :stories, :photo_url, :string
  end
end
