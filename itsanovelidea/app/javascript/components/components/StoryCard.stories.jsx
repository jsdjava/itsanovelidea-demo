import React from 'react';
import StoryCard from "./StoryCard.jsx";

const getNormalText = (text)=>JSON.stringify({
    "ops": [
        {
          "insert": text,
        },
      ]
});

const text = getNormalText(`Hummingbirds are birds native to the Americas and constitute the biological family Trochilidae. They are the smallest of birds, 
most species measuring 7.5–13 cm (3–5 in) in length. The smallest extant bird species is the 5 cm (2.0 in) bee hummingbird, 
which weighs less than 2.0 g (0.07 oz).

They are known as hummingbirds because of the humming sound created by their beating wings which flap at high frequencies audible to humans.
They hover in mid-air at rapid wing-flapping rates, which vary from around 12 beats per second in the largest species, 
to in excess of 80 in some of the smallest. Of those species that have been measured in wind tunnels, their top speed exceeds 15 m/s 
(54 km/h; 34 mph) and some species can dive at speeds in excess of 22 m/s (79 km/h; 49 mph).[1][2]

Hummingbirds have the greatest mass-specific metabolic rate of any homeothermic animal.[3] 
To conserve energy when food is scarce, and nightly when not foraging, they can go into torpor, a state similar to hibernation,
 slowing metabolic rate to 1/15th of its normal rate.[4]`);

const url = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg/1024px-Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg";
const title = "Hummingbirds";
const author = "Sir James Hummingbird the 3rd";
const views = 400;
const rating = 3.25;
const isOwned = true;

export default {
  component: StoryCard,
  title: 'StoryCard',
};

export const storyCard = () => <StoryCard photo={{
   url,text,title,author,views,rating,isOwned,
}}/>;

export const storyCardMaxLengthTitle = ()=> <StoryCard photo={{
    url,
    text,
    title: [...new Array(30)].map(()=>'A'),
    author,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardMaxLengthAuthor = ()=> <StoryCard photo={{
    url,
    text,
    author: [...new Array(320)].map(()=>'A'),
    title,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardMinLengthStory = ()=> <StoryCard photo={{
    url,
    text:getNormalText("A"),
    author,
    title,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardWideImage = ()=> <StoryCard photo={{
    url:"https://upload.wikimedia.org/wikipedia/commons/4/44/Cucurbita_2018_G1.jpg",
    text,
    author,
    title,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardTallImage = ()=> <StoryCard photo={{
    url:"https://upload.wikimedia.org/wikipedia/commons/9/92/Persicaria_maculosa.jpg",
    text,
    author,
    title,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardNarrow = ()=> <div style={{width:"300px"}}><StoryCard photo={{
    url,
    text,
    author,
    title,
    views,
    rating,
    isOwned,
}}/></div>;

export const storyCardNoWordBreaks =  ()=> <StoryCard photo={{
    url,
    text:getNormalText([...new Array(10000)].map(()=>'A').join("")),
    author,
    title,
    views,
    rating,
    isOwned,
}}/>;

export const storyCardNotOwned =  ()=> <StoryCard photo={{
    url,
    text,
    author,
    title,
    views,
    rating,
    isOwned:false,
}}/>;