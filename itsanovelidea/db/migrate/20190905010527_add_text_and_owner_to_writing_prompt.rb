class AddTextAndOwnerToWritingPrompt < ActiveRecord::Migration[5.1]
  def change
    add_column :writing_prompts, :text, :string
  	add_reference :writing_prompts, :owner, index: true
  end
end
