describe('GET /health', () => {
    it('returns 401 without the api key', () => {
      cy.request({
        url:Cypress.config("itsanovelideaPhotosUrl")+"/health",
        failOnStatusCode:false,
      }).should(resp=>{
        expect(resp.status).to.equal(401);
      });
    });
    it('returns everything as healthy',()=>{
      cy.request({
        url:Cypress.config("itsanovelideaPhotosUrl")+"/health",
        headers:{
          apikey:Cypress.config("apiKey"),
        },
      }).should(resp=>{
        expect(resp.body).to.deep.include.members(
          [
            {
              name: "itsanovelidea-photos",
              status:{
                healthy:true,
              }
            },
            {
              name: "redis",
              status:{
                healthy:true,
              }
            },
            {
              name: "s3",
              status:{
                healthy:true,
              }
            }
          ]
        );
        
      });
    })
    })