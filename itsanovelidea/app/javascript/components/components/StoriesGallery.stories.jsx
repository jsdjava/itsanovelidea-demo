import React from 'react';
import StoriesGallery from "./StoriesGallery";

let publishedStories = require('./fixtures/publishedStories.json');

export default {
  component: StoriesGallery,
  title: 'StoriesGallery',
};

const getNormalText = (text)=>JSON.stringify({
    "ops": [
        {
          "insert": text,
        },
      ]
});

// Just replace the fixture eventually
publishedStories = publishedStories.map(publishedStory=>({
    ...publishedStory,
    text:getNormalText(publishedStory.text),
}));

export const storiesGallery = () => <StoriesGallery
    publishedStories={publishedStories}
/>;

export const storiesGalleryNoStories = ()=><div style={{
    backgroundColor:"gray",
    height:"100%",
    width:"100%",
}} ><StoriesGallery
    hasPublishedStories={false}
/></div>;

export const storiesGalleryLoading = ()=><StoriesGallery
    publishedStories={[]}
    hasMore={true}
/>;
