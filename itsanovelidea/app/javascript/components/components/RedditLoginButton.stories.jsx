import React from 'react';
import RedditLoginButton from "./RedditLoginButton.jsx";

export default {
  component: RedditLoginButton,
  title: 'RedditLoginButton',
};

export const redditLoginButton = () => <div style={{height:"600px"}}><RedditLoginButton/></div>;
