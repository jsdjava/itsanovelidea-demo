import React from "react";
import Carousel from 'react-material-ui-carousel';

import WritingPromptCard from "./WritingPromptCard.jsx";

class TopPrompts extends React.Component{
    render(){
        const renderPrompt = prompt=><WritingPromptCard 
          author={prompt.owner.email}
          text={prompt.text}
          rating={prompt.rating}
          date={prompt.created_at}
          stories={prompt.num_stories}
          views={prompt.impressions_count}/>; 
        const {
            highestRatedPrompt,
            mostViewedPrompt,
            mostRecentPrompt,
            mostRepliedPrompt,
        } = this.props;
        const promptItems = [
            {
                title: 'Highest Rated Prompt',
                prompt: highestRatedPrompt,
            },
            {
                title: 'Most Viewed Prompt',
                prompt: mostViewedPrompt,
            },
            {
                title: 'Most Recent Prompt',
                prompt: mostRecentPrompt,
            },
            {
              title: 'Most Replied Prompt',
              prompt: mostRepliedPrompt,
            },
        ].filter(({prompt})=>prompt);
        return <Carousel>
                { 
                    promptItems.map(({title,prompt})=><div>
                    <p>{title}</p>
                        {renderPrompt(prompt)}             
                    </div>)
                }
            </Carousel>;
    };
}

export default TopPrompts;
