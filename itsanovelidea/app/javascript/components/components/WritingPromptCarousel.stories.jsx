import React from 'react';
import WritingPromptCarousel from './WritingPromptCarousel';
const writingPrompts = require('./fixtures/writingPrompts.json');

export default {
  component: WritingPromptCarousel,
  title: 'WritingPromptCarousel',
};


export const writingPromptCarousel = () => <div style={{backgroundColor:"pink"}}><WritingPromptCarousel writingPrompts={writingPrompts}/></div>;

export const writingPromptCarouselMaxWritingPromptLength = ()=><div style={{backgroundColor:"pink"}}><WritingPromptCarousel writingPrompts={[
    {
        text: [...new Array(150)].fill('A '),
    }
]}/></div>;

export const writingPromptCarouselNoWritingPrompts = () => <div style={{backgroundColor:"pink"}}><WritingPromptCarousel writingPrompts={[]}/></div>;
