PUBLIC_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4 -s --max-time 30)
aws servicediscovery register-instance --service-id $SERVICE_ID --instance-id $INSTANCE_ID --attributes "{\"AWS_INSTANCE_IPV4\":\"${PUBLIC_IP}\"}"
