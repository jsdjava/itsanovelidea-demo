const signInPage = require("../pages/signIn.js");
const facebookPage = require("../pages/facebook.js");
const dashboardPage = require("../pages/dashboard.js");

describe('ssoing through facebook', () => {
    it('signs in through facebook', () => {
        const {email,password} = browser.config.users.facebook;
        browser.url(browser.config.baseUrl);
        signInPage.facebookLoginBtn.click();
        facebookPage.emailInput.setValue(email);
        facebookPage.passwordInput.setValue(password);
        facebookPage.loginBtn.click();
        expect(dashboardPage.emailHeader.getText()).toEqual(email);
    });
  })