class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
    
    def oauth(user)
      if user.persisted?
        sign_in_and_redirect user, event: :authentication
      else

        #TODO Clear up the error messages here
        redirect_to new_user_registration_url, alert: user.errors.full_messages.join("\n")
      end
    end
    
    def facebook
      @user = User.from_facebook(request.env["omniauth.auth"])
      oauth(@user)
    end

    def google_oauth2 
      @user = User.from_google(request.env['omniauth.auth'])   
      oauth(@user)   
    end

    def reddit
      @user = User.from_reddit(request.env['omniauth.auth'])   
      oauth(@user)
    end
  end