import React from 'react';
import WritingPromptCarouselInfinite from './WritingPromptCarouselInfinite';

const writingPrompts = require('./fixtures/writingPrompts.json');

export default {
  component: WritingPromptCarouselInfinite,
  title: 'WritingPromptCarouselInfinite',
};


export const writingPromptCarouselInfinite = () => { 
    return <div style={{backgroundColor:"pink"}}><WritingPromptCarouselInfinite loadWritingPrompts={(startIdx,pageSize)=>{
        const resp = [...new Array(pageSize)].map((x,spot)=>({text:(startIdx-1)*pageSize+spot}));
        return Promise.resolve(JSON.parse(JSON.stringify(resp)));
    }}/></div>;
};

export const writingPromptCarouselInfiniteNormalPrompts = ()=>{
    return <div style={{backgroundColor:"pink"}}><WritingPromptCarouselInfinite loadWritingPrompts={(startIdx,pageSize)=>{
        const resp = [...new Array(pageSize)].map((x,spot)=>(writingPrompts[spot % writingPrompts.length]));
        return Promise.resolve(JSON.parse(JSON.stringify(resp)));
    }}/></div>;
}

export const writingPromptCarouselInfiniteNoPrompts = ()=>{
    return <div style={{backgroundColor:"pink"}}><WritingPromptCarouselInfinite loadWritingPrompts={()=>{
        return Promise.resolve([]);
    }}/></div>;
}

export const writingPromptCarouselInfiniteSmallestPromptToLargest = ()=>{
    const writingPrompts = ["i",[...new Array(150)].fill('A ')];
    return <div style={{backgroundColor:"pink"}}><WritingPromptCarouselInfinite loadWritingPrompts={(startIdx,pageSize)=>{
        const resp = [...new Array(pageSize)].map((x,spot)=>({text:writingPrompts[spot % writingPrompts.length]}));
        return Promise.resolve(JSON.parse(JSON.stringify(resp)));
    }}/></div>;
}
