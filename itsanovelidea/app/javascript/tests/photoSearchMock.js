module.exports = (numResults)=>{
    return async (page,pageSize)=>{
        page--;
        const nextResults = numResults - page*pageSize;
        return Promise.resolve([...Array(Math.max(nextResults>pageSize ? pageSize: nextResults,0))].map((a,spot)=>page*pageSize+spot+1));
    }
}