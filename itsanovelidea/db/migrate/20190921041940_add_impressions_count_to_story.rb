class AddImpressionsCountToStory < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :impressions_count, :int
  end
end
