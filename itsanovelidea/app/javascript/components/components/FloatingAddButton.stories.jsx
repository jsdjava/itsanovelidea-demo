import React from 'react';
import FloatingAddButton from "./FloatingAddButton";

export default {
  component: FloatingAddButton,
  title: 'FloatingAddButton',
};

export const background = () => <div style={{width:"500px",height:"500px"}}>
    <FloatingAddButton/>
</div>;
