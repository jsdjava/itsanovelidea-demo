class CreateJoinTableUserStoriesUser < ActiveRecord::Migration[5.1]
  def change
    create_join_table :user_stories, :users, table_name: :rating do |t|
        t.index [:user_story_id, :user_id]
        t.index [:user_id, :user_story_id]
        t.integer :rating
    end
  end
end
