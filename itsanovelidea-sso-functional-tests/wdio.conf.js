exports.config = {
    runner: 'local',
    specs: [
        './test/specs/*.js'
    ],
    users:{
      facebook:{
        email:"itsanovelidea_sfgxihv_user@tfbnw.net",
        password: process.env.FACEBOOK_PASSWORD,
      },
      google:{
        email:"testing.itsanovelidea@gmail.com",
        password:process.env.GOOGLE_PASSWORD,
      },
      reddit:{
        email:"itsanovelidea-test@reddit.com",
        password:process.env.REDDIT_PASSWORD,
      }
    },
    maxInstances: 1,
    baseUrl: 'https://main.backend.itsanovelidea.com/users/sign_in',
    waitforTimeout: 10000,
    services: ['chromedriver','devtools'],
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    beforeTest:()=>{
      browser.cdp('Network','enable');
      browser.cdp('Network','setRequestInterception',{patterns:[{
        urlPattern:"*",
      }]});
      browser.on('Network.requestIntercepted',({interceptionId,request})=>{
        request.headers['apikey'] = process.env.API_KEY;
        browser.cdp('Network','continueInterceptedRequest', {
          interceptionId:interceptionId,
          headers: request.headers,
        });
      });
    }
}
