class RedditPage {
    get emailInput() { return $('#loginUsername') }
    get passwordInput() {return $("#loginPassword")}
    get loginBtn(){return $(".AnimatedForm__submitButton")}
    get allowBtn(){return $("input[type='submit']")}
  }
  
module.exports = new RedditPage();
  