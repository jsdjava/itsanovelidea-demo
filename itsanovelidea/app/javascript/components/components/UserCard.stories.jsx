import React from 'react';
import UserCard from "./UserCard.jsx";

export default {
  component: UserCard,
  title: 'UserCard',
};

const userCardDefaults = {
    email:"jsdjava@gmail.com",
    stories_rating:4.5,
    total_stories_views:20000,
    prompts_rating:3.7,
    total_writing_prompts_views:900,
    total_stories:50,
    total_writing_prompts:10,
    total_writing_prompts_responses:100,
};

export const userCard = () => <UserCard {...userCardDefaults}/>;
