import React from "react";

class StoriesListV2 extends React.Component{
    render(){
        const {photos,renderImage} = this.props;
        return <div className="storiesListDiv">    
            {photos.map(photo=>renderImage({imageProps:{},divProps:{
                width:"auto",
                height:"100%",
                marginLeft:"auto",
                marginRight:"auto",
            },photo:{
                ...photo,
                width:"auto",
                height:"100%",
            }}))}
            </div>;
    };
}

export default StoriesListV2;