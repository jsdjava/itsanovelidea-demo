import React from "react";
import StoriesListV2 from "./StoriesListV2.jsx";
import "../stylesheets/DoubledStoriesList.css";

class DoubledStoriesList extends React.Component{
 
    constructor(){
        super();
        this.state={
            singleColumn:true,
        };
        this.onResize = this.onResize.bind(this);
    }

    onResize(){
        const {singleColumn} = this.state;
        const newSingleColumn = window.innerWidth < 900;
        if(singleColumn!=newSingleColumn){
            this.setState({
                singleColumn:newSingleColumn,
            })
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.onResize);
        this.onResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }

    render(){
        const {singleColumn} = this.state;
        const leftPhotos = this.props.photos.filter((photo,spot)=>spot%2);
        const rightPhotos = this.props.photos.filter((photo,spot)=>!(spot%2));
        return singleColumn ? <StoriesListV2 {...this.props}/> : <div className="twoColumnStoriesListDiv">
            <StoriesListV2 {...{...this.props,photos:leftPhotos}}/>
            <StoriesListV2 {...{...this.props,photos:rightPhotos}} />
        </div>        
    };
}

export default DoubledStoriesList;