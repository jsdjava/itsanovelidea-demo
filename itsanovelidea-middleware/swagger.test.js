const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swagger = require('./swagger.js');

test('it properly configures the project title for swagger jsdoc', () => {
  swagger('projectTitle', null, []);
  expect(swaggerJSDoc).toBeCalledWith(expect.objectContaining({
    definition: expect.objectContaining({
      info: expect.objectContaining({
        title: 'projectTitle',
      }),
    }),
  }));
});

test('it modifies the list of apis to include the cwd', () => {
  swagger('projectTitle', '/tmp/project', ['./api.js', './health.js']);
  expect(swaggerJSDoc).toBeCalledWith(expect.objectContaining({
    apis: [
      '/tmp/project/./api.js',
      '/tmp/project/./health.js',
    ],
  }));
});

test('it returns the list of swagger ui middlewares', () => {
  // In reality, this will be a function, but its just easiest to mock the return value
  // to be a string
  swaggerUI.setup.mockReturnValueOnce('setupResp');
  const [swaggerServe, swaggerSetupResp] = swagger('projectTitle', '', []);
  expect(swaggerServe).toEqual(swaggerUI.serve);
  expect(swaggerSetupResp).toEqual('setupResp');
});
