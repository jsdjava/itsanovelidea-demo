# Itsanovelidea
## Demo

[![Thumbnail](pics/itsanovelidea.gif)](https://drive.google.com/file/d/1OOfgIVtc92UF7GBGudU0tzZYZY2oguTn/view?usp=sharing "Itsanovelidea - Click to open video")
[Click image (or here) to open video](https://drive.google.com/file/d/1OOfgIVtc92UF7GBGudU0tzZYZY2oguTn/view?usp=sharing)

## Description
Itsanovelidea is a webapp platform that allows users to create writing prompts and short stories and
share them with each other. It also was the project where I started to learn AWS a little better and began experimenting
with more of a distributed architecture as opposed to just creating one big server for everything.
Note that I removed the git history because I had some secrets committed, and I ommitted the package forks
as well as the importer script.

The different components in the project are:
Components:
* itsanovelidea - ruby server and react frontend for the main website (along with mysql schema)
* itsanovelidea-collaborative - nodejs microservice that uses websockets to store stories in sharedb (mongo) so they
have a full version history that can be reverted/viewed
* itsanovelidea-photos - nodejs microservice that searches and caches photos from the pixabay API in S3
Infrastructure:
* itsanovelidea-terraform - the ECS services for the project
* itsanovelidea-terraform-eternal - data stores + cloudmap (route53,s3 buckets,ecr,efs)
Tests:
* itsanovelidea-functional-tests - cypress UI and API tests for the components
* itsanovelidea-sso-functional-tests - wdio test specifically for just testing the SSO login flows
Packages
* itsanovelidea-middleware - middleware for the nodejs services to centralize things like apikey checks, logging, swagger and redis access
* adaptive-background - a fork of adaptive-background to pick the background colors for stories
* impressionist - a fork of impressionist to add some aggregation features
* react-infinite-scroll-component - a fork of react-infinite-scroll-component to fix a bug
Tools/Wrappers
* lets_encrypt - I did not write this, but wrapped some terraform around it. this is a python lambda that uses lets_encrypt
to keep the website certificate always up to date
* register-ec2-service - a tiny docker container that creates a ip address -> subdomain record using cloudmap
when an ECS service starts up
* itsanovelidea-nginx - small wrapper around nginx so that it can sit in front of the ruby server
* itsanovelidea-importer - local scripts that scrape stories from reddit/pixabay and load it into the mongo/mysql dbs

Major features completed are:
* SSO/standalone login
* Stories arranged by writing prompt view
* User view
* Expanded story view
* Rating stories and story views
* Writing stories and searching and selecting photos from pixabay
* Story history/reverting/publishing
* User profile view
* Discord integration
* Search by author and name
* Writing prompt prompt-er
* List of all authors, sorted by rating/views/etc
* Importing stories from reddit
* mysql storing users+writing prompts+links to stories
* mongo storing full story history
* automatic certificate updating
* ECS + cloudmap running each service (redis,mongo,mysql,itsanovelidea,itsanovelidea-collaborative,itsanovelidea-photos)
* Services registered at subdomains on startup using cloudmap
* Unit tests, integration tests, storybook tests, E2E tests, and perf tests
* Swagger,deep health endpoints, and apikeys on services

## Running Locally
YMMV, mostly just check each project readme then read the code

## Attribution
* images come from pixabay
* stories were pulled from the WritingPrompt subreddit of reddit.com
* lets encrypt was copied from https://arkadiyt.com/2018/01/26/deploying-effs-certbot-in-aws-lambda/
