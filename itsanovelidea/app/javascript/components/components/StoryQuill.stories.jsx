import React from 'react';
import StoryQuill from "./StoryQuill.jsx";

export default {
  component: StoryQuill,
  title: 'StoryQuill',
};

export const storyQuill = () => <div style={{
    height:"500px"
}}><StoryQuill storyTitle="i crash u crash"/></div>;

export const storyQuillRevertMode = () => <div style={{
    height:"500px"
}}><StoryQuill 
    storyTitle="i crash u crash"
    oldDocument="i let the time pass too fast i crash u crash i don't even know myself"
/></div>;
