resource "aws_ecr_repository" "itsanovelidea-functional-tests" {
  name                 = "itsanovelidea-functional-tests"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}
resource "aws_ecr_repository" "itsanovelidea-photos" {
  name                 = "itsanovelidea-photos"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}

resource "aws_ecr_repository" "itsanovelidea-sso-functional-tests" {
  name                 = "itsanovelidea-sso-functional-tests"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}

resource "aws_ecr_repository" "itsanovelidea-collaborative" {
  name                 = "itsanovelidea-collaborative"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}

resource "aws_ecr_repository" "itsanovelidea-main" {
  name                 = "itsanovelidea-main"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}

resource "aws_ecr_repository" "register-ec2-service" {
  name                 = "register-ec2-service"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}

resource "aws_ecr_repository" "itsanovelidea-nginx" {
  name                 = "itsanovelidea-nginx"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags =  {
      project = "itsanovelidea"
  }
}