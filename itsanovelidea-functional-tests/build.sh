#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Build Docker Image"
docker build -t itsanovelidea-functional-tests .
echo "Push to ECR Repository"
docker tag itsanovelidea-functional-tests:latest $AWS_ECR_REPOSITORY/itsanovelidea-functional-tests:latest
docker push $AWS_ECR_REPOSITORY/itsanovelidea-functional-tests:latest