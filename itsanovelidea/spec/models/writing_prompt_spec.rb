require 'rails_helper'
require_relative '../concerns/filterable_spec.rb'

RSpec.describe WritingPrompt, :type => :model do
  it_behaves_like "filterable", :writing_prompt

  owner = FactoryBot.create(:user)
  it "is valid with valid attributes" do
    expect(WritingPrompt.new(owner:owner,text:"abcdefghij")).to be_valid
  end
  it "is not valid without an owner" do
    expect(WritingPrompt.new(text:"abcdefghij")).to_not be_valid
  end
  it "is not valid without text" do
    expect(WritingPrompt.new(owner:owner)).to_not be_valid
  end
  it "is not valid with too short text" do
    expect(WritingPrompt.new(owner:owner,text:"asdf")).to_not be_valid
  end
  it "is not valid with too long text" do
    expect(WritingPrompt.new(owner:owner,text:"c"*301)).to_not be_valid
  end
  it "is not valid without an owner" do
    expect(WritingPrompt.new(text:"cuz im mr brightside")).to_not be_valid
  end
  it "returns the number of published stories written about the writing prompt" do
      writing_prompt = create(:writing_prompt)
      other_stories = create_list(:story,3)
      unpublished_stories = create_list(:story,4,writing_prompt:writing_prompt)
      prompt_stories = create_list(:story,2,writing_prompt:writing_prompt,published_text:"asdfasdfadfasdf")
      expect(writing_prompt.num_published_stories).to eql(2)
  end
  it "returns Not rated when there are no ratings" do
    writing_prompt = create(:writing_prompt)
    expect(writing_prompt.rating).to eql("Not rated")
  end
  it "returns the average rating rounded to 2 decimal places" do
    writing_prompt = create(:writing_prompt)
    user_writing_prompts = build_list(:user_writing_prompt,3, writing_prompt: writing_prompt)
    user_writing_prompts[0].rating = 1
    user_writing_prompts[1].rating = 2
    user_writing_prompts[2].rating = 4
    user_writing_prompts.each {|user_writing_prompt| user_writing_prompt.save!}
    expect(writing_prompt.rating).to eql(2.33)
  end
  describe "filter by text" do
    it "does nothing" do
      expected_writing_prompts = create_list(:writing_prompt,5)
      actual_writing_prompts = WritingPrompt.filter_by_text({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "filters by text that includes the provided text" do
      expected_writing_prompt_1 = create(:writing_prompt,text:"cha cha cha cha")
      create(:writing_prompt,text: "asdfasdfasdfasdf")
      expected_writing_prompt_2 = create(:writing_prompt, text: "test cha test test")
      actual_writing_prompts = WritingPrompt.filter_by_text({text:"cha"}).to_a
      expect(actual_writing_prompts).to eql([expected_writing_prompt_1,expected_writing_prompt_2])
    end
  end
  describe "filter by minimum published stories count" do
    it "does nothing" do
      expected_writing_prompts = create_list(:writing_prompt,5)
      actual_writing_prompts = WritingPrompt.filter_by_min_published_stories({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "excludes stories that aren't published" do
      writing_prompt_with_one_published_story = create(:writing_prompt)
      create_list(:story,4,writing_prompt:writing_prompt_with_one_published_story)
      create(:story,writing_prompt:writing_prompt_with_one_published_story,published_text:"overflowqwer")
      writing_prompt_with_published_stories = create(:writing_prompt)
      create_list(:story,2,writing_prompt:writing_prompt_with_published_stories,published_text:"ayyyyyyy")
      actual_writing_prompts = WritingPrompt.filter_by_min_published_stories({min_published_stories:2}).to_a
      expect(actual_writing_prompts).to eql([writing_prompt_with_published_stories])
    end
    it "filters down to writing prompts having at least the minimum number of published stories" do
      create_list(:writing_prompt,5)
      writing_prompt_with_2_stories = create(:writing_prompt)
      writing_prompt_with_3_stories = create(:writing_prompt)
      writing_prompt_with_1_story = create(:writing_prompt)
      create_list(:story,2,writing_prompt:writing_prompt_with_2_stories,published_text:"from mexico")
      create_list(:story,3,writing_prompt:writing_prompt_with_3_stories,published_text:"qowijerioj")
      create(:story,writing_prompt:writing_prompt_with_1_story,published_text:"xddddddddddd")
      actual_writing_prompts = WritingPrompt.filter_by_min_published_stories({min_published_stories:2}).to_a
      expect(actual_writing_prompts).to eql([writing_prompt_with_2_stories,writing_prompt_with_3_stories])
    end
  end
  describe "sort randomly" do
    it "does nothing" do
      expected_writing_prompts = create_list(:writing_prompt,5)
      actual_writing_prompts = WritingPrompt.sort_by_random({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts randomly" do

      #Odds are very low that they will be the same, but this test is not perfect
      sorted_writing_prompts = create_list(:writing_prompt,20)
      actual_writing_prompts = WritingPrompt.sort_by_random({sort_by_random:true}).to_a
      expect(sorted_writing_prompts).not_to eql(actual_writing_prompts)
    end
  end
  describe "sort by published stories" do
    it "does nothing" do
      expected_writing_prompts = create_list(:writing_prompt,5)
      actual_writing_prompts = WritingPrompt.sort_by_date({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts by the descending number of published stories " do
      writing_prompt_10_published_stories = create(:writing_prompt);
      writing_prompt_15_published_stories = create(:writing_prompt);
      writing_prompt_3_published_stories = create(:writing_prompt);
      writing_prompt_no_published_stories = create(:writing_prompt);
      create_list(:story,10,writing_prompt:writing_prompt_10_published_stories,published_text:"around to you")
      create_list(:story,15,writing_prompt:writing_prompt_15_published_stories,published_text:"around to you")
      create_list(:story,3,writing_prompt:writing_prompt_3_published_stories,published_text:"around to you")
      create_list(:story,6,writing_prompt:writing_prompt_3_published_stories)
      create_list(:story,7,writing_prompt:writing_prompt_no_published_stories)
      actual_writing_prompts = WritingPrompt.sort_by_published_stories({
        sort_by_published_stories:"desc"
      }).to_a
      expect(actual_writing_prompts).to eql([
        writing_prompt_15_published_stories,
        writing_prompt_10_published_stories,
        writing_prompt_3_published_stories,
        writing_prompt_no_published_stories
      ])
    end
  end
  describe "sort by rating" do  
    it "does nothing" do
      expected_writing_prompts = create_list(:writing_prompt,5)
      actual_writing_prompts = WritingPrompt.sort_by_rating({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts by descending rating" do
      average_writing_prompt,unrated_writing_prompt,good_writing_prompt,bad_writing_prompt=create_list(:writing_prompt,4)
      create(:user_writing_prompt,writing_prompt:average_writing_prompt,rating:3)
      create(:user_writing_prompt,writing_prompt:good_writing_prompt,rating:5)
      create(:user_writing_prompt,writing_prompt:bad_writing_prompt,rating:4)
      create(:user_writing_prompt,writing_prompt:bad_writing_prompt,rating:1)
      actual_writing_prompts = WritingPrompt.sort_by_rating({
        sort_by_rating:'desc'
      }).to_a
      expect(actual_writing_prompts).to eql([good_writing_prompt,average_writing_prompt,bad_writing_prompt,unrated_writing_prompt])
    end
  end
  describe "filtering + sorting edge cases (left joins and nulls)" do
    it "sorts by published stories and filters by user" do
      user = create(:user)
      writing_prompt_wrong_user = create(:writing_prompt)
      writing_prompt_1_published_story = create(:writing_prompt,owner:user)
      writing_prompt_no_published_stories = create(:writing_prompt,owner:user)
      writing_prompt_3_published_stories = create(:writing_prompt,owner:user)
      create_list(:story,5,writing_prompt:writing_prompt_1_published_story)
      create(:story,writing_prompt:writing_prompt_1_published_story,published_text:"neatoasdfasdf")
      create_list(:story,3,writing_prompt:writing_prompt_3_published_stories,published_text:"neatasdfasdf")
      actual_writing_prompts = WritingPrompt.get_filtered_writing_prompts({
        sort_by_published_stories:'desc',
        user:user,
      }).to_a
      expect(actual_writing_prompts).to eql([writing_prompt_3_published_stories,writing_prompt_1_published_story,writing_prompt_no_published_stories])
    end
    it "sorts by rating and filters by user" do
      user = create(:user)
      writing_prompt_wrong_user = create(:writing_prompt)
      writing_prompt_good_rating,writing_prompt_bad_rating,writing_prompt_average_rating = create_list(:writing_prompt,3,owner:user)
      create(:user_writing_prompt,writing_prompt:writing_prompt_good_rating,rating:4)
      create_list(:user_writing_prompt,3,writing_prompt:writing_prompt_average_rating,rating:3)
      create_list(:user_writing_prompt,2,writing_prompt:writing_prompt_bad_rating,rating:1)
      actual_writing_prompts = WritingPrompt.get_filtered_writing_prompts({
        sort_by_rating:'asc',
        user:user
      }).to_a
      expect(actual_writing_prompts).to eql([writing_prompt_bad_rating,writing_prompt_average_rating,writing_prompt_good_rating])
    end
  end
end
