import React from "react";
import { Flipper} from 'react-flip-toolkit';
import InfiniteScroll from 'react-infinite-scroll-component';
//import Gallery from 'react-photo-gallery';
import CircularProgress from '@material-ui/core/CircularProgress';

import 'react-toastify/dist/ReactToastify.css';
import "../stylesheets/StoriesGallery.css";

import Gallery from './Gallery.jsx';
import StoryTile from "./StoryTile.jsx";
import FocusedStory from "./FocusedStory.jsx";
import {fetchWithToken} from "../helpers/api.js";
import {success,error} from "../helpers/notifications.js";

class StoriesGallery extends React.Component {
  constructor(){
    super();
    this.state={}
  }

  render () {
    let {

      //TODO Eventually, just make this truthy falsy
      hasPublishedStories,
      publishedStories,
      loadedStories,
      StoryComponent=StoryTile,
      FocusedStoryComponent=FocusedStory,
      GalleryComponent=Gallery,
      height, 
      style,
      next,
      hasMore,
      containerClassName,
      onFocusedStoryChange=()=>{},
      refresh=()=>{},
      refreshStory=()=>{},
      fullScreen,
      viewOnly,
    } = this.props;

    console.log(`stories gallery props are ${JSON.stringify(this.props)}`);
    //TODO Eventually, just make this truthy falsy
    if(hasPublishedStories===false){
      return <div className="noStoriesGalleryDiv">
        <p className="noStoriesMessage">No stories have been written for this prompt yet. Sign in to write one yourself</p>
      </div>;
    }

    const {publishedStorySpot,hoveredStorySpot} = this.state;
    const publishedStory = publishedStories[publishedStorySpot];
    const mapStoryProps = (publishedStory,publishedStorySpot)=>{
      return {
        ...publishedStory,
        views: publishedStory.impressions_count,
        author: publishedStory.owner.email,
        authorUrl: publishedStory.owner.url,
        isOwned:publishedStory.isOwned,
        isSelected:this.state.publishedStorySpot === ""+publishedStorySpot,
        isHovered:false,
        url:publishedStory.photoUrl,
        src:publishedStory.photoUrl,
        backgroundColor:publishedStory.photoBackgroundColor,
        width:parseInt(publishedStory.photoWidth)/parseInt(publishedStory.photoHeight),
        title:publishedStory.name,
        text:publishedStory.text,
        publishedText:publishedStory.published_text,
        height:1,
        parentFlipId:`card-${publishedStorySpot}`,
        publishedStorySpot,
        openStory:()=>{
          this.setState({
            publishedStorySpot:""+publishedStorySpot,
            hoveredStorySpot:undefined,
          });
          onFocusedStoryChange(true);
        },
        leaveStory:()=>{
            this.setState({
              hoveredStorySpot:undefined,
            });
          onFocusedStoryChange(false);
        },
        deleteStory:()=>{
          fetchWithToken(publishedStory.url.replace(".json",""),'DELETE')
            .then((resp)=>{
              if(resp.status!==200){
                throw resp;
              }
              success("Story deleted!");
              refresh()
            })
            .catch(err=>{
              error("Could not delete story!");
              console.log("Could not delete story");
              console.log(err);
            });
        },
        publishStory:()=>{
          fetchWithToken(`${publishedStory.url}/publish`.replace(".json",""),'POST')
            .then((resp)=>{
              console.log(resp);
              if(resp.status!==200){
                throw resp;
              }
              success("Story published!");
            })
            .catch(err=>{
              error("Could not publish story!");
              console.log("Could not publish story");
              console.log(err);
            });
        },
        hoverStory:()=>{
            this.setState({
              hoveredStorySpot:publishedStorySpot,
            });
      }}
    }
    return <Flipper flipKey={publishedStorySpot} className="storiesGalleryDiv" spring={{ 
      stiffness: window.stiffness || 200, 
      damping: window.damping || 26
    }} decisionData={publishedStorySpot} debug={window.debug}> 
        {!loadedStories ? <CircularProgress size="10vw" thickness={2} style={{
           position: "fixed",
           left: "45%",
           top: "45%",
           "z-index": "9001"
        }}/> : <InfiniteScroll
            loader={<div className="storiesGalleryLoadingDiv"><CircularProgress/></div>}
            next = {next}    
            style={{
              ...(style||{}),
              overflowX:"unset", 
              overflowY:"unset",
            }}
            height={height}
            dataLength={publishedStories.length}
            hasMore={hasMore}
            className={containerClassName}>
            <GalleryComponent 
            publishedStorySpot={publishedStorySpot}
            photos={publishedStories.map(mapStoryProps)}
            renderImage={(props)=> StoryComponent ? <StoryComponent {...{...props,key:`${props.key}-${props.index}`}}/> : null}/>
        </InfiniteScroll>
        }{FocusedStoryComponent && publishedStorySpot!==undefined ? <FocusedStoryComponent 
          viewOnly = {viewOnly}
          fullScreen={fullScreen} key={(publishedStory || {}).rating} publishedStory={{
          ...publishedStory,
          writingPromptText:publishedStory.writing_prompt.text,
          writingPromptUrl:`${publishedStory.writing_prompt.url.split(".json")[0]}.html`,
        }} publishedStorySpot={publishedStorySpot} close={()=>{
          this.setState({
            publishedStorySpot:undefined,
          });
        }} refreshStory={()=>{
          refreshStory(publishedStorySpot);
        }}/> : null}
    </Flipper>
  }
}

StoriesGallery.propTypes = {
};

export default StoriesGallery;