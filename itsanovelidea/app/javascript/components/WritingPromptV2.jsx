import React from "react";
   
import {fetchWithToken} from "./helpers/api.js";
import StoriesGallery from "./components/StoriesGallery.jsx";

import "./stylesheets/Welcome.css";

class WritingPromptV2 extends React.Component {
  constructor(props){
    super(props);
    this.state={
      stories:[],
      pageSize:12,
      page:0,
    };
    this.getStories = this.getStories.bind(this);
    this.getStoriesCount = this.getStoriesCount.bind(this);
  }

  async getStoriesCount(){
    const {publishedStoriesUrl,writingPrompt} = this.props;

    //TODO Are we fine with just throwing up the error?
    const resp = await fetchWithToken(`${publishedStoriesUrl}/count`)
    if(!resp.ok){
      throw "Could not get stories count"
    }
    const {stories_count} = (await resp.json());
    return stories_count;
  }

  async getStories(){
    const {publishedStoriesUrl,writingPrompt} = this.props;
    const {stories,pageSize,page} = this.state;
    let {storiesCount} = this.state;

    try{
      if(storiesCount===undefined){
        storiesCount = await this.getStoriesCount(writingPrompt);
      }
      const resp = await fetchWithToken(`${publishedStoriesUrl}?page=${page+1}&page_size=${pageSize}`)
      if(resp.ok){
        const newStories = await resp.json();
        this.setState({
          loadedStories:true,
          page:page+1,
          storiesCount,
          stories:[...stories,...newStories]
        });
      }
    }catch(err){
      console.log("Could not get stories or stories count");
      console.log(err);
    }
  }

  componentDidMount(){
    this.getStories();
  }

  render () {
    const {writingPrompt,logged_in} = this.props;
    const {
      stories,
      storiesCount,
      loadedStories,
    } = this.state;
    console.log("FIND MEEEEEEE writingpromptsv2");
    console.log(storiesCount);
    return <div>
      <p style={{
        fontSize: "16px",
        textAlign: "center",
        marginTop: "1rem",
        marginBottom: "0.5rem",
      }}>{writingPrompt.text}</p>
      <StoriesGallery
        fullScreen={logged_in}
        viewOnly={!logged_in}
        loadedStories={loadedStories}
        key={writingPrompt.id}
        hasMore={storiesCount>stories.length}
        publishedStories={stories} 
        next = {()=>{this.getStories()}}
        containerClassName="welcomePageDiv"
        onFocusedStoryChange={(focused)=>this.setState({focusedStory:focused})}
      />
    </div>
  }
}

export default WritingPromptV2;