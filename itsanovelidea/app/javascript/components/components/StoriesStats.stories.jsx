import React from 'react';
import StoriesStats from "./StoriesStats.jsx";

export default {
  component: StoriesStats,
  title: 'StoriesStats',
};

export const storiesStats = () => <StoriesStats
    storiesCount={1}
    rating={2}
    totalViews={3}
/>;
