import React from "react";
import PropTypes from "prop-types";

import FloatingAddButton from "./components/FloatingAddButton.jsx";
import WritingPromptsList from "./components/WritingPromptsList.jsx";
import "./stylesheets/WritingPrompts.css";

class WritingPrompts extends React.Component{
    render(){
        const {create_writing_prompt_url} = this.props;
        return <div className="writingPromptsDiv">
            <div>
                <FloatingAddButton onClick = {()=>window.location.href = create_writing_prompt_url}/>
            </div>
            <WritingPromptsList {...this.props}/>
        </div>
    };
}

WritingPrompts.propTypes = {
  create_writing_prompt_url: PropTypes.string.isRequired,
};

export default WritingPrompts;