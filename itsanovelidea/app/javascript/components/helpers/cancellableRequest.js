
module.exports = url=>{
    const abortController = new AbortController();

    const {signal} = abortController;
    return {
        promise:fetch(url,{signal}),
        cancel: ()=>abortController.abort(),
    }
}