process.env.API_KEY = 'test';
const apiKey = require('./apiKey.js');

test('it 401s on a missing apikey', (done) => {
  // Need to make sure next() doesn't ALSO get called
  const next = () => done.fail();

  // Mock out express Response object
  const res = {
    sendStatus: (status) => {
      expect(status).toEqual(401);
      done();
    },
  };

  // Mock out express Request object
  const req = {
    header: () => {},
  };
  apiKey(req, res, next);
});

test('it 401s on a wrong apikey', (done) => {
  // Need to make sure next() doesn't ALSO get called
  const next = () => done.fail();

  // Mock out express Response object
  const res = {
    sendStatus: (status) => {
      expect(status).toEqual(401);
      done();
    },
  };

  // Mock out express Request object
  const req = {
    header: () => 'not test',
  };
  apiKey(req, res, next);
});

test('it continues (calls next) on a matching api key', (done) => {
  // Mock out express Request object
  const req = {
    header: () => 'test',
  };
  apiKey(req, {}, done);
});

test('it allows websocket requests through without checking the apikey', (done) => {
  const req = {
    header: () => 'NOT TEST',
    ws: 'a websocket',
  };
  apiKey(req, {}, done);
});

test('it errors on no defined expected api key', () => {
  jest.resetModules();
  delete process.env.API_KEY;

  // eslint-disable-next-line
  expect(() => require('./apiKey.js')).toThrow();
});
