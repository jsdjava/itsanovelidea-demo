require 'rails_helper'

RSpec.describe User, :type => :model do
  it "returns the total number of created stories" do
    user = FactoryBot.create(:user)
    create_list(:story,5,owner:user)

    #Other user
    create_list(:story,2)
    expect(user.total_stories).to eql(5)
  end

  it "returns the total number of created writing prompts" do
    user = FactoryBot.create(:user)
    create_list(:writing_prompt,4,owner:user)

    #Other prompts
    create_list(:writing_prompt,3)
    expect(user.total_writing_prompts).to eql(4)
  end

  it "returns Not rated when there are no rated stories" do
    user = FactoryBot.create(:user)
    expect(user.stories_rating).to eql("Not rated")
  end

  it "returns Not rated when there are no writing prompts" do
    user = FactoryBot.create(:user)
    expect(user.writing_prompts_rating).to eql("Not rated")
  end

  #TODO A little bit of a copy paste here bud
  it "returns the average rating of all writing prompts rounded to 2 decimal places" do
    user = FactoryBot.create(:user)
    
    #Create another rated writing prompt
    other_writing_prompt = FactoryBot.create(:writing_prompt)
    create(:user_writing_prompt,writing_prompt:other_writing_prompt,user:user)

    my_writing_prompts = create_list(:writing_prompt,2,owner:user)

    create(:user_writing_prompt,writing_prompt:my_writing_prompts[0],rating:2)
    create_list(:user_writing_prompt,2,writing_prompt:my_writing_prompts[1],rating:5)

    expect(user.writing_prompts_rating).to eql(4)
  end

  it "returns the average rating of all stories rounded to 2 decimal places" do
    user = FactoryBot.create(:user)
    
    #Create another rated story
    other_story = create(:story)
    create(:user_story,story:other_story,user:user)

    my_stories = create_list(:story,3,owner:user)

    create(:user_story,story:my_stories[0],rating:1)
    create_list(:user_story,2,story:my_stories[1],rating:2)
    create_list(:user_story,3,story:my_stories[2],rating:5)

    expect(user.stories_rating).to eql(3.33)
  end

  it "returns 0 views when there are no writing prompts" do
    user = FactoryBot.create(:user)
    expect(user.total_writing_prompts_views).to eql(0)
  end
  it "returns the sum of all views on all the writing prompts for the user" do
    user = FactoryBot.create(:user)
    other_user = FactoryBot.create(:user)

    my_writing_prompt_1 = create(:writing_prompt,owner:user,impressions_count:3)
    my_writing_prompt_2 = create(:writing_prompt,owner:user,impressions_count:30)
    my_writing_prompt_3 = create(:writing_prompt,owner:user)

    other_writing_prompt = create(:writing_prompt,owner:other_user,impressions_count:25)
    
    expect(user.total_writing_prompts_views).to eql(33)
  end

  it "returns 0 when no published stories have been written for the user's prompts" do
    user = FactoryBot.create(:user)
    writing_prompt = create(:writing_prompt,owner:user)
    story = create(:story,writing_prompt:writing_prompt)

    expect(user.total_writing_prompts_responses).to eql(0)
  end

  it "returns the total number of stories written for all of the prompts created by the user" do
    user = FactoryBot.create(:user)

    writing_prompt_1 = create(:writing_prompt,owner:user)
    writing_prompt_2 = create(:writing_prompt,owner:user)
    writing_prompt_3 = create(:writing_prompt,owner:user)

    writing_prompt_1_story_1 = create(:story,writing_prompt:writing_prompt_1,published_text:"kick me when im down")
    writing_prompt_1_story_2 = create(:story,writing_prompt:writing_prompt_1,published_text:"kick me when im down")
    writing_prompt_1_story_3 = create(:story,writing_prompt:writing_prompt_1,published_text:"kick me when im down")

    writing_prompt_3_story = create(:story,writing_prompt:writing_prompt_3,published_text:"xdddd")

    expect(user.total_writing_prompts_responses).to eql(4)
  end
  
  it "returns the story with the most views" do
    user = FactoryBot.create(:user)
    FactoryBot.create(:story,impressions_count:10, owner:user)
    expected_most_viewed_story = FactoryBot.create(:story,owner:user,impressions_count:500)
    FactoryBot.create(:story,owner:user, impressions_count:400)
    expect(user.most_viewed_story).to eql(expected_most_viewed_story)
  end

  it "returns nil for the story with the most views when there are no stories" do
    user = FactoryBot.create(:user)
    expect(user.most_viewed_story).to be_nil
  end

  it "returns the most popular story" do
    user = FactoryBot.create(:user)
    unrated_story,best_story,ok_story = create_list(:story,3,owner:user)
    
    create(:user_story,user:user,story:ok_story,rating:5)
    create(:user_story,user:user,story:ok_story,rating:3)
    create(:user_story,user:user,story:best_story,rating:5)
    create(:user_story,user:user,story:best_story,rating:4)

    puts user.most_popular_story.inspect
    puts best_story.inspect

    expect(user.most_popular_story).to eql(best_story)
  end

  it "returns nil for the most popular story when there are no stories" do
    user = FactoryBot.create(:user)
    expect(user.most_popular_story).to be_nil
  end

  it "returns the most recently updated story" do
    user = FactoryBot.create(:user)
    
    create(:story,owner:user,updated_at:DateTime.new(2018))
    most_recent_story = create(:story,owner:user,updated_at:DateTime.new(2020))
    create(:story,owner:user,updated_at:DateTime.new(2019))

    expect(user.most_recent_story).to eql(most_recent_story)
  end

  it "returns nil for the most recently created story when there are no stories" do
    user = FactoryBot.create(:user)
    expect(user.most_recent_story).to be_nil
  end

  describe "sort by story rating" do
    it "does nothing" do
      user_1,user_2 = create_list(:user,2)

      writing_prompt = create(:writing_prompt,owner:user_1)
      story = create(:story,owner:user_2,writing_prompt:writing_prompt)
      create(:user_story,user:user_2,story:story,rating:4)

      actual_users = User.filter_users({})
      expect(actual_users).to eq([user_1,user_2])
    end
    it "sorts by rating ascending" do
      highest_rated_user,lowest_rated_user = create_list(:user,2)
      writing_prompt=create(:writing_prompt,owner:highest_rated_user)
      story = create(:story,owner:highest_rated_user,writing_prompt:writing_prompt)
      create(:user_story,user:lowest_rated_user,story:story,rating:4)
      actual_user_ratings = User.filter_users({sort_by_story_rating:'asc'})
      expect(actual_user_ratings).to eql([lowest_rated_user,highest_rated_user]) 
    end
    it "sorts by rating descending" do
      highest_rated_user,lowest_rated_user,unrated_user,average_rated_user,no_stories_user = create_list(:user,5)

      # Make a dummy writing prompt with one of the users set as owner to prevent the factory from spawning tons 
      # of extra users on story creation
      writing_prompt = create(:writing_prompt,owner:highest_rated_user)

      highest_rated_user_stories = create_list(:story,3,owner:highest_rated_user,writing_prompt:writing_prompt)
      average_rated_user_story = create(:story,owner:average_rated_user,writing_prompt:writing_prompt)
      lowest_rated_user_stories = create_list(:story,2,owner:lowest_rated_user,writing_prompt:writing_prompt)
      create_list(:story,2,owner:unrated_user,writing_prompt:writing_prompt)

      # highest rated user's story ratings
      create(:user_story,user:average_rated_user,story:highest_rated_user_stories[0],rating:4)
      create(:user_story,user:unrated_user,story:highest_rated_user_stories[0],rating:5)
      create(:user_story,user:lowest_rated_user,story:highest_rated_user_stories[1],rating:3)
      create(:user_story,user:average_rated_user,story:highest_rated_user_stories[1],rating:5)
      create(:user_story,user:lowest_rated_user,story:highest_rated_user_stories[2],rating:5)

      # average rated user's story ratings
      create(:user_story,user:lowest_rated_user,story:average_rated_user_story,rating:3)
      create(:user_story,user:highest_rated_user,story:average_rated_user_story,rating:4)

      # lowest rated user's story ratings
      create(:user_story,user:average_rated_user,story:lowest_rated_user_stories[0],rating:1)
      create(:user_story,user:highest_rated_user,story:lowest_rated_user_stories[0],rating:2)

      actual_user_ratings = User.filter_users({sort_by_story_rating:'desc'})
      expect(actual_user_ratings).to eql([highest_rated_user,average_rated_user,lowest_rated_user,unrated_user,no_stories_user])
    end
  end

  describe "sort by email" do
    it "does nothing" do
      a = FactoryBot.create(:user,email:'a@xd.com')
      c = FactoryBot.create(:user,email:'c@y.com')
      b = FactoryBot.create(:user,email:'b@meme.com')
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([a,c,b])
    end
    it "sorts emails by ascending" do 
      josh = FactoryBot.create(:user,email:"josh@gmail.com")
      adam = FactoryBot.create(:user,email:"adam@gmail.com")
      bill = FactoryBot.create(:user,email:"bill@yahoo.com")
      actual_users = User.filter_users({sort_by_email:"asc"}).to_a
      expect(actual_users).to eql([adam,bill,josh])    
    end
    it "sorts emails by descending" do
      josh = FactoryBot.create(:user,email:"josh@gmail.com")
      adam = FactoryBot.create(:user,email:"adam@gmail.com")
      bill = FactoryBot.create(:user,email:"bill@yahoo.com")
      actual_users = User.filter_users({sort_by_email:"desc"}).to_a
      expect(actual_users).to eql([josh,bill,adam])   
    end
  end
  describe "sort by story views" do
    it "does nothing" do
      least_viewed_user,most_viewed_user = create_list(:user,2)
      
      writing_prompt = create(:writing_prompt,owner:most_viewed_user)
      create(:story,owner:least_viewed_user,writing_prompt:writing_prompt,impressions_count:4)
      create(:story,owner:most_viewed_user,writing_prompt:writing_prompt,impressions_count:20)
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([least_viewed_user,most_viewed_user])
    end
    it "sort by story views descending" do
      least_viewed_user,most_viewed_user,unviewed_user = create_list(:user,3)
      
      writing_prompt = create(:writing_prompt,owner:most_viewed_user)
      create(:story,owner:most_viewed_user,writing_prompt:writing_prompt,impressions_count:4)
      create(:story,owner:most_viewed_user,writing_prompt:writing_prompt,impressions_count:20)
      create(:story,owner:least_viewed_user,writing_prompt:writing_prompt,impressions_count:22)
      actual_users = User.filter_users({sort_by_story_views:'desc'}).to_a
      expect(actual_users).to eql([most_viewed_user,least_viewed_user,unviewed_user])
    end
    it "sorts by story views ascending" do 
      least_viewed_user,most_viewed_user,unviewed_user = create_list(:user,3)
      
      writing_prompt = create(:writing_prompt,owner:most_viewed_user)
      create(:story,owner:most_viewed_user,writing_prompt:writing_prompt,impressions_count:4)
      create(:story,owner:most_viewed_user,writing_prompt:writing_prompt,impressions_count:20)
      create(:story,owner:least_viewed_user,writing_prompt:writing_prompt,impressions_count:22)
      actual_users = User.filter_users({sort_by_story_views:'asc'}).to_a
      expect(actual_users).to eql([unviewed_user,least_viewed_user,most_viewed_user])  
    end
  end

  # TODO Pretty heavy copy pastes of story ratings/story prompts sorting tests
  describe "sort by prompt views" do
    it "does nothing" do
      least_viewed_user,most_viewed_user = create_list(:user,2)
      
      create(:writing_prompt,owner:most_viewed_user,impressions_count:20)
      create(:writing_prompt,owner:least_viewed_user,impressions_count:12)
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([least_viewed_user,most_viewed_user])
    end
    it "sort by prompt views ascending" do
      least_viewed_user,most_viewed_user,unviewed_user = create_list(:user,3)
      
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:3)
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:5)
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:8)
      writing_prompt = create(:writing_prompt,owner:least_viewed_user,impressions_count:15)

      actual_users = User.filter_users({sort_by_prompts_views:'asc'}).to_a
      expect(actual_users).to eql([unviewed_user,least_viewed_user,most_viewed_user])
    end
    it "sorts by prompt views descending" do 
      least_viewed_user,most_viewed_user,unviewed_user = create_list(:user,3)
      
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:3)
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:5)
      writing_prompt = create(:writing_prompt,owner:most_viewed_user,impressions_count:8)
      writing_prompt = create(:writing_prompt,owner:least_viewed_user,impressions_count:15)

      actual_users = User.filter_users({sort_by_prompts_views:'desc'}).to_a
      expect(actual_users).to eql([most_viewed_user,least_viewed_user,unviewed_user])  
    end
  end
  describe "sort by prompt rating" do
    it "does nothing" do
      user_1,user_2 = create_list(:user,2)

      writing_prompt = create(:writing_prompt,owner:user_1)
      create(:user_writing_prompt,user:user_2,writing_prompt:writing_prompt,rating:4)

      actual_users = User.filter_users({})
      expect(actual_users).to eq([user_1,user_2])
    end
    it "sorts by rating ascending" do
      highest_rated_user,lowest_rated_user = create_list(:user,2)
      highest_rated_prompts=create_list(:writing_prompt,2,owner:highest_rated_user)
      lowest_rated_prompts=create_list(:writing_prompt,2,owner:lowest_rated_user)

      create(:user_writing_prompt,user:highest_rated_user,writing_prompt:highest_rated_prompts[0],rating:1)
      create(:user_writing_prompt,user:lowest_rated_user,writing_prompt:highest_rated_prompts[0],rating:5)
      create(:user_writing_prompt,user:highest_rated_user,writing_prompt:highest_rated_prompts[1],rating:4)

      create(:user_writing_prompt,user:lowest_rated_user,writing_prompt:lowest_rated_prompts[0],rating:2)
      create(:user_writing_prompt,user:highest_rated_user,writing_prompt:lowest_rated_prompts[1],rating:1)

      actual_user_ratings = User.filter_users({sort_by_prompts_rating:'asc'})
      expect(actual_user_ratings).to eql([lowest_rated_user,highest_rated_user]) 
    end

    it "sorts by rating descending" do
      highest_rated_user,lowest_rated_user,unrated_user,average_rated_user,no_stories_user = create_list(:user,5)

      highest_rated_user_prompts = create_list(:writing_prompt,3,owner:highest_rated_user)
      average_rated_user_prompt = create(:writing_prompt,owner:average_rated_user)
      lowest_rated_user_prompts = create_list(:writing_prompt,2,owner:lowest_rated_user)
      create_list(:writing_prompt,2,owner:unrated_user)

      # highest rated user's prompts ratings
      create(:user_writing_prompt,user:average_rated_user,writing_prompt:highest_rated_user_prompts[0],rating:4)
      create(:user_writing_prompt,user:unrated_user,writing_prompt:highest_rated_user_prompts[0],rating:5)
      create(:user_writing_prompt,user:lowest_rated_user,writing_prompt:highest_rated_user_prompts[1],rating:3)
      create(:user_writing_prompt,user:average_rated_user,writing_prompt:highest_rated_user_prompts[1],rating:5)
      create(:user_writing_prompt,user:lowest_rated_user,writing_prompt:highest_rated_user_prompts[2],rating:5)

      # average rated user's prompts ratings
      create(:user_writing_prompt,user:lowest_rated_user,writing_prompt:average_rated_user_prompt,rating:3)
      create(:user_writing_prompt,user:highest_rated_user,writing_prompt:average_rated_user_prompt,rating:4)

      # lowest rated user's prompts ratings
      create(:user_writing_prompt,user:average_rated_user,writing_prompt:lowest_rated_user_prompts[0],rating:1)
      create(:user_writing_prompt,user:highest_rated_user,writing_prompt:lowest_rated_user_prompts[0],rating:2)

      actual_user_ratings = User.filter_users({sort_by_prompts_rating:'desc'})
      expect(actual_user_ratings).to eql([highest_rated_user,average_rated_user,lowest_rated_user,unrated_user,no_stories_user])
    end
  end
  
  describe "sort by number of stories" do
    it "does nothing" do
      user_1,user_2 = create_list(:user,2)
      writing_prompt = create(:writing_prompt,owner:user_2)
      create_list(:story,10,owner:user_2,writing_prompt:writing_prompt,published_text:"denzel curry")
      create(:story,owner:user_1,writing_prompt:writing_prompt,published_text:"asdfasdfad")
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([user_1,user_2])
    end
    it "sorts number of stories by ascending" do  
      most_stories_user,no_stories_user,least_stories_user = create_list(:user,3)
      writing_prompt = create(:writing_prompt,owner:most_stories_user)
      create_list(:story,15,owner:most_stories_user,writing_prompt:writing_prompt,published_text:"maymay")
      create_list(:story,3,owner:least_stories_user,writing_prompt:writing_prompt,published_text:"lil uzi")
      actual_users = User.filter_users({sort_by_num_stories:'asc'}).to_a
      expect(actual_users).to eql([no_stories_user,least_stories_user,most_stories_user])  
    end
    it "sorts number of stories by descending" do  
      most_stories_user,no_stories_user,least_stories_user = create_list(:user,3)
      writing_prompt = create(:writing_prompt,owner:most_stories_user)
      create_list(:story,15,owner:most_stories_user,writing_prompt:writing_prompt,published_text:"xd")
      create_list(:story,3,owner:least_stories_user,writing_prompt:writing_prompt,published_text:"mone")
      actual_users = User.filter_users({sort_by_num_stories:'desc'}).to_a
      expect(actual_users).to eql([most_stories_user,least_stories_user,no_stories_user])   
    end
    it "ignores unpublished stories" do
      most_stories_user,unpublished_stories_user,least_stories_user = create_list(:user,3)
      writing_prompt = create(:writing_prompt,owner:most_stories_user)
      create_list(:story,15,owner:most_stories_user,writing_prompt:writing_prompt,published_text:"xd")
      create_list(:story,3,owner:least_stories_user,writing_prompt:writing_prompt,published_text:"mone")
      create_list(:story,16,owner:unpublished_stories_user,writing_prompt:writing_prompt)
      actual_users = User.filter_users({sort_by_num_stories:'desc'}).to_a
      expect(actual_users).to eql([most_stories_user,least_stories_user,unpublished_stories_user])   
    end
  end
  describe "sort by number of prompts" do
    it "does nothing" do
      user_1,user_2 = create_list(:user,2)
      create(:writing_prompt,owner:user_1)
      create_list(:writing_prompt,3,owner:user_2)
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([user_1,user_2])
    end
    it "sorts number of prompts by ascending" do  
      most_prompts_user,no_prompts_user,least_prompts_user = create_list(:user,3)
      create_list(:writing_prompt,15,owner:most_prompts_user)
      create_list(:writing_prompt,3,owner:least_prompts_user)
      actual_users = User.filter_users({sort_by_num_prompts:'asc'}).to_a
      expect(actual_users).to eql([no_prompts_user,least_prompts_user,most_prompts_user])  
    end
    it "sorts number of prompts by descending" do  
      most_prompts_user,no_prompts_user,least_prompts_user = create_list(:user,3)
      create_list(:writing_prompt,15,owner:most_prompts_user)
      create_list(:writing_prompt,3,owner:least_prompts_user)
      actual_users = User.filter_users({sort_by_num_prompts:'desc'}).to_a
      expect(actual_users).to eql([most_prompts_user,least_prompts_user,no_prompts_user])   
    end
  end
  describe "sort by number of responses(stories) to prompts" do
    it "does nothing" do
      user_1,user_2 = create_list(:user,2)
      writing_prompt = create(:writing_prompt,owner:user_1)
      create_list(:story,3,writing_prompt:writing_prompt,owner:user_1)
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql([user_1,user_2])
    end
    it "sorts number of responses by ascending" do  
      most_responses_user,no_responses_user,no_prompts_user,least_responses_user = create_list(:user,4)
      
      most_responses_prompts = create_list(:writing_prompt,4,owner:most_responses_user)
      least_responses_prompts = create_list(:writing_prompt,2,owner:least_responses_user)
      create_list(:writing_prompt,5,owner:no_responses_user)

      create_list(:story,2,writing_prompt:most_responses_prompts[0],owner:no_responses_user,published_text:"lil peep")
      create_list(:story,3,writing_prompt:most_responses_prompts[2],owner:no_responses_user,published_text:"just like my")
      create_list(:story,4,writing_prompt:most_responses_prompts[3],owner:no_responses_user,published_text:"suck my blood")
      
      create_list(:story,5,writing_prompt:least_responses_prompts[0],owner:no_responses_user,published_text:"asdfasdf")
      create_list(:story,1,writing_prompt:least_responses_prompts[1],owner:no_responses_user,published_text:"asdfsdf")

      actual_users = User.filter_users({sort_by_num_responses:'asc'}).to_a
      expect(actual_users).to eql([no_responses_user,no_prompts_user,least_responses_user,most_responses_user])  
    end
    it "sorts number of prompts by descending" do  
      most_responses_user,no_responses_user,no_prompts_user,least_responses_user = create_list(:user,4)
      
      most_responses_prompts = create_list(:writing_prompt,4,owner:most_responses_user)
      least_responses_prompts = create_list(:writing_prompt,2,owner:least_responses_user)
      create_list(:writing_prompt,5,owner:no_responses_user)

      create_list(:story,2,writing_prompt:most_responses_prompts[0],owner:no_responses_user,published_text:"lil peep")
      create_list(:story,3,writing_prompt:most_responses_prompts[2],owner:no_responses_user,published_text:"just like my")
      create_list(:story,4,writing_prompt:most_responses_prompts[3],owner:no_responses_user,published_text:"suck my blood")
      
      create_list(:story,5,writing_prompt:least_responses_prompts[0],owner:no_responses_user,published_text:"asdfasdf")
      create_list(:story,1,writing_prompt:least_responses_prompts[1],owner:no_responses_user,published_text:"asdfsdf")
      
      actual_users = User.filter_users({sort_by_num_responses:'desc'}).to_a

      # TODO Shows how sort_by_num_responses does not strictly reverse (i.e. no responses vs no prompts will be same order ASC or DESC)
      # This is probably fine
      expect(actual_users).to eql([most_responses_user,least_responses_user,no_responses_user,no_prompts_user])
    end
    it "ignores unpublished stories" do
      least_responses_user,most_responses_user=create_list(:user,2)
      
      most_responses_prompt = create(:writing_prompt,owner:most_responses_user)
      least_responses_prompt = create(:writing_prompt,owner:least_responses_user)
      
      create_list(:story,2,writing_prompt:most_responses_prompt,owner:most_responses_user,published_text:"lil peep")
      create_list(:story,5,writing_prompt:least_responses_prompt,owner:least_responses_user)

      actual_users = User.filter_users({sort_by_num_responses:'desc'}).to_a
      expect(actual_users).to eql([most_responses_user,least_responses_user])
    end
  end

  # TODO Also copy pasted from filterable_spec
  # Could think about making a separate paging concern,
  # thats probably the right long term solution
  describe "paging" do
    it "defaults to page 1 and 10 max items" do
      expected_users = create_list(:user,15)[0..9]
      actual_users = User.filter_users({}).to_a
      expect(actual_users).to eql(expected_users)
    end
    it "returns up to a page of users" do
      expected_users = create_list(:user,15)[0..9]
      actual_users = User.filter_users({page:1,page_size:15}).to_a
      expect(actual_users).to eql(expected_users)
    end
    it "returns the last page of users" do
      expected_users = create_list(:user,15)[10..14]
      actual_users = User.filter_users({page:2,page_size:10}).to_a
      expect(actual_users).to eql(expected_users)
    end
  end
end
