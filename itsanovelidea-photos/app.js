const express = require('express');
const {
  apiKey,
  redis,
  swagger,
  health,
  healthCheck,
  logger,
} = require('itsanovelidea-middleware');

const photo = require('./photo.js');
const bucket = require('./bucket.js');

const app = express();
app.use(apiKey);
app.use('/swagger', swagger('Itsanovelidea-photos', __dirname, ['./app.js']));

/**
 * @swagger
 *
 * /health:
 *   get:
 *     description: Health of itsanovelidea-photos service and its dependencies
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Everything is healthy
 *       424:
 *         description: Something is not healthy
 */
app.use('/health', health({
  'itsanovelidea-photos': healthCheck(),
  redis: healthCheck(redis.ping),

  // Not really sure the appropriate way to healthcheck the S3 bucket, but
  // I want to make sure I have perms to do stuff, not just that I can
  // access the address it's hosted at.
  // I override the timeout to be longer b/c the default timeout of 2s was causing it
  // to sometimes resolve and sometimes fail
  s3: healthCheck(() => bucket.uploadTextFile('healthCheck.txt', 'HEALTHY'), 10000),
}));

/**
 * @swagger
 *
 * /photo:
 *   get:
 *     description: Search for photos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: search
 *         description: Search term
 *         in: query
 *         required: true
 *         type: string
 *       - name: page
 *         description: Page to grab photos from
 *         in: query
 *         required: false
 *         type: integer
 *       - name: numResults
 *         description: Number of photos per page
 *         in: query
 *         required: false
 *         type: integer
 *       - name: skipCache
 *         description: Don't return cached results
 *         in: query
 *         required: false
 *         type: boolean
 *     responses:
 *       200:
 *         description: Page of photo search results from pixabay (or from the cache)
 *       400:
 *         description: Missing `search` in query string
 */
app.get('/photo', async (req, res) => {
  let {
    page = 1,
    numResults = 10,
  } = req.query;
  const { search, skipCache } = req.query;
  numResults = parseInt(numResults, 10);
  page = parseInt(page, 10);

  if (!search) {
    res.status(400).send('Missing search parameter from query string');
    return;
  }

  // The pixabay API has some of the weirdest rules i've ever seen.
  // https://pixabay.com/api/docs/
  if (numResults < 3 || numResults > 200) {
    res.status(400).send('numResults must be in the range 3-200');
    return;
  }
  // Apparently, you can't get past 500 hits...
  if (numResults * page > 500) {
    res.status(400).send("Can't search further than 500 images");
  }
  try {
    const photos = await photo.search(search, parseInt(page, 10), parseInt(numResults, 10), skipCache === 'true');
    res.json(photos);
  } catch (err) {
    logger.log('error', err);
    res.status(500).send(err);
  }
});

/**
 * @swagger
 *
 * /photo/{searchKey}:
 *   post:
 *     description: Process one of the photos that has been searched for.
 *       Will parse the major colors in the photo as well as copy the photo to
 *       an S3 bucket
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: searchKey
 *         description: the key used to identify the page of photos (returned in `/search`)
 *         in: path
 *         required: true
 *         type: string
 *       - name: index
 *         description: The index of the photo to process
 *         in: query
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Processed photo results (colors, s3 bucket location, etc)s
 */
app.post('/photo/:searchKey', async (req, res) => {
  const { searchKey } = req.params;
  const { index } = req.query;
  logger.log('info', `Reading ${encodeURIComponent(searchKey)}`);

  try {
    const preparedPhoto = await photo.prepare(searchKey, parseInt(index, 10));
    if (!preparedPhoto) {
      res.sendStatus(404);
      return;
    }
    const {
      largeImageURL,
      id,
      backgroundColor,
      textColor,
      luminanceClass,
      height,
      width,
    } = preparedPhoto;

    // This is a little bit strange. Basically, I don't want to force calls to photo/:searchKey to
    // wait for the full image to be cached in the S3 bucket as it might take some time.
    // Instead, what I do is write out a text file that initially points to the pixabay
    // url. When the photo stream completes writing out the file, I then change that
    // text file to point to the S3 bucket url of the image. The rails server initially
    // references the pixabay url, but it calls the photo microservice back every time
    // it loads the photo until the url has changed to the S3 bucket.
    const idUrl = await bucket.uploadTextFile(`${id}.txt`, largeImageURL);
    const extension = largeImageURL.split('.').pop();
    bucket.uploadImageFromUrl(largeImageURL, `${id}.${extension}`, (url) => {
      bucket.uploadTextFile(`${id}.txt`, url);
    });
    res.json({
      idUrl,
      height,
      width,
      backgroundColor,
      textColor,
      luminanceClass,
    });
  } catch (err) {
    logger.log('error', err);
    res.status(500).send(err.toString());
  }
});

module.exports = app;
