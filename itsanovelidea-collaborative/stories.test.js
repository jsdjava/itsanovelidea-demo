const storiesFn = require('./stories.js');

jest.mock('./utils.js');

test('stories authorized for the user can be read from sharedb', async () => {
  const allowReadMock = jest.fn();
  storiesFn({
    access: {
      allowRead: allowReadMock,
      allowUpdate: jest.fn(),
    },
  });
  expect(allowReadMock).toHaveBeenCalled();
  const [, authFn] = allowReadMock.mock.calls[0];

  // Make sure that when the authorized document id and actual document id are equal,
  // the user is allowed to edit the document
  const authResult = await authFn(1, {}, 1);
  expect(authResult).toBeTruthy();
});

test("stories not authorized for the user can't be read from sharedb", async () => {
  const allowReadMock = jest.fn();
  storiesFn({
    access: {
      allowRead: allowReadMock,
      allowUpdate: jest.fn(),
    },
  });
  expect(allowReadMock).toHaveBeenCalled();
  const [, authFn] = allowReadMock.mock.calls[0];
  const authResult = await authFn(1, {}, 2);
  expect(authResult).toBeFalsy();
});

test('stories authorized for the user can be updated from sharedb', async () => {
  const allowUpdateMock = jest.fn();
  storiesFn({
    access: {
      allowRead: jest.fn(),
      allowUpdate: allowUpdateMock,
    },
  });
  expect(allowUpdateMock).toHaveBeenCalled();
  const [, authFn] = allowUpdateMock.mock.calls[0];

  // Make sure that when the authorized document id and actual document id are equal,
  // the user is allowed to edit the document. Slightly different syntax then
  // the allowRead function
  const authResult = await authFn(1, {}, {}, [], 1);
  expect(authResult).toBeTruthy();
});

test("stories not authorized for the user can't be updated in sharedb", async () => {
  const allowUpdateMock = jest.fn();
  storiesFn({
    access: {
      allowRead: jest.fn(),
      allowUpdate: allowUpdateMock,
    },
  });
  expect(allowUpdateMock).toHaveBeenCalled();
  const [, authFn] = allowUpdateMock.mock.calls[0];
  const authResult = await authFn(1, {}, {}, [], 2);
  expect(authResult).toBeFalsy();
});

test('getting a story pulls the story snapshot from sharedb', async () => {
  const fetchSnapshotMock = jest.fn();
  const { getStory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    fetchSnapshot: fetchSnapshotMock,
  });
  await getStory(1, 20);
  expect(fetchSnapshotMock).toHaveBeenCalledWith('stories', 1, 20);
});

test('creating a story creates a document in shareDb', async () => {
  const createDocumentMock = jest.fn();
  const { createStory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    createDocument: createDocumentMock,
  });
  await createStory(4);
  expect(createDocumentMock).toHaveBeenCalledWith('stories', 4);
});

test('getStoryHistory should use the passed in version', async () => {
  const fetchSnapshotMock = jest.fn();
  const fetchSnapshotMetadataMock = jest.fn();
  const { getStoryHistory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    fetchSnapshot: fetchSnapshotMock
      .mockReturnValue({
        v: 5,
      }),
    fetchSnapshotMetadata: fetchSnapshotMetadataMock
      .mockReturnValue({
        m: {
          ts: 6,
        },
      }),
  });

  await getStoryHistory(1, {
    // Asking for version 23 when milestone size is 20 will cause it to hit the edge case
    // where it loads only the 20th snapshot, and then diffs it with the 1st
    version: 23,
    numberResults: 3,
  });
  expect(fetchSnapshotMock.mock.calls).toEqual([
    ['stories', 1],
    ['stories', 1, 20],
    ['stories', 1, 1],
  ]);
  expect(fetchSnapshotMetadataMock.mock.calls).toEqual([
    // 5 doesn't really matter, its being mapped out from th fetchSnpshot return
    ['stories', 1, 5],
  ]);
});

test('getStoryHistory should return descending snapshots starting at the newest and until number results', async () => {
  const fetchSnapshotMock = jest.fn();
  const fetchSnapshotMetadataMock = jest.fn();
  const { getStoryHistory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    // Setup fake snapshots from 0 - 99
    fetchSnapshot: fetchSnapshotMock
    // The first call to fetch snapshot is reading the newest snapshot
      .mockReturnValueOnce({ v: 99 })
    // Although I'm only calling for three results, I need to return 4
    // because its a diff
      .mockReturnValueOnce({ v: 99 })
      .mockReturnValueOnce({ v: 80 })
      .mockReturnValueOnce({ v: 60 })
      .mockReturnValueOnce({ v: 40 }),
    // Basically the same as fetch snapshot, but returns the timestamp from mongo
    fetchSnapshotMetadata: fetchSnapshotMetadataMock
      .mockReturnValueOnce({ m: { ts: 99 } })
      .mockReturnValueOnce({ m: { ts: 80 } })
      .mockReturnValueOnce({ m: { ts: 60 } })
      .mockReturnValueOnce({ m: { ts: 40 } }),
  });

  const storyHistory = await getStoryHistory(1, {
    numberResults: 3,
  });
  expect(fetchSnapshotMock.mock.calls).toEqual([
    ['stories', 1],
    ['stories', 1, 99],
    ['stories', 1, 80],
    ['stories', 1, 60],
    ['stories', 1, 40],
  ]);
  // Only need 3 metadatas, one for each diff
  expect(fetchSnapshotMetadataMock.mock.calls).toEqual([
    ['stories', 1, 80],
    ['stories', 1, 60],
    ['stories', 1, 40],
  ]);
  expect(storyHistory).toEqual([{
    timestamp: 99,
    newSnapshot: { v: 99 },
    oldSnapshot: { v: 80 },
  }, {
    timestamp: 80,
    newSnapshot: { v: 80 },
    oldSnapshot: { v: 60 },
  }, {
    timestamp: 60,
    newSnapshot: { v: 60 },
    oldSnapshot: { v: 40 },
  }]);
});

test('getStoryHistory should return ascending snapshots, including the latest if not enough results', async () => {
  const fetchSnapshotMock = jest.fn();
  const fetchSnapshotMetadataMock = jest.fn();
  const { getStoryHistory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    // Setup fake snapshots from 0 - 99
    fetchSnapshot: fetchSnapshotMock
    // The first call to fetch snapshot is reading the newest snapshot
      .mockReturnValueOnce({ v: 99 })
      .mockReturnValueOnce({ v: 80 }),
    // Basically the same as fetch snapshot, but returns the timestamp from mongo
    fetchSnapshotMetadata: fetchSnapshotMetadataMock
      .mockReturnValueOnce({ m: { ts: 80 } })
      .mockReturnValueOnce({ m: { ts: 99 } }),
  });

  const storyHistory = await getStoryHistory(1, {
    numberResults: 5,
    version: 83,
    ascending: true,
  });
  expect(fetchSnapshotMock.mock.calls).toEqual([
    ['stories', 1],
    ['stories', 1, 80],
    // Doesn't need to call for latest, it just got it on the first call
  ]);
  expect(fetchSnapshotMetadataMock.mock.calls).toEqual([
    ['stories', 1, 80],
  ]);

  expect(storyHistory).toEqual([{
    timestamp: 80,
    newSnapshot: { v: 99 },
    oldSnapshot: { v: 80 },
  }]);
});

test('getStoryHistory should handle the edge case of descending, only 1 snapshot exists', async () => {
  const fetchSnapshotMock = jest.fn();
  const { getStoryHistory } = storiesFn({
    // Unfortunately, have to mock these just to avoid errors when initializing
    // stories from storiesFn
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    // Setup fake snapshots from 0 - 99
    fetchSnapshot: fetchSnapshotMock
    // The first call to fetch snapshot is reading the newest snapshot
      .mockReturnValueOnce({ v: 1 })
      .mockReturnValueOnce({ v: 1 }),
  });

  const storyHistory = await getStoryHistory(1, {
    numberResults: 10,
  });
  expect(fetchSnapshotMock.mock.calls).toEqual([
    ['stories', 1],
    ['stories', 1, 1],
  ]);
  expect(storyHistory).toEqual([]);
});
