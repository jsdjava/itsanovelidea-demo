class AddAuthorIdToStories < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :reddit_author_id, :string
  end
end
