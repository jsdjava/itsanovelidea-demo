import React from 'react';
import PhotoCarouselInfinite from "./PhotoCarouselInfinite.jsx";

export default {
  component: PhotoCarouselInfinite,
  title: 'PhotoCarouselInfinite',
};

export const photoCarouselInfiniteSequential = () => <PhotoCarouselInfinite 
  searchPhotos={async (page,pageSize)=>{
    return [...new Array(pageSize)].map((x,spot)=>({
      largeImageURL:`https://via.placeholder.com/1000x1000.png?text=${(page-1)*pageSize+spot+1} `,
    }));
  }}
/>;

export const photoCarouselInfinite = ()=> <PhotoCarouselInfinite 
searchPhotos={async (page,pageSize)=>{
  const resp = await fetch(`https://picsum.photos/v2/list?page=${page}&limit=${pageSize}`);
  const json = await resp.json();
  return json.map(({download_url})=>({
    largeImageURL:download_url,
  }));
}}
/>;

export const photoCarouselInfiniteNoPhotos = ()=> <PhotoCarouselInfinite 
searchPhotos={async ()=>{
  return [];
}}
/>;

export const photoCarouselInfinite4thPhotoSelected = ()=> <PhotoCarouselInfinite 
selectedIndex={3}
searchPhotos={async (page,pageSize)=>{
  const resp = await fetch(`https://picsum.photos/v2/list?page=${page}&limit=${pageSize}`);
  const json = await resp.json();
  return json.map(({download_url})=>({
    largeImageURL:download_url,
  }));
}}
/>;
