import React from "react";

import CircularProgress from '@material-ui/core/CircularProgress';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

import "../stylesheets/WritingPromptCarousel.css";

class WritingPromptCarousel extends React.Component{

  constructor(){
      super();
      this.state = {
        writingPromptIndex:0,
      };
  }

  render(){
    const {
        writingPrompts,
        onChange,
        loadedWritingPrompts,
    } = {
        onChange:()=>{},
        ...this.props
    };
    const {writingPromptIndex} = this.state;
    console.log(writingPrompts);
    return <div className = "writingPromptCarouselDiv">
        {loadedWritingPrompts ? writingPrompts.length ? 
        <Carousel 
            infinite
            centered
            slidesPerPage={1} 
            arrows 
            slidesToScroll={1} 
            value={writingPromptIndex}
            onChange={index => {
                if(writingPrompts[index]){
                    this.setState({ writingPromptIndex:index});
                    onChange(writingPrompts[index]);
                }
            }}>
            {
                writingPrompts.map(writingPrompt=><p><a style ={{color:"black"}} href={`${writingPrompt.url.split(".json")[0]}.html`}>{writingPrompt.text}</a></p>)
            }
        </Carousel>
        : "No writing prompts found": <CircularProgress size="10vw" thickness={2} style = {{
            position: "fixed",
            left: "45%",
            top: "45%",
            "z-index": "9001",
        }}/>   }
    </div>
  }
}

export default WritingPromptCarousel;