module "itsanovelidea-service-photos" {
  source = "./itsanovelidea-service"
  itsanovelidea_service_name = "itsanovelidea-service-photos"
  itsanovelidea_cluster_id=aws_ecs_cluster.itsanovelidea.id
  itsanovelidea_task_definition_arn=aws_ecs_task_definition.itsanovelidea-photos-task.arn
  itsanovelidea_vpc_subnets=module.vpc.public_subnets
  itsanovelidea_subdomain_prefix = "photos"
}

resource "aws_ecs_task_definition" "itsanovelidea-photos-task" {
  family                = "itsanovelidea-photos-task"
  requires_compatibilities = ["EC2"]
  container_definitions = "${file("ecs/itsanovelidea-photos.json")}"
  # So this if fun. Terraform seems bugged, and won't just read the fun
  # arn from the dynamically created task role...
  #execution_role_arn="${module.itsanovelidea-service-photos.task_role_id}"
  execution_role_arn="arn:aws:iam::242929592693:role/itsanovelidea-service-photos-task"
  volume {
    name      = "itsanovelidea-storage"
  }
  tags =  {
    project = "itsanovelidea"
  }
  placement_constraints {
    type       = "memberOf"
    expression = "attribute:itsanovelidea-type==photos"
  }
}
