import React from "react";

import TopPrompts from "./TopPrompts.jsx";
import TopStories from "./TopStories.jsx";
import PromptsStats from "./PromptsStats.jsx";
import StoriesStats from "./StoriesStats.jsx";

import {getUser,getStoriesCount,getWritingPromptsCount} from "../helpers/api.js";

class Profile extends React.Component{
    constructor(){
        super();
        this.state = {
            user:{}
        };
        this.getUser = this.getUser.bind(this);
        this.getStoriesCount = this.getStoriesCount.bind(this);
        this.getWritingPromptsCount = this.getWritingPromptsCount.bind(this);
    }

    componentDidMount(){
        this.getUser();
        this.getWritingPromptsCount();
        this.getStoriesCount();
    }

    // These fns still feel a little copy-pasted, i need to see if i can bind this multiple times?
    // feel like u can...    
    async getUser(){
        const {userUrl} = this.props;
        const user = await getUser(userUrl);
        if(user){
          this.setState({
            user,
          });
        }
    }

    async getWritingPromptsCount (){
        const {
            writingPromptsCountUrl,
            userId,
        } = this.props;
        const writingPromptsCount = await getWritingPromptsCount(writingPromptsCountUrl,userId);
        if(writingPromptsCount!==undefined){
            this.setState({
                writingPromptsCount,
            })
        }
    }

    async getStoriesCount(){
        const {
          storiesCountUrl,
          userId,
        } = this.props;
        const storiesCount = await getStoriesCount(storiesCountUrl,userId);
        if(storiesCount!==undefined){
          this.setState({
            storiesCount,
          })
        }
    }

    render(){
        const {
            user,
            writingPromptsCount,
            storiesCount
        } = this.state;
        return <div className="profileRightColumnDiv">
            <PromptsStats writingPromptsCount={writingPromptsCount}
                rating={user.prompts_rating}
                totalResponses={user.total_writing_prompts_responses}
                totalViews={user.total_writing_prompts_views}/>
            <TopPrompts highestRatedPrompt={user.most_popular_prompt}
                mostViewedPrompt={user.most_viewed_prompt}
                mostRecentPrompt={user.most_recent_prompt}
                mostRepliedPrompt={user.most_replied_prompt}/>
            <StoriesStats
                storiesCount={storiesCount}
                rating={user.stories_rating}
                totalViews={user.total_stories_views}/>
            <TopStories 
                highestRatedStory={user.most_popular_story}
                mostViewedStory={user.most_viewed_story}
                mostRecentStory={user.most_recent_story}
            />
        </div>;
    };
}

export default Profile;
