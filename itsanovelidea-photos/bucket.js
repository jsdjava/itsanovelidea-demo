const { PassThrough } = require('stream');
const AWS = require('aws-sdk');
const request = require('request');

const Bucket = 'itsanovelidea-photos';

AWS.config.update({
  region: 'us-east-1',
});

const s3 = new AWS.S3({
  apiVersion: '2006-03-01',
});

const uploadTextFile = async (fileName, text) => {
  const uploadParams = {
    Bucket,
    Key: fileName,
    Body: text,
  };
  const { Location } = await s3.upload(uploadParams).promise();
  return Location;
};

const uploadImageFromUrl = (url, filename, cb) => {
  const imageStream = new PassThrough();
  const photoStream = request(url).pipe(imageStream);
  s3.upload({
    Bucket,
    Key: filename,
    Body: photoStream,
  }, (err, { Location }) => {
    if (!err) {
      cb(Location);
    }
  });
};

module.exports = {
  uploadImageFromUrl,
  uploadTextFile,
};
