class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :category
      t.string :description
      t.string :photo_search
      t.timestamps
    end
  end
end
