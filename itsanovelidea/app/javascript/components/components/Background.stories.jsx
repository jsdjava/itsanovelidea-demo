import React from 'react';
import Background from "./Background.jsx";

export default {
  component: Background,
  title: 'Background',
};

export const background = () => <Background/>;
