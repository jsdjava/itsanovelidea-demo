class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
	before_action :authenticate_user!
	before_action :authenticate_api_key!
  before_action :set_urls

	def authenticate_api_key!
		if request.headers["apikey"] != Rails.configuration.node_api_key && request.remote_ip!="65.29.172.120" && request.remote_ip!="65.29.169.244"
			head 403
		end
	end

  def set_urls
  	@urls = {
			"Stories" => Rails.application.routes.url_helpers.url_for(controller:'stories',only_path:true, action:'index'),
			"Prompts" => Rails.application.routes.url_helpers.url_for(controller:'writing_prompts',only_path:true,action:'index'),
			"Authors" => Rails.application.routes.url_helpers.url_for(controller:'users',only_path:true,action:'index'),
			"Log out" => Rails.application.routes.url_helpers.url_for(controller:'devise/sessions',only_path:true,action:'destroy'),
  	}
  end

  #Thanks http://stevenyue.com/blogs/sharing-sessions-and-authentication-between-rails-and-node-js-using-redis/
	def after_sign_in_path_for(resource_or_scope)
    #store session to redis
	  if current_user
			
			# an unique MD5 key
		  ukey = "#{session[:session_id]}:#{current_user.id}"
		
		  #TODO Duplicated in devise.rb
		  cookies["_validation_token_key"] = {:value=>Digest::MD5.hexdigest(ukey),:expires => Time.now+1.day}
		
	    # store session data or any authentication data you want here, generate to JSON data
	    stored_session = JSON.generate({
	      "user_id" => current_user.id,
	      "user_email" => current_user.email
	    })
		Redis.current.hset("auth_keys", cookies["_validation_token_key"], stored_session)
		current_user.create_profile
	  end
	  super
  end

  def after_sign_out_path_for(resource_or_scope)
    #expire session in redis
	  if cookies["_validation_token_key"].present?
	    Redis.current.hdel("auth_keys", cookies["_validation_token_key"])
	  end
	  super
  end
end
