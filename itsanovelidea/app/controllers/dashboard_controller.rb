class DashboardController < ApplicationController
  
  def index
    @node_url = 'wss://'+Rails.application.config.node_host
  end

  def stories
  end

  def prompts
  end

end
