const {config} = require('./wdio.conf.js');

exports.config = {
  ...config,
  capabilities: [{
    maxInstances: 1,
    browserName: 'chrome',
  }],
}