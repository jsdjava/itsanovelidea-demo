import React from 'react';
import Button from '@material-ui/core/Button';

import DashboardMenu from "./DashboardMenu.jsx";

export default {
  component: DashboardMenu,
  title: 'DashboardMenu',
};

export const dashboardMenu = () => <DashboardMenu>
    {
        (toggle)=><Button onClick = {toggle}>
            Itsanovelidea
        </Button>
    }
</DashboardMenu>;
