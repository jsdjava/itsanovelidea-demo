import React from "react";
import {TextField,TableSortLabel} from '@material-ui/core';
import update from 'immutability-helper';

import {fetchWithToken} from "../helpers/api.js";
import {getGalleryPadding} from "../helpers/utils.js";

import "../stylesheets/WritingPromptStories.css";

const debounce = require('debounce'); 

class WritingPrompt extends React.Component {
  constructor(props){
    super(props);
    this.state={
      stories:[],
      page:1,
      pageSize:20,
      orderDirection:'desc',
    };
    this.refreshStory = this.refreshStory.bind(this);
    this.debouncedGetFilteredStories = debounce(this.getFilteredStories.bind(this),1000);
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  updateDimensions() {
    const numColumns = Math.ceil(window.innerWidth/600)
    this.setState({ numColumns });
  };

  componentWillUnmount() { 
    window.removeEventListener('resize', this.updateDimensions);
  }

  //TODO Essentially copy pasted from dashboardv2
  async refreshStory(storySpot){
    const {stories} = this.state;
    const story = stories[storySpot];
    const {url} = story;
    try{
      const resp = await fetchWithToken(url);
      if(!resp.ok){
        throw resp;
      }
      const updatedStory = await resp.json();
      this.setState(update(this.state,{
        stories:{
          [storySpot]:{
            $set: updatedStory,
          }
        }
      }));
    }catch(e){
      console.log("Could not refresh story");
      console.log(e);
    }
  }

  async getFilteredStories(newState = {}){
    const {writingPrompt} = this.props;
    const {
        page,
        pageSize,
        author,
        name,
        orderProp,
        orderDirection,
    } = {
        ...this.state,
        ...newState,
    };
    this.setState({
      loading:true,
    });
    const searchUrl = `?page=${page}&page_size=${pageSize}&${author ? "user_like="+author : ''}&${name ? "name="+name : ''}&${orderProp}=${orderDirection}`;
    const storiesUrl = `${writingPrompt.published_stories_url}${searchUrl}`;
    const storiesCountUrl = `${writingPrompt.published_stories_count_url}${searchUrl}`;
    try{
      const resp = await fetchWithToken(storiesCountUrl);
      const storiesCount = (await resp.json()).stories_count;
      this.setState({
        storiesCount,
      })
    } catch(e){
      console.log('Could not get stories count');
      console.log(e);
    }
    fetchWithToken(storiesUrl)
      .then(resp=>{
        if(resp.ok){
          resp.json().then(newStories=>{
            const stories = page === 1 ? newStories : [...this.state.stories,...newStories];
            this.setState({
              stories,
              page,
              loading:false,
            });
          });
        }
      })
      .catch(err=>{
        console.log("Could not get stories");
        console.log(err);
      });
  }

  // A little bit of a copy paste from WritingPromptsList
  sort(newOrderProp){
    const {orderDirection,orderProp} = this.state;
    const isDesc = orderProp === newOrderProp && orderDirection === 'desc';
    this.setState({
        orderProp: newOrderProp,
        orderDirection: isDesc ? 'asc' : 'desc',
        stories:[],
    })
    this.debouncedGetFilteredStories({
        page:1,
    });
  }

  componentDidMount(){
    this.debouncedGetFilteredStories();
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }

  render () {
    const {
      writingPrompt,
      hidePrompt,
    } = this.props;
    const {
      numColumns,
      stories,
      storiesCount,
      page,
      name,
      author,
      orderProp,
      orderDirection,
      loading,
    } = this.state;
    const galleryPadding = getGalleryPadding(numColumns);

    return <div className="writingPromptStoriesDiv">
      {
      hidePrompt ? null : <p>
        {writingPrompt.text}
      </p>
      }
      <div style={{
        width:"98%",
        marginLeft:"1%",
      }}>
      <div className = "searchContainerDiv">
      <div className = "searchDiv" style={{
        display:"flex",
        paddingLeft:`${galleryPadding}%`,
        paddingRight:`${galleryPadding}%`,
      }}> 
        <div>
         <TextField 
            label="Name" 
            onChange={e=>{
                this.setState({
                    name:e.target.value,
                });
               this.debouncedGetFilteredStories({page:1}); 
            }}
            value={name}
        />
         <TextField label="Author"
            onChange={e=>{
                this.setState({
                    author:e.target.value,
                });
                this.debouncedGetFilteredStories({page:1}); 
            }}
            value={author}
        />
        </div>
        <div style = {{
          display:"flex",
          marginLeft:"auto",
        }}>
         <TableSortLabel
            style={{
              marginTop:"auto",
            }}
            active={orderProp==='sort_by_rating'}
            direction={orderDirection}
            onClick={()=>this.sort("sort_by_rating")}>
            Rating
         </TableSortLabel>
         <TableSortLabel
            style={{
              marginTop:"auto",
            }}
            active={orderProp==='sort_by_date'}
            direction={orderDirection}
            onClick={()=>this.sort("sort_by_date")}>
            Date
         </TableSortLabel>
         <TableSortLabel
            style={{
              marginTop:"auto",
            }}
            active={orderProp==='sort_by_views'}
            direction={orderDirection}
            onClick={()=>this.sort("sort_by_views")}>
            Views
         </TableSortLabel>
         </div>
      </div>
      </div>
      </div>
      {
          this.props.children({
              loading,
              stories,
              storiesCount,
              next:()=>this.debouncedGetFilteredStories({page:page+1}),
              refreshStory: this.refreshStory,
          })
      }
    </div>
  }
}

export default WritingPrompt;