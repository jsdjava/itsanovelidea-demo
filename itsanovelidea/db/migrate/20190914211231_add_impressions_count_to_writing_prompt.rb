class AddImpressionsCountToWritingPrompt < ActiveRecord::Migration[5.1]
  def change
    add_column :writing_prompts, :impressions_count, :int
  end
end
