class AddAuthorIdToPrompts < ActiveRecord::Migration[5.1]
  def change
    add_column :writing_prompts, :reddit_author_id, :string
  end
end
