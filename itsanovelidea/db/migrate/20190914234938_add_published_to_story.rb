class AddPublishedToStory < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :published_text, :text
  end
end
