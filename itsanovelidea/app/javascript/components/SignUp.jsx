import React from "react";
import {TextValidator} from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LockIcon from '@material-ui/icons/Lock';

import Account from "./components/Account.jsx";

class SignUp extends React.Component{
    render(){

        //TODO Need a max password length
        const {passwordLength,signUpUrl} = this.props;
        return <Account url = {signUpUrl} 
            rules={{
                isMatchingPassword:(value,{state})=>{
                    return value === state.password;
                }
            }}
            title="Sign up"
            btnDataQaTag="signUpBtn"
            btnTitle="Register"
            commit="Sign Up"
            formKeys={["password","password_confirmation","email"]}
            render={ (setFormValue,{email,password,password_confirmation})=>
                <div>
                <div className="accountTextField">
                <TextValidator
                    name="email"
                    label="Your email"
                    value={email} 
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <MailOutlineIcon/>
                        </InputAdornment>
                      ),
                    }}
                    type="email"
                    onChange={(e)=>setFormValue("email",e)}
                    data-qa="emailInput"
                    validators={['required', 'isEmail']}
                    errorMessages={['*required', 'email is not valid']}
                />
                </div>
                <div className="accountTextField">
                <TextValidator
                    name="password"
                    label="Your password"
                    value={password}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <LockIcon/>
                        </InputAdornment>
                      ),
                    }}
                    type="password"
                    onChange={(e)=>setFormValue("password",e)}
                    data-qa="passwordInput"
                    validators={['required',`minStringLength:${passwordLength}`]}
                    errorMessages={['*required', `password must be at least ${passwordLength} characters`]}
                />
                </div>
                <div className="accountTextField">
                <TextValidator
                    value={password_confirmation}
                    name="password_confirmation"
                    label="Confirm your password"
                    InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <LockIcon/>
                          </InputAdornment>
                        ),
                    }}
                    type="password"
                    onChange={(e)=>setFormValue("password_confirmation",e)}
                    data-qa="confirmedPasswordInput"
                    validators={['required','isMatchingPassword']}
                    errorMessages={['*required','passwords must match']}
                />
                </div>
                </div>
        }/>
        
    };
}

export default SignUp;
