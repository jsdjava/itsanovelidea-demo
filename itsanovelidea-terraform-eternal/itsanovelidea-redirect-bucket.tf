resource "aws_s3_bucket" "itsanovelidea-redirect" {
  bucket = "www.itsanovelidea.com"
  acl    = "public-read"
  website {
    redirect_all_requests_to = "http://main.backend.itsanovelidea.com"
  }
}