import React from 'react';
import UsersList from "./UsersList.jsx";

const users = require('./fixtures/users.json');

export default {
  component: UsersList,
  title: 'UsersList',
};

export const usersList = () => <UsersList users={users}/>;
