require 'rails_helper'
require_relative '../concerns/filterable_spec.rb'

RSpec.describe Story, :type => :model do
  it_behaves_like "filterable", :story

  owner = FactoryBot.create(:user)
  writing_prompt = FactoryBot.create(:writing_prompt)

  it "is not valid without an owner" do
    expect(Story.new(writing_prompt:writing_prompt)).to_not be_valid
  end
  it "is not valid without a writing prompt" do
    expect(Story.new(owner:owner)).to_not be_valid
  end
  it "returns Not rated when no one has rated the story" do
    expect(Story.new.rating).to eql("Not rated")
  end
  it "returns the average rating of the story rounded to 2 decimal places" do
    story = create(:story)
    create_list(:user_story,2,story:story,rating:3)
    expect(story.rating).to eql(3)
  end

  it "creates a document in the document ms" do
    story = create(:story)
    RestClient = spy
    story.create_document()
    expect(RestClient).to have_received(:post).with("testDocumentServer/documents/#{story.id.to_s}","{}", 
    {:apikey=>"testApiKey", :content_type=>:json, :userid=>story.owner.id})
  end
  
  it "creates a chat in the chat ms" do
    story = create(:story)
    RestClient = spy
    story.create_chat()
    expect(RestClient).to have_received(:post).with("testChatServer/chat", "{\
\"resourceId\":\"#{story.id}\",\
\"resourceType\":\"story\"\
}", {:apikey=>"testApiKey", :content_type=>:json, :userid=>story.owner.id})
  end

  it "downloads a photo in the photo ms" do
    story = create(:story)
    RestClient = spy
    story.download_photo("love me again",9)
    expect(RestClient).to have_received(:post).with("testPhotoServer/photo/love%20me%20again?index=9","{}",{
      apikey:"testApiKey"
    })
  end

  it "gets the url for a downloaded photo from the photo ms" do
    story = create(:story)
    RestClient = spy
    story.get_photo_url
    expect(RestClient).to have_received(:get).with("testPhotoServer/photo/#{story.photo_id}",{
      apikey:"testApiKey"
    })
  end
  
  it "maps the photo metadata response from the download_photo endpoint to the story's photo data" do
    story = create(:story)
    story.update_photo_metadata({
      idFile:"test.png",
      width:"600px",
      height:"500px",
      backgroundColor:"FFFFFFFF",
      textColor:"AAAAAAAA",
    }.stringify_keys)
    expect(story).to have_attributes({
      photo_id:"test.png",
      photo_width:"600px",
      photo_height:"500px",
      photo_background_color:"FFFFFFFF",
      photo_text_color:"AAAAAAAA",
    })
  end

  describe "filtering by name" do
    it "does nothing" do
      expected_stories = create_list(:story,3)
      actual_stories = Story.filter_by_name({}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
    it "filters down to stories with names containing the name" do
      create_list(:story,4,name:"asdfasdf")
      expected_stories = create_list(:story,2,name:"testing")
      expected_stories.push create(:story,name:"atest")
      actual_stories = Story.filter_by_name({name:"test"}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
  end

  describe "filtering by published" do
    it "does nothing" do
      expected_stories = create_list(:story,3,published_text:"xd")
      expected_stories = expected_stories + create_list(:story,2)
      actual_stories = Story.filter_by_published({}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
    it "filters down to published stories" do
      create_list(:story,2)
      expected_stories = create_list(:story,3,published_text:"but i like and my mind")
      actual_stories = Story.filter_by_published({published:"true"}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
    it "filters down to unpublished stories" do
      expected_stories = create_list(:story,2)
      create_list(:story,3,published_text:"but i like and my mind")
      actual_stories = Story.filter_by_published({published:"false"}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
  end

  describe "filtering by writing prompt" do
    it "does nothing" do
      expected_stories = create_list(:story,3)
      actual_stories = Story.filter_by_published({}).to_a
      expect(actual_stories).to eql(expected_stories)
    end
    it "filters down to the writing prompt" do
      writing_prompt = create(:writing_prompt)
      create(:story)
      expected_stories = create_list(:story,3,writing_prompt:writing_prompt)
      create(:story)
      actual_stories = Story.filter_by_writing_prompt({
        writing_prompt_id:writing_prompt.id
      }).to_a
      expect(actual_stories).to eql(expected_stories)
    end
  end

  describe "sort by rating" do
    it "does nothing" do
      expected_good_story,expected_bad_story,expected_average_story = create_list(:story,3)
      create_list(:user_story,2,story:expected_good_story,rating:5)
      create_list(:user_story,2,story:expected_bad_story,rating:1)
      create_list(:user_story,2,story:expected_average_story,rating:3)
      actual_stories = Story.sort_by_rating({}).to_a
      expect(actual_stories).to eql([expected_good_story,expected_bad_story,expected_average_story])    
    end
    it "sorts ratings by ascending" do 
      expected_average_story,expected_unrated_story,expected_good_story = create_list(:story,3)
      create_list(:user_story,2,story:expected_average_story,rating:3)
      create_list(:user_story,3,story:expected_good_story,rating:5)
      actual_stories = Story.sort_by_rating({
        sort_by_rating: 'asc'
      }).to_a
      expect(actual_stories).to eql([expected_unrated_story,expected_average_story,expected_good_story])
    end
  end
end