require 'rails_helper'

RSpec.describe "writing_prompts/new", type: :view do
  before(:each) do
    assign(:writing_prompt, WritingPrompt.new())
  end

  it "renders new writing_prompt form" do
    render

    assert_select "form[action=?][method=?]", writing_prompts_path, "post" do
    end
  end
end
