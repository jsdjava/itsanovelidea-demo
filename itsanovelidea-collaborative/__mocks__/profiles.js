/*
 Because I need to inject shareDb into profiles, its coded as a function
 that returns getProfile and createProfile.
*/

const getProfile = jest.fn();
const createProfile = jest.fn();
module.exports = () => ({
  getProfile,
  createProfile,
});
