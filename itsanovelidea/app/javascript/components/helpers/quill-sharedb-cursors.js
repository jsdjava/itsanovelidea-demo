import {throttle} from 'throttle-debounce'

export default function(quill,doc,update=()=>{}) {
  update = throttle(500,update); 
  doc.subscribe(err=> {
    if (err) {
      throw err;
    }
    quill.setContents(doc.data);
    quill.on('text-change', (delta, oldDelta, source)=> {
      if (source == 'user') {
        doc.submitOp(delta, {source: quill}, err=> {
          if (err)
            console.error('Submit OP returned an error:', err);
        });
      }
    });

    doc.on('op', (op, source)=> {
      update();
      if (source !== quill) {
        quill.updateContents(op);
      }
    });
  });

  quill.enable();
  return {
    revert:previousSnapshot=>{
      console.log(previousSnapshot);
      return doc.submitSnapshot(previousSnapshot.data)
    },
  }
};
