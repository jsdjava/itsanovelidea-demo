import React from "react";
import "../stylesheets/StoriesGallery.css";

const anime = require('animejs/lib/anime.js');

const DefaultDiv = props=> {
  return <div {...props}>{props.children}</div>
};

class Story extends React.Component{
    constructor(){
      super();
      this.state = {};
    } 

    render() {
      const {
        containerClassName="storyDiv",
        StoryContainer=DefaultDiv,
        StoryContent=DefaultDiv,
        StoryImage=DefaultDiv,
        StoryText=DefaultDiv,
        divProps,
        photo:{
          publishedStorySpot,
          openStory=()=>{},
          width,
          height,
          backgroundColor,
          isSelected,
          isHovered,
          hoverStory,
          leaveStory,
      }} = this.props;
      const hoverStyle = false && isHovered ? {
        transition: "all .1s ease-in-out", 
        transform: "scale(1.1)",
        zIndex:100,
        position: "relative",
        textAlign:"center",
        transformOrigin:"none",
      } : {
        zIndex:"initial",
        textAlign:"center",
      };
      return (<div
              className={`${containerClassName} storyTile-${publishedStorySpot}`}
              onClick={()=>(!window.frozen) && openStory()}
              role="button"
              style={{
                ...hoverStyle,
              }}>
              <StoryContainer>
                <div className="storyContentDiv" style={{
                 width,
                 height,
                 ...(divProps || {}),
                }}>
                <StoryContent>
                  < StoryText/>
                </StoryContent>
                </div>
              </StoryContainer>
            </div>
      );
    }
}

export default Story;