## Setup
`bundle install`
`yarn install`
## Tests
`rm db/test.sqlite3`
`rake db:test:prepare`
`rspec spec/controllers/categories_controller_spec.rb`
## Running in production
`mongod (in one tab)`
`in another tab, mongo`
`use itsanovelidea`
`db.dropDatabase()`
`set RAILS_ENV=production& set DISABLE_DATABASE_ENVIRONMENT_CHECK=1& rake db:drop& rake db:create& rake db:migrate`
`rake assets:precompile`
`rails server -e production`
After signing up as a user, seed the database with itsanovelidea-importer:
Fill out the secrets.js file using chrome devtools as the logged in user to get the secrets and then run
`node src/import.js`