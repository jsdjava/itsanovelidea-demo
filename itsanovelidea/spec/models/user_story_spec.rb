require 'rails_helper'

#TODO, Yeah, its just a copy paste of the writing prompt ratings tests...
RSpec.describe UserStory, type: :model do
  user = FactoryBot.create(:user)
  story = FactoryBot.create(:story)

  it "is valid with valid attributes" do
    expect(UserStory.new(user:user,story:story,rating:4)).to be_valid
  end
  it "is not valid without a user" do
    expect(UserStory.new(story:story,rating:2)).to_not be_valid
  end
  it "is not valid without a story" do
    expect(UserStory.new(user:user,rating:4)).to_not be_valid
  end
  it "is not valid without rating" do
    expect(UserStory.new(user:user,story:story)).to_not be_valid
  end
  it "is not valid with too small rating" do
    expect(UserStory.new(user:user,story:story,rating:0)).to_not be_valid
  end
  it "is not valid with too large rating" do
    expect(UserStory.new(user:user,story:story,rating:7)).to_not be_valid
  end
end
