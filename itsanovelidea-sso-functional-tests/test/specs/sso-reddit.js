const signInPage = require("../pages/signIn.js");
const redditPage = require("../pages/reddit.js");
const dashboardPage = require("../pages/dashboard.js");

describe('ssoing through reddit', () => {
    it('signs in through reddit', () => {
        const {email,password} = browser.config.users.reddit;
        browser.url(browser.config.baseUrl);
        signInPage.redditLoginBtn.click();
        redditPage.emailInput.setValue(email.split("@")[0]);
        redditPage.passwordInput.setValue(password);
        redditPage.loginBtn.click();
        redditPage.allowBtn.click();
        expect(dashboardPage.emailHeader.getText()).toEqual(email);
    });
  })