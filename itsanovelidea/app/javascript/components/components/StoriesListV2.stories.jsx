import React from 'react';
import StoriesGallery from "./StoriesGallery.jsx";
import StoryCard from "./StoryCard.jsx";
import DoubledStoriesList from "./DoubledStoriesList.jsx";

const publishedStories = require('./fixtures/publishedStories.json');

export default {
  component: StoriesGallery,
  title: 'StoriesListV2',
};

export const storiesList = () => <StoriesGallery 
    StoryComponent = {StoryCard}
    GalleryComponent = {DoubledStoriesList}
    publishedStories = {publishedStories}
    next = {()=>[]}
/>
