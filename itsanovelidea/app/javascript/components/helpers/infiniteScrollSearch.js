function Node(value,prev,next){
    this.prev = prev;
    this.next = next;
    this.value = value;
}

const arrayToNodes = (valuesList)=>{
    if(!valuesList.length){
        return [];
    }
    let lastNode;
    const firstNode = valuesList.reverse().reduce((nextNode,curValue)=>{
        const curNode = new Node(curValue,null,nextNode);
        if(nextNode){
            nextNode.prev = curNode;
        }else{
            lastNode = curNode;
        }
        return curNode;
    },null);
    return [firstNode,lastNode];
}

class InfiniteScrollSearch{

    constructor(pageSize,scrollSource,refresh=()=>{}){
        this.index = 0;
        this.pageSize = pageSize;
        this.scrollSource = scrollSource;
        this.firstMidNode = null;
        this.lastMidNode = null;
        this.refresh = refresh;
    }

    async load(){
        this.loading = true;
        try{
            const initialPages = await this.scrollSource(1,this.pageSize*2);
            [this.curNode,this.lastNode] = arrayToNodes(initialPages);
            this.firstNode = this.curNode;
            this.endCount = this.pageSize-(initialPages.length%this.pageSize);
        } catch(e){
            console.log("Could not load initial page");
            console.log(e);
        }
        this.refresh(this,this.current(),0);
        this.loading = false;
    }

    async next(){
        if(this.curNode && this.curNode.next && !this.loading){
            this.loading = true;
            try{
                if(this.endCount){
                    this.curNode = this.curNode.next;
                    this.index++;
                    this.endCount--;
                }
                if(this.index%this.pageSize===0){
                    const nextPageList = await this.scrollSource(Math.floor(this.index/this.pageSize)+2,this.pageSize);
                    const [nextPageFirstNode,nextPageLastNode] = arrayToNodes(nextPageList);
                    this.endCount = nextPageList.length;
                    if(this.firstMidNode){
                        //Trash the previous page
                        this.firstNode = this.firstMidNode;
                        if(this.firstNode.prev)this.firstNode.prev.next = null;
                        this.firstNode.prev = null;
                    }
                    if(nextPageFirstNode){
                        this.lastMidNode = this.lastNode;
                    } else{
                        this.refresh(this,this.current(),+1);
                        this.loading = false;
                        return;
                    }
                    this.lastNode.next = nextPageFirstNode;
                    nextPageFirstNode.prev = this.lastNode;
                    this.lastNode = nextPageLastNode;
                    this.firstMidNode = this.curNode;
                }
            }catch(e){
                console.log("Could not move forward");
                console.log(e);
                //Reverse w/e we did
                if(this.endCount){
                    this.curNode = this.curNode.prev;
                    this.index--;
                    this.endCount++;
                }
            }
            this.refresh(this,this.current(),+1);
            this.loading = false;
        }
    }

    async previous(){
        if(this.curNode && this.curNode.prev && !this.loading){
            this.loading = true;
            const prevEndCount = this.endCount;
            const prevLastNode = this.lastNode;
            const prevLastMidNodeNext = this.lastMidNode ? this.lastMidNode.next : null;
            const prevFirstMidNode = this.firstMidNode;
            try{
                this.curNode = this.curNode.prev;
                this.endCount++;
                if(this.index%this.pageSize===0 ){
                    if(this.lastMidNode){
                        this.lastNode = this.lastMidNode;
                        if(this.lastNode.next) {
                            this.lastNode.next.prev = null;
                        }
                        this.lastNode.next = null;
                        this.firstMidNode = this.firstNode;
                    }
                    if(this.index/this.pageSize-1<=0) {
                        this.index--;
                        this.refresh(this,this.current(),-1);
                        this.loading = false;
                        return;
                    }
                    this.endCount = 1;
                    const prevPageList = await this.scrollSource(this.index/this.pageSize-1,this.pageSize);
                    const [prevPageFirstNode,prevPageLastNode] = arrayToNodes(prevPageList);
                    this.firstNode.prev = prevPageLastNode;
                    prevPageLastNode.next = this.firstNode;
                    this.firstNode = prevPageFirstNode;
                    if(this.index>this.pageSize){
                        this.lastMidNode = this.curNode;
                    }
                }
                this.index--;
            } catch(e){
                console.log("Could not move back");
                console.log(e);
                this.endCount = prevEndCount;
                if(this.lastMidNode){
                    this.lastNode = prevLastNode;
                    if(this.lastMidNode.next) {
                        this.lastMidNode.next.prev = this.lastMidNode;
                    }
                    this.lastMidNode.next = prevLastMidNodeNext;
                    this.firstMidNode = prevFirstMidNode;
                }

            }
            this.refresh(this,this.current(),-1);
            this.loading = false;
        }
    }

    spot(){
        return this.index;
    }

    page(node){
        let curValues = [];
        while(node && curValues.length<this.pageSize){
            curValues.push(node.value);
            node = node.next;
        }
        return curValues;
    }

    current(){
        return this.page(this.curNode);
    }

    first(){
        return this.curNode;
    }

    all(){
        let values = [];
        let startNode = this.firstNode;
        while(startNode){
            values.push(startNode.value);
            startNode = startNode.next;
        }
        return values;
    }
}

//module.exports = InfiniteScrollSearch;
 export default InfiniteScrollSearch;