const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

// Configures swagger for a project.
// - projectTitle is what you want the big header at the top of the swagger to say
// - cwd should always be __dirname
// - apis is a list of RELATIVE paths to express routers with @swagger comments
//   example: ['./api.js']
// Should be used like:
/*
  const swaggerMiddlewares = swagger("itsanovelidea",__dirname,['./api.js]);
  app.use('/swagger',swaggerMiddlewares);
*/
module.exports = (projectTitle, cwd, apis) => {
  const swaggerOptions = {
    definition: {
      info: {
        title: projectTitle,
        version: '1.0.0',
      },
    },

    // Because I'm requiring the module that does this, I will need to concatenate the actual
    // directory that the requiring project is in
    apis: apis.map((api) => `${cwd}/${api}`),
  };

  const swaggerDocument = swaggerJSDoc(swaggerOptions);

  // This list of mwares can be passed directly into app.use("/swagger",<list of mwares>);
  return [swaggerUI.serve, swaggerUI.setup(swaggerDocument, {

    // By default, swaggerUI was putting this huge, annoying block at the top with an authorize
    // button that did nothing. This will hide it.
    customCss: '.swagger-ui .scheme-container { display: none}',
  })];
};
