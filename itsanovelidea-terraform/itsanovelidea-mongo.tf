module "itsanovelidea-service-mongo" {
  source = "./itsanovelidea-service"
  itsanovelidea_service_name = "itsanovelidea-service-mongo"
  itsanovelidea_cluster_id=aws_ecs_cluster.itsanovelidea.id
  itsanovelidea_task_definition_arn=aws_ecs_task_definition.itsanovelidea-mongo-task.arn
  itsanovelidea_vpc_subnets=module.vpc.public_subnets
  itsanovelidea_subdomain_prefix = "mongo"
}

resource "aws_ecs_task_definition" "itsanovelidea-mongo-task" {
  family                = "itsanovelidea-mongo"
  requires_compatibilities = ["EC2"]
  container_definitions = "${file("ecs/itsanovelidea-mongo.json")}"
  # So this if fun. Terraform seems bugged, and won't just read the fun
  # arn from the dynamically created task role...
  #execution_role_arn="${module.itsanovelidea-service-mongo.task_role_id}"
  execution_role_arn="arn:aws:iam::242929592693:role/itsanovelidea-service-mongo-task"
  volume {
    name      = "efs-mongo"
    efs_volume_configuration {
        file_system_id = "fs-d548e055"
        root_directory= "/"
    }
  }
  tags =  {
    project = "itsanovelidea"
  }
  placement_constraints {
    type       = "memberOf"
    expression = "attribute:itsanovelidea-type==mongo"
  }
}
