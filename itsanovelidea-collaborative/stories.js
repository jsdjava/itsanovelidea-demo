/* eslint-disable no-param-reassign */

const richTextType = require('rich-text').type;

const utils = require('./utils.js');

const storiesCollection = 'stories';
const versionMilestoneSize = 20;

// stories should have shareDb injected, i.e.
//   const stories = require('./stories.js)(shareDb);
// Alternatively, I could probably take advantage of module caching
// and just load shareDb.js in here, but that feels less clean
module.exports = (shareDb) => {
  // Documents are only accessible to the owner
  shareDb.access.allowRead(storiesCollection,
    async (docId, doc, authorizedDocumentId) => docId === authorizedDocumentId);
  // docId - id of your doc for access-control
  // oldDoc  - document object (before update)
  // newDoc  - document object (after update)
  // ops    - array of OT operations
  // authorizedDocumentId - session determined id of the story you are allowed to update.
  //   You are guaranteed to own this in the rails server
  // eslint-disable-next-line
  shareDb.access.allowUpdate(storiesCollection, async (docId, oldDoc, newDoc, ops, authorizedDocumentId) => docId === authorizedDocumentId);

  const getStory = async (documentId, version) => {
    const snapshot = await shareDb.fetchSnapshot(storiesCollection, documentId, version);
    return snapshot;
  };

  const createStory = async (storyId) => shareDb.createDocument(storiesCollection, storyId);

  const getStoryHistory = async (storyId, { version, numberResults, ascending }) => {
    // Because I'm actually returning a list of diffs,not snapshots, I will need to
    // pull one more snapshot than numberResults.
    // For example, if I ask for 3 diffs, I need 4 snapshots because I need to take
    // - A B C D
    // and return
    // [A diff B, B diff C, C diff D]
    numberResults++;
    const latestSnapshot = await shareDb.fetchSnapshot(storiesCollection, storyId);
    const latestVersion = latestSnapshot.v;
    // If you don't specify the version, the assumption is that you want to start
    // at the latest
    if (!version) {
      version = latestVersion;
    }
    // Version milestones are equally spaced, starting at 20 - 40 - 60 - 80 - 100
    // Assuming I pass in a version of 83, for example, I don't actually want that version
    // Instead, I want the nearest milestone version of 80.
    // If we are descending, though, and we are beginning at the latest version, then
    // we shouldn't truncate it down.
    let milestoneVersion = version;
    // I'm not 100% sure here on version and latestVersion being the same type, so I'm
    // going to leave this != for now
    // eslint-disable-next-line
    if (ascending || version != latestVersion) {
      milestoneVersion = version - (version % versionMilestoneSize);
    }
    let snapshotPromises = [];
    if (ascending) {
      // eslint-disable-next-line
      for (let i = milestoneVersion; i < latestVersion && snapshotPromises.length < numberResults; i += versionMilestoneSize) {
        snapshotPromises.push(shareDb.fetchSnapshot(storiesCollection, storyId, i || 1));
      }

      // Edge case where you reach the last version before getting as many snapshots as
      // numberResults.
      // For example, If I have a milestone size of 10 and 35 total snapshots, and I ask
      // for 5 snapshots starting at version 1 and ascending, I'm going to load snapshots:
      // - 1
      // - 11
      // - 21
      // - 31
      // in the for loop and then quit out, but really I should also be grabbing snapshot
      // 35 too. Because its < 41, though, it won't get grabbed in the loop.
      if (snapshotPromises.length < numberResults) {
        snapshotPromises.push(latestSnapshot);
      }
      // Even though we're ascending, we always want the returned list of history
      // snapshots to be in descending order
      snapshotPromises = snapshotPromises.reverse();
    } else {
      // So, if you are descending and you start at the latest, you aren't going to
      // be at a milestone number, like 100. Instead, you'll be at something like 107.
      // This makes sure to cap it down by only 7 the first time
      // eslint-disable-next-line
      for (let i = milestoneVersion; i > 1 && snapshotPromises.length < numberResults; i -= i % versionMilestoneSize || versionMilestoneSize) {
        snapshotPromises.push(shareDb.fetchSnapshot(storiesCollection, storyId, i));
      }
      // Edge case, see above comment
      if (snapshotPromises.length < numberResults) {
        snapshotPromises.push(shareDb.fetchSnapshot(storiesCollection, storyId, 1));
      }
    }
    // Call for each milestone snapshot in parallel
    // TODO THIS NEEDS TO BE SORTED LMAO
    const snapshots = await Promise.all(snapshotPromises);
    const snapshotHistory = [];
    for (let i = 0; i < snapshots.length - 1; i++) {
      // So, snapshots are in descending order, meaning they look like:
      //   [1000,800,600,400,200,0]
      // This means that ascending indices = descending versions
      const curSnapshot = snapshots[i];
      const prevSnapshot = snapshots[i + 1];
      // So, eslint is right, we could parallelize here. For now, I'm not going to do that
      // eslint-disable-next-line
      const metadata = await shareDb.fetchSnapshotMetadata(storiesCollection, storyId, prevSnapshot.v);
      const diff = utils.diffToList(
        richTextType.diff(prevSnapshot.data, curSnapshot.data),
        utils.snapshotToText(prevSnapshot),
      );
      snapshotHistory.push({
        // timestamp of the form 1579155076777
        timestamp: metadata.m.ts,
        newSnapshot: curSnapshot,
        oldSnapshot: prevSnapshot,
        diff,
      });
    }
    return snapshotHistory;
  };

  return {
    getStory,
    createStory,
    getStoryHistory,
  };
};
