class AddStoriesOwnerImpressionsPublishedIndex < ActiveRecord::Migration[5.1]
  def change
    add_index :stories, [:owner_id,:impressions_count,:published_at]
  end
end
