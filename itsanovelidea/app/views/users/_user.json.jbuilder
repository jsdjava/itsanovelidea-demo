json.user_url user_path(user.id)
json.extract! user, :id, :email, :created_at, :updated_at
json.stories_rating user.stories_rating
json.total_stories_views user.total_stories_views
json.total_stories user.total_stories
json.total_writing_prompts user.total_writing_prompts
json.prompts_rating user.writing_prompts_rating
json.total_writing_prompts_views user.total_writing_prompts_views
json.total_writing_prompts_responses user.total_writing_prompts_responses

if !user.thin?
json.profile user.get_profile
["most_viewed_prompt","most_popular_prompt","most_recent_prompt","most_replied_prompt"].select do |prompt|
	user.send(prompt).present?
end.each do |prompt|
	json.set! prompt do
		json.partial! 'writing_prompts/writing_prompt', writing_prompt: user.send(prompt)
	end
end
["most_viewed_story","most_popular_story","most_recent_story"].select do |story|
	user.send(story).present?
end.each do |story|
	json.set! story do
		story_obj = user.send(story)
		# Need to inflate so it pulls the actual story text from the node server
		story_obj.inflate
		json.partial! 'stories/story', story: story_obj
	end
end
end