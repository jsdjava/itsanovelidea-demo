module.exports = {
  redis: {
    get: jest.fn(),
    set: jest.fn(),
  },
  logger: {
    log: jest.fn(),
  },
};
