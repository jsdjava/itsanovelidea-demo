class AddOwnerToStory < ActiveRecord::Migration[5.1]
  def change
  	add_reference :stories, :owner, index: true
	add_foreign_key :stories, :users, column: :owner_id
  end
end
