const itsanovelideaMiddleware = require('itsanovelidea-middleware');

const webSocketRoutesFn = require('./webSocketRoutes.js');

test('/ws/profiles should pass the cookie from the request to getUser', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  const { profilesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await profilesRoute({
    on: () => {},
  }, {
    query: {
      cookie: 'cookie',
    },
  });
  expect(itsanovelideaMiddleware.getUser).toHaveBeenCalledWith('cookie');
});

test('/ws/profiles should store the user id from getUser() on the share db connection', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({
    user_id: 24,
  });
  const shareDbSession = {};
  const { profilesRoute } = webSocketRoutesFn({
    listen: () => shareDbSession,
  });
  await profilesRoute({
    on: () => {},
  }, {
    query: {},
  });
  expect(shareDbSession.connectSession).toEqual(24);
});

test('/ws/profiles should set the websocket to alive initially and on a pong event', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  const wsMock = {
    on: jest.fn(),
  };
  const { profilesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await profilesRoute(wsMock, {
    query: {},
  });
  expect(wsMock.isAlive).toBeTruthy();
  wsMock.isAlive = false;
  // Make sure that the pong event sets isAlive back to true again
  // Apparently WebSocketJSONStream is registering message and close
  // as events, so I'm skipping those 2 to check my pong event
  wsMock.on.mock.calls[2][1]();
  expect(wsMock.isAlive).toBeTruthy();
});

test('/ws/stories should pass the cookie from the request to getUser', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  const { storiesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await storiesRoute({
    on: () => {},
    terminate: () => {},
  }, {
    query: {
      cookie: 'cookie',
    },
  });
  expect(itsanovelideaMiddleware.getUser).toHaveBeenCalledWith('cookie');
});

test('/ws/stories should pass the user id and storyid to hasStoryAccess', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({
    user_id: 5,
  });
  const { storiesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await storiesRoute({
    on: () => {},
    terminate: () => {},
  }, {
    query: {
      storyId: 10,
    },
  });
  expect(itsanovelideaMiddleware.hasStoryAccess).toHaveBeenCalledWith(5, 10);
});

test('/ws/stories should terminate the websocket when the user does not have access to the story', async () => {
  const terminateMockFn = jest.fn();
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  const { storiesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await storiesRoute({
    on: () => {},
    terminate: terminateMockFn,
  }, {
    query: {},
  });
  expect(terminateMockFn).toHaveBeenCalled();
});

test('/ws/stories should store the story id from getUser() on the share db connection', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  itsanovelideaMiddleware.hasStoryAccess.mockReturnValueOnce(true);
  const shareDbSession = {};
  const { storiesRoute } = webSocketRoutesFn({
    listen: () => shareDbSession,
  });
  await storiesRoute({
    on: () => {},
    terminate: () => {},
  }, {
    query: {
      storyId: 24.0,
    },
  });
  expect(shareDbSession.connectSession).toEqual(24);
});

test('/ws/stories should set the websocket to alive initially and on a pong event', async () => {
  itsanovelideaMiddleware.getUser.mockReturnValueOnce({});
  itsanovelideaMiddleware.hasStoryAccess.mockReturnValueOnce(true);
  const wsMock = {
    on: jest.fn(),
    terminate: () => {},
  };
  const { storiesRoute } = webSocketRoutesFn({
    listen: () => ({}),
  });
  await storiesRoute(wsMock, {
    query: {},
  });
  expect(wsMock.isAlive).toBeTruthy();
  wsMock.isAlive = false;
  // Make sure that the pong event sets isAlive back to true again
  // Apparently WebSocketJSONStream is registering message and close
  // as events, so I'm skipping those 2 to check my pong event
  wsMock.on.mock.calls[2][1]();
  expect(wsMock.isAlive).toBeTruthy();
});

test('cleanupSocket should return when isAlive = undefined', () => {
  const { cleanupSocket } = webSocketRoutesFn();
  expect(cleanupSocket({})).toBeFalsy();
});

test('cleanupSocket should terminate the websocket when isAlive = false', () => {
  const terminateMockFn = jest.fn();
  const { cleanupSocket } = webSocketRoutesFn();
  cleanupSocket({
    isAlive: false,
    terminate: terminateMockFn,
  });
  expect(terminateMockFn).toHaveBeenCalled();
});

test('cleanupSocket should set isAlive to false and call ping() when isAlive = true', () => {
  const { cleanupSocket } = webSocketRoutesFn();
  const wsMock = {
    isAlive: true,
    ping: jest.fn(),
  };
  cleanupSocket(wsMock);
  expect(wsMock.isAlive).toBeFalsy();
  expect(wsMock.ping).toHaveBeenCalled();
});
