import React from "react"
import PropTypes from "prop-types"
import { TextField,Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';

import PhotoCarouselInfinite from "./components/PhotoCarouselInfinite.jsx";
import {fetchWithToken} from "./helpers/api.js";
import {success,error} from "./helpers/notifications.js";
import "./stylesheets/CreateStory.css";

class CreateStory extends React.Component { 

  constructor(){ 
    super();
    this.state = {
      baseIndex:0, 
      newBaseIndex:0,
      nameErrorText:"Story name is too short",
    };
    this.createStory = this.createStory.bind(this);
    this.setPhotoIndex = this.setPhotoIndex.bind(this);
  }

  async createStory(){
    const {url,writingPromptId} = this.props;
    const {name,relativeSelectedIndex,selectedHash} = this.state;
    this.setState({disabled:true});
    try{
      const resp = await fetchWithToken(url,'POST',JSON.stringify({story:{
        name,
        index:relativeSelectedIndex,
        search_hash:selectedHash,
        writing_prompt_id:writingPromptId,
      }}));
      if(!resp.ok){
        throw resp;
      }
      const storyId = (await resp.json()).id; 
      success("Story created!");
      window.location.href = `${this.props.url}/${storyId}/edit`;
    }catch(e){
      console.log("Could not create story");
      console.log(e);
      error("Could not create story");
    }
    this.setState({disabled:false});
  }
  
  setPhotoIndex(selectedIndex){
    const {
      searchHash,
      baseIndex,
    } = this.state;
    this.setState({
      selectedIndex,
      selectedHash:searchHash,
      relativeSelectedIndex:selectedIndex-baseIndex,
    });
  }

  render () {

    const {
      photosUrl,
      writingPromptText
    } = this.props;
    const {
      searchKey,
      name,
      nameErrorText,
      disabled,
      selectedIndex,
      newBaseIndex,
      baseIndex,
    } = this.state;
    const isValidPhoto = selectedIndex >=0;
    return (
      <div>
        <h2> Create a Story </h2>
        <p> {writingPromptText} </p>
        <Grid container>
        <Grid item sg={12} md={6}>
        <TextField
            label="Story Title" 
            required
            value={name}
            error={nameErrorText}
            helperText={nameErrorText}
            onChange={e=>{
              const name = e.target.value.trim();
              let nameErrorText;
              if(name.length<5){
                nameErrorText = "Story name is too short";
              } else if(name.length>30){
                nameErrorText = "Story name is too long";
              }
              this.setState({
                name,
                nameErrorText,
              });
            }}
        /></Grid> 
        <Grid item sg={12} md={6}>
        <TextField name ="photoSearch" label = "Photo Search" value={searchKey} onChange={(evt)=>{
          this.setState({
            searchKey:evt.target.value,
            baseIndex:0,
            newBaseIndex:0,
            relativeSelectedIndex:0,
            selectedIndex:undefined,
            searchHash:"",
            selectedHash:"",
          });
        }} required 
        error={!isValidPhoto}
        helperText={isValidPhoto ? "" :"Select a photo for your story"}/>
        </Grid>
        </Grid> 
        <PhotoCarouselInfinite 
          key={searchKey} 
          setPhotoIndex={this.setPhotoIndex}
          selectedIndex={selectedIndex}
          searchPhotos={async (page,pageSize)=>{
            const {hits} = (await fetchWithToken(`${photosUrl}?search=${searchKey}&page=${page}&numResults=${pageSize}`).then(resp=>resp.json()));
            this.setState({
              baseIndex:newBaseIndex,
              newBaseIndex:pageSize+baseIndex,
              searchHash:`${searchKey}-${page}-${pageSize}`,
            });
            return hits;
          }}
        />
        <Button variant="contained" color="primary" onClick={this.createStory} disabled={disabled || nameErrorText || !isValidPhoto}>
          Create Story
        </Button>
      </div>
    );
  }
}
       
CreateStory.propTypes = {
  url: PropTypes.string.isRequired,
  photosUrl: PropTypes.string.isRequired,
  writingPromptText: PropTypes.string.isRequired,
  writingPromptId: PropTypes.number.isRequired,
};

export default CreateStory
