import React from "react";
import Grid from '@material-ui/core/Grid';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';

import StoriesStats from "./components/StoriesStats.jsx";
import StoriesGallery from "./components/StoriesGallery.jsx";
import TopStories from "./components/TopStories.jsx";
import ProfileV2 from "./components/ProfileV2.jsx";
import MyPromptsV2 from "./components/MyPromptsV2.jsx";
import {fetchWithToken,getUser,getStoriesCount} from './helpers/api';

import "./stylesheets/Dashboard.css";
import "./stylesheets/MyStories.css";

const moment = require('moment');

const tabs = ["Stories","Profile","Prompts"];

class UserV2 extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      storiesPage:1,
      publishedStoriesPage:1,
      pageSize:10,
      publishedStories:[],
      stories:[],
      user:{},
      tab:0,
      storiesFilterTab:0,
    }
    this.getUser = this.getUser.bind(this);
    this.getStories = this.getStories.bind(this);
    this.getPublishedStories = this.getPublishedStories.bind(this);
    this.getStories = this.getStories.bind(this);
    this.getPublishedStoriesCount = this.getPublishedStoriesCount.bind(this);
    this.getStoriesCount = this.getStoriesCount.bind(this);
    this.getStoriesCountHelper = this.getStoriesCountHelper.bind(this);
    this.getStoriesHelper = this.getStoriesHelper.bind(this);
  }

  async getPublishedStories(){
    return this.getStoriesHelper({
      pageProp:'publishedStoriesPage',
      storiesProp: 'publishedStories',
      url: this.props.publishedStoriesUrl,
    });
  }

  async getStories(){
    return this.getStoriesHelper({
      pageProp:'storiesPage',
      storiesProp: 'stories',
      url: this.props.storiesUrl,
    });
  }

  async getPublishedStoriesCount(){
    return this.getStoriesCountHelper({
      storiesCountProp: 'publishedStoriesCount',
      url: this.props.publishedStoriesCountUrl,
    });
  }

  async getStoriesCount(){
    return this.getStoriesCountHelper({
      storiesCountProp: 'storiesCount',
      url: this.props.storiesCountUrl,
    });
  }

  async getStoriesHelper({pageProp,storiesProp,url}=this.state){
    const {
      pageSize,
    } = this.state;
    const {
      userId
    } = this.props;
    const page = this.state[pageProp];
    try{
      const resp = await fetchWithToken(`${url}?page=${page}&numberResults=${pageSize}&user=${userId}`,'GET');
      if(resp.ok){
        const stories = await resp.json();
        this.setState({
          [storiesProp]:[ ...this.state[storiesProp],...stories.map(story=>({
            ...story,
            lastModified:moment(story.lastModified).format("dddd, MMMM Do YYYY, h:mm:ss a"),
          }))],
          [pageProp]:page+1,
        });
        return;
      }
      throw resp;
    }catch(err){
      console.log(`Could not get ${storiesProp}`);
      console.log(err);
    }
  }

  async getStoriesCountHelper({
    storiesCountProp,
    url,
  }){
    const {
      userId,
    } = this.props;
    const storiesCount = await getStoriesCount(`${url}?user=${userId}`);
    if(storiesCount!==undefined){
      this.setState({
        [storiesCountProp]:storiesCount,
      })
    }
  }

  async getUser(){
    const {userUrl} = this.props;
    const user = await getUser(userUrl);
    if(user){
      this.setState({
        user,
      });
    }
  }

  componentDidMount(){
    const {logged_in} = this.props;
    this.getUser();
    this.getPublishedStoriesCount();
    this.getPublishedStories();
    if(logged_in){
      this.getStoriesCount();
      this.getStories();
    }
  }

  render () {
    const {
      editable,
      logged_in,
    } = this.props;
    const {
      user,
      publishedStories,
      stories,
      publishedStoriesCount,
      storiesCount,
      tab,
      storiesFilterTab,
    } = this.state;

    console.log(this.state);

    const profileTab = <ProfileV2 {...this.props}/>;

    const storiesTab = <div>
    <Grid container>
      <Grid item md={6} sm={12}>
        <StoriesStats
          storiesCount={publishedStoriesCount}
          rating={user.stories_rating}
          totalViews={user.total_stories_views}/>
        {editable ? <Tabs value={storiesFilterTab} onChange={(e,v)=>{
          this.setState({
            storiesFilterTab:v,
          });
        }}>
          <Tab label="Published"/>
          <Tab label="In Progress"/>
        </Tabs> : null}
      </Grid>
      <Grid item md={6} sm={12}>
        <TopStories 
          highestRatedStory={user.most_popular_story}
          mostViewedStory={user.most_viewed_story}
          mostRecentStory={user.most_recent_story}
        />
      </Grid>
    </Grid>
      <StoriesGallery 
        viewOnly = {!logged_in}
        fullScreen={logged_in} 
        loadedStories={true}
        hasMore={storiesFilterTab ? storiesCount>stories.length : publishedStoriesCount > publishedStories.length}
        publishedStories = {storiesFilterTab ? stories: publishedStories} 
        containerClassName="dashboardStoriesDiv"
        next = {()=> storiesFilterTab ? this.getStories() : this.getPublishedStories()} 
        refresh={()=>{}}
        refreshStory={()=>{}} 
        key= "storiesGalleryKey"
        style={{
          overflowX:"hidden",
        }}/>
      </div>;

    const promptsTab = <MyPromptsV2 {...this.props}/>

    const tabViews = [storiesTab,profileTab,promptsTab];

    return <div className="myStoriesDiv">
      <h1> {user.email} </h1>
      <Tabs value={tab} onChange={(e,v)=>{
          this.setState({
            tab:v,
          });
        }}>
          {
            tabs.map(tab=><Tab label={tab}/>)
          }
        </Tabs>
      {
        tabViews[tab]
      }
      </div>
  }
}

export default UserV2;
