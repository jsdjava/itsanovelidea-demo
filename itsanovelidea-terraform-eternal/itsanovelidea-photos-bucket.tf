resource "aws_s3_bucket" "itsanovelidea-photos" {
  bucket = "itsanovelidea-photos"
}

resource "aws_s3_bucket_policy" "itsanovelidea-photos" {
  bucket = "${aws_s3_bucket.itsanovelidea-photos.id}"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "itsanovelidea-photos",

  "Statement": [

    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::itsanovelidea-photos/*",
      "Condition": {
         "IpAddress": {"aws:SourceIp": "12.145.176.170"}
      }
    },
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::itsanovelidea-photos/*"
    }
  ]
}
POLICY
}