FactoryBot.define do
  factory :writing_prompt do
    created_at {Faker::Time.between(from: DateTime.now - 1, to: DateTime.now)}
    updated_at {Faker::Time.between(from: DateTime.now - 1, to: DateTime.now)}
    text {"do re mi fa so done with u"}
    association :owner, factory: :user
    impressions_count {0}
  end
end
