import React from "react";
import {
    Table,TableBody,TableCell,TableFooter,TablePagination,TableRow,TableSortLabel,TableHead
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

import MaterialLoadingOverlay from "./MaterialLoadingOverlay.jsx";

import UserCard from "./UserCard.jsx";

import {fetchWithToken} from "../helpers/api.js";

import "../stylesheets/UsersList.css";

const debounce = require('debounce'); 

class UsersList extends React.Component{
    
    constructor(){
        super();
        this.state = {
            users:[],
            usersCount:0,
            page:1,
            orderDirection:'desc',
            pageSize:10,
            firstLoad:true,
        }
        this.debouncedGetFilteredUsers = debounce(this.getFilteredUsers.bind(this),1000);
    }

    async getFilteredUsers(newState={}){
        const {
            url,
            count_url,
        } = this.props;
        const {
            pageSize,
            page,
            orderProp,
            orderDirection,
        } = {
            ...this.state,
           ...newState,
           ...this.props,
        };
        this.setState({
            loaded:false,
        })
        try{
            const paramsUrl = `${orderProp}=${orderDirection}&page=${page}&pageSize=${pageSize}`;
            const usersResp = await fetchWithToken(`${url}?${paramsUrl}`);
            const usersCountResp = await fetchWithToken(`${count_url}?${paramsUrl}`);
            if(!usersResp.ok){
                throw usersResp;
            }
            if(!usersCountResp.ok){
                throw usersCountResp;
            }
            const users = await usersResp.json();
            const usersCount = (await usersCountResp.json()).users_count;
            this.setState({
                users,
                usersCount,
                page,
                loaded:true,
                firstLoad:false,
                orderProp,
                orderDirection,
            });
        } catch(e){
            console.log("Could not get users");
            console.log(e);
        }
    }
    componentDidMount(){
        this.debouncedGetFilteredUsers();
    }

    sort(newOrderProp){
        const {orderDirection,orderProp} = this.state;
        console.log(orderProp);
        console.log(newOrderProp)
        console.log(orderDirection);                                          
        const isDesc = orderProp === newOrderProp && orderDirection === 'desc';
        this.debouncedGetFilteredUsers({
            orderProp:newOrderProp,
            orderDirection: isDesc ? 'asc' : 'desc',
            page:1,
        });
    }

    render(){
        //Passing props as well just for the storybook
        const {
            users,
            usersCount,
            orderDirection,
            orderProp,
            page,
            pageSize,
            loaded,
            firstLoad,
        } = {...this.state,...this.props};
        
        const sortLabels = {
            'Email':'sort_by_email',
            'Story Rating':'sort_by_rating',
            'Story Views': 'sort_by_story_views',
            'Prompts Rating': 'sort_by_prompts_rating',
            'Prompts Views': 'sort_by_prompts_views',
            'Stories':'sort_by_num_stories',
            'Prompts':'sort_by_num_prompts',
            'Responses':'sort_by_num_responses',
        };
        return <div className="usersListDiv">
        {firstLoad ? <CircularProgress size="10vw" thickness={2} style={{
        position: "fixed",
        left: "45%",
        top: "45%",
        "z-index": "9001"
     }}/> :
        <MaterialLoadingOverlay loading={!loaded}>
        <Table size="small">
        <TableHead style={{
            textAlign:"center",
        }}>
            <TableRow>
                {
                    Object.entries(sortLabels).map(([key,value])=><TableCell align="left">
                    <TableSortLabel
                        active={orderProp==={value}}
                        direction={orderDirection}
                        onClick={()=>this.sort(value)}>
                    {key}
                    </TableSortLabel>
                </TableCell>)
                }
            </TableRow>
        </TableHead>
        <div className="userCardsListDiv">
            {
                users.map(user=><UserCard {...user}/>)
            }
        </div>
        <TableBody>
            {users.map((user,spot) => (
              <TableRow key={spot}>
                <TableCell align="right" size="small">
                    <a href={user.user_url}> {user.email}</a>
                </TableCell>
                <TableCell align="right" size="small">{user.stories_rating}</TableCell>
                <TableCell align="right" size="small">{user.total_stories_views}</TableCell>
                <TableCell align="right" size="small">{user.prompts_rating}</TableCell>
                <TableCell align="right" size="small">{user.total_writing_prompts_views}</TableCell>
                <TableCell align="right" size="small">{user.total_stories}</TableCell>
                <TableCell align="right" size="small">{user.total_writing_prompts}</TableCell>
                <TableCell align="right" size="small">{user.total_writing_prompts_responses}</TableCell>
              </TableRow>
            ))}

            {!users.length && (
              <TableRow style={{ height: 48 * users.length }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                colSpan={3}
                count={usersCount}
                rowsPerPageOptions={[pageSize]}
                rowsPerPage={pageSize}
                page={page-1}
                onChangePage={(event,page)=>{
                    this.debouncedGetFilteredUsers({page:page+1});
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
        </MaterialLoadingOverlay>
        }
        </div>
    }
}

export default UsersList;