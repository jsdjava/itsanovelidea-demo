class AddAccessRuleToStories < ActiveRecord::Migration[5.1]
  def change
    add_reference :stories, :access_rule, foreign_key: true
  end
end
