const Joi = require('@hapi/joi');

describe('GET /photo', () => {
    it('returns 401 without the api key', () => {
      cy.request({
          url:Cypress.config("itsanovelideaPhotosUrl")+"/photo?search=cat",
          failOnStatusCode:false,
      }).should(resp=>{
          expect(resp.status).to.equal(401);
      });
    });
    it('returns the search results and the search key',()=>{
        // I need to avoid the caching that itsanovelidea-photos does by default, so
        // I'm going to randomly pick a page every time I make the request.
        // This makes it fairly unlikely that I hit one that has already been
        // cached. There's some weird pixabay requirement of <500 total hits, so
        // I'm working around that
        const page = Math.floor(Math.random()* 100);
        cy.request({
            url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo?search=cat&page=${page}&numResults=5`,
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).should(resp=>{
            expect(resp.body.searchKey).to.equal(`cat-${page}-5`);
            const searchPhotosSchema = Joi.array().items(
                Joi.object({
                    largeImageURL: Joi.string().required(),
                    imageWidth: Joi.number().required(),
                    imageHeight: Joi.number().required(),
                }).unknown(true),
            );
            
            const { error } = searchPhotosSchema.validate(resp.body.hits);
            expect(error).to.be.undefined;
        });
    })
  })