import React from "react";
import PropTypes from "prop-types";

import VisibilityIcon from '@material-ui/icons/Visibility';

import "../stylesheets/Views.css";

const HRNumbers = require('human-readable-numbers');

class Views extends React.Component{

  render(){
    const {views} = this.props;
    return <div className="views">
        {HRNumbers.toHumanString(views)} 
        &nbsp;
        <VisibilityIcon/>
      </div>;
  }
}

Views.propTypes = {
    views: PropTypes.number.required,
};

export default Views;