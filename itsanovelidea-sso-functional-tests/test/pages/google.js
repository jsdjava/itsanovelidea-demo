class GooglePage {
    get emailInput() { return $("input[type='email']") }
    get passwordInput() {return $("input[type='password']")}
    get loginBtn(){return $("#loginbutton")}
    get passwordNextBtn(){return $("#passwordNext")}
    get emailNextBtn(){return $("#identifierNext,#next")}
  }
  
module.exports = new GooglePage();
  