import React from "react";
import Typography from 'typography'
import altonTheme from 'typography-theme-alton'
import { TypographyStyle, GoogleFont } from 'react-typography'

altonTheme.baseFontSize = "15px";
altonTheme.overrideThemeStyles = ()=>({
  a:{
    color: "#3f51b5",
  },
  html: {
    overflowY: 'initial',
  },
});

const typography = new Typography(altonTheme);
typography.baseFontSize= "17px"

class ItsanovelideaTypography extends React.Component{
  render(){
    return <span>
        <TypographyStyle typography={typography} />
        <GoogleFont typography={typography} />
    </span>;
  }
}

export default ItsanovelideaTypography;