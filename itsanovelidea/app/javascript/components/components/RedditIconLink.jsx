import React from "react";
import RedditLogo from "super-tiny-icons/images/svg/reddit.svg";

class RedditIconLink extends React.Component{

  render(){
    const {link} = this.props;
    if(!link){
      return null;
    }
    return <a href={link} target="_blank" onClick={(e)=>{
      e.stopPropagation();
    }} style={{
      width:"15px",
      marginRight:"7px",
      float:"right",
    }}><img src={RedditLogo} style={{
      marginBottom:0,
    }}/></a>;
  }
}

export default RedditIconLink;