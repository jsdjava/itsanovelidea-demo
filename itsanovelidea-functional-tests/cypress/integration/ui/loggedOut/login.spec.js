const {appUrl} = require('../../../support/app.json');
const {loginPage,dashboardPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route,emailInput,facebookBtn,googlePlusBtn,redditBtn} = loginPage;
const {userHeading} = dashboardPage;

describe('logging in', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
    });
    it('successful login(user and password exist)', function() {
        login(email,password);
        cy.get(userHeading).should('contain',email);
    });

    it("failed login(password is wrong)",function(){
        login(email,"wrongpassword",false);
        cy.get(emailInput).should('be.visible');
    });
  });
