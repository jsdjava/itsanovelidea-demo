const pify = require('pify');
const { MongoClient } = require('mongodb');
const ShareDB = require('sharedb');
const shareDbAccess = require('sharedb-access');
const shareDbMongo = require('sharedb-mongo');
const richTextType = require('rich-text').type;

const { config } = require('itsanovelidea-middleware');

const logger = require('./logger.js');

const db = 'itsanovelidea';
const mongoConnectionStr = [
  'mongodb://',
  `itsanovelidea:${process.env.MONGO_INITDB_ROOT_PASSWORD}@`,
  `${config.get('mongoHost')}/${db}?`,
  'authSource=admin',
].join('');

let mongoClient;
const getMongoClient = () => mongoClient;
const shareMongoClient = shareDbMongo((cb) => {
  const mongoConnect = () => MongoClient.connect(mongoConnectionStr, (err, createdMongoClient) => {
    mongoClient = createdMongoClient;
    if (err) {
      // eslint-disable-next-line
      logger.log('error','Could not connect to mongodb');
      // eslint-disable-next-line
      logger.log('error',err.toString());
      // Keep trying until we can connect
      setTimeout(mongoConnect, 10000);
    } else {
      // If mongo can't connect, I don't want to callback sharedb to register it, because
      // sharedb will just throw the error in here, and crash the server. Instead, I'm just
      // going to not call sharedb back at all. I'm curious to see if this works fine when
      // I call various things in sharedb without mongo registered. I'm hoping it just throws
      // up a useful error
      cb(err, mongoClient);
    }
  });
  mongoConnect();
});

// Sort of strange. Basically, I need to make some calls to mongo directly, so
// I'm instantiating my own mongo client. sharedb-mongo requires that I pass
// a function in which gets called with yet another function (cb) that looks like:
/*
    // From https://github.com/share/sharedb-mongo/blob/master/index.js
    var finish = function(err, client) {
    if (err) throw err;
    if (isLegacyMongoClient(client)) {
      self.mongo = self._mongoClient = client;
    } else {
      self.mongo = client.db();
      self._mongoClient = client;
    }
    self._flushPendingConnect();
  };
*/
// I use this function to register the mongo client I created myself
// with sharedb mongo

ShareDB.types.register(richTextType);
// Could also pass in pubsub if I needed to share info between
// multiple itsanovelidea-collaborative procs. I don't think
// I need it right now, though,
const shareDb = new ShareDB({ db: shareMongoClient });

// Access middlware - stops users from reading/modifying stories they don't own
// and modifying the profiles of other users
shareDbAccess(shareDb);

// Unfortunately, sharedb is still using callbacks in 2020, so I'm going
// to wrap the calls I need to make to it using pify, so I can just pretend
// that the callbacks aren't there in the rest of my code
const shareClient = shareDb.connect();

// sharedb literally doesn't support version=undefined, so I have to default it
// to being null
const fetchSnapshot = async (collection, id, version = null) => pify(
  shareClient.fetchSnapshot.bind(shareClient),
)(collection, id, version);

// Can't seem to find a way to include the timestamp directly in the fetchSnapshot call,
// so going to try doing it here
const fetchSnapshotMetadata = async (collection, id, version) => {
  // Pick just the current version (from version to version+1, non-inclusive)
  const [metadata] = await pify(shareDb.getOps.bind(shareDb))(
    shareClient.agent, collection, id, version, version + 1, {
      opsOptions: {
        metadata: true,
      },
    },
  );
  return metadata;
};

const createDocument = async (collectionName, documentId) => {
  const doc = shareClient.get(collectionName, documentId);
  // I don't want the browser client to be creating the document, so I'm doing it
  // in the server, and just inserting a new line. Browser clients shouldn't have permission
  // to create documents over websockets
  return pify(doc.create.bind(doc))([{ insert: '\n' }], 'rich-text');
};

const listen = (stream, req) => shareDb.listen(stream, req);

module.exports = {
  access: shareDb,
  fetchSnapshot,
  fetchSnapshotMetadata,
  createDocument,
  listen,
  getMongoClient,
};
