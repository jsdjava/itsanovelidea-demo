import React from 'react';
import StoryHistory from "./StoryHistory.jsx";

const history = require('./fixtures/storyHistory.json');

export default {
  component: StoryHistory,
  title: 'StoryHistory',
};

export const storyHistory = ()=> <StoryHistory
  history={history}
/>;

export const storyHistoryNarrow = ()=> <div style={{height:"500px",width:"320px"}}><StoryHistory
  history={history}
/></div>;

export const storyHistoryNoStories = () => <StoryHistory/>;
