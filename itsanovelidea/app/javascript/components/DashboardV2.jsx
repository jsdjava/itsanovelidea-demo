import React from "react";
import PropTypes from "prop-types";
import Grid from '@material-ui/core/Grid';
import {Tabs,Tab} from "@material-ui/core";
import update from 'immutability-helper';

import Rate from "./components/Rate.jsx";
import StoriesGallery from "./components/StoriesGallery.jsx";
import StoriesListV2 from "./components/StoriesListV2.jsx";
import StoryCard from "./components/StoryCard.jsx";
import WritingPromptsList from "./components/WritingPromptsList.jsx";
import DashboardSearchBar from "./components/DashboardSearchBar.jsx";
import {fetchWithToken} from './helpers/api';

import "./stylesheets/Dashboard.css";

const moment = require('moment');
const debounce = require('debounce');

class Dashboard extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      page:1,
      rating : "N/A",
      pageSize:10,
      myFilteredStories:[],
      activeTab:0,
      user:{},
    }
    this.debouncedGetMyFilteredStories = debounce(this.getMyFilteredStories.bind(this),1000);
    this.refreshStory = this.refreshStory.bind(this);
    this.getUser = this.getUser.bind(this);
    this.getWritingPromptsCount = this.getWritingPromptsCount.bind(this);
    this.getStoriesCount = this.getStoriesCount.bind(this);
  }

  // TODO It feels like im repeating this pattern a lot - is there a way to abstract this out so I dry things up, 
  // but I don't make a tangled chain of fn's calling fn's calling fn's with tons of params?
  async getWritingPromptsCount (){
    const {
      writingPromptsCountUrl,
      userId,
    } = this.props;
    try{
      const resp = await fetchWithToken(`${writingPromptsCountUrl}?user=${userId}`);
      if(resp.ok){
        const writingPromptsCount = await resp.json();
        this.setState({
          writingPromptsCount:writingPromptsCount.num_writing_prompts,
        })
      }else{
        throw resp;
      }
    }catch(err){
      console.log("Could not get writing prompts count");
      console.log(err);
    }
  }

  async getStoriesCount(){
    const {
      storiesCountUrl,
      userId,
    } = this.props;
    try{
      const resp = await fetchWithToken(`${storiesCountUrl}?user=${userId}`);
      if(resp.ok){
        const storiesCount = await resp.json();
        this.setState({
          storiesCount:storiesCount.stories_count,
        });
      } else{
        throw resp;
      }
    }catch(err){
      console.log("Could not get stories count");
      console.log(err);
    }
  }

  async getUser(){
    const {userUrl} = this.props;
    try{
      const resp = await fetchWithToken(userUrl);
      if(resp.ok){
        const user = await resp.json();
        this.setState({
          user,
        })
      } else{
        throw resp;
      }
    }catch(e){
      console.log("Could not get user");
      console.log(e);
    }
  }

  async getMyFilteredStories({page,searchName,myFilteredStories:oldMyFilteredStories,published}=this.state){
    const {storiesUrl} = this.props;
    const {pageSize} = this.state;

    try{
      const resp = await fetchWithToken(`${storiesUrl}/?page=${page}&numberResults=${pageSize}&${searchName?`name=${searchName}`:""}&${published?`published=${published}`:""}`,'GET');
      if(resp.ok){
        const filteredStories = await resp.json();
        this.setState({
          myFilteredStories:[ ...oldMyFilteredStories,...filteredStories.map(filteredStory=>({
            ...filteredStory,
            lastModified:moment(filteredStory.lastModified).format("dddd, MMMM Do YYYY, h:mm:ss a"),
          }))],
          page,
          searchName,
          published,
        })
        return;
      }
      throw resp;
    }catch(err){
      console.log("Could not get my filtered stories");
      console.log(err);
    }
  }

  async refreshStory(storySpot){
    const {
      myFilteredStories,
    } = this.state;
    const story = myFilteredStories[storySpot];
    const {url} = story;
    try{
      const resp = await fetchWithToken(url);
      if(!resp.ok){
        throw resp;
      }
      const updatedStory = await resp.json();
      this.setState(update(this.state,{
        myFilteredStories:{
          [storySpot]:{
            $set: updatedStory,
          }
        }
      }));
    }catch(e){
      console.log("Could not refresh story");
      console.log(e);
    }
  }


  componentDidMount(){
    this.getMyFilteredStories();
    this.getUser();
    this.getWritingPromptsCount();
    this.getStoriesCount();
  }

  render () {
    const {writingPromptsUrl,writingPromptsCountUrl} = this.props;
    const {
      myFilteredStories,
      page,
      searchName,
      published,
      activeTab,
      user,
      writingPromptsCount,
      storiesCount,
    } = this.state;
    return <Grid container>
        <Grid item sg={12} md={6}>
        <Tabs value = {activeTab} onChange={(e,activeTab)=>{this.setState({activeTab})}}>
            <Tab label="My Stories"/>
            <Tab label="My Prompts"/>
          </Tabs>
        <div hidden={activeTab !== 0} className="tabDiv">
          <DashboardSearchBar
            search={(searchProps)=>{
              this.debouncedGetMyFilteredStories({searchName,page:1,myFilteredStories:[],published,...searchProps});
            }}/>
           <StoriesGallery 
              StoryComponent = {StoryCard}
              GalleryComponent = {StoriesListV2}
              publishedStories = {myFilteredStories} 
              containerClassName="dashboardStoriesDiv"
              hasMore={storiesCount>myFilteredStories.length}
              next = {()=>this.getMyFilteredStories({page:page+1,searchName,published,myFilteredStories,})} 
              refresh={()=>this.getMyFilteredStories({page:1,searchName,published,myFilteredStories:[]})}
              refreshStory={(storySpot)=>this.refreshStory(storySpot)}
              key= {`${searchName}-${published}`}
              height={"100%"} 
              style={{
                overflowX:"hidden",
            }}/>
        </div>
        <div hidden={activeTab !== 1} className="tabDiv">
          <WritingPromptsList url = {writingPromptsUrl} count_url = {writingPromptsCountUrl} authorSearch={user}/>
        </div>
        </Grid>
        <Grid item sg={12} md={6}>
          <Tab label="Profile"/>
          <p> Prompts </p>
          <p>Total: {writingPromptsCount}</p> 
          <Rate key={user.prompts_rating} rating={user.prompts_rating}/>
          <p> Stories </p>
          <p>Total: {storiesCount}</p> 
          <Rate key={user.stories_rating} rating={user.stories_rating}/> 
        </Grid>
      </Grid>
  }
}

Dashboard.propTypes = {
  url: PropTypes.string,
  storiesUrl:PropTypes.string,
  userUrl: PropTypes.string,
  writingPromptsCountUrl: PropTypes.string,
  storiesCountUrl:PropTypes.string,
  userId: PropTypes.number,
};
export default Dashboard;
