import React from "react";
import { TextField,Button } from '@material-ui/core';

import {fetchWithToken} from "./helpers/api.js";
import {success,error} from "./helpers/notifications.js";
import RandomPromptWord from "./components/RandomPromptWord.jsx";

import 'react-toastify/dist/ReactToastify.css';
import "./stylesheets/CreateWritingPrompt.css";

class CreateWritingPrompt extends React.Component{
    constructor(){
        super();
        this.state=  {
          text:"",
          errorText:"Prompt is too short",
        };
        this.createWritingPrompt = this.createWritingPrompt.bind(this);
    }

    createWritingPrompt(){
        const {text} = this.state;
        fetchWithToken(this.props.url,'POST',JSON.stringify({
            text:text.trim(),
        })).then((resp)=>{
            if(!resp.ok){
                throw resp;
            }
            window.history.back();
            console.log("Writing prompt created!");
            success("Writing prompt created!");
        }).catch(()=>{
            console.log("Could not create writing prompt");
            error("Could not create writing prompt");
        });
    }

    render(){
        const {
            text,
            errorText
        } = this.state;
        const {
            randomWordUrl
        } = this.props;
        return (<div className="createWritingPromptDiv">
        <RandomPromptWord randomWordUrl={randomWordUrl}/>
                <TextField
                    required
                    placeholder="Prompt..."
                    multiline={true}
                    rows={5}
                    rowsMax={5}
                    value={text}
                    error={errorText}
                    helperText={errorText}
                    onChange={e=>{ 
                        const text = e.target.value;
                        let errorText = ''
                        if(text.trim().length<10){
                          errorText = "Prompt is too short"
                        } else if(text.trim().length>300){
                          errorText = "Prompt is too long"
                        }
                        this.setState({
                          text,
                          errorText,
                        });
                    }}
                />
                { !errorText ? <Button data-qa="createWritingPromptBtn" onClick={this.createWritingPrompt}>Create Prompt </Button> : null}
            </div>);
    };
}

export default CreateWritingPrompt;
