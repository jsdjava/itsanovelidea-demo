class AddRatingToStoriesUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :stories_users, :rating, :integer
  end
end
