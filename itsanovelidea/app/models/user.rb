require 'rest-client'
require 'json'

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, 
         :confirmable, :omniauthable, :timeoutable,
         :omniauthable, omniauth_providers: [:google_oauth2,:reddit,:facebook]
  has_many :user_writing_prompts
  has_many :user_stories
  has_many :writing_prompts, through: :user_writing_prompts
  has_many :stories, through: :user_stories
  has_many :created_writing_prompts, foreign_key: "owner_id", class_name: "WritingPrompt"
  has_many :created_stories, foreign_key: "owner_id", class_name: "Story"
  attr_accessor :computed_stories_rating, 
    :computed_total_stories_views, 
    :computed_total_stories,
    :computed_total_writing_prompts_views, 
    :computed_total_writing_prompts_responses,  
    :computed_total_writing_prompts,
    :computed_writing_prompts_rating, 
    :thin

  def thin?
    self.thin
  end

  def self.from_facebook(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.skip_confirmation!
    end
  end

  def self.from_reddit(access_token)
    data = access_token.info
    email = "#{data['name']}@reddit.com" 
    user = User.where(email: email).first
    unless user
      user = User.new(email: email,
        password: Devise.friendly_token[0,20]
      )
      user.skip_confirmation!
      user.save!
    end
    user.confirm
    user
  end

  def self.from_google(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first
    unless user
      user = User.new(email: data['email'],
        password: Devise.friendly_token[0,20]
      )
      user.skip_confirmation!
      user.save!
    end
    user.confirm
    user
  end

  def most_viewed_story
    self.created_stories.where.not(published_text:nil).order(impressions_count: :desc).first
  end

  def most_popular_story
    self.created_stories.joins(:user_stories)
      .group("stories.id")
      .order("AVG(user_stories.rating) DESC").first
  end

  def most_recent_story
    self.created_stories.order(updated_at: :desc).first
  end

  def most_replied_prompt
    self.created_writing_prompts.joins(:stories)
      .where.not(:stories=>{published_text:nil})
      .group("writing_prompts.id")
      .order("COUNT(writing_prompts.id) DESC")
      .first
  end

  def most_viewed_prompt
    self.created_writing_prompts.order(impressions_count: :desc).first
  end

  def most_popular_prompt
    self.created_writing_prompts.joins(:user_writing_prompts).maximum("user_writing_prompts.rating")
  end

  def most_recent_prompt
    self.created_writing_prompts.order(updated_at: :desc).first
  end

  def total_stories_views
    self.computed_total_stories_views.nil? ? self.created_stories.where.not(published_text:nil).sum(:impressions_count) : self.computed_total_stories_views
  end

  def total_writing_prompts_views
    self.computed_total_writing_prompts_views.nil? ? self.created_writing_prompts.sum(:impressions_count): self.computed_total_writing_prompts_views
  end

  def total_writing_prompts_responses
    self.computed_total_writing_prompts_responses.nil? ? self.created_writing_prompts.joins(:stories).where.not(:stories=>{published_text:nil}).count : self.computed_total_writing_prompts_responses
  end

  def total_stories
    self.computed_total_stories.nil? ? self.created_stories.where.not(published_text:nil).count : self.computed_total_stories
  end

  def total_writing_prompts
    self.computed_total_writing_prompts.nil? ? self.created_writing_prompts.count : self.computed_total_writing_prompts
  end

  def stories_rating
    self.computed_stories_rating || self.created_stories.joins(:user_stories).average("user_stories.rating") || "Not Rated"
  end

  def writing_prompts_rating
    self.computed_writing_prompts_rating || self.created_writing_prompts.joins(:user_writing_prompts).average("user_writing_prompts.rating") || "Not Rated"
  end

  def create_profile
    RestClient.post("https://#{Rails.configuration.node_host}/profiles/#{self.id}", {}.to_json, {
      apikey: Rails.configuration.node_api_key,
      content_type: :json,
    })
  end

  def get_profile

    #TODO Maybe wrap in a try catch? not sure...
    resp = RestClient.get("https://#{Rails.configuration.node_host}/profiles/#{self.id}", {
      apikey: Rails.configuration.node_api_key,
      content_type: :json,
    })
    JSON.parse(resp.body)
  end

  def self.stories_view
    Story.all
      .joins('LEFT JOIN users on stories.owner_id=users.id')
      .group("users.id")
      .select('
        users.id,
        COALESCE(sum(stories.impressions_count),0) as computed_total_stories_views,
        COALESCE(sum(case when stories.published_at is not null then 1 else 0 end),0) as computed_total_stories
      ')
  end

  def self.stories_ratings_view
    Story.all
    .joins('LEFT JOIN users on stories.owner_id=users.id')
    .joins("LEFT JOIN user_stories ON stories.id=user_stories.story_id")
    .group("users.id")
    .select('
      users.id,
      COALESCE(round(avg(user_stories.rating),2),"Not Rated") as computed_stories_rating
    ')
  end

  def self.prompts_view
    WritingPrompt.all
      .joins('LEFT JOIN users on writing_prompts.owner_id = users.id')
      .group("users.id")
      .select('
        users.id,
        COALESCE(sum(writing_prompts.impressions_count),0) as computed_total_writing_prompts_views,
        count(writing_prompts.id) as computed_total_writing_prompts   
      ')
  end

  def self.prompts_ratings_view
    WritingPrompt.all
      .joins('LEFT JOIN users on writing_prompts.owner_id = users.id')
      .joins("LEFT JOIN user_writing_prompts ON writing_prompts.id=user_writing_prompts.writing_prompt_id")
      .group("users.id")
      .select('
        users.id,
        COALESCE(round(avg(user_writing_prompts.rating),2),"Not Rated") as computed_writing_prompts_rating
      ')
  end

  def self.prompts_responses_view
    WritingPrompt.all
      .joins('LEFT JOIN users on writing_prompts.owner_id=users.id')
      .joins("LEFT JOIN stories on stories.writing_prompt_id = writing_prompts.id")
      .group("users.id")
      .select('
        users.id,
        count(stories.id) as computed_total_writing_prompts_responses
      ')    
  end

  def self.expand(query)
    user_ids = query.to_a.pluck(:id)
    return [] if user_ids.empty?
    users = User.all.where("users.id IN (#{user_ids.join(',')})").load
    user_stories = stories_view.where("users.id IN (#{user_ids.join(',')})").load
    user_prompts = prompts_view.where("users.id IN (#{user_ids.join(',')})").load
    user_prompts_responses = prompts_responses_view.where("users.id IN (#{user_ids.join(',')})").load
    user_stories_ratings = stories_ratings_view.where("users.id IN (#{user_ids.join(',')})").load
    user_prompts_ratings = prompts_ratings_view.where("users.id IN (#{user_ids.join(',')})").load
    sorted_users = []
    users.each { |x|
      user_story_attributes = user_stories.find{|y| y.id==x.id}&.attributes || {
        "computed_total_stories_views" => 0,
        "computed_total_stories" => 0,
      }
      user_prompt_attributes = user_prompts.find{|y| y.id==x.id}&.attributes || {
        "computed_total_writing_prompts_views" => 0,
        "computed_total_writing_prompts" => 0,
      }
      x.computed_total_writing_prompts_responses = user_prompts_responses.find{|y| y.id == x.id}&.attributes&.[]("computed_total_writing_prompts_responses") || 0
      x.computed_writing_prompts_rating = user_prompts_ratings.find{|y| y.id == x.id}&.attributes&.[]("computed_writing_prompts_rating") || "Not Rated"
      x.computed_stories_rating = user_stories_ratings.find{|y| y.id == x.id}&.attributes&.[]("computed_stories_rating") || "Not Rated"
      x.computed_total_stories_views = user_story_attributes["computed_total_stories_views"]
      x.computed_total_stories = user_story_attributes["computed_total_stories"]
      x.computed_total_writing_prompts_views = user_prompt_attributes["computed_total_writing_prompts_views"]
      x.computed_total_writing_prompts = user_prompt_attributes["computed_total_writing_prompts"]
      x.thin = true
      spot = user_ids.find_index{|id| id === x.id}
      sorted_users[spot] = x
    }
    sorted_users
  end

  def self.sort_users(params,get_count=false)
    return self.all.count if get_count
    return expand(page(params,self.all.order("email #{params[:sort_by_email] === 'asc' ? 'asc' : 'desc'}"))) if params[:sort_by_email]
    return expand(page(params,self.stories_ratings_view.order("computed_stories_rating #{params[:sort_by_story_rating]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_story_rating]
    return expand(page(params,self.stories_view.order("computed_total_stories_views #{params[:sort_by_story_views]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_story_views]
    return expand(page(params,self.prompts_ratings_view.order("computed_writing_prompts_rating #{params[:sort_by_prompts_rating]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_prompts_rating]
    return expand(page(params,self.prompts_view.order("computed_total_writing_prompts_views #{params[:sort_by_prompts_views]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_prompts_views]
    return expand(page(params,self.stories_view.order("computed_total_stories #{params[:sort_by_num_stories]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_num_stories]
    return expand(page(params,self.prompts_view.order("computed_total_writing_prompts #{params[:sort_by_num_prompts]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_num_prompts]
    return expand(page(params,self.prompts_responses_view.order("computed_total_writing_prompts_responses #{params[:sort_by_num_responses]=='asc' ? 'asc' : 'desc'}"))) if params[:sort_by_num_responses]
    return expand(page(params,self.all))
  end

  # TODO I copy pasted this from the filterable concern as I didn't think it was worth the effort to try
  # abstracting it
  def self.page (params,query=self.all)
    max_page_size = 10
    page_size = (1...max_page_size).include?(params[:page_size].to_i) ? params[:page_size].to_i : max_page_size
    page = params[:page].to_i >0 ? params[:page].to_i : 1
    query.paginate(:page => page, :per_page => page_size)
  end
  # TODO What about views and ratings on unpublished stories?

  def wipe
    UserStory.where(user:self.id).destroy_all
    UserWritingPrompt.where(user:self.id).destroy_all
  end
end