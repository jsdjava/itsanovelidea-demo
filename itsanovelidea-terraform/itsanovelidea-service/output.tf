output "task_role_id" {
  value       =  aws_iam_role.itsanovelidea-service-task.id
  description = "Task execution role to assign to the task definition"
}