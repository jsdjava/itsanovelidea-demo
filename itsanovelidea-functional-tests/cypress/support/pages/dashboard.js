module.exports = {
    userHeading:".myStoriesDiv h1",
    successAlert: "[data-qa='success']",
    getErrorAlert: (spot)=>`[data-qa='error-${spot}']`
}