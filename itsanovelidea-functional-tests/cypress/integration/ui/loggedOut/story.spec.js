const {appUrl} = require('../../../support/app.json');
const {forceVisit,expandStory} = require('../../../support/actions/shared.js');
const {
  welcomePage,
  storyPage,
} = require('../../../support/pages');

const {
  route,
  expandedStoryLink,
} = welcomePage;
const {
  routeRegex,
  storyName,
  storyUserLink,
  storyRedditLink,
  storyText,
  storyImage,
  storyViews,
  storyLink,
} = storyPage;

describe('story screen', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
      expandStory();
    });

    // Could check if it equals the same values of the expanded one, like we do in 
    // welcome.spec.js
    it('displays the story',function(){
      cy
        .get(expandedStoryLink)
        .click();
      cy.url().should('match',routeRegex);
      cy.get(storyName).should('exist');
      cy.get(storyUserLink).should('exist');
      cy.get(storyRedditLink).should('exist');
      cy.get(storyText).should('exist');
      cy.get(storyImage).should('exist');
      cy.get(storyViews).should('exist');
      cy.get(storyLink).should('exist');
    });
});