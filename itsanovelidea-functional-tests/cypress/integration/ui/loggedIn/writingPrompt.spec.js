const {appUrl} = require('../../../support/app.json');
const {navbar,loginPage,writingPromptPage,newStoryPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {createStoryDiv} = newStoryPage;
const {authorsLink} = navbar;
const {
  route:writingPromptRoute,
  firstStoryDiv,
  promptSearchInput,
  authorSearchInput,
  storyNames,
  storyAuthors,
  storyViews,
  storyRatingWrappers,
  storyRatings,
  sortByRatingBtn,
  sortByDateBtn,
  sortByViewsBtn,
  expandedStoryDiv,
  createStoryBtn,
} = writingPromptPage;

describe('writing prompt page', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
      login(email,password);
      cy.visit(`${appUrl}/${writingPromptRoute}`);
    });
    it('renders a list of stories',()=>{
      cy.get(firstStoryDiv).should('exist');
    });
    it('filters stories for the prompt by name',()=>{
      cy.get(promptSearchInput).type("er");
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyNames)].map(x=>x.innerText))
        .should(x=>{
          expect(x.length).to.be.above(2);
          x.map(storyName=>expect(storyName.toLowerCase()).to.contain("er"));
        });
    });
    it('filters stories for the prompt by author',()=>{
      cy.get(authorSearchInput).type("default");
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyAuthors)].map(x=>x.innerText))
        .should(x=>{
          expect(x.length).to.be.above(2);
          x.map(storyAuthor=>expect(storyAuthor.toLowerCase()).to.contain("default"));
        });
    });
    it('sorts by rating',()=>{
      cy.get(sortByRatingBtn).click();
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyRatingWrappers)].map(x=>[...x.querySelectorAll(storyRatings)].length))
        .should(x=>{
          expect(x.length).to.be.above(2);
          expect(x.filter(rating=>rating).length).to.be.above(2);
          expect(x).to.eql([...x].sort().reverse());
        });
    });

    // This is kind of tricky to test because i can't see the date when a story is written in the ui
    // Instead im just goint to make sure that it changes
    it('sorts by date',()=>{
      let oldStoryNames;
      cy.get(storyNames).then(x=>{
        oldStoryNames = [...x].map(storyName=>storyName.innerText);
      });
      console.log(oldStoryNames);
      cy.get(sortByDateBtn).click();
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyNames)].map(storyName=>storyName.innerText))
        .should(x=>{
          expect(x.length).to.eql(oldStoryNames.length);
          expect(x).to.not.eql(oldStoryNames);
        });
    });
    it('sorts by views',()=>{
      cy.get(sortByViewsBtn).click();
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyViews)].map(x=>parseInt(x.innerText)))
        .should(x=>{
          expect(x.length).to.be.above(2);
          expect(x.filter(views=>views).length).to.be.above(2);
          expect(x).to.eql([...x].sort().reverse());
        });
    });
    it("does a compound search (author +name) and sort",()=>{
      cy.get(promptSearchInput).type("er");
      cy.document()
      .pipe(x=>[...x.querySelectorAll(storyNames)].map(x=>x.innerText))
      .should(x=>{
        expect(x.length).to.be.above(2);
        x.map(storyName=>expect(storyName.toLowerCase()).to.contain("er"));
      });
      cy.get(authorSearchInput).type("w");
      cy.document()
        .pipe(x=>{
          const authors = [...x.querySelectorAll(storyAuthors)].map(x=>x.innerText);
          const names = [...x.querySelectorAll(storyNames)].map(x=>x.innerText);
          return {authors,names};
        })
        .should(({authors,names})=>{
          expect(authors.length).to.be.above(1);
          expect(authors.length).to.eql(names.length)
          authors.map(storyAuthor=>expect(storyAuthor.toLowerCase()).to.contain("w"));
          names.map(storyName=>expect(storyName.toLowerCase()).to.contain("er"));
        });
    });
    it("pages",()=>{
      let oldStoryNames;
      cy.get(storyNames)
        .should('have.length',12)
        .then(x=>{
          oldStoryNames = [...x].map(storyName=>storyName.innerText);
        })
      cy.scrollTo('bottom');
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyNames)].map(storyName=>storyName.innerText))
        .should(x=>{
          expect(x.length).to.be.above(15);
          expect(x.slice(0,12)).to.eql(oldStoryNames)
        })
    });
    it("pages, maintaining a search and sort",()=>{
      cy.get(authorSearchInput).type("a");
      cy.document()
        .pipe(x=>[...x.querySelectorAll(storyAuthors)].map(x=>x.innerText))
        .should(x=>{
          expect(x.length).to.be.above(2);
          x.map(storyAuthor=>expect(storyAuthor.toLowerCase()).to.contain("a"));
        });
      cy.get(sortByViewsBtn).click();
      cy.document()
      .pipe(x=>[...x.querySelectorAll(storyViews)].map(x=>parseInt(x.innerText)))
      .should(x=>{
        expect(x.length).to.be.above(2);
        expect(x.filter(views=>views).length).to.be.above(2);
        expect(x).to.eql([...x].sort().reverse());
      });
      cy.scrollTo('bottom');
      cy.document()
        .pipe(x=>{
          const views = [...x.querySelectorAll(storyViews)].map(view=>parseInt(view.innerText));
          const authors = [...x.querySelectorAll(storyAuthors)].map(author=>author.innerText);
          return {
            views,
            authors
          }
        })
        .should(({views,authors})=>{
          expect(views.length).to.be.above(12);
          expect(authors.length).to.eql(views.length);
          authors.map(author=>expect(author.toLowerCase()).to.contain("a"));
          expect(views).to.eql([...views].sort().reverse())
        })
    });
    it("expands a story",()=>{
      cy.get(firstStoryDiv).click();
      cy.get(expandedStoryDiv).should('exist');
    })
    it.only("creates a story",()=>{
      cy.get(createStoryBtn).click();
      cy.get(createStoryDiv).should('exist');
    });
    it("allows you to rate a story");
    it("correctly updates the outer story gallery on a story being rated")
  });
