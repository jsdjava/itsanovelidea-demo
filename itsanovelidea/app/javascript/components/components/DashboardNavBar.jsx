import React, {Component} from 'react';
import PropTypes from "prop-types";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import DashboardMenu from "./DashboardMenu.jsx";

import "../stylesheets/DashboardNavBar.css";

//TODO Possibly just combine with WelcomeNavBar, css and code is fairly duped

class DashboardNavBar extends Component {

    constructor(){
        super();
        this.state = {}
    }

    render(){
      const {anchorEl} = this.state;
      const {
        urls={},
        myPromptsUrl,
        myStoriesUrl,
      } = this.props;
      return <AppBar position="static" className="dashboardNavBar">
        <Toolbar variant="dense">
            <Typography variant="h6" className="brandHeader" onClick={()=>{
              window.location.href="/";
            }}>
                Itsanovelidea
            </Typography>
            {
              Object.keys(urls).map((urlName,spot)=>{
                return (
                    <Button className={spot===Object.keys(urls).length-1 ? "dashboardNavBarLastLink" : ""} onClick={()=>{
                        window.location.href = urls[urlName];
                      }}>{urlName}</Button>
                )})
            }
          <IconButton aria-label="menu" className="dashboardMenuToggle" onClick={e=>this.setState({
            anchorEl:e.currentTarget,
          })}>
            <MenuIcon />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={anchorEl}
            onClose={()=>this.setState({anchorEl:null})}>
            {
              Object.keys(urls).map(urlName=>{
                return (
                    <MenuItem onClick={()=>{
                        this.setState({anchorEl:null});
                        window.location.href = urls[urlName];
                    }}>{urlName}
                    </MenuItem>
                )})
            }
          </Menu>
        </Toolbar>
      </AppBar>;
    }
}

DashboardNavBar.propTypes = {
    urls: PropTypes.object.isRequired,
    myPromptsUrl: PropTypes.string.isRequired,
    myStoriesUrl: PropTypes.string.isRequired,
  };
  
export default DashboardNavBar;