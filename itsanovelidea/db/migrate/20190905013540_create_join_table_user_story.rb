class CreateJoinTableUserStory < ActiveRecord::Migration[5.1]
  def change
    create_join_table :users, :stories, table_name: :user_stories do |t|
      t.integer :rating
    end
  end
end
