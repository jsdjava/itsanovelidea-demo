class CreateJoinTableUserWritingPrompt < ActiveRecord::Migration[5.1]
  def change
    create_join_table :users, :writing_prompts, table_name: :user_writing_prompts do |t|
      t.integer :rating
    end
  end
end
