// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this,
      args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

// Thanks https://stackoverflow.com/questions/10730362/get-cookie-by-name
export function getCookie(name) {
  let cookie = {};
  document.cookie.split(';').forEach(function(el) {
    let [k,v] = el.split('=');
    cookie[k.trim()] = v;
  })
  return cookie[name];
}

/*
 

        return <GridList cellHeight={300} cols={numColumns} style={{
          overflowY:"visible",
          width:"100%",
          paddingLeft: `${(100-(columnPercent-shadowPercent)*numColumns)/2}%`,
        }}>   
*/

export function getStoryWidth(numColumns){
  const columnPercent = Math.floor(100/numColumns);
  // 7 comes from the 5 px margin for .MuiGridListTile-root, tweaked to be slightly higher to account
  // for padding in any other spots
  // 2 is the number of margins applied per column (one for left, one for right)
  // multiply by 100 to get a percent
  const shadowPercent = ((2*7)/window.innerWidth)*100;
  return columnPercent - shadowPercent;
}

export function getGalleryPadding(numColumns){
  const storyWidth = getStoryWidth(numColumns);
  return (100-(storyWidth)*numColumns)/2
}