const {appUrl} = require('../../../support/app.json');
const {forceVisit,clickUserLink} = require('../../../support/actions/shared.js');
const {
  welcomePage,
  userPage,
} = require('../../../support/pages');

const {
  route,
  storyTileUserLink,
} = welcomePage;

const{
  userHeading,
  storyStats:{
    numStoriesCell,
    viewsCell,
    ratingCell,
  },
  promptStats:{
    numPromptsCell,
    promptsViewsCell,
    promptsRatingCell,
    numResponsesCell,
  },
  profileBio,
  profileTab,
  storiesTab,
  promptsTab,
  storiesGallery,
  storiesCarousel,
  expandedStoryDiv,
  storyDiv,
} = userPage;

describe('user', function() {
    let userName;
    beforeEach(()=>{
      forceVisit(appUrl,route);
      cy.get(storyTileUserLink)
        .first()
        .invoke('text')
        .then(x=>userName = x);
      clickUserLink();
    });

    it("displays the user name as the heading",()=>{
      cy.get(userHeading,{timeout:20000})
       .first()
       .contains(userName)
    });

    describe("stories tab",()=>{
      it("shows stats about the user's stories",()=>{
        cy.get(numStoriesCell)
        .invoke('text')
        .should(x=>{
          expect(parseInt(x)).to.be.at.least(1);
        })
        cy.get(ratingCell)
          .should('exist');
        cy.get(viewsCell)
          .invoke('text')
          .should(x=>{
            expect(parseInt(x)).to.be.at.least(0);
          })
      });
      it("should show the stories carousel",()=>{
        cy.get(storiesCarousel,{timeout:20000})
          .should('exist');
      });
      it("should show the stories gallery",()=>{
        cy.get(storiesGallery)
          .should('exist');
      });
      it("should expand a story (but leave the top navbar visible)",()=>{
        cy.get(storyDiv) 
          .click({
            force:true,
          });

        cy.get(expandedStoryDiv)
          .first()
          .should('have.css', 'top', '48px')
        });
    });

    describe("profile tab",()=>{
      
      beforeEach(()=>{
        cy.get(profileTab)
          .click();
      });

      it("shows the bio for the author",()=>{
        // Wait for page load, basically
        cy.get(storiesCarousel,{timeout:20000})
          .should('exist');
        cy.get(profileBio)
          .first()
          .should(x=>{
            expect(x.text().length).to.be.at.least(1);
          })
      });
      it("shows prompts stats",()=>{
        cy.get(numPromptsCell)
        .invoke('text')
        .should(x=>{
          expect(parseInt(x)).to.be.at.least(0);
        })
        cy.get(promptsRatingCell)
          .should('exist');
        cy.get(promptsViewsCell)
          .invoke('text')
          .should(x=>{
            expect(parseInt(x)).to.be.at.least(0);
          })
        cy.get(numResponsesCell)
          .invoke('text')
          .should(x=>{
            expect(parseInt(x)).to.be.at.least(0);
          })
      });
      it("shows story stats",()=>{
        cy.get(numStoriesCell)
        .invoke('text')
        .should(x=>{
          expect(parseInt(x)).to.be.at.least(1);
        })
        cy.get(ratingCell)
          .should('exist');
        cy.get(viewsCell)
          .invoke('text')
          .should(x=>{
            expect(parseInt(x)).to.be.at.least(0);
          })
      });
      it("shows the stories carousel",()=>{
        cy.get(storiesCarousel,{timeout:20000})
        .should('exist');
      });
    });
    
    describe("prompts tab",()=>{
      // TODO - A little hard to test
    })
});