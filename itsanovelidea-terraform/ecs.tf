resource "aws_iam_role" "itsanovelidea" {
  name = "itsanovelidea"
  path = "/ecs/"
  tags =  {
    project = "itsanovelidea"
  }

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "itsanovelidea" {
  name = "itsanovelidea"
  role = aws_iam_role.itsanovelidea.name
}

resource "aws_iam_role_policy_attachment" "itsanovelidea" {
  role = aws_iam_role.itsanovelidea.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_ecs_cluster" "itsanovelidea" {
  name = "itsanovelidea"
  tags =  {
    project = "itsanovelidea"
  }
}

module "this" {
  source  = "terraform-aws-modules/security-group/aws//modules/nfs"
  version = "~> 3.0"
  description = "EC2 to EFS"
  vpc_id = module.vpc.vpc_id
  name = "ec2-efs-itsanovelidea"
  number_of_computed_ingress_with_source_security_group_id=1
  computed_ingress_with_source_security_group_id=[{
    source_security_group_id = module.vpc.default_security_group_id
    rule = "nfs-tcp"
  }]
  ingress_cidr_blocks = ["10.0.0.0/16"]
}

resource "aws_efs_mount_target" "itsanovelidea-redis" {
  file_system_id = "fs-d448e054"
  subnet_id      = "${module.vpc.public_subnets[0]}"
  security_groups = [module.this.this_security_group_id]
}

resource "aws_efs_mount_target" "itsanovelidea-mongo" {
  file_system_id = "fs-d548e055"
  subnet_id      = "${module.vpc.public_subnets[0]}"
  security_groups = [module.this.this_security_group_id]
}

resource "aws_efs_mount_target" "itsanovelidea-mysql" {
  file_system_id = "fs-42ed70c2"
  subnet_id      = "${module.vpc.public_subnets[0]}"
  security_groups = [module.this.this_security_group_id]
}