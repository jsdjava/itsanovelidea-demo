import React from "react";
import PropTypes from "prop-types";
import ReactQuill from 'react-quill';
import ReconnectingWebSocket from 'reconnecting-websocket';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.bubble.css';

import quillShareDbCursors from "../helpers/quill-sharedb-cursors.js";
import {getCookie} from "../helpers/utils.js";

const ShareDB = require('@teamwork/sharedb/lib/client');
ShareDB.types.register(require('rich-text').type);

const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],
  ['blockquote', 'code-block'],
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  [{ 'color': [] }, { 'background': [] }],          
  [{ 'font': [] }],
  [{ 'align': [] }],
  ['clean'],                                         
  ['preview'],
];

const quillModules = {
  history: {
    userOnly: true
  },
  toolbar: toolbarOptions,
}; 
class ProfileBuilder extends React.Component{

  constructor(props){
    super(props);
    this.state = {};
    const {nodeUrl,userId} = props;
    const documentSocket = new ReconnectingWebSocket(`${nodeUrl}/ws/profiles?cookie=${getCookie('_validation_token_key')}`);
    const shareDb = new ShareDB.Connection(documentSocket);

    //TODO AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH   
    this.profileDocument = shareDb.get('profiles',""+userId);
  }

  render(){
    const {preview} = this.state;
    return <div>
      {
        preview ? <ArrowBackIcon onClick={()=>this.setState({
          preview:false,
        })}/> : null

      }
    <ReactQuill modules={quillModules}
      theme={preview ? "bubble" : "snow"}
      readOnly={preview}
      defaultValue=""
      ref={(reactQuill)=>{
        
        //TODO Not reactish, should maybe make some custom quill toolbars...
        const previewButton = document.querySelector('.ql-preview');
        if(previewButton){
          previewButton.addEventListener('click',()=>{
            console.log("how many times are u calling me....");
            this.setState({
              preview:true,
            })
          });
        }
        if(reactQuill && !this.quill){
          this.quill = reactQuill.getEditor();
          quillShareDbCursors(this.quill,this.profileDocument)
        } else{
          this.quill.enable();
        }
    }}/>
    </div>
  }
}

ProfileBuilder.propTypes = {
  nodeUrl: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
};

export default ProfileBuilder;