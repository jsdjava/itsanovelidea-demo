process.env.PIXABAY_API_KEY = 'apiKey';

const adaptiveBackground = require('adaptive-background');
const { redis } = require('itsanovelidea-middleware');
const request = require('request-promise');
const photo = require('./photo.js');

test('search should return cached results', async () => {
  redis.get.mockReturnValueOnce(Promise.resolve(JSON.stringify({
    url: 'asdf',
    size: 900,
  })));
  const photos = await photo.search('ayyy', 1, 10);
  expect(redis.get).toBeCalledWith('ayyy-1-10');
  expect(photos).toMatchObject({
    url: 'asdf',
    size: 900,
  });
});

test('search should search pixabay with the correct url', async () => {
  request.mockReturnValueOnce(Promise.resolve({
    url: 'xddd',
    size: 1000,
  }));
  const photos = await photo.search('cats', 1, 10);
  expect(request).toBeCalledWith(expect.objectContaining({
    uri: 'https://pixabay.com/api/?key=apiKey&q=cats&safesearch=true&page=1&per_page=10',
  }));
  expect(photos).toMatchObject({
    url: 'xddd',
    size: 1000,
  });
});

test('search should allow you to skip the cache', async () => {
  redis.get.mockReturnValueOnce(Promise.resolve(JSON.stringify({
    url: 'expiredUrl',
  })));
  request.mockReturnValueOnce(Promise.resolve({
    url: 'newUrl',
  }));
  const photos = await photo.search('ayyy', 1, 10, true);
  expect(photos).toMatchObject({
    url: 'newUrl',
  });
});

test('search should cache the results of a pixabay request', async () => {
  request.mockReturnValueOnce(Promise.resolve({
    url: 'josh',
  }));
  await photo.search('cats', 1, 10);
  expect(redis.set).toBeCalledWith('cats-1-10', JSON.stringify({
    url: 'josh',

    // I have to attach the search key to the results before they are cached because it
    // is used to reference the photo you want to prepare.
    searchKey: 'cats-1-10',
  }), expect.anything(), expect.anything());
});

test('prepare should return undefined on no cache hits for the searchKey', async () => {
  const processedPhoto = await photo.prepare('key', 1);
  expect(processedPhoto).toBeFalsy();
});

test('prepare should return undefined on an invalid index (no cache results at tht index)', async () => {
  redis.get.mockReturnValueOnce(Promise.resolve(JSON.stringify({
    hits: [
      '1st photo',
      '2nd photo',
    ],
  })));
  const processedPhoto = await photo.prepare('key', 2);
  expect(redis.get).toBeCalledWith('key');
  expect(processedPhoto).toBeFalsy();
});

test('prepare should return the processed image results', async () => {
  adaptiveBackground.mockReturnValueOnce(Promise.resolve({
    backgroundColor: 'A',
    textColor: 'B',
    luminanceClass: 'grey',
  }));

  redis.get.mockReturnValueOnce(Promise.resolve(JSON.stringify({
    hits: [
      '1st photo',
      {
        largeImageURL: 'google.com',
        id: 'mittens',
        imageHeight: 24,
        imageWidth: 24,
      },
    ],
  })));

  const processedPhoto = await photo.prepare('key', 1);
  expect(adaptiveBackground).toBeCalledWith('google.com');
  expect(processedPhoto).toMatchObject({
    largeImageURL: 'google.com',
    id: 'mittens',
    height: 24,
    width: 24,
    backgroundColor: 'A',
    textColor: 'B',
    luminanceClass: 'grey',
  });
});
