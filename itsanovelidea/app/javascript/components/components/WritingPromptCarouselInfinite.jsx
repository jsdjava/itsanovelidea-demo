import React from "react";
import Slide from '@material-ui/core/Slide';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import InfiniteScrollSearch from "../helpers/infiniteScrollSearch.js";
import "../stylesheets/WritingPromptCarouselInfinite.css";

class WritingPromptCarouselInfinite extends React.Component{

  constructor(){
      super();
      this.state = {
          pageSize:10,
          slideIndex:1,
          oldSlideIndex:0,
      };
  }

  componentDidMount(){
    const {
        loadWritingPrompts,
        onRefresh=()=>{},
    } = this.props;
    const {pageSize} = this.state;
    this.infiniteScrollSearch = new InfiniteScrollSearch(pageSize,loadWritingPrompts,(issRef,writingPrompts,direction)=>{
        if(issRef === this.infiniteScrollSearch){
            const {slideIndex} = this.state;
            this.setState({
                direction,
                oldSlideIndex: slideIndex,
                slideIndex: slideIndex+1,
            });
            onRefresh((this.infiniteScrollSearch.first()||{}).value);
        }
    });
    this.infiniteScrollSearch.load();
  }

  render(){
    if(!this.infiniteScrollSearch){
        return null;
    }
    const {oldSlideIndex,slideIndex,direction} = this.state;
    const writingPromptNode = this.infiniteScrollSearch.first() || {value:{}};
    const previousPromptNode = writingPromptNode.prev;
    const nextPromptNode = writingPromptNode.next; 
    const slidingOutText = ((direction >=0 ? previousPromptNode : nextPromptNode) || {value:{}}).value.text;
    const slidingInText = writingPromptNode.value.text;
    return <div className = "writingPromptCarouselInfiniteDiv">
    { writingPromptNode.value.text!==undefined ?
      <div>
      <ArrowBackIcon onClick={()=>previousPromptNode ? this.infiniteScrollSearch.previous(): ''}/>
      <Slide key={oldSlideIndex} direction={direction>=0 ? "right":"left"} in={false} mountOnEnter unmountOnExit timeout={500}>
          <p className="promptTextParagraphExit">{slidingOutText}</p>
      </Slide>
      <Slide key={slideIndex} direction={direction>=0 ? "left":"right"} in={true} mountOnEnter unmountOnExit timeout={500}>
          <p className="promptTextParagraph">{slidingInText}</p>
      </Slide>
      <ArrowForwardIcon onClick={()=>nextPromptNode ? this.infiniteScrollSearch.next(): ''}/></div>:
      "No writing prompts found"
    }
  </div>
  }
}

export default WritingPromptCarouselInfinite;