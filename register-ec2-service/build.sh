#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Build Docker Image"
docker build -t register-ec2-service .
echo "Push to ECR Repository"
docker tag register-ec2-service:latest $AWS_ECR_REPOSITORY/register-ec2-service:latest
docker push $AWS_ECR_REPOSITORY/register-ec2-service:latest