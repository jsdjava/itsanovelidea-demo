import React from "react";
import Grid from '@material-ui/core/Grid';
import PropTypes from "prop-types";

import ProfileViewer from "./ProfileViewer.jsx";
import ProfileBuilder from "./ProfileBuilder.jsx";
import ProfileRightColumn from "./ProfileRightColumn.jsx";

class ProfileV2 extends React.Component{
    constructor(){
        super();
        this.state = {};
    }

    render(){
        const {
            userId,
            userUrl,
            writingPromptsCountUrl,
            publishedStoriesCountUrl,
            editable,
            nodeUrl,
        } = this.props;
        return <div className="profileDiv">
            <Grid container>
                <Grid item sg={12} md={6}>
                  {editable ? <ProfileBuilder nodeUrl={nodeUrl} userId={userId}/> : <ProfileViewer userUrl={userUrl}/>}
                </Grid>
                <Grid item sg={12} md={6}>
                    <ProfileRightColumn
                        userUrl={userUrl} 
                        writingPromptsCountUrl={writingPromptsCountUrl}
                        userId={userId}
                        storiesCountUrl={publishedStoriesCountUrl}/>
                </Grid>
            </Grid>
        </div>;
    };
}

ProfileV2.propTypes = {
    nodeUrl: PropTypes.string.isRequired,
    userId: PropTypes.number.isRequired,
  };

export default ProfileV2;
