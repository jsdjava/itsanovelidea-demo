import React from "react";
import PropTypes from "prop-types";

import FloatingAddButton from "./components/FloatingAddButton.jsx";
import StoriesGallery from "./components/StoriesGallery.jsx";
import WritingPromptStories from "./components/WritingPromptStories.jsx";

class WritingPrompt extends React.Component {

  render () {
    const {
        createStoryUrl,
        writingPrompt,
        publishedStoriesUrl,
        publishedStoriesCountUrl,
        numPublishedStories,
    } = this.props;
    return <div>
      <div>
        <FloatingAddButton onClick = {()=>{window.location.href = createStoryUrl;}}/>
      </div>
      <WritingPromptStories writingPrompt={{
        ...writingPrompt,
        published_stories_url:publishedStoriesUrl,
        published_stories_count_url:publishedStoriesCountUrl
      }}>
        {
          ({stories,next,refreshStory,loading,storiesCount})=><StoriesGallery 
            fullScreen={true}
            loadedStories={!loading || stories.length}
            hasMore={storiesCount>stories.length}
            publishedStories={stories}
            next = {next}
            style={{
              overflowX:"hidden",
            }}
            refreshStory={refreshStory} 
            containerClassName="welcomePageDiv"
          />
        }
      </WritingPromptStories>
    </div>
  }
}

WritingPrompt.propTypes = {
  createStoryUrl: PropTypes.string.isRequired,
  writingPrompt: PropTypes.object.isRequired,
  publishedStoriesUrl: PropTypes.string,
};

export default WritingPrompt;