import React from 'react';
import WelcomeNavBar from "./WelcomeNavBar.jsx";

export default {
  component: WelcomeNavBar,
  title: 'WelcomeNavBar',
};

export const welcomeNavBar = () => <WelcomeNavBar/>;
