import React from "react"

import Grid from '@material-ui/core/Grid';

import UsersList from "./components/UsersList.jsx";
import DiscordV2 from "./components/DiscordV2.jsx";
import "./stylesheets/Users.css";

class Users extends React.Component {

  render () { 
    const {url,count_url} = this.props;
    return <div>
      <UsersList url={url} count_url={count_url}/>
      <DiscordV2/>
    </div>
  }
}

export default Users;
