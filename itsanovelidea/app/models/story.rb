require 'rest-client'
require 'json'
require 'date'

class Story < ApplicationRecord
    extend Filterable
    belongs_to :owner, class_name: "User"
    belongs_to :writing_prompt
    has_many :user_stories, dependent: :destroy
    has_many :users, through: :user_stories
    validates :owner, :writing_prompt, presence: true
    validates_length_of :name, minimum: 5, maximum: 30
    validates :photo_url, presence: true, allow_blank: false
    is_impressionable :counter_cache => true, :unique=>true
    before_destroy :destroy_impressions
    attr_accessor :computed_rating, :computed_email
    #t.string "photo_id"
    #t.string "photo_width"
    #t.string "photo_height"
    #t.string "photo_background_color"
    #t.string "photo_text_color"
     
  def rating
    self.attributes["computed_rating"] || self.user_stories.average(:rating) || "Not Rated"
  end

  def create_document()
    RestClient.post("https://#{Rails.configuration.node_host}/stories/"+self.id.to_s, {}.to_json, {
      apikey: Rails.configuration.node_api_key,
      content_type: :json,
      userid: self.owner.id
    })
  end

  def create_chat()
    RestClient.post(Rails.configuration.chat_host+"/chat",{
      resourceId:self.id.to_s,
      resourceType:"story",
    }.to_json,{
      apikey: Rails.configuration.node_api_key,
      userid: self.owner.id,
      content_type: :json
    })
  end

  def download_photo(search_hash,index)
    RestClient.post(Rails.configuration.photo_host+"/photo/"+URI.escape(search_hash, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))+"?index="+index.to_s,{}.to_json,{
      apikey: Rails.configuration.node_api_key,
    })
  end

  def get_photo_url
    RestClient.get(self.photo_id)
  end
  
  def update_photo_metadata(photo_metadata)
    self.photo_id = photo_metadata["idUrl"]
    self.photo_width = photo_metadata["width"]
    self.photo_height = photo_metadata["height"]
    self.photo_background_color = photo_metadata["backgroundColor"]
    self.photo_text_color = photo_metadata["textColor"]
  end

  def get_document_text
    RestClient.get "https://#{Rails.configuration.node_host}/stories/#{self.id.to_s}", {'apiKey' => Rails.configuration.node_api_key}
  end

  def get_document_version
    resp = RestClient.get "https://#{Rails.configuration.node_host}/stories/#{self.id.to_s}/history?numberResults=1&ascending=true", {'apiKey' => Rails.configuration.node_api_key}
    body = JSON.parse(resp.body)
    body[0]["newSnapshot"]['v']
  end

  def publish
    self.published_text = get_document_text
    self.published_version = get_document_version
    self.published_at = DateTime.now
    self.save!
  end

  def owner_email
    return self.attributes["computed_email"] || self.owner.email
  end

  def self.get_published_filtered_stories(params)
    params[:published] = true
    stories = filter_stories(params)
    stories.each {|story| story.inflate}
    stories
  end

  def self.get_published_filtered_stories_count(params)
    params[:published] = true
    stories_count = self.get_filtered_stories_count(params)
    stories_count
  end

  def self.get_filtered_stories_count(params)
    filter_stories(params,true)
  end

  def self.get_my_filtered_stories(user,params)
    params[:user] = user.id
    stories = filter_stories(params)
    stories.each {|story| story.inflate}
    stories
  end

  def self.stories_view(get_count=false)
    query = self.all
      .left_joins(:writing_prompt)
      .left_joins(:owner)
      .left_joins(:user_stories)
      .group(:id)
    return query if get_count
    query.select('
      stories.*,
      users.email as computed_email,
      COALESCE(AVG(user_stories.rating),"Not Rated") as computed_rating
    ')
  end

  def self.filter_stories(params,get_count=false)
    query = stories_view(get_count)
    query = filter_by_writing_prompt(params,query)
    query = filter_by_user(params,query)
    query = filter_by_like_user(params,query)
    query = filter_by_name(params,query)
    query = filter_by_published(params,query)
    query = sort_by_date(params,query)
    query = sort_by_views(params,query)
    query = sort_by_rating_v2(params,query)
    return query.count.values.sum if get_count
    expand(page(params,query))
  end
  
  def self.expand(query)
    writing_prompts = WritingPrompt.get_writing_prompts_by_ids(query.pluck(:writing_prompt_id))
    query.each {|x| x.writing_prompt = writing_prompts.find {|y| y.id == x.writing_prompt_id}}
    query
  end

  def self.sort_by_date(params,query=self.all)
    return query unless params[:sort_by_date]
    query.order('stories.created_at '+(params[:sort_by_date]==='asc' ? 'ASC' : 'DESC'))
  end

  def self.sort_by_views(params,query=self.all)
      return query unless params[:sort_by_views]
      query.order('stories.impressions_count '+(params[:sort_by_views]==='asc' ? 'ASC' : 'DESC'))
  end

  def self.sort_by_rating_v2(params, query=self.all)
    return query unless params[:sort_by_rating]
    query.order("AVG(user_stories.rating) #{params[:sort_by_rating]==='asc' ? 'asc' : 'desc'}")
  end

  def self.filter_by_writing_prompt(params,query=self.all)
      return query unless params[:writing_prompt_id]
      query.where(writing_prompt:params[:writing_prompt_id])
  end

  def self.filter_by_name(params,query=self.all)
    return query unless params[:name]
    query.where('name like ?',"%#{params[:name]}%")
  end

  def self.filter_by_published(params,query=self.all)
    return query unless params[:published]
    params[:published].to_s.downcase == "true" ? query.where.not(published_at:nil) :  query.where(published_at:nil)
  end

  # TODO Need to rethink this a little, really just want to inflate the urls...
  def inflate
    begin
      self.text = self.get_document_text.body
      # When using the importer, the photo_url will be nil to start
      if self.photo_url.nil? || self.photo_url.start_with?("https://pixabay.com")
        self.photo_url = self.get_photo_url
        self.save!
      end
    rescue StandardError => msg 
      puts "AHHHHHHHHHHHHHHHHH";
      #It's fine, we inflated what we could
      puts msg
    end
  end

  def get_versions(params)
    response = RestClient.get "https://#{Rails.configuration.node_host}/stories/"+self.id.to_s+"/history?"+"version="+params[:version]+"&numberResults="+params[:numberResults]+"&ascending="+params[:ascending], {'apiKey' => Rails.configuration.node_api_key}
    snapshotVersions = JSON.parse(response.body)
    return snapshotVersions.to_json
  end

  def owned?(user)
    self.owner == user
  end

  def destroy_impressions
    Impression.where(impressionable_type:"Story",impressionable_id:self.id).destroy_all
  end

end
