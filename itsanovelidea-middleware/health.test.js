const expressHealth = require('express-nemo-route-health');
const MockExpressResponse = require('mock-express-response');

const {
  healthCheck,
  health,
} = require('./health.js');

test('healthCheck returns a healthy object on no errors from getHealth()', async () => {
  const resp = await healthCheck()();
  expect(resp).toEqual({
    healthy: true,
  });
});

test('healthCheck times out hanging getHealth() calls', async () => {
  // Will never resolve, so needs to get timed out or the test will time out
  // eslint-disable-next-line
  await healthCheck(() => new Promise((resolve) => {}))();
});

test('healthCheck spreads object responses from getHealth() in the health object', async () => {
  const resp = await healthCheck(() => ({
    url: 'www.google.com',
  }))();
  expect(resp).toEqual(expect.objectContaining({
    url: 'www.google.com',
  }));
});

test('healthCheck returns the toStringed() error returned by getHealth()', async () => {
  const resp = await healthCheck(() => {
    throw Error("Couldn't connect to redis");
  })();
  expect(resp).toEqual(Error("Couldn't connect to redis").toString());
});

test('healthCheck returns a default error when the error is falsy', async () => {
  const resp = await healthCheck(() => {
    // eslint-disable-next-line
    throw 0;
  })();
  expect(resp).toEqual('Undefined error thrown!');
});

test('the health check response is pretty printed', () => {
  const mockExpressResponse = new MockExpressResponse();
  health({});
  const { responseTemplate } = expressHealth.mock.calls[0][0];
  responseTemplate([{
    'itsanovelidea-middleware': {
      healthy: true,
    },
  }], undefined, mockExpressResponse);

  // eslint-disable-next-line
  expect(mockExpressResponse._responseData.toString('utf8')).toEqual(JSON.stringify([{
    'itsanovelidea-middleware': {
      healthy: true,
    },
  }], null, 2));
});

test('the health check response returns 200 on all healthy dependencies', () => {
  const mockExpressResponse = new MockExpressResponse();
  health({});
  const { responseTemplate } = expressHealth.mock.calls[0][0];
  responseTemplate([{
    status: {
      healthy: true,
    },
  }, {
    status: {
      healthy: true,
    },
  }], undefined, mockExpressResponse);
  expect(mockExpressResponse.statusCode).toEqual(200);
});

test('the health check response returns 424 on any non-healthy dependency', () => {
  const mockExpressResponse = new MockExpressResponse();
  health({});
  const { responseTemplate } = expressHealth.mock.calls[0][0];
  responseTemplate([{
    status: {
      healthy: true,
    },
  }, {
    status: {
      healthy: false,
    },
  }], undefined, mockExpressResponse);
  expect(mockExpressResponse.statusCode).toEqual(424);
});

test('health check maps from a health object to the express-nemo-route-health array', () => {
  health({
    itsanovelidea: 'A',
    redis: 'B',
  });
  const { checks } = expressHealth.mock.calls[0][0];
  expect(checks).toEqual([
    {
      name: 'itsanovelidea',
      check: 'A',
    },
    {
      name: 'redis',
      check: 'B',
    },
  ]);
});
