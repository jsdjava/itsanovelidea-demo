import React from "react";
import {Flipped } from 'react-flip-toolkit';

import Story from "./Story.jsx";
import Rating from "./Rate.jsx";
import Views from "./Views.jsx";
import StoryViewer from "./StoryViewer.jsx";

import "../stylesheets/StoryTile.css";
import RedditIconLink from "./RedditIconLink.jsx";
import LinkIcon from '@material-ui/icons/Link';

class StoryTile extends React.Component {
    render(){
        const {
            imageProps,
            photo:{
                publishedStorySpot,
                url,
                width,
                height,
                title,
                text,
                imageHeight,
                author,
                authorUrl,
                rating,
                views,
                redditLink,
            }
        } = this.props;

        //TODO I put this in just for the storybook
        const fullImageUrl = url.startsWith("http") ? url : `${window.location.origin}/${url}`;
        return <Story {...this.props} containerClassName = "storyTileDiv" StoryImage={({flippedProps})=><img {...{...imageProps,...flippedProps}} 
            src={fullImageUrl} 
            width={width} 
            height={imageHeight || height}/> 
        } StoryText={({flippedProps})=><div>
        {this.props.children}
        <Flipped flipId={`header-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
            return publishedStorySpot==(prevStorySpot || curStorySpot);
        }}>
            <div className="storyContentHeaderDiv" style={{height:"68px", willChange:"transform"}}>
            <Flipped inverseFlipId={`header-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
                return publishedStorySpot==(prevStorySpot || curStorySpot);
            }}>                
            <div style={{
                willChange:"transform"
            }}>
                <h3 className="storyContentHeaderTitle"> {title} </h3>
                <p className="storyContentHeaderAuthor"> &nbsp;by&nbsp;&nbsp;<a href={authorUrl} onClick={(e)=>{
                  e.stopPropagation();
                }}>{author}</a> </p>
                <div className="storyContentHeaderBottomRow">
                    <Rating editable={false} rating={rating} key={rating}/>
                    <Views views={views}/>
                    <RedditIconLink link={redditLink}/>
                    <LinkIcon link={url} target="_blank"/>
                </div>
                </div>
                </Flipped>
            </div>
            </Flipped>
            <StoryViewer story={text} imageUrl={fullImageUrl} publishedStorySpot={publishedStorySpot}/>
            </div>
        }/>
    }
};

export default StoryTile;
