class ChangeUserStoryRatingsToUserStory < ActiveRecord::Migration[5.1]
  def change
    remove_column :user_story_ratings, :rating
    rename_table :user_story_ratings, :user_story
  end
end
