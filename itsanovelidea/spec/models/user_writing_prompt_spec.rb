require 'rails_helper'

RSpec.describe UserWritingPrompt, type: :model do
  user = FactoryBot.create(:user)
  writing_prompt = FactoryBot.create(:writing_prompt)

  it "is valid with valid attributes" do
    expect(UserWritingPrompt.new(user:user,writing_prompt:writing_prompt,rating:4)).to be_valid
  end
  it "is not valid without a user" do
    expect(UserWritingPrompt.new(writing_prompt:writing_prompt,rating:3)).to_not be_valid
  end
  it "is not valid without a writing_prompt" do
    expect(UserWritingPrompt.new(user:user,rating:3)).to_not be_valid
  end
  it "is not valid without rating" do
    expect(UserWritingPrompt.new(user:user,writing_prompt:writing_prompt)).to_not be_valid
  end
  it "is not valid with too small rating" do
    expect(UserWritingPrompt.new(user:user,writing_prompt:writing_prompt,rating:0)).to_not be_valid
  end
  it "is not valid with too large rating" do
    expect(UserWritingPrompt.new(user:user,writing_prompt:writing_prompt,rating:7)).to_not be_valid
  end
end
