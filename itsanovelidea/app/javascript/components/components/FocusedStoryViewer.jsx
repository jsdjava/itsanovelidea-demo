import React from "react";
import ReactQuill from 'react-quill';
import {Flipped } from 'react-flip-toolkit';
import Collapse from '@material-ui/core/Collapse';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import IconButton from '@material-ui/core/IconButton';

import 'react-quill/dist/quill.bubble.css';
import 'react-quill/dist/quill.snow.css';

class StoryViewer extends React.Component{

  constructor(props){
    super(props);
    this.state = {};
  }

  render(){
    const {
        imageUrl,
        className,
        publishedStorySpot,
        writingPromptText,
        writingPromptUrl,
    } = this.props;
    const {
      showPrompt
    } = this.state;
    return<Flipped flipId={`body-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}>
     <div className = "focusedStoryViewerDiv" style={{
      willChange:"transform",
    }}> 
     <Flipped inverseFlipId={`body-${publishedStorySpot}`} shouldInvert={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}>
    <div className="containerDiv" style={{
      willChange:"transform",
    }}>
      <IconButton style={{    
        height: "14px",
        position: "absolute",
        right: "0px",
        zIndex: "10000",
      }} onClick={()=>{
        this.setState({
          showPrompt:!showPrompt,
        });
      }}>
      {showPrompt ? <ArrowDropUpIcon style={{
        position: "absolute",
        right:"0px",
      }}/> : < ArrowDropDownIcon style={{
        position: "absolute",
        right:"0px",
      }}/>}
      </IconButton>
      <Collapse in={showPrompt}>
        <p><a style ={{color:"black"}} href={writingPromptUrl}>{writingPromptText}</a></p>
      </Collapse>
     <Flipped flipId={`image-${publishedStorySpot}`} shouldFlip={(prevStorySpot,curStorySpot)=>{
      return publishedStorySpot==(prevStorySpot || curStorySpot);
    }}>
    <img className = {className} src={imageUrl} style={{
      willChange:"transform",
    }}/>
    </Flipped>
    <ReactQuill
      className={className}
      theme={"bubble"}
      readOnly={true}
      ref={(reactQuill)=>{
        if(reactQuill && !this.quill){
          const {story} = this.props;
          let parsedStory = {};
          try{
            parsedStory = JSON.parse(story).data;
          }catch(err){
            console.log("Couldn't parse the text for the story");
            console.log(err);
            console.log(story);
          }
          this.quill = reactQuill.getEditor();
          this.quill.setContents(parsedStory);

          // TODO Not sure if i need this or not .....
          //this.quill.enable();
        }
    }}
    />
    </div>
    </Flipped>
    </div>
    </Flipped>
  }
}

export default StoryViewer;