
//TODO Refactor/cleanup a little
export async function getMatchingStories({url},{page,pageSize,filters=[]}){
  const combinedFilters = [...filters,{
    id:'page',
    value:page,
  },{
    id:'pageSize',
    value:pageSize,
  }];
  const storiesUrl = combinedFilters.filter(({value})=>value).reduce((combinedFilterStr,{id,value})=>`${combinedFilterStr}${id}=${value}&`,".json?");
  const stories = await fetch(`${url}${storiesUrl}`).then(resp=>resp.json());
  return {
    stories,
  };
};

export async function fetchWithToken(url,method='GET',body, overrideHeaders={},overrideFetch={}){
  console.log(url);
  const token = document.querySelector("meta[name='csrf-token']").content;
  return fetch(url,{
    method,
    headers:{
      'X-CSRF-Token': token,  
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...overrideHeaders,
    },
    body,
    ...overrideFetch
  })
}

export async function getUser(userUrl){
  try{
    const resp = await fetchWithToken(userUrl);
    if(resp.ok){
      const user = await resp.json();
      return user;
    } else{
      throw resp;
    }
  }catch(e){
    console.log("Could not get user");
    console.log(e);
  }
}

export async function getWritingPromptsCount (writingPromptsCountUrl,userId){
  try{
    const resp = await fetchWithToken(`${writingPromptsCountUrl}?user=${userId}`);
    if(resp.ok){
      const writingPromptsCount = await resp.json();
      return writingPromptsCount.num_writing_prompts;
    }else{
      throw resp;
    }
  }catch(err){
    console.log("Could not get writing prompts count");
    console.log(err);
  }
}

export async function getStoriesCount(storiesCountUrl,userId){
  try{
    const resp = await fetchWithToken(`${storiesCountUrl}${userId ? `?user=${userId}`:""}`);
    if(resp.ok){
      const storiesCount = await resp.json();
      return storiesCount.stories_count;
    } else{
      throw resp;
    }
  }catch(err){
    console.log("Could not get stories count");
    console.log(err);
  }
}