import React from 'react';
import DashboardNavBar from "./DashboardNavBar.jsx";

export default {
  component: DashboardNavBar,
  title: 'DashboardNavBar',
};

export const dashboardNavBar = () => <DashboardNavBar/>;

export const dashboardNavBarWithLinks = ()=><DashboardNavBar urls={{
    "Item 1":"a",
    "Item 2":"b",
    "Item 3":"c",
}}/>;