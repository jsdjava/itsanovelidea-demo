class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:show]

  def users_count
    @users_count = User.sort_users(params,true)
    render :json => {users_count:@users_count}
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.sort_users(params)
  end

  def show
    @node_url = 'wss://'+Rails.application.config.node_host
    @user = User.find_by_id(params[:id])
  end

  def wipe

    # Keep this in even after the apikey is lifted, this endpoint isn't ready for normal
    # users yet, but i need it to cleanup
    if request.headers["apikey"] != Rails.configuration.node_api_key
			head 403
		end
    WritingPrompt.where(owner:current_user).destroy_all
    Story.where(owner:current_user).destroy_all
    current_user.wipe
    head 200
  end

end
