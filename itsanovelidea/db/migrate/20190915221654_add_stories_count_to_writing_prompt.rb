class AddStoriesCountToWritingPrompt < ActiveRecord::Migration[5.1]
  def change
    add_column :writing_prompts, :stories_count, :integer
  end
end
