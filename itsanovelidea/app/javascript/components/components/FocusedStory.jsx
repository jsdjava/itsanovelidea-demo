import React from "react";
import { Flipped } from 'react-flip-toolkit';
import LinkIcon from '@material-ui/icons/Link';

import Views from "./Views.jsx";
import Rate from "./Rate.jsx";
import RedditIconLink from "./RedditIconLink.jsx";
import FocusedStoryViewer from "./FocusedStoryViewer.jsx";

import "../stylesheets/FocusedStory.css";
import { Link } from "@material-ui/core";

class FocusedStory extends React.Component{

  constructor(){
    super();
    this.state={};
    this.showElements = this.showElements.bind(this);
  }

  showElements(){
    this.setState({
      unhideChildren:true,
    })
  }

  render() {
    const {
      publishedStory,
      publishedStorySpot, 
      refreshStory, 
      close,
      viewOnly,
      hideX,
      fullScreen,
    } = this.props;
    const {unhideChildren} = this.state; 
    const {photoBackgroundColor,photoUrl,name,text,rateUrl,url,owner,impressions_count,redditLink, writingPromptText,writingPromptUrl} = publishedStory;
    const author = owner ? owner.email : "";
    const authorUrl = owner ? owner.url : "";
    const hideClass = (classNames="")=>`${classNames} ${true || unhideChildren ? "" :"hideChildren"}`;
    //TODO Only in here because of storybook
    const backgroundImageUrl = photoUrl.startsWith("http") ? photoUrl : `${window.location.origin}/${photoUrl}`; 
    return (<Flipped flipId={`card-${publishedStorySpot}`} onComplete={this.showElements}>
      <div className="focusedItemBackground" style={{
        top: fullScreen ? "0px" : "48px",
        willChange:"transform",
        backgroundColor:photoBackgroundColor,
      }}>
      <Flipped inverseFlipId={`card-${publishedStorySpot}`}>
          <div
          style={{
            willChange:"transform",
          }}
            className="gridItem gridItemFocused"
            ref={el => (this.el = el)}>
                <div>
                    <div id="publishedStoryExpandedDiv">
                      <div id="publishedStoryContainerDiv">
                        <Flipped flipId={`header-${publishedStorySpot}`}>
                          <div className="focusedStoryContentHeaderDiv"  style={{
                                  willChange:"transform",
                        }}>
                          <Flipped inverseFlipId={`header-${publishedStorySpot}`} >
                          <div style = {{
                                    willChange:"transform",
                          }}>
                            {
                              hideX ? null : <button
                                  className={hideClass("smallCloseStoryBtn")}
                                  data-fade-in
                                  onClick={close}>
                                  ✖
                              </button>
                            }
                              <h3 className={hideClass("focusedStoryContentHeaderTitle")}> {name} </h3>
                            <p className={hideClass("focusedStoryContentHeaderAuthor")}> &nbsp;by&nbsp;&nbsp;<a onClick={(e)=>{
                              e.stopPropagation();
                            }}href={authorUrl}>{author}</a> </p>
                            <div className="focusedStoryContentHeaderBottomRow">
                              <Rate url={url}/> {
                                viewOnly ? null :<div>|
                                   <Rate url={rateUrl} editable={true} onUpdatedRating={refreshStory} className={hideClass()}/>
                                </div>
                              }
                              <div style={{
                                marginLeft:"auto", 
                              }}>
                                <Views views={impressions_count} style={{float:"right"}}/>
                                <RedditIconLink link={redditLink}/>
                                <LinkIcon onClick={()=>{
                                  window.location.href = url.split(".json")[0];
                                }}/>
                              </div>
                          </div>
                          </div>
                          </Flipped>
                          </div>
                        </Flipped>
                      <FocusedStoryViewer 
                        story={text} 
                        imageUrl = {backgroundImageUrl} 
                        className={hideClass()} 
                        publishedStorySpot={publishedStorySpot}
                        writingPromptText={writingPromptText}
                        writingPromptUrl={writingPromptUrl}
                      />
                      </div>
                    </div>
                  </div>
          </div>
          </Flipped>
      </div>
      </Flipped>
    );
  }
}
                         
export default FocusedStory;