import React from "react";
import PropTypes from "prop-types";
import ReactQuill from 'react-quill';

import 'react-quill/dist/quill.bubble.css';

import {getUser} from "../helpers/api.js";


class ProfileViewer extends React.Component{

  constructor(props){
    super(props);
    this.state = {};
    this.getUser = this.getUser.bind(this);
    this.setQuillText = this.setQuillText.bind(this);
  }
  
  //TODO I think I can move this call up, im doubling it up with ProfileRightColumn...
  async getUser(){
      const {userUrl} = this.props;
      const user = await getUser(userUrl);
      if(user){
        this.setState({
          user,
        });
      }
  }

  async componentDidMount(){
    await this.getUser();
    this.setQuillText();
  }

  setQuillText(){
    const {user} = this.state;  
    if(user && this.quill){
      const userBio = user.profile.data ? user.profile.data : {ops:[
        {insert:"Apparently, this writer prefers to keep an air of mystery about them"},
      ]};
      console.log(userBio);
      this.quill.setContents(userBio);
      this.quill.disable();
    }
  }

  render(){
    return <div>
    <ReactQuill
      theme="bubble"
      readOnly={true}
      ref={(reactQuill)=>{
        if(reactQuill && !this.quill){
          this.quill = reactQuill.getEditor();
          this.setQuillText();
        }
    }}
    />
    </div>
  }
}

ProfileViewer.propTypes = {
  userUrl: PropTypes.string.isRequired,
};

export default ProfileViewer;