process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

const webpackConfig = environment.toWebpackConfig();

const {use} = webpackConfig.module.rules.find(x=>x.use && x.use[1] && x.use[1].loader && x.use[1].loader.includes("css-loader"))
use[1].options = {}

module.exports = webpackConfig;
