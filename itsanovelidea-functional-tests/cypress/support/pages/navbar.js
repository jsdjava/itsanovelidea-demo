module.exports = {
    userLink:".Header h6",
    storiesLink:".Header button:nth-child(2)",
    promptsLink:".Header button:nth-child(3)",
    authorsLink:".Header button:nth-child(4)",
    logOutBtn:".Header button:nth-child(5)",
}