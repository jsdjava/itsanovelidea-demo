import 'react-quill/dist/quill.snow.css';
import React, { Component} from 'react'
import Button from '@material-ui/core/Button';
import PropTypes from "prop-types"
const ShareDB = require('@teamwork/sharedb/lib/client');
const {uniqBy,get} = require('lodash'); 
const moment = require('moment');
import ReconnectingWebSocket from 'reconnecting-websocket';
import Grid from '@material-ui/core/Grid';

import StoryQuill from "./components/StoryQuill.jsx";
import StoryHistory from "./components/StoryHistory.jsx";
import quillShareDbCursors from "./helpers/quill-sharedb-cursors.js";
import {fetchWithToken} from './helpers/api';
import {success,error} from "./helpers/notifications.js";
import {getCookie} from "./helpers/utils.js";

import 'react-toastify/dist/ReactToastify.css';
import "./stylesheets/EditStory.css";

ShareDB.types.register(require('rich-text').type);

export default class EditStory extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      document:"",
      documentVersions:[],
      hasMoreVersions:true,
      activeTab:'1',
      story:{},
    };
    const {nodeUrl,documentId} = props;
    const documentSocket = new ReconnectingWebSocket(`${nodeUrl}/ws/stories?storyId=${documentId}&cookie=${getCookie('_validation_token_key')}`);
    const shareDb = new ShareDB.Connection(documentSocket);

    //TODO This is doubled up in the node server
    this.storyDocument = shareDb.get('stories',documentId);

    this.getDocumentVersions = this.getDocumentVersions.bind(this);
    this.publish = this.publish.bind(this);
    this.registerQuillSocket = this.registerQuillSocket.bind(this);
    this.revert = this.revert.bind(this);
    this.exit = this.exit.bind(this);
    this.getStory = this.getStory.bind(this);
  }
  
  async publish(){
    const {publishUrl} = this.props;
    try{
      const resp = await fetchWithToken(`${publishUrl}`,'POST');
      if(!resp.ok){
        throw resp;
      }
      success("Story published!");
      this.getStory();
    } catch(err){
      error("Could not publish story");
      console.log("Could not publish story");
      console.log(err);
    }
  }

  async getStory(){
    const {storyUrl} = this.props;
    try{
      const resp = await fetchWithToken(storyUrl);
      const story = await resp.json();
      this.setState({story});
    } catch(e){
      console.log("Could not fetch story");
      console.log(e);
    }
  }

  getDocumentVersions(direction=-1){
    const {railsUrl} = this.props;
    const {documentVersions} = this.state;
    const [firstVersion] = documentVersions;
    const lastVersion = documentVersions[documentVersions.length-1];
    const version = direction == -1 ? lastVersion : firstVersion;
    fetchWithToken(`${railsUrl}?version=${version ? version.oldSnapshot.v : 0}&numberResults=10&ascending=${direction>0}`).then(response=>{
        if(response.ok){
          response.json().then((resp)=>{
            const mergedVersions = [...documentVersions];
            // Need to dedupe versions so that we don't have multiple version ranging like:
            // - 1 -> 5
            // - 1 -> 8
            // - 1 -> 18
            // as we type and add more to the document
            for(const newVersion of resp){
              const matchedVersionIndex = documentVersions.findIndex(v=>v.oldSnapshot.v === newVersion.oldSnapshot.v);
              if(~matchedVersionIndex){
                // its a dupe, figure out if the new version is the winner
                // REST requests can return out of order. make sure that the "new" version
                // really has the newest (highest) version
                if(documentVersions[matchedVersionIndex].newSnapshot.v < newVersion.newSnapshot.v){
                  mergedVersions[matchedVersionIndex] = newVersion;
                }
                continue;
              }
              // We've crossed into a new milestone version, so its a completely new version
              mergedVersions.push(newVersion);
            }
            const sortedVersions = mergedVersions.sort((v1,v2)=>v2.oldSnapshot.v-v1.oldSnapshot.v);
            const earliestVersion = sortedVersions[sortedVersions.length-1];
            const [latestVersion] = sortedVersions;
            this.setState({
              documentVersions:sortedVersions,
              // Not sure about this...
              hasMoreVersions: earliestVersion ? earliestVersion.oldSnapshot.v!==1 : false,
              latestVersion: get(latestVersion,"oldSnapshot.v"),
            })
          });
        }
      }).catch(e=>{
        console.log("Couldn't load document versions");
        console.log(e);
      });
  }

  registerQuillSocket(quill){
    const {revert} = quillShareDbCursors(quill,this.storyDocument,()=>this.getDocumentVersions(1));
    this.revertDocument = revert;
  }

  revert(){
    const {oldDocumentSnapshot} = this.state;
    this.setState({
      oldDocument:null
    },()=>this.revertDocument(oldDocumentSnapshot));
  }

  exit(){
    this.setState({
      oldDocument:null,
    })
  }

  componentDidMount(){
    this.getDocumentVersions();
    this.getStory();
  }

  render(){
    const {
      writingPromptText,
      storyTitle,
    } = this.props;
    const {
      documentVersions,
      hasMoreVersions,
      oldDocument,
      story,
      latestVersion,
    } = this.state;
    let publishFooter = <div className="publishFooter">
      <Button onClick={()=>this.publish()}>Publish!</Button>
    </div>;
    if(story.published_at){
      publishFooter = <div className="publishFooter">
        { latestVersion && latestVersion !== story.published_version ? <Button onClick={()=>this.publish()}>Update</Button> : null}
        <p>Last published on {moment(story.published_at).format("MMMM Do, YYYY hh:mm:ss a")}  </p>
      </div>;
    }
    return <div className="editStoryDiv">
      <p className="writingPromptPara">{writingPromptText}</p>
      <Grid container>
      <Grid item sg={12} md={9}>
        <StoryQuill 
          storyTitle={storyTitle} 
          oldDocument={oldDocument} 
          registerQuillSocket={this.registerQuillSocket} 
          onRevert={this.revert} 
          onExit={this.exit}/>
          {publishFooter}
      </Grid>
      <Grid item sg={12} md={3}>
        <StoryHistory
          getHistory={()=>this.getDocumentVersions()}
          hasMoreHistory={hasMoreVersions}
          onHistorySelected={(documentVersion)=>{
            this.setState({
              oldDocumentSnapshot:documentVersion.oldSnapshot,
              oldDocument:documentVersion.oldSnapshot,
            });
          }}
          history={documentVersions}
        />
      </Grid>
    </Grid>
    </div>;
  }
}

EditStory.propTypes = {
  writingPromptText: PropTypes.string.isRequired,
  documentId:PropTypes.string.isRequired,
  nodeUrl: PropTypes.string.isRequired,
  railsUrl: PropTypes.string.isRequired,
  storyTitle: PropTypes.string.isRequired,
  publishUrl: PropTypes.string.isRequired,
  storyUrl: PropTypes.string.isRequired,
};