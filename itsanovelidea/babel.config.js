const fs = require('fs');

module.exports = {
    ...JSON.parse(fs.readFileSync("./.babelrc")),
    "env": {
        "test": {
            "plugins": ["@babel/plugin-transform-modules-commonjs","require-context-hook"]
        }
    }
}
