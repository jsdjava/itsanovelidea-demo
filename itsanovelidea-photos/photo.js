const requestPromise = require('request-promise');
const { redis, logger } = require('itsanovelidea-middleware');

const adaptiveBackground = require('adaptive-background');

const pixabayKey = process.env.PIXABAY_API_KEY;

// Search for photos in the pixabay API
const search = async (searchTerm, page, numResults, skipCache) => {
  const searchKey = `${searchTerm}-${page}-${numResults}`;
  // In order to not hit the pixabay API too much, I cache the results of each
  // search request in redis.
  const photoSearchKey = encodeURIComponent(searchKey);
  const cachedPhotos = await redis.get(photoSearchKey);

  // Pixabay api is totally inconsistent and will expire urls completely randomly, so I need
  // a way for the client to call back and skip the cache
  if (cachedPhotos && !skipCache) {
    return JSON.parse(cachedPhotos);
  }
  const photos = await requestPromise({
    uri: `https://pixabay.com/api/?key=${pixabayKey}&q=${searchTerm}&safesearch=true&page=${page}&per_page=${numResults}`,
    json: true,
  });

  // Modify the pixabay response to add the search key property that the caller can use to
  // select the photo page they want to prepare (passed into `prepare()`)
  photos.searchKey = searchKey;
  logger.log('info', `Caching ${photoSearchKey}`);

  // So, pixabay claims hotlinked urls are valid for 24 hours, but in practice they
  // seem to expire much earlier.
  // Instead, I'm just going to kill them off after 3 hours.
  await redis.set(photoSearchKey, JSON.stringify(photos), 'EX', 60 * 60 * 3);

  return photos;
};

// When a user actually selects a photo for their story, this performs some processing on it
// to figure out what the main colors in it are. The search must have already been performed
// (and cached) using search(), otherwise the call returns undefined
const prepare = async (searchKey, index) => {
  logger.log('info', `Reading ${encodeURIComponent(searchKey)}`);

  // If there are no results, then redis.get returns undefined so I need to default to a
  // valid stringified value (null) to prevent the JSON.parse from throwing an exception
  const results = JSON.parse(await redis.get(encodeURIComponent(searchKey)) || 'null');
  if (!results || !results.hits[index]) {
    logger.log('error', `${searchKey} not found`);
    return;
  }
  const {
    largeImageURL,
    id,
    imageHeight,
    imageWidth,
  } = results.hits[parseInt(index, 10)];
  logger.log('info', `Sending ${largeImageURL} to be processed`);
  const {
    backgroundColor,
    textColor,
    luminanceClass,
  } = await adaptiveBackground(largeImageURL);

  // eslint-disable-next-line
  return {
    largeImageURL,
    id,
    backgroundColor,
    textColor,
    luminanceClass,
    height: imageHeight,
    width: imageWidth,
  };
};

module.exports = {
  search,
  prepare,
};
