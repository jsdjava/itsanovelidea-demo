json.extract! story, :id, :text, :name, :created_at, :updated_at, :impressions_count, :published_version, :published_at
json.owner do 
	json.email story.owner_email
	json.url user_path(story.owner_id)
end
json.writing_prompt do 
	json.partial! 'writing_prompts/writing_prompt', writing_prompt: story.writing_prompt
end
json.rating story.rating
json.isOwned current_user.nil? ? false :current_user.id == story.owner_id
json.rateUrl rate_story_path(story)
json.url story_url(story, format: :json)
json.photoUrl story.photo_url
json.photoWidth story.photo_width
json.photoHeight story.photo_height
json.photoBackgroundColor story.photo_background_color
json.photoTextColor story.photo_text_color 	
json.redditLink "https://www.reddit.com#{story.reddit_permalink}"
