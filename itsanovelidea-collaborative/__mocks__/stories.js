/*
 Because I need to inject shareDb into stories, its coded as a function
 that returns getStoryHistory
*/

const getStoryHistory = jest.fn();
const getStory = jest.fn();
const createStory = jest.fn();
module.exports = () => ({
  getStoryHistory,
  getStory,
  createStory,
});
