import React from 'react';

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

import "../stylesheets/DashboardMenu.css";

class DashboardMenu extends React.Component{
  constructor(){
      super();
      this.state = {};
  }

  render(){
    const closeMenu = ()=>this.setState({isOpen:false});
    const {
        onClickProfile=closeMenu,
        onClickMyStories=closeMenu,
        onClickMyPrompts=closeMenu,
    } = this.props;
    const {isOpen} = this.state;
    return <div className="dashboardMenu">
      <div>
        {
            this.props.children(()=>this.setState({isOpen:!isOpen}))
        }
      <Popper open = {isOpen} transition disablePortal>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={()=>{this.setState({isOpen:false})}}>
                <MenuList id="menu-list-grow">
                  <MenuItem onClick={onClickProfile}>Profile</MenuItem>
                  <MenuItem onClick={onClickMyStories}>My stories</MenuItem>
                  <MenuItem onClick={onClickMyPrompts}>My prompts</MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  </div>
  }
}

export default DashboardMenu;
