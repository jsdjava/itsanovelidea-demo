class AddPublishFieldsToStories < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :published_version, :int
    add_column :stories, :published_at, :datetime
  end
end
