module "itsanovelidea-service-main" {
  source = "./itsanovelidea-service"
  itsanovelidea_service_name = "itsanovelidea-service-main"
  itsanovelidea_cluster_id=aws_ecs_cluster.itsanovelidea.id
  itsanovelidea_task_definition_arn=aws_ecs_task_definition.itsanovelidea-main-task.arn
  itsanovelidea_vpc_subnets=module.vpc.public_subnets
  itsanovelidea_subdomain_prefix = "main"
}

resource "aws_ecs_task_definition" "itsanovelidea-main-task" {
  family                = "itsanovelidea-main-task"
  requires_compatibilities = ["EC2"]
  container_definitions = "${file("ecs/itsanovelidea-main.json")}"
  # So this if fun. Terraform seems bugged, and won't just read the fun
  # arn from the dynamically created task role...
  #execution_role_arn="${module.itsanovelidea-service-main.task_role_id}"
  execution_role_arn="arn:aws:iam::242929592693:role/itsanovelidea-service-main-task"
  volume {
    name      = "itsanovelidea-storage"
  }
  tags =  {
    project = "itsanovelidea-a"
  }
  placement_constraints {
    type       = "memberOf"
    expression = "attribute:itsanovelidea-type==main"
  }
}
