# Overview
Pretty straight-forward nginx reverse proxy. This is intended to be run inside the ECS for the main
itsanovelidea app. It grabs the cert for itsanovelidea from the secrets manager, and then starts nginx,
which proxies over to the itsanovelidea rails docker image after forcing an upgrade from http to https.


