resource "aws_efs_file_system" "itsanovelidea-mongo" {
  creation_token = "itsanovelidea-mongo"

  tags = {
    Name = "itsanovelidea-mongo"
  }
}

resource "aws_efs_file_system" "itsanovelidea-redis" {
  creation_token = "itsanovelidea-redis"

  tags = {
    Name = "itsanovelidea-redis"
  }
}

resource "aws_efs_file_system" "itsanovelidea-mysql" {
  creation_token = "itsanovelidea-mysql"

  tags = {
    Name = "itsanovelidea-mysql"
  }
}
