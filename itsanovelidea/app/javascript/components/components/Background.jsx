import React from "react";

function importAll (r) {
  return r.keys().map(x=>r(x));  
}
const images = importAll(require.context('../images'));

class Background extends React.Component{

  constructor(){
    super();
    const urlParams = new URLSearchParams(window.location.search);
    const imageSpot = urlParams.get('image') || 0;
    this.state = {
      imageSpot,
    }
  }
  
  componentDidMount(){
    window.imageSpot = 0;
    window.inc = ()=>{
      const newImageSpot = window.imageSpot+1;
      window.imageSpot = newImageSpot < images.length ? newImageSpot : images.length-1;
      document.body.style.backgroundImage = `url(${images[window.imageSpot]})`;
      return window.imageSpot;
    };
    window.dec = ()=>{
      const newImageSpot = window.indexedDB-1;
      window.imageSpot = newImageSpot >=0 ? newImageSpot : 0;
      document.body.style.backgroundImage = `url(${images[window.imageSpot]})`;
      return window.imageSpot;
    } 
  }

  render(){
    /*const {imageSpot} = this.state;
    const image = images[imageSpot] || images[0];
    //TODO Revisit, this is NOT the react way to do things but it will work
    document.body.style.backgroundImage = `url(${image})`;*/
    return null;
  }
}

export default Background;