class AddPhotoMetadataToStories < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :photo_width, :string
    add_column :stories, :photo_height, :string
    add_column :stories, :photo_background_color, :string
    add_column :stories, :photo_text_color, :string
  end
end