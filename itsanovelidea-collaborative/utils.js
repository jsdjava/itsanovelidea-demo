// Converts a snapshot of the form:
/*
  {
    data:{
      ops:[
        {
          insert:"a b c"
        },
        {
          retain:9
        },
        {
          insert:"d"
        }
        ...
      ]
    }
  }
*/
// to just the text that the snapshot is inserting, so in this case:
//   "a b c d"
// This is useful for determining the actual text changes that have occurred
// between two versions, as you can take the snapshot of the previous version
// and "apply" the diff of the previous and the next version to the snapshot text to get just the
// the inserted and deleted text snippets
const snapshotToText = (snapshot) => {
  const snapshotText = snapshot.data.ops
    .map(({ insert }) => insert)
    .filter((x) => x)
    .join('');
  return snapshotText;
};

// Uses the diff between two snapshots (which is itself just a snapshot), of the form:
/*
  {
    ops:[
      {
        insert:"josh"
      },
      {
        retain:4,
      },
      {
        delete:2,
      },
      ...
    ]
  }
*/
// along with the text of the first snapshot, so a string like:
//   "billdy"
// and calculates the additions and deletions
// made to get from the first snapshot to the second snapshot. Result looks like:
/*
  [
    {
      add:"josh"
    },
    {
      delete:"dy"
    }
  ]
*/
const diffToList = (diff, snapshotText) => {
  let cursor = 0;
  const diffOps = diff.ops;
  const diffLines = [];
  for (let curOpSpot = 0; curOpSpot < diffOps.length; curOpSpot++) {
    const curOp = diffOps[curOpSpot];
    if (curOp.insert) {
      // Inserts do nothing to the cursor because they all occurred after the first snapshot
      diffLines.push({ add: curOp.insert });
    } else if (curOp.delete) {
      // Deletes are opertions that are perfomed on the text of the first snapshot in order
      // to create the next snapshot, and they move the cursor along the first snapshot
      diffLines.push({ delete: snapshotText.slice(cursor, cursor + curOp.delete) });
      cursor += curOp.delete;
    } else {
      // Retains also occur on the text of the first snapshot, but they only serve to
      // move the cursor so the next deletes occur in the right spot. There's nothing
      // to track in terms of a diff with a retain because its literally stating
      // "this area of text remained the same between both snapshots"
      cursor += curOp.retain;
    }
  }
  return diffLines;
};

module.exports = {
  diffToList,
  snapshotToText,
};
