import React from 'react';
import Account from "./Account.jsx";
import TextField from '@material-ui/core/TextField';

export default {
  component: Account,
  title: 'Account',
};

export const account = () => <div style={{height:"500px"}}>
    <Account title="Page Title" btnTitle="Button Title"/>
</div>;

export const accountWithFields = ()=> <div style={{height:"500px"}}><Account 
    title = "Page Title" 
    btnTitle="Button Title"
    render={()=>{
        return <div>
            <div className="accountTextField">
                <TextField label="Field1"/>
            </div>
            <div className="accountTextField">
                <TextField label="Field2"/>
            </div>
        </div>;
    }}/>
</div>;
