const {appUrl} = require('../../../support/app.json');
const {
  forceVisit,
  expandStory,
} = require('../../../support/actions/shared.js');

const {
  welcomePage,
  writingPromptPage,
} = require('../../../support/pages');

const {
  route,
} = welcomePage;

const {
  writingPromptExpandIcon,
  writingPromptLink,
  writingPromptHeader,
  storiesGallery,
  expandedStoryDiv,
  storyDiv,
} = writingPromptPage;

describe("writing prompt",()=>{
  let writingPromptText;
  beforeEach(()=>{
    forceVisit(appUrl,route);
    expandStory();
    cy.get(writingPromptExpandIcon)
      .click();
    cy.get(writingPromptLink)
      .invoke('text')
      .then(x=>writingPromptText=x);
    cy.get(writingPromptLink)
      .click();
  });
  it("displays the writing prompt",()=>{
    cy.get(writingPromptHeader)
      .should('have.text',writingPromptText);
  });
  it("displays the stories gallery",()=>{
    cy.get(storiesGallery)
      .should('exist'); 
  });
  it("should expand a story (but leave the top navbar visible)",()=>{
    cy.get(storyDiv) 
      .click({
        force:true,
      });

    cy.get(expandedStoryDiv)
      .first()
      .should('have.css', 'top', '48px')
    });
});