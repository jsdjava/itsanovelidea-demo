const express = require('express');

const {
  apiKey,
  redis,
  swagger,
  health,
  healthCheck,
} = require('itsanovelidea-middleware');

const logger = require('./logger.js');
const shareDb = require('./shareDb.js');
const profiles = require('./profiles.js')(shareDb);
const stories = require('./stories.js')(shareDb);

const app = express();

app.use(apiKey);
app.use('/swagger', swagger('Itsanovelidea-collaborative', __dirname, ['./app.js']));
app.use(express.json());

/**
 * @swagger
 *
 * definitions:
 *   Document:
 *     type: object
 *     properties:
 *       id:
 *         type: string
 *         description: the id of the document
 *       v:
 *         type: integer
 *         description: the latest version of the document
 *       type:
 *         type: string
 *         description: should always be rich text (at least for now)
 *       data:
 *         type: object
 *         description: the actual document rich text object, stored using https://github.com/ottypes/rich-text
 *         properties:
 *           ops:
 *             type: array
 *             items:
 *               type: object
 *               properties:
 *                 insert:
 *                   type: string
 *                   description: text to insert
 *                 delete:
 *                   type: integer
 *                   description: number of characters to delete
 *                 retain:
 *                   type: integer
 *                   description: |
 *                     number of characters to keep (useful for moving cursor
 *                     along current snapshot)
 *   StoryHistory:
 *     type: array
 *     description: list of story diffs
 *     items:
 *       type: object
 *       properties:
 *         timestamp:
 *           type: string
 *           description: |
 *             epoch time when the more recent version of the two in the diff
 *             was created (e.g 1579059498297)
 *         newSnapshot:
 *           type: object
 *           description: the more recent version of the story for the diff
 *           $ref: '#/definitions/Document'
 *         oldSnapshot:
 *           type: object
 *           description: the older version of the story for the diff
 *           $ref: '#/definitions/Document'
 *         diff:
 *           type: array
 *           description: |
 *             list of text that was added and text that was deleted to
 *             get from the oldSnapshot to the newSnapshot
 *           items:
 *             type: object
 *             properties:
 *               add:
 *                 type: string
 *                 description: text that was added for the new snapshot
 *               delete:
 *                 type: string
 *                 description: text that was removed from the old snapshot
 */

/**
 * @swagger
 *
 * /health:
 *   get:
 *     description: Health of itsanovelidea-collaborative service and its dependencies
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Everything is healthy
 *       424:
 *         description: Something is not healthy
 */
app.use('/health', health({
  'itsanovelidea-collaborative': healthCheck(),
  'itsanovelidea-main': healthCheck(),
  redis: healthCheck(redis.ping),
  mongo: healthCheck(async () => shareDb.getMongoClient().db().admin().ping()),
}));

/**
 * @swagger
 *
 * /profiles/{userId}:
 *   get:
 *     description: Returns the share db snapshot holding the profile for the user id passed in
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: the id of the user profile to load
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The profile for the passed in user id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/definitions/Document'
 */
app.get('/profiles/:userId', async (req, res) => {
  const { userId } = req.params;
  try {
    const profile = await profiles.getProfile(userId);
    res.send(profile);
  } catch (err) {
    // eslint-disable-next-line
    logger.log('error',err.toString());
    res.status(500).send(err.toString());
  }
});

/**
 * @swagger
 *
 * /profiles/{userId}:
 *   post:
 *     description: |
 *       Initializes the share db document used to back the profile.
 *       Note you don't actually pass a body into this -
 *       it literally just gets called every time a user logs in to make sure that
 *       the initial .create() call has been executed for sharedb
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: userId
 *         description: the id of the user profile to load
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The profile for the passed in userId
 */
app.post('/profiles/:userId', async (req, res) => {
  const { userId } = req.params;
  try {
    await profiles.createProfile(userId);
    res.sendStatus(200);
  } catch (err) {
    if (err.code === 'ERR_DOC_ALREADY_CREATED') {
      // This is ok, in fact its expected. Every time you login,
      // this route gets called to make sure that a profile has been
      // made for the user
      res.sendStatus(200);
      return;
    }
    // eslint-disable-next-line
    logger.log('error',err.toString());
    res.status(500).send(err.toString());
  }
});

/**
 * @swagger
 *
 * /stories/{storyId}/history:
 *   get:
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: storyId
 *         description: the id of the story to get history for
 *         in: path
 *         required: true
 *         type: string
 *       - name: version
 *         description: the version of the story to start collecting at
 *         in: query
 *         required: false
 *         type: integer
 *       - name: numberResults
 *         description: the number of versions of the story to return
 *         in: query
 *         required: false
 *         type: integer
 *       - name: ascending
 *         description: |
 *           whether to collect versions going up to latest (asc) or down to earliest (desc)
 *         in: query
 *         required: false
 *         type: boolean
 *     responses:
 *       200:
 *         description: The profile for the passed in user id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/definitions/StoryHistory'
 */
app.get('/stories/:storyId/history', async (req, res) => {
  let {
    numberResults = 10,
    version,
    ascending,
  } = req.query;

  // I guess there's no parseBoolean fn in js...
  ascending = req.query.ascending === 'true';
  version = parseInt(version, 10);
  numberResults = parseInt(numberResults, 10);
  try {
    const storyHistory = await stories.getStoryHistory(req.params.storyId, {
      version,
      numberResults,
      ascending,
    });
    res.status(200).json(storyHistory);
  } catch (err) {
    // eslint-disable-next-line
    logger.log('error',err.toString());
    res.status(500).send(err.toString());
  }
});

/**
 * @swagger
 *
 * /stories/{storyId}:
 *   get:
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: storyId
 *         description: the id of the story to get
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The story for the passed in story id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/definitions/Document'
 */
app.get('/stories/:storyId', async (req, res) => {
  try {
    const story = await stories.getStory(req.params.storyId);
    res.status(200).json(story);
  } catch (err) {
    // eslint-disable-next-line
    logger.log('error',err.toString());
    res.status(500).send(err.toString());
  }
});

/**
 * @swagger
 *
 * /stories/{storyId}:
 *   post:
 *     description: Initializes the backing document for a story
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: storyId
 *         description: the id of the story to create
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The story for the passed in story id
 */
app.post('/stories/:storyId', async (req, res) => {
  try {
    await stories.createStory(req.params.storyId);
    res.sendStatus(200);
  } catch (err) {
    // eslint-disable-next-line
    logger.log('error',err.toString());
    res.status(500).send(err.toString());
  }
});

module.exports = app;
