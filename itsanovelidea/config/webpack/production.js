process.env.NODE_ENV = process.env.NODE_ENV || 'production'

const environment = require('./environment')

const webpackConfig = environment.toWebpackConfig();

// Weird hack used to fix versioning issue with the webpack and css-loader.
const {use} = webpackConfig.module.rules.find(x=>x.use && x.use[0].includes && x.use[0].includes("mini-css-extract-plugin"))
use[1].options = {}

module.exports = webpackConfig;
