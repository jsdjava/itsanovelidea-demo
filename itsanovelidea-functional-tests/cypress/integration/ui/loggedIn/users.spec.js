const {appUrl} = require('../../../support/app.json');
const {navbar,loginPage,usersPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit,assertSort} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {authorsLink} = navbar;
const {
  usersTable,
  sortByStoryRatingBtn,
  userStoryRatings,
  sortByStoryViewsBtn,
  sortByPromptsRatingBtn,
  sortByPromptViewsBtn,
  sortByStoriesBtn,
  sortByPromptsBtn,
  sortByPromptResponsesBtn,
  userStoryViews,
  userPromptRatings,
  userPromptViews,
  userStories,
  userPrompts,
  userPromptResponses,
  nextPageBtn,
  crateShadowDom,
  crateBtn,
  crateExpanded,
} = usersPage;

describe('users page', function() {
    beforeEach(()=>{
      forceVisit(appUrl,route);
      login(email,password);
      cy.get(authorsLink).click();
    });

    it('displays a table of users', function() {
      cy.get(usersTable).should('be.visible');
    });

    // This is pretty fun because the mysql sort is custom and there is no well defined spec for it,
    // so there isn't something i can use in JS to make sure the array has been correctly ordered
    it("sorts by email");

    it("sorts by story rating",()=>{
        assertSort({
            sortBtn:sortByStoryRatingBtn,
            sortCells:userStoryRatings,
            isInt:false,
        });
    });
    it("sorts by story views",()=>{
        assertSort({
            sortBtn:sortByStoryViewsBtn,
            sortCells:userStoryViews,
            isInt:true,
        });
    });
    it("sorts by prompts rating",()=>{
        assertSort({
            sortBtn:sortByPromptsRatingBtn,
            sortCells:userPromptRatings,
            isInt:false,
        });
    });
    it("sorts by prompts views",()=>{
        assertSort({
            sortBtn:sortByPromptViewsBtn,
            sortCells:userPromptViews,
            isInt:true,
        })
    });
    it("sorts by stories",()=>{
        assertSort({
            sortBtn:sortByStoriesBtn,
            sortCells:userStories,
            isInt:true,
        });
    });
    it("sorts by prompts",()=>{
        assertSort({
            sortBtn:sortByPromptsBtn,
            sortCells:userPrompts,
            isInt:true,
        });
    });
    it("pages maintaining order",()=>{
        assertSort({
            sortBtn: sortByStoryViewsBtn,
            sortCells: userStoryViews,
            isInt:true,
        });
        let oldPage;
        cy.get(userStoryViews).then(x=>{
          oldPage = [...x].map(x=>parseInt(x.innerText));
        });
        cy.get(nextPageBtn).click();
        cy.document()
          .pipe((x)=>x.querySelectorAll(userStoryViews),{timeout:15000})
          .should(x=>{
            const actual = [...x].map(x=>parseInt(x.innerText));
            expect(actual[0]).to.be.below(oldPage[oldPage.length-1]);
            expect([...actual].sort((a,b)=>a-b).reverse()).to.eql(actual);
          });
    });
    it("sorts by responses",()=>{
        assertSort({
            sortBtn:sortByPromptResponsesBtn,
            sortCells:userPromptResponses,
            isInt:true,
        });
    });
    it("expands the discord widget",()=>{
        cy.get(crateShadowDom).should(e => { 
            const [dom] = e.get();
            dom.shadowRoot.querySelector(crateBtn).click();
        });
        cy.get(crateShadowDom)
          .pipe(x=>x,{timeout:15000})
          .should((e)=>{
            const [dom] = e.get();
            const isExpanded = dom.shadowRoot.querySelector(crateExpanded);
            expect(isExpanded).to.be.ok
          });
    });
    it("closes the discord widget",()=>{
        cy.get(crateShadowDom).should(e => { 
            const [dom] = e.get();
            dom.shadowRoot.querySelector(crateBtn).click();
        });
        cy.get(crateShadowDom)
          .pipe(x=>x,{timeout:15000})
          .should((e)=>{
            const [dom] = e.get();
            const isExpanded = dom.shadowRoot.querySelector(crateExpanded);
            expect(isExpanded).to.be.ok
          });
        cy.get(crateShadowDom).should(e => { 
            const [dom] = e.get();
            dom.shadowRoot.querySelector(crateBtn).click();
        });
        cy.get(crateShadowDom)
        .pipe(x=>x,{timeout:15000})
        .should((e)=>{
          const [dom] = e.get();
          const isExpanded = dom.shadowRoot.querySelector(crateExpanded);
          expect(isExpanded).to.not.be.ok
        });
    });
    it("allows you to sign in to discord?");
  });
