process.env.NODE_CONFIG_DIR = `${__dirname}/config`;

const config = require('config');
const utils = require('./utils.js');
const apiKey = require('./apiKey.js');
const redis = require('./redis.js');
const swagger = require('./swagger.js');
const health = require('./health.js');
const logger = require('./logger.js');

module.exports = {
  apiKey,
  redis,
  swagger,
  config,
  logger,
  ...health,
  ...utils,
};
