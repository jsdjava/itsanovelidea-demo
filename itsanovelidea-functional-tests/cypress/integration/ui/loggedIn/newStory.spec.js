const {appUrl} = require('../../../support/app.json');
const {loginPage,newStoryPage,editStoryPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {
    route:newStoryRoute,
    storyTitleInput,
    photoSearchInput,
    storyTitleErrorMessage,
    photoSearchErrorMessage,
    photoSearchImages,
    photoSearchBackBtn,
    photoSearchForwardBtn,
    selectedPhotoImage,
    createStoryBtn,
} = newStoryPage;

const {
    editStoryDiv,
} = editStoryPage;

describe("create story page",()=>{
  beforeEach(()=>{
    forceVisit(appUrl,route);
    login(email,password);  
    cy.visit(`${appUrl}/${newStoryRoute}`);
  });
  it("shows an error for too short of a story title",()=>{
    cy.get(storyTitleInput).type("as");
    cy.get(storyTitleErrorMessage).should('contain','too short');
  });
  it("shows an error for too long of a story title",()=>{
    cy.get(storyTitleInput).type("asasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfa");
    cy.get(storyTitleErrorMessage).should('contain','too long');
  });
  it("shows an error for an unselected story photo",()=>{
    cy.get(photoSearchInput).type("cat");
    cy.get(photoSearchErrorMessage).should('contain','Select a photo');
  });
  it("shows photos for a search",()=>{
    cy.get(photoSearchInput).type("cat");
    cy.document()
      .pipe(x=>[...x.querySelectorAll(photoSearchImages)])
      .should(x=>{
          expect(x.length).to.be.above(4);
      });
  });
  it("scrolls forward on photos for a search",()=>{
    cy.get(photoSearchInput).type("cat");
    let oldPhotoUrls;
    cy.get(photoSearchImages).then(x=>{
        oldPhotoUrls = x.toArray().map(x=>x.getAttribute("src")).slice(1);
    });
    cy.get(photoSearchForwardBtn).click();
    cy.document()
      .pipe(x=>[... x.querySelectorAll(photoSearchImages)],{timeout:15000})
      .should(x=>{
        const newPhotoUrls = x.map(y=>y.getAttribute("src")).slice(0,-1);
        expect(oldPhotoUrls).to.eql(newPhotoUrls);
      });
  });
  it("scrolls forward then back for a search",()=>{
    cy.get(photoSearchInput).type("cat");
    let oldPhotoUrls;
    cy.get(photoSearchImages).then(x=>{
        oldPhotoUrls = x.toArray().map(x=>x.getAttribute("src"));
    });
    cy.get(photoSearchImages).eq(2).click();
    cy.get(selectedPhotoImage).should('exist');
    for(let i = 0; i<3; i++){
      cy.get(photoSearchForwardBtn).click();
    }
    cy.get(selectedPhotoImage).should('not.exist');
    for(let i = 0; i<3; i++){
        cy.get(photoSearchBackBtn).click();
    }
    cy.get(selectedPhotoImage).should('exist');
    cy.document()
      .pipe(x=>[...x.querySelectorAll(photoSearchImages)],{timeout:15000})
      .should(x=>{
        const newPhotoUrls = x.map(x=>x.getAttribute("src"));
        expect(oldPhotoUrls).to.eql(newPhotoUrls);
      });
  });
  it("selects a photo",()=>{
    cy.get(photoSearchInput).type("cat");
    cy.get(photoSearchImages).eq(2).click();
    cy.get(photoSearchErrorMessage).should('not.exist');
  });
  it("creates a story",()=>{
    cy.get(storyTitleInput).type("cool story");
    cy.get(photoSearchInput).type("cat");
    cy.get(photoSearchImages).eq(2).click();
    cy.get(photoSearchErrorMessage).should('not.exist');
    cy.get(createStoryBtn).click();
    cy.get(editStoryDiv).should('exist');
  })
});