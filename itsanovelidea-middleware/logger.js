const winston = require('winston');

// There isn't really anything to test here, I'm just configured winston to write out
// to output.log to help me troubleshoot in my node services
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'output.log' }),
  ],
});

module.exports = logger;
