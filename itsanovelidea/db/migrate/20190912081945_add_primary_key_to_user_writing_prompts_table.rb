class AddPrimaryKeyToUserWritingPromptsTable < ActiveRecord::Migration[5.1]
  def change
    add_column :user_writing_prompts, :id, :primary_key
  end
end
