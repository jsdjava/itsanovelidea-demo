const config = require('config');
const redis = require('redis');
const pify = require('pify');
const redisCommands = require('redis-commands');

const redisClient = redis.createClient({
  host: config.get('redisHost'),
  password: process.env.REDIS_PASSWORD,
});

// It looks like I have to do this in order to stop redis.js from throwing
// exceptions when it can't connect and crashing the server.
redisClient.on('error', (err) => {
  console.log('Could not connect to redis');
  console.log(err);
});

redisCommands.list.filter((x) => redisClient[x]).forEach((redisCommand) => {
  // I don't want to deal with callbacks in my code, so this wraps the
  // standard redis approach of redisClient.get(...args,(val)=>{
  //   console.log(val);
  // }) with a promise so instead I can do
  // const val = await redisClient.get(...args);
  redisClient[redisCommand] = pify(redisClient[redisCommand]).bind(redisClient);
});

module.exports = redisClient;
