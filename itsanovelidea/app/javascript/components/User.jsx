import React from "react";
import Grid from '@material-ui/core/Grid';
import PropTypes from "prop-types";

import ProfileViewer from "./components/ProfileViewer.jsx";
import ProfileRightColumn from "./components/ProfileRightColumn.jsx";

class Profile extends React.Component{
    constructor(){
        super();
        this.state = {};
    }

    render(){
        const {
            userId,
            userUrl,
            writingPromptsCountUrl,
            storiesCountUrl,
            user
        } = this.props;
        return <div className="profileDiv">
            <Grid container>
                <Grid item sg={12} md={6}>
                    <h2>{user}</h2>
                    <ProfileViewer userUrl={userUrl}/>
                </Grid>
                <Grid item sg={12} md={6}>
                    <ProfileRightColumn
                        userUrl={userUrl} 
                        writingPromptsCountUrl={writingPromptsCountUrl}
                        userId={userId}
                        storiesCountUrl={storiesCountUrl}/>
                </Grid>
            </Grid>
        </div>;
    };
}

Profile.propTypes = {

  //TODO Feels extremely redundant... just make the User call once in here
  user: PropTypes.string.isRequired,
  userUrl:PropTypes.string.isRequired,
  writingPromptsCountUrl:PropTypes.string.isRequired,
  storiesCountUrl:PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
};

export default Profile;
