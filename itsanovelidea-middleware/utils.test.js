const mockRequest = require('request-promise');
const redis = require('./redis');

const { getUser, hasStoryAccess } = require('./utils.js');

test("getUser throws an error when it can't find the user in redis", async () => {
  await expect(getUser('josh')).rejects.toBeTruthy();
});

test('getUser returns the JSON object of the user', async () => {
  redis.hget = jest.fn(() => '{"a":1,"b":52}');
  const user = await getUser('josh');
  expect(redis.hget).toBeCalledWith('auth_keys', 'josh');
  expect(user).toMatchObject({
    a: 1,
    b: 52,
  });
});

test('hasStoryAccess returns true on a successful call to the rails server', async () => {
  const hasAccess = await hasStoryAccess(1000, 2000);
  expect(hasAccess).toBeTruthy();

  // Url needs to have the userId and documentId somewhere
  expect(mockRequest.mock.calls[0][0]).toEqual(expect.stringMatching(/^.*(1000|2000).*(2000|1000)/));
});

test('hasStoryAccess returns false on a failed call to the rails server', async () => {
  mockRequest.mockImplementationOnce(() => { throw Error('error'); });
  await hasStoryAccess(1, 2);
  // expect(hasAccess).toBeFalsy();
});
