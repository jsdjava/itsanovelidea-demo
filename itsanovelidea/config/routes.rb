Rails.application.routes.draw do
  
  get 'stories/published/count', :to => 'stories#published_count'
  get 'stories/published'

  get 'writing_prompts/count', :to=> 'writing_prompts#count'
  resources :writing_prompts
  put 'writing_prompts/:id/rate', :to => 'writing_prompts#rate'

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  devise_for :views

  resources :categories

  get 'stories/count', :to => 'stories#stories_count', as: :stories_count
  resources :stories
  get 'stories/:id/users/:user_id', :to => 'stories#owned'
  get 'stories/:id/versions', :to => 'stories#versions'
  put 'stories/:id/rating', :to => 'stories#rate', as: :rate_story
  get 'stories/:id/rating', :to => 'stories#rating'
  post 'stories/:id/publish', :to => 'stories#publish'
  get 'stories/published/:writing_prompt_id/count', :to => 'stories#published_count'
  get 'stories/published/:writing_prompt_id', :to => 'stories#published', as: :published_stories

  get 'dashboard/index'
  get 'dashboard/stories', :to=> 'dashboard#stories', as: :my_stories
  get 'dashboard/prompts', :to=> 'dashboard#prompts', as: :my_prompts
  get 'dashboard/rating', :to => 'dashboard#rating'

  match 'photo' => 'photos#search', via: :get
  match 'photo/:link', to: 'photos#download', via: :post
  match 'photo/:id', to: 'photos#getLink', via: :get
  match 'photos/:id', to: 'photos#files', via: :get

  get 'chat/:documentId/messages', :to=> 'chat#get_previous_messages'

  get 'users/count', :to => 'users#users_count', as: :users_count
  get 'users/index'

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    omniauth_callbacks: 'users/omniauth_callbacks',
  }

  delete 'users/wipe', :to=> 'users#wipe'
  get 'users/:id', :to=> 'users#show', as: :user

  get 'random_words', :to=> 'random_words#index'

  authenticated :user do
    root 'dashboard#index'
  end
  
  root 'welcome#index'
end
