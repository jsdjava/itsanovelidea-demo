import React from 'react';
import RandomPromptWord from "./RandomPromptWord.jsx";

export default {
  component: RandomPromptWord,
  title: 'RandomPromptWord',
};

export const randomPromptWord = () => <RandomPromptWord randomWord="random word"/>

export const randomPromptWordLoading = () => <RandomPromptWord/>;
