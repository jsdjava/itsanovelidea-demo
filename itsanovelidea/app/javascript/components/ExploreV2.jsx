import React from "react"
import PropTypes from "prop-types"

import FloatingAddButton from "./components/FloatingAddButton.jsx";
import WritingPromptStories from "./components/WritingPromptStories.jsx";
import StoriesGallery from "./components/StoriesGallery.jsx";
import WritingPromptCarouselInfinite from "./components/WritingPromptCarouselInfinite.jsx";
import {fetchWithToken} from "./helpers/api.js";

import "./stylesheets/o-teaser.css";
import "./stylesheets/StoriesList.css";
import "./stylesheets/Explore.css";

class Explore extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      ...props,
      writingPrompt:{},
    }
  }

  render () { 
    const {writingPromptUrl} = this.props;
    const {writingPrompt} = this.state;
    return (
        <div>
          <FloatingAddButton onClick = {()=>{window.location.href = writingPrompt.create_story_url}}/>
          <WritingPromptCarouselInfinite loadWritingPrompts={async (page,pageSize)=>{
            try{
              const resp = await fetchWithToken(`${writingPromptUrl}?page=${page}&page_size=${pageSize}`);
              if(!resp.ok){
                throw resp;
              }
              return resp.json();
            } catch(e){
              console.log("Could not get writing prompts");
              console.log(e);
            }
          }} onRefresh = {(writingPrompt)=>{
            this.setState({writingPrompt})
          }}/>
          <WritingPromptStories key={writingPrompt.id} writingPrompt={writingPrompt} hidePrompt={true}>
            {
              ({stories,next, loading, storiesCount})=>{
                console.log(storiesCount);
              return<div id="exploreGalleryDiv">
                <StoriesGallery
                  fullScreen={true}
                  hasMore = {stories.length<storiesCount}
                  loadedStories={true}
                  key={writingPrompt.id}
                  publishedStories={stories} 
                  next = {next}
                />
                </div>}
            }
          </WritingPromptStories>
        </div>
    );
  }
}

Explore.propTypes = {
  writingPromptUrl: PropTypes.string.isRequired,
};

export default Explore;
