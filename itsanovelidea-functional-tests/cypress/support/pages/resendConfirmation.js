module.exports = {
    route:"users/confirmation/new",
    emailInput:"[data-qa='emailInput']",
    resendConfirmationEmailBtn: "[data-qa='resendConfirmationEmailBtn']",
    successAlert: "[data-qa='success']",
    getErrorAlert: (spot)=>`[data-qa='error-${spot}']`,
}