const redis = require('./redis.js');

test('redis.get/ redis.set are promisified', async () => {
  await redis.set('key', 2);
  const key = await redis.get('key');
  expect(key).toEqual('2');
});

test('redis should catch all errors', async () => {
  // Kind of weird, basically I'm just making sure the error gets caught and not
  // thrown all the way up to failing the test
  redis.emit('error');
});
