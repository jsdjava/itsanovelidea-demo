FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "test-#{n.to_s.rjust(3, "0")}@sample.com" }
    password {"testtest"}
  end

  factory :story do
    text {"talking all that"}
    created_at {Faker::Time.between(from: DateTime.now - 1, to: DateTime.now)}
    updated_at {Faker::Time.between(from: DateTime.now - 1, to: DateTime.now)}
    name {"named"}
    association :writing_prompt
    association :owner, factory: :user
    photo_url {"1234test"}
  end

end