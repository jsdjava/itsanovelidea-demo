const signInPage = require("../pages/signIn.js");
const googlePage = require("../pages/google.js");
const dashboardPage = require("../pages/dashboard.js");

describe('ssoing through google', () => {
    it('signs in through google', () => {
        const {email,password} = browser.config.users.google;
        browser.url(browser.config.baseUrl);
        signInPage.googleLoginBtn.click();
        googlePage.emailInput.setValue(email);
        googlePage.emailNextBtn.click();
        console.log(browser.execute(()=>document.documentElement.innerHTML));
        googlePage.passwordInput.waitForClickable();
        googlePage.passwordInput.setValue(password);
        googlePage.passwordNextBtn.click();
        expect(dashboardPage.emailHeader.getText()).toEqual(email);
    });
  })