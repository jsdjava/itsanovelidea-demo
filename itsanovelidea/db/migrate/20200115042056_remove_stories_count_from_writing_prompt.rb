class RemoveStoriesCountFromWritingPrompt < ActiveRecord::Migration[5.1]
  def change
    remove_column :writing_prompts, :stories_count, :integer
  end
end
