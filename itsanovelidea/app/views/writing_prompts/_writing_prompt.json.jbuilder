json.url writing_prompt_url(writing_prompt, format: :json)
json.published_stories_url published_stories_path(writing_prompt_id:writing_prompt.id)
json.published_stories_count_url url_for(action:'published_count',controller:'stories',writing_prompt_id:writing_prompt.id)
json.create_story_url url_for(action:'new',controller:'stories',writing_prompt_id:writing_prompt.id)

json.extract! writing_prompt, :id, :created_at, :updated_at, :text, :impressions_count
json.rating writing_prompt.rating
json.num_published_stories writing_prompt.num_published_stories
json.owner do 
	json.email writing_prompt.owner_email
end

