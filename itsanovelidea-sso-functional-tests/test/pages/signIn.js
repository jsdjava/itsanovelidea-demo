class SignInPage {
  get facebookLoginBtn() { return $('.loginSocialIconsDiv a:nth-child(3)') }
  get googleLoginBtn() { return $('.loginSocialIconsDiv a:nth-child(4)') }
  get redditLoginBtn() { return $('.loginSocialIconsDiv a:nth-child(5)') }

}

module.exports = new SignInPage();
