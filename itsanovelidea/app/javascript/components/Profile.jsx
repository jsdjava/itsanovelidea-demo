import React from "react";
import Grid from '@material-ui/core/Grid';
import PropTypes from "prop-types";

import ProfileBuilder from "./components/ProfileBuilder.jsx";
import ProfileRightColumn from "./components/ProfileRightColumn.jsx";

class Profile extends React.Component{
    constructor(){
        super();
        this.state = {};
    }

    render(){
        const {
            nodeUrl,
            userId,
            userUrl,
            writingPromptsCountUrl,
            storiesCountUrl,
        } = this.props;
        return <div className="profileDiv">
            <Grid container>
                <Grid item sg={12} md={6}>
                    <h2>Profile</h2>
                    <ProfileBuilder nodeUrl={nodeUrl} userId={userId}/>
                </Grid>
                <Grid item sg={12} md={6}>
                    <ProfileRightColumn
                        userUrl={userUrl} 
                        writingPromptsCountUrl={writingPromptsCountUrl}
                        userId={userId}
                        storiesCountUrl={storiesCountUrl}/>
                </Grid>
            </Grid>
        </div>;
    };
}

Profile.propTypes = {
    nodeUrl: PropTypes.string.isRequired,
    userId: PropTypes.number.isRequired,
  };

export default Profile;
