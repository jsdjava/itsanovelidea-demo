import React from "react";
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import PropTypes from "prop-types";

import {fetchWithToken} from '../helpers/api';
import "../stylesheets/Rate.css";

class Rate extends React.Component{

  constructor(){
    super();
    this.state = {
        rating:"N/A"
    }
  }

  componentDidMount(){
    const {url} = this.props;
    if(url){
      this.getRating();
    }
  }

  async rate(newRating){
    this.setState({
      disabled:true,
    });
    const {url,onUpdatedRating} = {
      onUpdatedRating:()=>{},
      ...this.props
    };
    try{
      const resp = await fetchWithToken(url,'PUT',JSON.stringify({
        rating:newRating,
      }));
      if(!resp.ok){
        throw resp;
      }
      const {rating} = (await resp.json());
      this.setState({
        rating,
      });
      onUpdatedRating();
    }catch(e){
      console.log("Could not update rating");
      console.log(e);
    }
    this.setState({
      disabled:false,
    });
  }

  async getRating(){
    const {url} = this.props;
    try{
      const resp = await fetchWithToken(url)
      if(!resp.ok){
        throw resp;
      }
      const {rating} = (await resp.json());
      this.setState({
        rating,
      });
    } catch(e){
      console.log("Could not get rating");
      console.log(e);
    }
  }

  render(){
    const {editable} = this.props;
    const {rating,disabled} = {...this.state,...this.props};
    return <Rater rating={rating} total={5} interactive={Boolean(!disabled && editable)} onRate={({rating})=>{
      this.rate(rating);
    }}/>
  }
}

Rate.propTypes = {
  url: PropTypes.string,
};

export default Rate;