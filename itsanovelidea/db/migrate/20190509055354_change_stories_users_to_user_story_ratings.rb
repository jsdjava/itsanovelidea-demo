class ChangeStoriesUsersToUserStoryRatings < ActiveRecord::Migration[5.1]
  def change
    rename_table :stories_users, :user_story_ratings
  end
end
