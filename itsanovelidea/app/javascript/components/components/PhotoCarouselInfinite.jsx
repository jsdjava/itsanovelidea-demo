import React from "react"
import LoadingOverlay from 'react-loading-overlay';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
const debounce = require('debounce'); 

import InfiniteScrollSearch from "../helpers/infiniteScrollSearch.js";
import "../stylesheets/PhotoCarouselInfinite.css";

//Also need to update in css
const slideSpeed = 250;

class PhotoCarouselInfinite extends React.Component{
    
    constructor(props){
      super(props);
      this.state={
        pageSize:5,
        loading:true,
        slideIndex:0,
        absoluteIndex:0,
        numRows:5,
      }
      this.previous = debounce(this.previous.bind(this),100,true);
      this.next = debounce(this.next.bind(this),100,true);
      this.onResize = this.onResize.bind(this);
    }

    onResize(){
      const sizes = {
        500:1,
        600:2,
        800:3,
        1000:4,
      };
      const numRows = sizes[Object.keys(sizes).find(v=>window.innerWidth<=v)] || 5;

      // This is so bad. I'm trying to make divs visible again that should be when you size from small
      // to big, but also avoid running the fking animation
      const photoDivs = document.querySelectorAll(".photoDiv");
      for(let i = 0; i<numRows; i++){
        const photoDiv = photoDivs[i];
        if(photoDiv){
          photoDiv.style.display = "";
        }
      }

      this.setState({
        numRows,
      });
    }

    previous(){
      this.infiniteScrollSearch.previous();
    }

    next(){
      this.infiniteScrollSearch.next();
    }

    componentDidMount(){
      const {
          searchPhotos,
          onRefresh=()=>{},
      } = this.props;
      const {pageSize} = this.state;
      this.infiniteScrollSearch = new InfiniteScrollSearch(pageSize,searchPhotos,(issRef,photos,direction)=>{
          if(issRef === this.infiniteScrollSearch){
              const {slideIndex,absoluteIndex} = this.state;
              this.setState({
                  direction, 
                  oldSlideIndex: slideIndex,
                  slideIndex: slideIndex+1,
                  absoluteIndex:absoluteIndex+direction,
              });
              onRefresh((this.infiniteScrollSearch.first()||{}).value);
          }
      });
      this.infiniteScrollSearch.load();
      window.addEventListener('resize', this.onResize);
      this.onResize();
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.onResize);
    }

    render(){
      if(!this.infiniteScrollSearch){
        return null;
      }
      const {
        setPhotoIndex=()=>{},
        selectedIndex,
      } = this.props;
      const {
        slideIndex,
        absoluteIndex,
        direction,
        numRows,
      } = this.state;
      const photoNode = this.infiniteScrollSearch.first() || {value:{}};
      const previousPhotoNode = photoNode.prev;
      let nextPhotoNode = photoNode;
      for(let i = 0; i< numRows && nextPhotoNode; i++){
        nextPhotoNode = nextPhotoNode.next;
      }
      console.log(numRows);
      let slidingInPhotos = this.infiniteScrollSearch.current();
      let directionClass = "negative";
      if(direction<0){
        directionClass = "positive";
      }
      return <div className = "photoCarouselDiv">
        {/* I'm using this to buffer the loading of the images so they don't take time when they get on screen*/}
        <div className="hiddenDiv">
          {this.infiniteScrollSearch.all().map(({largeImageURL})=><img src={largeImageURL}/>)}
        </div>
        <LoadingOverlay
            active={false}
            spinner
            text='Searching...'
            className="loadingOverlay">
        { photoNode.value.largeImageURL!==undefined ?
          <div className={`photoContainerDiv ${directionClass}`}>
            <div className="arrowContainerDiv">
              <div>
                <ArrowBackIcon onClick={()=>previousPhotoNode ? this.previous(): ''}/>
              </div>
            </div>
            <div className="photosOuterDiv">
              <ReactCSSTransitionGroup
                transitionName={{
                  enter: `background-enter-${directionClass}`,
                  leave: `background-leave-${directionClass}`,
                }}
                transitionEnterTimeout={slideSpeed}
                transitionLeaveTimeout={slideSpeed}>
                  <div className = "photosDiv" key={slideIndex}>
                    {slidingInPhotos.map(({largeImageURL},index)=><div className="photoDiv" style={{
                      display: index >= numRows ? "none" : "visible",
                    }}><div className={absoluteIndex+index===selectedIndex ? "selected":""}><img onClick={()=>{
                      setPhotoIndex(absoluteIndex+index);
                    }} src={largeImageURL}></img></div></div>)}
                  </div>
              </ReactCSSTransitionGroup>
            </div>
            <div className="arrowContainerDiv">
              <div>
                <ArrowForwardIcon onClick={()=>nextPhotoNode ? this.next(): ''}/>
              </div>
            </div>
          </div>:
          "No photos found"
        }
        </LoadingOverlay>
      </div>;
    }
}

export default PhotoCarouselInfinite;