#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Build Docker Image"
docker build -t itsanovelidea-sso-functional-tests .
echo "Push to ECR Repository"
docker tag itsanovelidea-sso-functional-tests:$BUILD_TAG $AWS_ECR_REPOSITORY/itsanovelidea-sso-functional-tests:$BUILD_TAG
docker push $AWS_ECR_REPOSITORY/itsanovelidea-sso-functional-tests:$BUILD_TAG