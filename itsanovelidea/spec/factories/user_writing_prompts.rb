FactoryBot.define do
  factory :user_writing_prompt do
    association :user
    association :writing_prompt
    rating {4}
  end
end
