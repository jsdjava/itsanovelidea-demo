module Filterable
    extend ActiveSupport::Concern

    included do
        belongs_to :owner, class_name: "User"
    end

    def sort_by_date(params,query=self.all)
        return query unless params[:sort_by_date]
        query.order('created_at '+(params[:sort_by_date]==='asc' ? 'ASC' : 'DESC'))
    end

    def sort_by_views(params,query=self.all)
        return query unless params[:sort_by_views]
        query.order('impressions_count '+(params[:sort_by_views]==='asc' ? 'ASC' : 'DESC'))
    end

    def page (params,query=self.all)
        max_page_size = 12
        page_size = (1...max_page_size).include?(params[:page_size].to_i) ? params[:page_size].to_i : max_page_size
        page = params[:page].to_i >0 ? params[:page].to_i : 1
        query.paginate(:page => page, :per_page => page_size)
    end
    
    def filter_by_like_user(params,query=self.all)
        return query unless params[:user_like]
        query.where("users.email like ?","%#{params[:user_like].to_s}%")
    end

    def filter_by_user(params,query=self.all)
        return query unless params[:user]
        query.where(owner:params[:user])
    end
end
  