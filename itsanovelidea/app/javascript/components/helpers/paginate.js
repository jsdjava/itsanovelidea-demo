export default function (text,maxLineLength,linesPerPage){
    text = text.replace("\t","    ");
    const lines = text.split("\n");
    const linesWords = lines.map(line=>{
        const words = line.split(" ");
        return words.map((word,spot)=>words.length>1 && spot<words.length-1 ? `${word} ` : word);
    });
    for(let i = 0; i< linesWords.length; i++){
        const lineWords = linesWords[i];
        let lineLength = 0;
        for(let j = 0; j<lineWords.length; j++){
            const word = lineWords[j];
            lineLength+=word.length;
            if(!j && maxLineLength<lineLength){
                const curLine = word.substring(0,maxLineLength);
                const newNextLineWords = [word.substring(maxLineLength),...lineWords.slice(1)];
                linesWords[i] = [curLine];
                linesWords[i+1] = linesWords[i+1] ? [...newNextLineWords,...linesWords[i+1]] : newNextLineWords;
                break;
            }
            else if(lineLength>maxLineLength){
                const newNextLineWords = lineWords.splice(j);
                linesWords[i+1] = linesWords[i+1] ? [...newNextLineWords,...linesWords[i+1]] : newNextLineWords;
                break;
            }
        }
    }
    const wrappedLines = linesWords.map(lineWords=>{
        return lineWords.join("")
    });
    const pages = [];
    for(let i =0; i< wrappedLines.length; i++){
        const wrappedLine = wrappedLines[i];
        const curPage = Math.floor(i/linesPerPage);
        pages[curPage] = pages[curPage]!==undefined ? `${pages[curPage]}\n${wrappedLine}` : wrappedLine;
    }
    console.log(JSON.stringify(pages));
    return pages;
}