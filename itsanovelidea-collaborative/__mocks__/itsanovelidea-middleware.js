module.exports = {
  config: {
    get: jest.fn(),
  },
  getUser: jest.fn(),
  hasStoryAccess: jest.fn(),
};
