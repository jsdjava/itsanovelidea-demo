import React from "react";
import {TextValidator} from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import LockIcon from '@material-ui/icons/Lock';

import Account from "./components/Account.jsx";

class ChangePassword extends React.Component{
    render(){
        const {passwordLength,resetPasswordToken,changePasswordUrl} = this.props;
        return <Account url = {changePasswordUrl} 
            rules={{
                isMatchingPassword:(value,{state})=>{
                    return value === state.password;
                }
            }}
            title="Change your password"
            btnDataQaTag="changePasswordBtn"
            btnTitle="Change my password"
            reset_password_token={resetPasswordToken}
            commit="Change my password"
            method="PUT"
            formKeys={["password","password_confirmation","reset_password_token"]}
            render={ (setFormValue,{password,password_confirmation})=>
                <div>
                <div className="accountTextField">
                <TextValidator
                    name="password"
                    label="Your password"
                    value={password}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <LockIcon/>
                        </InputAdornment>
                      ),
                    }}
                    type="password"
                    onChange={(e)=>setFormValue("password",e)}
                    data-qa="passwordInput"
                    validators={['required',`minStringLength:${passwordLength}`]}
                    errorMessages={['*required', `password must be at least ${passwordLength} characters`]}
                />
                </div>
                <div className="accountTextField">
                <TextValidator
                    value={password_confirmation}
                    name="password_confirmation"
                    label="Confirm your password"
                    InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <LockIcon/>
                          </InputAdornment>
                        ),
                    }}
                    type="password"
                    onChange={(e)=>setFormValue("password_confirmation",e)}
                    data-qa="confirmedPasswordInput"
                    validators={['required','isMatchingPassword']}
                    errorMessages={['*required','passwords must match']}
                />
                </div>
                </div>
        }/>
    };
}

export default ChangePassword;
