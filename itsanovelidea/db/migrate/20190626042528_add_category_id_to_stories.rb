class AddCategoryIdToStories < ActiveRecord::Migration[5.1]
  def change
    add_reference :stories, :category, index: true, foreign_key: true
  end
end
