const logger = require('./logger.js');

const profileCollection = 'profiles';

// profiles should have shareDb injected, i.e.
//   const profiles = require('./profiles.js)(shareDb);
// Alternatively, I could probably take advantage of module caching
// and just load shareDb.js in here, but that feels less clean
module.exports = (shareDb) => {
  // Profiles are accessible to anyone
  shareDb.access.allowRead(profileCollection, async () => true);
  shareDb.access.allowUpdate(profileCollection,
    async (userId, oldDoc, newDoc, ops, authorizedUserId) => {
      logger.log('info', `userId: ${userId}, authorizedUserId ${authorizedUserId}, userId type: ${typeof userId}, authorizedUserId type ${typeof authorizedUserId}`);

      // Yeah, one is a string and another is a number
      // eslint-disable-next-line
      return userId == authorizedUserId;
    });

  const getProfile = async (userId) => shareDb.fetchSnapshot(profileCollection, userId);

  const createProfile = (userId) => shareDb.createDocument(profileCollection, userId);
  return {
    getProfile,
    createProfile,
  };
};
