import React from 'react';
import FocusedStory from './FocusedStory';
import { get } from 'animejs';

export default {
  component: FocusedStory,
  title: 'FocusedStory',
};

const fishText = `Fish are gill-bearing aquatic craniate animals that lack limbs with digits. They form a sister group to the tunicates, together forming the olfactores. Included in this definition are the living hagfish, lampreys, and cartilaginous and bony fish as well as various extinct related groups. Tetrapods emerged within lobe-finned fishes, so cladistically they are fish as well. However, traditionally fish are rendered paraphyletic by excluding the tetrapods (i.e., the amphibians, reptiles, birds and mammals which all descended from within the same ancestry). Because in this manner the term "fish" is defined negatively as a paraphyletic group, it is not considered a formal taxonomic grouping in systematic biology, unless it is used in the cladistic sense, including tetrapods.[1][2] The traditional term pisces (also ichthyes) is considered a typological, but not a phylogenetic classification.

The earliest organisms that can be classified as fish were soft-bodied chordates that first appeared during the Cambrian period. Although they lacked a true spine, they possessed notochords which allowed them to be more agile than their invertebrate counterparts. Fish would continue to evolve through the Paleozoic era, diversifying into a wide variety of forms. Many fish of the Paleozoic developed external armor that protected them from predators. The first fish with jaws appeared in the Silurian period, after which many (such as sharks) became formidable marine predators rather than just the prey of arthropods.

Most fish are ectothermic ("cold-blooded"), allowing their body temperatures to vary as ambient temperatures change, though some of the large active swimmers like white shark and tuna can hold a higher core temperature.[3][4]

Fish can communicate in their underwater environments through the use of acoustic communication. Acoustic communication in fish involves the transmission of acoustic signals from one individual of a species to another. The production of sounds as a means of communication among fish is most often used in the context of feeding, aggression or courtship behaviour.[5] The sounds emitted by fish can vary depending on the species and stimulus involved. They can produce either stridulatory sounds by moving components of the skeletal system, or can produce non-stridulatory sounds by manipulating specialized organs such as the swimbladder.[6]

Fish are abundant in most bodies of water. They can be found in nearly all aquatic environments, from high mountain streams (e.g., char and gudgeon) to the abyssal and even hadal depths of the deepest oceans (e.g., cusk-eels and snailfish), although no species has yet been documented in the deepest 25% of the ocean.[7] With 34,300 described species, fish exhibit greater species diversity than any other group of vertebrates.[8]

Fish are an important resource for humans worldwide, especially as food. Commercial and subsistence fishers hunt fish in wild fisheries (see fishing) or farm them in ponds or in cages in the ocean (see aquaculture). They are also caught by recreational fishers, kept as pets, raised by fishkeepers, and exhibited in public aquaria. Fish have had a role in culture through the ages, serving as deities, religious symbols, and as the subjects of art, books and movies.
The term "fish" most precisely describes any non-tetrapod craniate (i.e. an animal with a skull and in most cases a backbone) that has gills throughout life and whose limbs, if any, are in the shape of fins.[17] Unlike groupings such as birds or mammals, fish are not a single clade but a paraphyletic collection of taxa, including hagfishes, lampreys, sharks and rays, ray-finned fish, coelacanths, and lungfish.[18][19] Indeed, lungfish and coelacanths are closer relatives of tetrapods (such as mammals, birds, amphibians, etc.) than of other fish such as ray-finned fish or sharks, so the last common ancestor of all fish is also an ancestor to tetrapods. As paraphyletic groups are no longer recognised in modern systematic biology, the use of the term "fish" as a biological group must be avoided.

Many types of aquatic animals commonly referred to as "fish" are not fish in the sense given above; examples include shellfish, cuttlefish, starfish, crayfish and jellyfish. In earlier times, even biologists did not make a distinction – sixteenth century natural historians classified also seals, whales, amphibians, crocodiles, even hippopotamuses, as well as a host of aquatic invertebrates, as fish.[20] However, according to the definition above, all mammals, including cetaceans like whales and dolphins, are not fish. In some contexts, especially in aquaculture, the true fish are referred to as finfish (or fin fish) to distinguish them from these other animals.

A typical fish is ectothermic, has a streamlined body for rapid swimming, extracts oxygen from water using gills or uses an accessory breathing organ to breathe atmospheric oxygen, has two sets of paired fins, usually one or two (rarely three) dorsal fins, an anal fin, and a tail fin, has jaws, has skin that is usually covered with scales, and lays eggs.

Each criterion has exceptions. Tuna, swordfish, and some species of sharks show some warm-blooded adaptations – they can heat their bodies significantly above ambient water temperature.[18] Streamlining and swimming performance varies from fish such as tuna, salmon, and jacks that can cover 10–20 body-lengths per second to species such as eels and rays that swim no more than 0.5 body-lengths per second.[21] Many groups of freshwater fish extract oxygen from the air as well as from the water using a variety of different structures. Lungfish have paired lungs similar to those of tetrapods, gouramis have a structure called the labyrinth organ that performs a similar function, while many catfish, such as Corydoras extract oxygen via the intestine or stomach.[22] Body shape and the arrangement of the fins is highly variable, covering such seemingly un-fishlike forms as seahorses, pufferfish, anglerfish, and gulpers. Similarly, the surface of the skin may be naked (as in moray eels), or covered with scales of a variety of different types usually defined as placoid (typical of sharks and rays), cosmoid (fossil lungfish and coelacanths), ganoid (various fossil fish but also living gars and bichirs), cycloid, and ctenoid (these last two are found on most bony fish).[23] There are even fish that live mostly on land or lay their eggs on land near water.[24] Mudskippers feed and interact with one another on mudflats and go underwater to hide in their burrows.[25] A single, undescribed species of Phreatobius, has been called a true "land fish" as this worm-like catfish strictly lives among waterlogged leaf litter.[26][27] Many species live in underground lakes, underground rivers or aquifers and are popularly known as cavefish.[28]`

const getNormalText = (text)=>JSON.stringify({
  data:{
  "ops": [
      {
        "insert": text,
      },
    ]
  }
});

const photoUrl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Altolamprologus_compressiceps_-_Karlsruhe_Zoo_01.jpg/1280px-Altolamprologus_compressiceps_-_Karlsruhe_Zoo_01.jpg';

export const focusedStory = () => <FocusedStory publishedStory={{
  photoBackgroundColor:"#AAAAAA",
  photoUrl,
  impressions_count:30,
  owner:{email:"sample author"},
  name:"Cool Fish",
  text:getNormalText(fishText),
  rateUrl:"rate",
  url:"get",
}}/>;

export const focusedStoryLongText = ()=><FocusedStory publishedStory={{
  photoBackgroundColor:"#BBBBBB",
  photoUrl,
  owner:{email:"sample author"},
  name: "Really long text",
  impressions_count:40,
  text: getNormalText([...new Array(1000)].map(x=>"AAAAAA").join("")),
  rateUrl:"rate",
  url:"get",
}}/>;

export const focusedStoryMaxLengthTitle = ()=><FocusedStory publishedStory={{
  photoBackgroundColor:"#BBBBBB",
  photoUrl,
  owner:{email:"sample author"},
  impressions_count:50,
  name: [...new Array(30)].map(()=>'A'),
  text: getNormalText("Really long title"),
  rateUrl:"rate",
  url:"get",
}}/>;

export const focusedStoryWideImage = ()=><FocusedStory publishedStory={{
  photoBackgroundColor:"#BBBBBB",
  photoUrl:"https://upload.wikimedia.org/wikipedia/commons/4/44/Cucurbita_2018_G1.jpg",
  owner:{email:"sample author"},
  impressions_count:50,
  name: "Wide image",
  text: getNormalText("Please won't you push me for the last time"),
  rateUrl:"rate",
  url:"get",
}}/>;

export const focusedStoryNarrowImage = ()=><FocusedStory publishedStory={{
  photoBackgroundColor:"#BBBBBB",
  photoUrl:"https://upload.wikimedia.org/wikipedia/commons/9/92/Persicaria_maculosa.jpg",
  owner:{email:"sample author"},
  impressions_count:50,
  name: "Narrow image",
  text: getNormalText("Please won't you push me for the last time"),
  rateUrl:"rate",
  url:"get",
}}/>;
