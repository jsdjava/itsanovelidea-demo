variable "itsanovelidea_service_name" {
  description = "The name to use for all the service resources"
  type        = string
}

variable "itsanovelidea_task_definition_arn" {
  description = "Task definition for the service"
  type        = string
}

variable "itsanovelidea_cluster_id" {
  description = "ECS Cluster where all the services are running"
  type = string
}

variable "itsanovelidea_vpc_subnets" {
  description = "Subnets to put the asg for the service on"
  type = list(string)
}

variable "itsanovelidea_subdomain_prefix" {
  description = "the prefix for the subdomain to use  for the service, i.e mongo.itsanovelidea.com for mongo"
  type = string
}
