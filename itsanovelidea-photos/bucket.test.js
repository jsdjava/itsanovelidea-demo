const fs = require('fs');
const request = require('request');
const { uploadTextFile, uploadImageFromUrl } = require('./bucket.js');

test('uploadTextFile should return the location of the file it has uploaded', async () => {
  const location = await uploadTextFile('nirvana', 'come as you are');
  expect(location).toEqual('./tmp/buckets/itsanovelidea-photos/nirvana');
});
test('uploadTextFile should upload to s3 ', async () => {
  const location = await uploadTextFile('nirvana', 'come as you are');
  expect(fs.existsSync(location)).toBeTruthy();
  expect(fs.readFileSync(location).toString('utf8')).toEqual('come as you are');
});

test('uploadImageFromUrl should should pipe the image from the url using request', (done) => {
  const mockPipe = jest.fn((imgStream) => {
    // Need to fill the imgStream with some fake values
    imgStream.push(Buffer.from('IM TOTALLY AN IMAGE DUDE'));
    imgStream.push(null);
    return imgStream;
  });
  request.mockReturnValueOnce({

    // I have to return a mock pipe function so I can make sure that the pipe function is
    // being chained off the request function
    pipe: mockPipe,
  });
  uploadImageFromUrl('https://image.com', 'file.png', () => {
    expect(request).toHaveBeenCalledWith('https://image.com');
    expect(mockPipe).toHaveBeenCalled();
    done();
  });
});

test('uploadImageUrl should callback with the location of the image', (done) => {
  request.mockReturnValueOnce({

    // Can't return an img stream, the code in the mock package is buggy...
    pipe: () => 'IM TOTALLY AN IMAGE DUDE',
  });
  uploadImageFromUrl('https://image.com', 'image.txt', (location) => {
    expect(location).toEqual('./tmp/buckets/itsanovelidea-photos/image.txt');
    expect(fs.existsSync(location)).toBeTruthy();
    expect(fs.readFileSync(location).toString('utf8')).toEqual('IM TOTALLY AN IMAGE DUDE');
    done();
  });
});
