module.exports = {
    routeRegex:/stories\/[\d]+/,
    storyName: ".focusedItemBackground .focusedStoryContentHeaderTitle",
    storyUserLink: ".focusedItemBackground .focusedStoryContentHeaderAuthor a",
    storyRedditLink: ".focusedItemBackground .focusedStoryContentHeaderBottomRow a",
    storyText: ".focusedItemBackground .ql-editor p",
    storyImage: ".focusedItemBackground .focusedStoryViewerDiv img",
    storyViews: ".focusedItemBackground .focusedStoryContentHeaderBottomRow .views",
    storyLink: ".focusedItemBackground .focusedStoryContentHeaderBottomRow svg",
}