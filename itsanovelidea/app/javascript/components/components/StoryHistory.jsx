import React, { Component} from 'react'
import {Timeline, TimelineEvent} from 'react-event-timeline';
import InfiniteScroll from 'react-infinite-scroller';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { IconButton } from '@material-ui/core';

const moment = require('moment');

import "../stylesheets/StoryHistory.css";

class StoryHistory extends Component{

  constructor(){    
    super();
    this.state = {}
    this.onResize = this.onResize.bind(this);
  }
 
  onResize(){
    const {useDrawer} = this.state;
    const newUseDrawer = window.innerWidth < 960;
    if(useDrawer!=newUseDrawer){
        this.setState({
            useDrawer:newUseDrawer,
        })
    }
  }

  componentDidMount() {
      window.addEventListener('resize', this.onResize);
      this.onResize();
  }

  componentWillUnmount() {
      window.removeEventListener('resize', this.onResize);
  }

    render(){
        const {
            getHistory=()=>{},
            hasMoreHistory,
            onHistorySelected=()=>{},
            history=[],
        } = this.props;
        
        const {
          useDrawer,
          isOpen
        } = this.state;

        const timeline = <Timeline>
          <InfiniteScroll
            useWindow={false}
            className="infiniteScroll"
            pageStart={0}
            loadMore={()=>{
              getHistory();
            }}
            hasMore={hasMoreHistory}
            loader={<div className="loader" key={0}>Loading ...</div>}>
            {history.map(documentVersion=>{
              return <TimelineEvent 
                style={{ 
                  maxWidth: "95%",
                  flex: 1,
                  overflowX:"hidden",
                }}
                onClick = {()=>{
                  this.setState({
                    isOpen:false,
                  })
                  onHistorySelected(documentVersion)
                }}
                className = "timelineEvent"
                createdAt={moment(documentVersion.timestamp).format("MM-DD-YYYY hh:mm:ss A")}
                icon={<i className="material-icons md-18">{documentVersion.oldSnapshot.v}</i>}>
                {documentVersion.diff.map(documentChange=>{
                  const [changeType,change] = Object.entries(documentChange)[0];
                  if(changeType === 'add'){
                    return <div className="addDiv">+&nbsp;{change}</div>;
                  }
                  return <div className="deleteDiv">-&nbsp;{change}</div>;
                })}
              </TimelineEvent>
            })}
            </InfiniteScroll>
          </Timeline>;
        return <div className="storyHistoryDiv">
        {
          !history || history.length ?
          useDrawer ? 
          <div>
            {isOpen ? null : <IconButton onClick={()=>{
              this.setState({isOpen:true});
            }} aria-label="expandHistory">
              <ArrowBackIosIcon />
            </IconButton>}
          <SwipeableDrawer open={isOpen} anchor="right" className="storyHistoryDiv" 
          onClose={()=>{this.setState({isOpen:false})}}
          ModalProps={{
            onClick:()=>{
              this.setState({isOpen:false});
            }
          }} SlideProps={{
            onClick:(e)=>{
              e.stopPropagation();
            }
          }}>
            {timeline}
          </SwipeableDrawer></div>: timeline: <p>No history</p>
        }
      </div>
    }
}

export default StoryHistory;