import React, {Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import TermsOfUse from "./TermsOfUse.jsx";
import Background from "./Background.jsx";
 
import "../stylesheets/WelcomeNavBar.css";

const signInRoute = "/users/sign_in";
const signUpRoute = "/users/sign_up";

class WelcomeNavBar extends Component {

    constructor(){
        super();
        this.state = {}
    }

    render(){
      const {anchorEl,showTermsOfUse} = this.state;
      return <div>
      <Background/>
      <TermsOfUse 
        isOpen = {showTermsOfUse} 
        hideTermsOfUse={()=>this.setState({showTermsOfUse:false})}
      /><AppBar position="fixed" className="welcomeNavBar">
        <Toolbar variant="dense">
          <Typography variant="h6" className="brandHeader" onClick={()=>{
            window.location.href = "/";
          }}>
            Itsanovelidea
          </Typography>
          <Button onClick={()=>{
            window.location.href = signInRoute;
          }}>Sign In</Button>
          <Button onClick={()=>{
            window.location.href = signUpRoute;
          }}>Sign Up</Button>
          <Button data-qa="termsOfUseBtn" onClick={()=>{
            this.setState({
              showTermsOfUse:true
            })
          }}>Terms</Button>
          <IconButton aria-label="menu" className="welcomeMenuToggle" onClick={e=>this.setState({
            anchorEl:e.currentTarget,
          })}>
            <MenuIcon />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={anchorEl}
            onClose={()=>this.setState({anchorEl:null})}>
            <MenuItem onClick={()=>{
              this.setState({anchorEl:null});
              window.location.href = signInRoute;
            }}>Sign In</MenuItem>
            <MenuItem onClick={()=>{
              this.setState({anchorEl:null});
              window.location.href = signUpRoute;
            }}>Sign Up</MenuItem>
            <MenuItem onClick={()=>{
              this.setState({anchorEl:null});
              showTermsOfUse();
            }}>Terms</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
      </div>;
    }
}

export default WelcomeNavBar;