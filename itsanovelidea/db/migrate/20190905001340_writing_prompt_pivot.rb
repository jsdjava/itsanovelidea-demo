class WritingPromptPivot < ActiveRecord::Migration[5.1]
  def up
    remove_reference :stories, :access_rule, index: true, foreign_key: true
    remove_column :stories, :minRating
    remove_reference :stories, :group, index: true, foreign_key: true
    remove_reference :stories, :category, index: true, foreign_key: true
    drop_table :categories
    drop_table :groups
    drop_table :ratings
    drop_table :user_group_statuses
    drop_table :user_story
    drop_table :access_rules
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
