class AddPrimaryKeyToUserStory < ActiveRecord::Migration[5.1]
  def change
    add_column :user_story, :id, :primary_key
  end
end
