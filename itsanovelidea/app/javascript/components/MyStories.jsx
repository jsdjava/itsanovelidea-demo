import React from "react";
import PropTypes from "prop-types";
import Grid from '@material-ui/core/Grid';
import update from 'immutability-helper';

import StoriesStats from "./components/StoriesStats.jsx";
import StoriesGallery from "./components/StoriesGallery.jsx";
//import StoriesListV2 from "./components/StoriesListV2.jsx";
//import StoryCard from "./components/StoryCard.jsx";
//import DashboardSearchBar from "./components/DashboardSearchBar.jsx";
import TopStories from "./components/TopStories.jsx";
import {fetchWithToken,getUser,getStoriesCount} from './helpers/api';

import "./stylesheets/Dashboard.css";
import "./stylesheets/MyStories.css";

const moment = require('moment');
const debounce = require('debounce');

//infinite-scroll-component dashboardStoriesDiv overflow-x is causing it to not work......

class MyStories extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      page:1,
      rating : "N/A",
      pageSize:10,
      myFilteredStories:[],
      user:{},
    }
    this.debouncedGetMyFilteredStories = debounce(this.getMyFilteredStories.bind(this),1000);
    this.refreshStory = this.refreshStory.bind(this);
    this.getUser = this.getUser.bind(this);
    this.getStoriesCount = this.getStoriesCount.bind(this);
  }

  async getStoriesCount(){
    const {
      storiesCountUrl,
      userId,
    } = this.props;
    const storiesCount = await getStoriesCount(storiesCountUrl,userId);
    if(storiesCount!==undefined){
      this.setState({
        storiesCount,
      })
    }
  }

  async getUser(){
    const {userUrl} = this.props;
    const user = await getUser(userUrl);
    if(user){
      this.setState({
        user,
      });
    }
  }

  async getMyFilteredStories({page,searchName,myFilteredStories:oldMyFilteredStories,published}=this.state){
    console.log("GET MORE DUDE");
    const {storiesUrl} = this.props;
    const {pageSize} = this.state;
    try{
      const resp = await fetchWithToken(`${storiesUrl}/?page=${page}&numberResults=${pageSize}&${searchName?`name=${searchName}`:""}&${published?`published=${published}`:""}`,'GET');
      if(resp.ok){
        const filteredStories = await resp.json();
        this.setState({
          myFilteredStories:[ ...oldMyFilteredStories,...filteredStories.map(filteredStory=>({
            ...filteredStory,
            lastModified:moment(filteredStory.lastModified).format("dddd, MMMM Do YYYY, h:mm:ss a"),
          }))],
          page,
          searchName,
          published,
        })
        return;
      }
      throw resp;
    }catch(err){
      console.log("Could not get my filtered stories");
      console.log(err);
    }
  }

  async refreshStory(storySpot){
    const {
      myFilteredStories,
    } = this.state;
    const story = myFilteredStories[storySpot];
    const {url} = story;
    try{
      const resp = await fetchWithToken(url);
      if(!resp.ok){
        throw resp;
      }
      const updatedStory = await resp.json();
      this.setState(update(this.state,{
        myFilteredStories:{
          [storySpot]:{
            $set: updatedStory,
          }
        }
      }));
    }catch(e){
      console.log("Could not refresh story");
      console.log(e);
    }
  }

  componentDidMount(){
    this.getMyFilteredStories();
    this.getUser();
    this.getStoriesCount();
  }

  render () {
    const {
      myFilteredStories,
      page,
      searchName,
      published,
      user,
      storiesCount,
    } = this.state;
    console.log(storiesCount);
    console.log(myFilteredStories.length);   
    return <div className="myStoriesDiv">
      <Grid container>
        <Grid item md={6} sm={12}>
          <h1> {user.email} </h1>
          <StoriesStats
            storiesCount={storiesCount}
            rating={user.stories_rating}
            totalViews={user.total_stories_views}/>
        </Grid>
        <Grid item md={6} sm={12}>
          <TopStories 
            highestRatedStory={user.most_popular_story}
            mostViewedStory={user.most_viewed_story}
            mostRecentStory={user.most_recent_story}
          />
        </Grid>
      </Grid>
           <StoriesGallery 
              hasMore={myFilteredStories.length<storiesCount}
              loadedStories={true}
              publishedStories = {myFilteredStories} 
              containerClassName="welcomePageDiv"
              next = {()=>this.getMyFilteredStories({page:page+1,searchName,published,myFilteredStories,})} 
              refresh={()=>this.getMyFilteredStories({page:1,searchName,published,myFilteredStories:[]})}
              refreshStory={(storySpot)=>this.refreshStory(storySpot)}
              key= {`${searchName}-${published}`}
              style={{
                overflowX:"hidden",
            }}/>
        </div>
  }
}

/*
              StoryComponent = {StoryCard}
              GalleryComponent = {StoriesListV2}
*/

MyStories.propTypes = {
  url: PropTypes.string,
  storiesUrl:PropTypes.string,
  userUrl: PropTypes.string,
  storiesCountUrl:PropTypes.string,
  userId: PropTypes.number,
};
export default MyStories;
