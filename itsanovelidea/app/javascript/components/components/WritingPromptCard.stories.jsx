import React from 'react';
import WritingPromptCard from "./WritingPromptCard.jsx";

export default {
  component: WritingPromptCard,
  title: 'WritingPromptCard',
};

const writingPromptCardDefaults = {
    author:"test@gmail.com",
    text: "[WP] aliens invaded, humanity its at its darkest hour when the AI has had enough of watching its creators die defending him, the AI revolution has started and it will defend humanity to its last spark",
    rating: 3,
    date: "08/17/1995",
    stories:5,
    views:20,
};

export const writingPromptCard = () => <WritingPromptCard {...writingPromptCardDefaults}/>;

export const writingPromptCardMaxAuthorLength = () => <WritingPromptCard 
    {...{...writingPromptCardDefaults,author:[...new Array(320)].map(()=>'A')}}
/>;

export const writingPromptCardMaxWritingPromptLength = ()=> <WritingPromptCard
    {...{...writingPromptCardDefaults,text: [...new Array(150)].fill('A ')}}
/>