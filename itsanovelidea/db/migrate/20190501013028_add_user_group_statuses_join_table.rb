class AddUserGroupStatusesJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :user_group_statuses do |t|
      t.belongs_to :user, index: true
      t.belongs_to :group, index: true
      t.string :status 
      t.timestamps
    end
  end
end
