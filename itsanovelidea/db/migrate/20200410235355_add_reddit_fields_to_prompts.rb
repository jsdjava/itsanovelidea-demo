class AddRedditFieldsToPrompts < ActiveRecord::Migration[5.1]
  def change
    add_column :writing_prompts, :reddit_permalink, :string
    add_column :writing_prompts, :reddit_id, :string
    add_column :writing_prompts, :reddit_score, :int
  end
end
