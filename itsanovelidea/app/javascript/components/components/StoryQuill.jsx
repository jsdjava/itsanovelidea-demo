import React, { Component} from 'react'
import IconButton from '@material-ui/core/IconButton';
import ReactQuill, { Quill } from 'react-quill';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import QuillCursors from 'quill-cursors/src/cursors';

import StoryViewer from "./StoryViewer.jsx";

import 'react-quill/dist/quill.snow.css';
import '../stylesheets/StoryQuill.css';

//Quill.register('modules/cursors', QuillCursors);

const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],
  ['blockquote', 'code-block'],
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  [{ 'color': [] }, { 'background': [] }],          
  [{ 'font': [] }],
  [{ 'align': [] }],
  ['clean'],                                         
];

const quillModules = {
  //cursors: {
  //  autoRegisterListener: false
  //},
  history: {
    userOnly: true 
  },
  toolbar:toolbarOptions,
};

class StoryQuill extends Component {
  
  render(){
    const {
        storyTitle,
        oldDocument,
        registerQuillSocket=()=>{},
        onRevert = ()=>{},
        onExit = ()=>{},
    } = this.props;

    const RevertToolbar = () => (
        <div id="toolbar">
            <IconButton aria-label="revert">
                <CheckIcon onClick={onRevert}/>
            </IconButton>
            <IconButton aria-label="cancel">
                <CloseIcon onClick={onExit}/>
            </IconButton>
        </div>
      )

    const editQuill = <ReactQuill modules={quillModules}
      className = {oldDocument ? "hidden" : ""}
      readOnly={true}
      defaultValue=""
      ref={(reactQuill)=>{
        if(reactQuill && !this.quill){
          this.quill = reactQuill.getEditor();
          registerQuillSocket(this.quill) || (()=>{});
        } else{
          this.quill.enable();
        }
    }}/>;

    const revertQuill = <StoryViewer
      theme="snow"
      story={JSON.stringify(oldDocument || {})}
      modules={{...quillModules, toolbar: {
          container: "#toolbar"
      }}}>
        <RevertToolbar/>
      </StoryViewer>;

    return <div className="storyQuillDiv">
        <h3> {storyTitle}</h3>
        {
            oldDocument ? <div className="revertDiv">
                {revertQuill}
            </div> : null
        }
        {editQuill}
    </div>;
  }
}

export default StoryQuill;