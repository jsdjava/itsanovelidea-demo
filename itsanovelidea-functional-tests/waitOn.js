process.env["NODE_CONFIG_DIR"] = __dirname + "/cypress/config";

const waitOn = require('wait-on');
const config = require('config');

const [,,serviceUrlKey] = process.argv;
const serviceUrl = config.get(serviceUrlKey);

waitOn({
  resources:[
    `${serviceUrl}:80/health`,
  ],
  headers:{
    apikey:process.env.API_KEY,
  }
}, err=> {
  // Not sure I have to do this, but the cypress stuff runs inside the cypress dir, so
  // figure I should reset it just to be sure
  delete process.env.NODE_CONFIG_DIR;
  if (err) {
    console.log(err);
    process.exit(1);
  }
  process.exit();
});