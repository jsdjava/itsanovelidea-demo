class ChatController < ApplicationController
    def get_previous_messages
        @story = Story.find_by_id(params[:documentId])
        return head(404) unless @story != nil
        return head(403) unless @story.owned?(current_user)
        response = RestClient.get(Rails.configuration.chat_host+request.fullpath,{
          apikey: Rails.configuration.node_api_key,
        })
        render :json=>response
    end
end
