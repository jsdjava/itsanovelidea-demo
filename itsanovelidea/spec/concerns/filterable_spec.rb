require 'rails_helper'

#TODO Replace writing prompt with filterable....
RSpec.shared_examples "filterable" do |filterable_symbol|
  describe "sorting by date" do
    it "does nothing" do
      expected_writing_prompts = create_list(filterable_symbol,5)
      actual_writing_prompts = described_class.sort_by_date({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts by desc created at" do
      expected_writing_prompts = create_list(filterable_symbol,5).sort_by {|writing_prompt|writing_prompt.created_at}.reverse
      actual_writing_prompts = described_class.sort_by_date({:sort_by_date=>'desc'}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts by asc created at" do
      expected_writing_prompts = create_list(filterable_symbol,5).sort_by {|writing_prompt|writing_prompt.created_at}
      actual_writing_prompts = described_class.sort_by_date({:sort_by_date=>'asc'}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
  end
  describe "sorting by views" do
    it "does nothing" do
      expected_writing_prompts = create_list(filterable_symbol,5)
      actual_writing_prompts = described_class.sort_by_views({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "sorts by descending views" do
      expected_writing_prompt_1 = create(filterable_symbol,impressions_count:3)
      expected_writing_prompt_2 = create(filterable_symbol,impressions_count:7)
      expected_writing_prompt_3 = create(filterable_symbol,impressions_count:5)
      actual_writing_prompts = described_class.sort_by_views({:sort_by_views=>'desc'}).to_a
      expect(actual_writing_prompts).to eql([expected_writing_prompt_2,expected_writing_prompt_3,expected_writing_prompt_1])
    end
    it "sorts by ascending views" do
      expected_writing_prompt_1 = create(filterable_symbol,impressions_count:3)
      expected_writing_prompt_2 = create(filterable_symbol,impressions_count:7)
      expected_writing_prompt_3 = create(filterable_symbol,impressions_count:5)
      actual_writing_prompts = described_class.sort_by_views({:sort_by_views=>'asc'}).to_a
      expect(actual_writing_prompts).to eql([expected_writing_prompt_1,expected_writing_prompt_3,expected_writing_prompt_2])
    end
  end

  describe "paging" do
    it "defaults to page 1 and 10 max items" do
      expected_writing_prompts = create_list(filterable_symbol,15)[0..9]
      actual_writing_prompts = described_class.page({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "returns up to a page of writing prompts" do
      expected_writing_prompts = create_list(filterable_symbol,15)[0..9]
      actual_writing_prompts = described_class.page({page:1,page_size:15}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "returns the last page of writing prompts" do
      expected_writing_prompts = create_list(filterable_symbol,15)[10..14]
      actual_writing_prompts = described_class.page({page:2,page_size:10}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
  end

  describe "filter by user" do
    it "does nothing" do
      expected_writing_prompts = create_list(filterable_symbol,5)
      actual_writing_prompts = described_class.filter_by_user({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "filters by the matching user id" do
      user = create(:user)
      create_list(filterable_symbol,3)
      expected_writing_prompt_1 = create(filterable_symbol,owner:user)
      create_list(filterable_symbol,2)
      expected_writing_prompt_2 = create(filterable_symbol,owner:user)
      actual_writing_prompts = described_class.filter_by_user({:user=>user.id}).to_a
      expect(actual_writing_prompts).to eql([expected_writing_prompt_1,expected_writing_prompt_2])
    end
  end
  describe "filter by user email" do
    it "does nothing" do
      expected_writing_prompts = create_list(filterable_symbol,5)
      actual_writing_prompts = described_class.filter_by_like_user({}).to_a
      expect(actual_writing_prompts).to eql(expected_writing_prompts)
    end
    it "filters by the username containing the user email search text" do
      user = create(:user,email:"meme@gmail.com")
      expected_writing_prompt = create(filterable_symbol,owner:user)
      other_user = create(:user,email:"boutyu@yahoo.com")
      create_list(filterable_symbol,2,owner:other_user)
      actual_writing_prompts = described_class.filter_by_like_user({:user_like=>"mem"}).to_a
      expect(actual_writing_prompts).to eql([expected_writing_prompt])
    end
  end
end