const Joi = require("@hapi/joi");

describe('POST /photo/:searchKey', () => {
    // Processing a photo requires that the photo has alread been searched for
    // and cached
    let searchKey;
    before(()=>{
        cy.request({
            url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo?search=cat&page=3&numResults=5`,
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).then(resp=>{
            searchKey = resp.body.searchKey;
        })
    });
    it('returns 401 without the api key', () => {
      cy.request({
          method:"POST",
          url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo/${searchKey}?index=2`,
          failOnStatusCode:false,
      }).should(resp=>{
          expect(resp.status).to.equal(401);
      });
    });
    it('returns information about the processed photo (height, width, colors, s3 link,etc)',()=>{
        cy.request({
            method:"POST",
            url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo/${searchKey}?index=2`,
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).should(resp=>{
            const processPhotosSchema = Joi.object({
                idUrl: Joi.string().required(),
                height: Joi.number().required(),
                width: Joi.number().required(),
                backgroundColor: Joi.string().required(),
                textColor: Joi.string().required(),
                luminanceClass: Joi.string().required(),
            });
            const { error } = processPhotosSchema.validate(resp.body);
            expect(error).to.be.undefined;
        });
    });
    it('uploads a link to the photo in a text file in s3',()=>{
        cy.request({
            method:"POST",
            url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo/${searchKey}?index=2`,
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).then(resp=>{
            cy.request(resp.body.idUrl).then(resp=>{
                // So, I have no guarantee for when the image is uploaded from pixabay to s3 as its 
                // done asynchronously, so I will just check to see if the url in the txt file is
                // either pixabay or s3
                expect(resp.body).to.match(/pixabay|s3/);
            })
        });
    });
    it('eventually converts the link from pixabay to a valid s3 link',()=>{
        cy.request({
            method:"POST",
            url:`${Cypress.config("itsanovelideaPhotosUrl")}/photo/${searchKey}?index=2`,
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).then(resp=>{
            const pollForS3Url= ()=>{
                //https://docs.cypress.io/api/commands/request.html#Request-Polling
                cy.request(resp.body.idUrl).then(resp=>{
                    // So, I have no guarantee for when the image is uploaded from pixabay to s3 as its 
                    // done asynchronously, so I will just check to see if the url in the txt file is
                    // s3. If not, I will try agan
                    if(/s3/.test(resp.body)){
                        // Just make sure we get a valid response when we go to load the actual iamge
                        return cy.request(resp.body).then(resp=>{
                            // Just make sure the response is pretty big since its an image
                            expect(resp.body.length).to.be.above(50000);
                        });                    
                    }
                    return pollForS3Url();
                })
            }
            pollForS3Url();
        });
    });
  })