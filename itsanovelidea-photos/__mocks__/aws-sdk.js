const fs = require('fs-extra');
const awsMock = require('mock-aws-s3');

const bucketPath = './tmp/buckets';
fs.emptyDirSync(bucketPath);

awsMock.config.basePath = bucketPath;
awsMock.config.update = () => {};
module.exports = awsMock;
