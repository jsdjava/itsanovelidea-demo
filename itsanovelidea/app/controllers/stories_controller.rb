require 'rest-client'
require 'uri'

class StoriesController < ApplicationController
  before_action :set_story, only: [:show, :edit, :update, :destroy, :owned, :versions, :rate, :rating, :publish]
  before_action :check_story_owned, only: [:edit, :update, :destroy, :publish]
  skip_before_action :authenticate_user!, only: [:owned, :published, :published_count, :show]
  skip_before_action :authenticate_api_key!, only: [:owned, :published, :published_count]

  # POST /stories
  # POST /stories.json
  def create
    @writing_prompt = WritingPrompt.find_by(id:story_params[:writing_prompt_id].to_i)
    @story = Story.new(story_params.except(:search_hash, :index, :writing_prompt_id))
    @story.writing_prompt = @writing_prompt
    @story.owner = current_user
    saved = true
    ActiveRecord::Base.transaction do
      begin 
        @story.save(:validate => false)
        @story.create_document()
        #@story.create_chat()
        photo_metadata_response = @story.download_photo(story_params[:search_hash],story_params[:index])
        puts photo_metadata_response.inspect
        photo_metadata = JSON.parse(photo_metadata_response.body)
        @story.update_photo_metadata(photo_metadata)
        @story.photo_url = @story.get_photo_url.body
        @story.save!
      rescue => error
        puts error.inspect
        saved = false
        raise ActiveRecord::Rollback
      end
    end
    respond_to do |format|
      if saved
        format.html { redirect_to @story, notice: 'Story was successfully created.' }
        format.json { render :show, status: :created, location: @story }
      else
        format.html { render :new }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  def publish
    @story.publish
    render :json => {published:true}
  end

  def published
    @stories = Story.get_published_filtered_stories(params)
    impressionist_aggregate(@stories)
  end

  def published_count
    @stories_count = Story.get_published_filtered_stories_count(params)
    render :json => {stories_count:@stories_count}
  end

  def stories_count
    @stories_count = Story.get_filtered_stories_count(params)
    render :json => {stories_count:@stories_count}
  end

  # GET /stories
  # GET /stories.json
  def index
    @stories = Story.get_my_filtered_stories(current_user,params)
    
    # TODO I'm taking advantage of writing prompts not being deletable here (i.e. guaranteed a contiguous sequence)
    @writing_prompt = WritingPrompt.first 
    @max_writing_prompt = WritingPrompt.last
  end

  def versions
    @versions = @story.get_versions(params);
    render json: @versions
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
  end

  # GET /stories/new
  def new
    @writing_prompt = WritingPrompt.find(params[:writing_prompt_id])
    head(404) unless !@writing_prompt.nil?
  end

  # GET /stories/1/edit
  def edit
    @node_url = 'wss://'+Rails.application.config.node_host
  end

  def rate
    return head(404) unless !@story.nil?
    user_story = UserStory.find_or_create_by(user:current_user,story:@story)
    user_story.rating = params[:rating]
    user_story.save!
    render :json => {rating:user_story.rating}
  end

  def rating
    user_story = UserStory.find_by(user:current_user,story:@story)
    return head(404) unless !user_story.nil?
    render :json => {rating:user_story.rating}
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @story, notice: 'Story was successfully updated.' }
        format.json { render :show, status: :ok, location: @story }
      else
        format.html { render :edit }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy

    #TODO This doesn't get rid of the document....
    @story.destroy
    respond_to do |format|
      format.html { redirect_to stories_url, notice: 'Story was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def owned
    #TODO Use an apikey too probably
    user = User.find(params[:user_id])
    if @story.owned?(user)
      return head :ok
    end
    head :unauthorized
  end

  private

    def check_story_owned
      head :unauthorized unless @story.owner == current_user
    end

    def define_view_vars
      @stories = Story.get_filtered_stories(current_user,params)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_story
      @story = Story.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def story_params
      params.require(:story).permit(:search_hash, :index, :writing_prompt_id, :name, :page, :page_size,:published, :sort_by_date, :sort_by_views, :rating)
    end

end
