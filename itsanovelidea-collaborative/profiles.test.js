const profilesFn = require('./profiles.js');

test('it should allow anyone to read a profile', async () => {
  const allowReadMock = jest.fn();
  profilesFn({
    access: {
      allowRead: allowReadMock,
      allowUpdate: jest.fn(),
    },
  });
  expect(allowReadMock).toHaveBeenCalled();
  // 2nd parameter passed to allowRead is the authorization function it should use
  const [, authFn] = allowReadMock.mock.calls[0];
  expect(await authFn()).toBeTruthy();
});

test('it should allow the owner of a profile to edit it', async () => {
  const allowUpdateMock = jest.fn();
  profilesFn({
    access: {
      allowRead: jest.fn(),
      allowUpdate: allowUpdateMock,
    },
  });
  expect(allowUpdateMock).toHaveBeenCalled();
  const [, authFn] = allowUpdateMock.mock.calls[0];

  // actualId, 3 parameters that don't matter, expected id
  expect(await authFn(1, {}, {}, {}, 1)).toBeTruthy();
});

test('it should not allow unauthorized people to edit a profile', async () => {
  const allowUpdateMock = jest.fn();
  profilesFn({
    access: {
      allowRead: jest.fn(),
      allowUpdate: allowUpdateMock,
    },
  });
  expect(allowUpdateMock).toHaveBeenCalled();
  const [, authFn] = allowUpdateMock.mock.calls[0];
  // actualId, 3 parameters that don't matter, expected id
  expect(await authFn(1, {}, {}, {}, 2)).toBeFalsy();
});

test('getProfile calls into sharedb for the profile', async () => {
  const fetchSnapshotMock = jest.fn();
  const { getProfile } = profilesFn({
    // Needed to get past initial middleware registration in profiles.js
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    fetchSnapshot: fetchSnapshotMock,
  });
  await getProfile(24);
  expect(fetchSnapshotMock).toHaveBeenCalled();
  expect(fetchSnapshotMock).toHaveBeenCalledWith('profiles', 24);
});

test('createProfile calls into sharedb to make the profile', async () => {
  const createDocumentMock = jest.fn();
  const { createProfile } = profilesFn({
    access: {
      allowRead: jest.fn(),
      allowUpdate: jest.fn(),
    },
    createDocument: createDocumentMock,
  });
  await createProfile(25);
  expect(createDocumentMock).toHaveBeenCalled();
  expect(createDocumentMock).toHaveBeenCalledWith('profiles', 25);
});
