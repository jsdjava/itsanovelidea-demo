import React from "react";
import PropTypes from "prop-types";

import {TextValidator} from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LockIcon from '@material-ui/icons/Lock';
import {SocialMediaIconsReact} from 'social-media-icons-react';

import Account from "./components/Account.jsx";
import "./stylesheets/Login.css";

class Login extends React.Component{
    render(){
        const {
            loginUrl,
            facebookUrl,
            googleUrl,
            redditUrl,
        } = this.props;
        return <Account url = {loginUrl}  
            title="Sign in"
            btnDataQaTag="loginBtn"
            btnTitle="Login"
            commit="Login"
            formKeys={["email","password"]}
            render={ (setFormValue,{email,password})=><div className="loginInnerDiv">
                    <div className="accountTextField">
                    <TextValidator
                        name="email"
                        label="Your email"
                        value={email}
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <MailOutlineIcon/>
                            </InputAdornment>
                        ),
                        }}
                        inputProps={{
                          "data-qa":"emailInput",
                        }}
                        type="email"
                        onChange={(e)=>setFormValue("email",e)}
                        validators={['required', 'isEmail']}
                        errorMessages={['*required', 'email is not valid']}
                    />
                    </div>
                    <div className="accountTextField">
                    <TextValidator
                        name="password"
                        label="Your password"
                        value={password}
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <LockIcon/>
                            </InputAdornment>
                        ),
                        }}
                        inputProps={{
                            "data-qa":"passwordInput",
                        }}
                        type="password"
                        onChange={(e)=>setFormValue("password",e)}
                        validators={['required']}
                        errorMessages={['*required']}
                    />
                    </div>
                </div>
            }
            renderAfter={()=><div className = "loginSocialIconsDiv">
                    <br/>
                    <h3 style={{color:"black"}}> or </h3>
                    <SocialMediaIconsReact 
                        borderColor="rgba(0,0,0,0.25)" 
                        borderWidth="0" 
                        borderStyle="groove" 
                        icon="facebook" 
                        iconColor="rgba(255,255,255,1)" 
                        backgroundColor="rgb(59, 89, 152)" 
                        iconSize="5" 
                        roundness="23%" 
                        url={facebookUrl} 
                        target=''
                        size="38" />
                    <SocialMediaIconsReact 
                        borderColor="rgba(0,0,0,0.25)" 
                        borderWidth="0" 
                        borderStyle="groove" 
                        icon="googleplus" 
                        iconColor="rgba(255,255,255,1)" 
                        backgroundColor="rgb(220, 78, 65)" 
                        iconSize="5" 
                        roundness="23%" 
                        url={googleUrl} 
                        target=''
                        size="38" />
                    <SocialMediaIconsReact 
                        borderColor="rgba(0,0,0,0.25)" 
                        borderWidth="0" 
                        borderStyle="groove" 
                        icon="reddit" 
                        iconColor="rgba(255,255,255,1)" 
                        backgroundColor="rgb(255, 69, 0)" 
                        iconSize="5" 
                        roundness="23%" 
                        url={redditUrl} 
                        target=''
                        size="38" />
                </div>
            }/>
    };
}

Login.propTypes = {
  googleUrl: PropTypes.string.isRequired,
  facebookUrl: PropTypes.string.isRequired,
  redditUrl: PropTypes.string.isRequired,
};

export default Login;