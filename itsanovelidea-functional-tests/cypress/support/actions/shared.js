const {sample} = require('lodash');
const md5 = require('md5');
const faker = require('faker');

const {loginPage,signUpPage,welcomePage} = require('../pages');

const {loginBtn} = loginPage;
const {confirmedPasswordInput,signUpBtn} = signUpPage;
const {
  storyTileDivs,
  storyTileUserLink,
} = welcomePage;

const clickUserLink = ()=>{
    cy.get(storyTileUserLink)
      .first()
      .should('be.visible')
      .click({force:true});
}

const forceVisit = (appUrl,route)=>{
    cy.visit(`${appUrl}/whatahack`,{
        failOnStatusCode:false,
    });
    cy.clearCookies();
    cy.visit(`${appUrl}/${route}`);
}

const expandStory = ()=>{
  cy.get(storyTileDivs)
    .first()
    .should('be.visible')
    .click({force:true});
}

const login = (email,password, waitForLogin = true)=>{
    cy.window().then(window=>{
        window.initial=true
    });
  
    cy.get(loginPage.emailInput)
      .type(email)
      .should('have.value', email);
    
    cy.get(loginPage.passwordInput)
      .type(password)
      .should('have.value', password);
    
    cy.get(loginBtn)
      .click();
  
    if(!waitForLogin) return;
    cy.window()
    .pipe(x=>x,{timeout:15000})
    .should(window=>{
      expect(window.initial).not.to.eq(true);
    })
}
  
const signUp = (email, password, confirmedPassword = password)=>{
    cy.get(signUpPage.emailInput)
        .type(email)
        .should('have.value', email);
    
    cy.get(signUpPage.passwordInput)
        .type(password)
        .should('have.value',password);
    
    cy.get(confirmedPasswordInput)
        .type(confirmedPassword)
        .should('have.value',confirmedPassword);
    
    cy.get(signUpBtn)
        .click();
}

const rapidApiHost = "privatix-temp-mail-v1.p.rapidapi.com";
const rapidApiApiKey = "redacted";

const requestRapidApi = (route,options={})=>{
    return cy.request({
        url:`https://privatix-temp-mail-v1.p.rapidapi.com/request/${route}/`,
        headers:{
            "X-RapidAPI-Host": rapidApiHost,
            "X-RapidAPI-Key": rapidApiApiKey,
        },
        ...options,
    });
}

const getRandomEmail = ()=>{
    return requestRapidApi("domains").then(({body})=>{
        expect( body.length).to.be.above(0);
        return `${faker.lorem.word()}${sample(body)}`;
    });
}

const getEmail = (emailAddress,curTry=0,emailIndex=0)=>{
    const pollInterval = 5000;
    const pollTries = 3;

    //rapid-api will literally charge you money, so im waiting here before i make the call
    cy.wait(pollInterval);
    return requestRapidApi(`mail/id/${md5(emailAddress)}`).then(({body})=>{
        if(body.length){
            return body[emailIndex].mail_text;
        }else if (curTry<pollTries){ 
            return getEmail(emailAddress,++curTry,emailIndex);
        } else{
            return;
        }
    });
}

const assertSort = ({sortBtn,sortCells,isInt,isDate})=>{
    cy.get(sortBtn).click();
    cy.document()
        .pipe((x)=>x.querySelectorAll(sortCells),{timeout:15000})
        .should(x=>{
          if(isInt){
            const actual = [...x].map(x=>parseInt(x.innerText));
            expect(actual.filter(x=>x).length).to.be.at.least(3);
            expect([...actual].sort((a,b)=>a-b).reverse()).to.eql(actual);
          } else if(isDate){
            const actual = [...x].map(x=>new Date(x.innerText));
            expect(actual.filter(x=>x).length).to.be.at.least(3);
            expect([...actual].sort((a,b)=>a-b).reverse()).to.eql(actual);
          }else{
            const actual = [...x].map(x=>x.innerText);
            expect(actual.length).to.be.at.least(1);
            expect([...actual].sort().reverse()).to.eql(actual);
          }
        });
}

module.exports = {
    login,
    forceVisit,
    signUp,
    expandStory,
    getRandomEmail,
    getEmail,
    clickUserLink,
    assertSort,
};