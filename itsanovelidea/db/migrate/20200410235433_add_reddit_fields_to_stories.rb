class AddRedditFieldsToStories < ActiveRecord::Migration[5.1]
  def change
    add_column :stories, :reddit_total_awards_received, :int
    add_column :stories, :reddit_id, :string
    add_column :stories, :reddit_score, :int
    add_column :stories, :reddit_permalink, :string
  end
end
