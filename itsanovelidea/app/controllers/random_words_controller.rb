require 'spicy-proton'

class RandomWordsController < ApplicationController
    skip_before_action :authenticate_user!
    
    def index
        render :json => {
            random_word: Spicy::Proton.pair(" ")
        }
    end

end
