class CreateAccessRules < ActiveRecord::Migration[5.1]
  def change
    create_table :access_rules do |t|
      t.string :rule_type

      t.timestamps
    end
  end
end
