const InfiniteScrollSearch = require("../components/helpers/infiniteScrollSearch.js");

const {expect} = require('chai')
const photoSearchMock = require("./photoSearchMock.js");

describe("infinite image search scrolling",()=>{
  describe("initial load",()=>{
    it("should return an empty [] for no results",async ()=>{
        const iss = new InfiniteScrollSearch(8,photoSearchMock(0));
        await iss.load();
        expect(iss.current()).to.deep.eq([]);
    });
    it("should return 1 item for 1 result",async ()=>{
        const iss = new InfiniteScrollSearch(8,photoSearchMock(1));
        await iss.load();
        expect(iss.current()).to.deep.eq([1]);
    });
    it("should return the page size for lots of items in current",async ()=>{
        const iss = new InfiniteScrollSearch(4,photoSearchMock(30));
        await iss.load();
        expect(iss.current()).to.deep.eq([1,2,3,4,]);
    });
    it("should return the page size*2 for lots of items in all",async ()=>{
        const iss = new InfiniteScrollSearch(4,photoSearchMock(30));
        await iss.load();
        expect(iss.all()).to.deep.eq([1,2,3,4,5,6,7,8]);
    });
  })
  describe("scroll left",()=>{
    describe("current",()=>{
        it("should move back by one item",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.next();
            await iss.previous();
            expect(iss.current()).to.deep.equal([1,2,]);
        });
        it("should stop at the first item",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.previous();
            expect(iss.current()).to.deep.equal([1,2,]);
        });
        it("should page correctly",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.previous();
            await iss.previous();
            expect(iss.current()).to.deep.equal([2,3]);
        });
    });
    describe("all",()=>{
        it("should not clear when moving back to the 1st page",async()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.previous();
            await iss.previous();
            expect(iss.all()).to.deep.equal([1,2,3,4]);
        });
        it("should clear when moving back to the 2nd page or >",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.previous();
            await iss.previous();
            expect(iss.all()).to.deep.equal([1,2,3,4,5,6]);
        });
    });
  });
  describe("scroll right",()=>{
    describe("current",()=>{
        it("should move forward by one item",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(15));
            await iss.load();
            await iss.next();
            expect(iss.current()).to.deep.equal([2,3]);
        });
        it("should stop at the last item",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(1));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            expect(iss.current()).to.deep.equal([1]);
        });
        it("should cross pages correctly",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(20));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.next();
            expect(iss.current()).to.deep.equal([5,6]);
        });
        it("should return up to page size at the end of a list",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(5));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.next();
            expect(iss.current()).to.deep.equal([4,5]);
        });
        it("should correctly setup paging when a list of items is too short for the 2*pagesize initial load",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(3));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            expect(iss.current()).to.deep.equal([2,3]);
        });
    })
    describe("all",()=>{
        it("should correctly load hitting the end of a list",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(7));
            await iss.load();
            for(let i = 0; i<7; i++){
                await iss.next();
            }
            expect(iss.all()).to.deep.equal([3,4,5,6,7]);
        });
        it("should correctly load hitting the end of a list and moving back",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(7));
            await iss.load();
            for(let i = 0; i<7; i++){
                await iss.next();
            }
            await iss.previous();
            expect(iss.all()).to.deep.equal([3,4,5,6,7]);
        });
        it("should correctly load at the end of a list of even page size",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(6));
            await iss.load();
            for(let i = 0; i<7; i++){
                await iss.next();
            }
            expect(iss.all()).to.deep.equal([3,4,5,6]);
        });
        it("should move correctly all the way to the end of an even list and then back",async ()=>{
            const iss = new InfiniteScrollSearch(2,photoSearchMock(6));
            await iss.load();
            for(let i = 0; i<7; i++){
                await iss.next();
            }
            for(let i = 0; i<7; i++){
                await iss.previous();
            }
            expect(iss.all()).to.deep.equal([1,2,3,4]);
        });
        it("should move correctly all the way to the end of an odd list and then back",async ()=>{
            const iss = new InfiniteScrollSearch(3,photoSearchMock(17));
            await iss.load();
            for(let i = 0; i<20; i++){
                await iss.next();
            }
            for(let i = 0; i<17; i++){
                await iss.previous();
            }
            expect(iss.all()).to.deep.equal([1,2,3,4,5,6]);
        });
        it("should correctly 'jitter'",async ()=>{
            const iss = new InfiniteScrollSearch(3,photoSearchMock(17));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.next();
            await iss.previous();
            await iss.next();
            await iss.previous();
            await iss.previous();
            await iss.previous();
            expect(iss.all()).to.deep.equal([1,2,3,4,5,6]);
        });
        it("should move back and forward on a too small list",async ()=>{
            const iss = new InfiniteScrollSearch(3,photoSearchMock(4));
            await iss.load();
            await iss.next();
            await iss.next();
            await iss.previous();
            expect(iss.all()).to.deep.equal([1,2,3,4]);
        });
        it("should correctly jitter in the middle",async ()=>{
            const iss = new InfiniteScrollSearch(3,photoSearchMock(17));
            await iss.load();
            await iss.next(); //2
            await iss.next(); //3
            await iss.next(); //4
            await iss.next(); //5
            await iss.next(); //6
            await iss.next();
            await iss.previous(); //5
            await iss.next(); //6
            expect(iss.all()).to.deep.equal([4,5,6,7,8,9,10,11,12]);
        });
        it("should move correctly from the start of a list to a few pages and then back and forward again",async ()=>{
            const iss = new InfiniteScrollSearch(4,photoSearchMock(1000));
            await iss.load();
            for(let i = 0; i<25; i++){
                await iss.next();
            }
            for(let i = 0; i<30; i++){
                await iss.previous();
            }
            for(let i =0; i<10; i++){
                await iss.next();
            }
            expect(iss.current()).to.deep.equal([11,12,13,14]);
            expect(iss.all()).to.deep.equal([7,8,9,10,11,12,13,14]);
        });
    });
  });
});