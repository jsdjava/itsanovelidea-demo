require 'rails_helper'

RSpec.describe "writing_prompts/edit", type: :view do
  before(:each) do
    @writing_prompt = assign(:writing_prompt, WritingPrompt.create!())
  end

  it "renders the edit writing_prompt form" do
    render

    assert_select "form[action=?][method=?]", writing_prompt_path(@writing_prompt), "post" do
    end
  end
end
