module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "itsanovelidea"
  cidr = "10.0.0.0/16"

  manage_default_network_acl= true

  azs             = ["us-east-1d"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]
  elasticache_subnets = ["10.0.31.0/24"]

  enable_dns_hostnames = true
  enable_dns_support   = true
  
  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
      Project="itsanovelidea"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = module.vpc.vpc_id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port   = 80
  }

  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    to_port   = 443
  }

  # So, this is a little iffy. Because I'm taking advantage of the public dns services, (so using addresses
  # like redis.itsanovelidea.com) in the instances, I need to open those ports up to the internet because
  # thats where the requests come in from, instead of staying inside the vpc. As a more secure, long term
  # solution, I should make it so that the ec2's have the private ip addresses of these services (probably not super hard)
  # For now, I will open them up to the internet since i have decent passwords set for both

  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port   = 22
  }

  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 6379
    to_port   = 6379
  }

  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 27017
    to_port   = 27017
  }


  ingress {
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 3306
    to_port   = 3306
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}