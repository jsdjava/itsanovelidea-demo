module.exports = {
    loginPage:require('./login.js'),
    dashboardPage:require('./dashboard.js'),
    signUpPage:require('./signUp.js'),
    resendConfirmationPage:require('./resendConfirmation.js'),
    welcomePage:require('./welcome.js'),
    storyPage:require('./story.js'),
    userPage:require('./user.js'),
    writingPromptPage:require('./writingPrompt.js'),
    navbar: require('./navbar.js'),
    usersPage: require('./users.js'),
    writingPromptsPage: require('./writingPrompts.js'),
    newWritingPromptPage: require('./newWritingPrompt.js'),
    newStoryPage: require('./newStory.js'),
    editStoryPage: require('./editStory.js'),
};
