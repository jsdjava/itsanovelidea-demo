const {appUrl} = require('../../../support/app.json');
const {navbar,loginPage,writingPromptsPage,newWritingPromptPage} = require('../../../support/pages');
const users = require('../../../support/users.json');
const {login,forceVisit,assertSort} = require('../../../support/actions/shared.js');
const [email,password] = Object.entries(users)[0];
const {route} = loginPage;
const {promptsLink} = navbar;

const {    
  createPromptBtn,
  promptSearchInput,
  promptTexts,
} = writingPromptsPage;

const {
  randomPromptWordDiv,
  createWritingPromptTextArea,
  createWritingPromptErrorText,
  randomPromptWordBtn,
  createWritingPromptBtn,
} = newWritingPromptPage;

describe("create writing prompt page",()=>{
  beforeEach(()=>{
    forceVisit(appUrl,route);
    login(email,password);
    cy.get(promptsLink).click();
    cy.get(createPromptBtn).click();
  });
  it("generates a random prompt word on page load",()=>{
    cy.get(randomPromptWordDiv).should('contain','Try creating a prompt that uses');
  })
  it("displays an error message (and no create btn) for too short of a prompt",()=>{
    cy.get(createWritingPromptTextArea).type("asdf");
    cy.get(createWritingPromptErrorText).should('contain','too short');
  });
  it("displays an error message (and no create btn) for too long of a prompt",()=>{
    let longPrompt = '';
    for(let i = 0; i<400; i++){
      longPrompt+="a";
    }
    cy.get(createWritingPromptTextArea).type(longPrompt);
    cy.get(createWritingPromptErrorText).should('contain','too long');
  });
  it("generates a new random prompt word",()=>{
    let randomPromptWord;
    cy.get(randomPromptWordDiv).then(x=>{
      randomPromptWord = x.text().trim();
    });
    cy.get(randomPromptWordBtn).click();
    cy.document()
      .pipe(x=>x.querySelector(randomPromptWordDiv))
      .should(x=>{
        expect(randomPromptWord).to.not.equal(x.innerText.trim());
      });
  })
  it("creates a writing prompt",()=>{
    cy.get(createWritingPromptTextArea).type("DELETE THIS IT IS FOR A TEST DELETE IT");
    cy.get(createWritingPromptBtn).click();
    cy.get(promptSearchInput).should('exist');
  });
  it("displays the prompt in the list of prompts",()=>{
    cy.get(createWritingPromptTextArea).type("DELETE THIS IT IS FOR A TEST DELETE IT");
    cy.get(createWritingPromptBtn).click();
    cy.get(promptSearchInput).type("DELETE THIS IT IS FOR A TEST DELETE IT");
    cy.get(promptTexts).should('exist');
  })
});