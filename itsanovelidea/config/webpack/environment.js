const path = require('path');
const { environment } = require('@rails/webpacker')

const ManifestPlugin = require('webpack-manifest-plugin');
 
environment.config.merge({
  plugins: [
    new ManifestPlugin(),
  ],
  stats: {
    errors: true,
    errorDetails: true,
    colors: true,
    optimizationBailout: true
  },
});

module.exports = environment;
