class AddUserStoryPrimaryKey < ActiveRecord::Migration[5.1]
  def change
    add_column :user_stories, :id, :primary_key
  end
end
