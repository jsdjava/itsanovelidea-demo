# Itsanovelidea-middleware 
Holds node specific functions for
- Accessing redis
- Apikey middleware for express
- Swagger middleware for express
- Health check middleware for express
- Getting users from redis
- Checking user access to stories

## Commands
- `npm run test`
- `npm run lint`

## Publishing
1. Push to gitlab
2. Make an MR from dev to master.
3. Merge the MR

Because I don't want to host verdaccio or sinopia,but I also don't really want to publish to npm, im just going to share this
package using gitlab links and a private token. In order to prevent bad code
from being pushed in even with failed tests, I moved to the dev branch, and I
make it required to merge dev to master, which won't work if the test job
has failed.