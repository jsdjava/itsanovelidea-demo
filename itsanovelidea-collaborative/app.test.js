const supertest = require('supertest');

// I don't want to mock out all of the middlewares included in here
// as I want to test to make sure that they exist as routes
jest.unmock('itsanovelidea-middleware');

// Need to reset modules so that apikey middleware can pick up new
// key value
jest.resetModules();
process.env.API_KEY = 'TEST';
const { redis } = require('itsanovelidea-middleware');

const profiles = require('./profiles.js');
const stories = require('./stories.js');

// Best I can tell, you have to do this to enable __mocks__ for requires of local files,
// but for node modules it just magically happens
jest.mock('./profiles.js');
jest.mock('./stories.js');

const app = require('./app.js');

test('should 401 /health on a missing api key', async () => {
  const res = await supertest(app).get('/health');
  expect(res.statusCode).toEqual(401);
});

test('should return /health', async () => {
  const res = await supertest(app)
    .get('/health')
    .set('apikey', 'TEST');

  // I don't really care what this route returns as a status code as long as it is authed
  // Its likely that some of the health calls could fail, i'm not really sure
  expect(res.statusCode).not.toEqual(401);
  expect(res.body).toEqual(expect.arrayContaining([
    expect.objectContaining({
      name: 'itsanovelidea-collaborative',
    }),
    expect.objectContaining({
      name: 'itsanovelidea-main',
    }),
    expect.objectContaining({
      name: 'redis',
    }),
    expect.objectContaining({
      name: 'mongo',
    }),
  ]));
// Increasing the test timeout because the S3 health check will take 10s to timeout
}, 15000);

test('should return /swagger', async () => {
  const res = await supertest(app)
    .get('/swagger')
  // Looks like the swagger mware redirects from /swagger to like /swagger/index or something
  // like that
    .redirects(1)
    .set('apikey', 'TEST');

  expect(res.statusCode).toEqual(200);
  // Just going to make sure that "swagger" is somewhere inside the giant html response
  expect(res.text).toMatch(/swagger/);
});

test('GET /profiles/1 should pass the userId to getProfile()', async () => {
  await supertest(app)
    .get('/profiles/1')
    .set('apikey', 'TEST');
  expect(profiles().getProfile).toHaveBeenCalledWith('1');
});

test('GET /profiles/1 should return the profile returned from getProfile()', async () => {
  profiles().getProfile.mockReturnValueOnce({ a: 'fake profile' });
  const res = await supertest(app)
    .get('/profiles/1')
    .set('apikey', 'TEST');
  expect(res.body).toMatchObject({ a: 'fake profile' });
});

test('GET /profiles/1 should return a 500 when getProfile() throws an error', async () => {
  profiles().getProfile.mockImplementationOnce(() => {
    throw Error;
  });
  const res = await supertest(app)
    .get('/profiles/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('POST /profiles/1 should pass the userId to createProfile()', async () => {
  await supertest(app)
    .post('/profiles/1')
    .set('apikey', 'TEST');
  expect(profiles().createProfile).toHaveBeenCalledWith('1');
});

test('POST /profiles/1 should return 200', async () => {
  const res = await supertest(app)
    .post('/profiles/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(200);
});

test('POST /profiles/1 should return a 500 when getProfile() throws an error', async () => {
  profiles().createProfile.mockImplementationOnce(() => {
    throw Error;
  });
  const res = await supertest(app)
    .post('/profiles/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('POST /profiles/1 should return 200 when getProfile() throws a DOC_ALREADY_CREATED error', async () => {
  profiles().createProfile.mockImplementationOnce(() => {
    // eslint-disable-next-line
    throw {
      code: 'ERR_DOC_ALREADY_CREATED',
    };
  });
  const res = await supertest(app)
    .post('/profiles/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(200);
});

test('GET /stories/1/history with 5 results, starting at version 50, descending calls getStoryHistory with the right parameters', async () => {
  await supertest(app)
    .get('/stories/1/history?numberResults=5&version=50&ascending=false')
    .set('apikey', 'TEST');
  expect(stories().getStoryHistory).toHaveBeenCalledWith('1', {
    numberResults: 5,
    version: 50,
    ascending: false,
  });
});

test('GET /stories/1/history defaults to 10 results', async () => {
  await supertest(app)
    .get('/stories/1/history')
    .set('apikey', 'TEST');
  expect(stories().getStoryHistory.mock.calls[0][1]).toEqual(expect.objectContaining({
    numberResults: 10,
  }));
});

test('GET /stories/1/history returns the results from story.getStoryHistory()', async () => {
  stories().getStoryHistory.mockReturnValueOnce([{
    timestamp: 1579059498297,
    oldSnapshot: {},
    newSnapshot: {},
    diff: {},
  }]);
  const res = await supertest(app)
    .get('/stories/1/history')
    .set('apikey', 'TEST');
  expect(res.body).toMatchObject([{
    timestamp: 1579059498297,
    oldSnapshot: {},
    newSnapshot: {},
    diff: {},
  }]);
});

test('GET /stories/1/history returns a 500 when story.getStoryHistory() throws an error', async () => {
  stories().getStoryHistory.mockImplementationOnce(() => {
    throw Error;
  });
  const res = await supertest(app)
    .get('/stories/1/history')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('GET /stories/1 should pass the storyId to getStory()', async () => {
  await supertest(app)
    .get('/stories/1')
    .set('apikey', 'TEST');
  expect(stories().getStory).toHaveBeenCalledWith('1');
});

test('GET /stories/1 should return the story returned from getStory()', async () => {
  stories().getStory.mockReturnValueOnce({ a: 'fake story' });
  const res = await supertest(app)
    .get('/stories/1')
    .set('apikey', 'TEST');
  expect(res.body).toMatchObject({ a: 'fake story' });
});

test('GET /stories/1 should return a 500 when getStory() throws an error', async () => {
  stories().getStory.mockImplementationOnce(() => {
    throw Error;
  });
  const res = await supertest(app)
    .get('/stories/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

test('POST /stories/1 should pass the storyId to createStory()', async () => {
  await supertest(app)
    .post('/stories/1')
    .set('apikey', 'TEST');
  expect(stories().createStory).toHaveBeenCalledWith('1');
});

test('POST /stories/1 should return 200', async () => {
  const res = await supertest(app)
    .post('/stories/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(200);
});

test('POST /stories/1 should return a 500 when createStory() throws an error', async () => {
  stories().createStory.mockImplementationOnce(() => {
    throw Error;
  });
  const res = await supertest(app)
    .post('/stories/1')
    .set('apikey', 'TEST');
  expect(res.statusCode).toEqual(500);
});

// I have to force kill redis because otherwise it hangs, causing jest to also hang
afterAll(async () => {
  await redis.end();
});
