const {configure} = require('@storybook/react');

configure(require.context('../app/javascript/components/components', true, /\.stories\.jsx$/), module);