import React from "react";

import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PublishIcon from '@material-ui/icons/Publish';
import BookIcon from '@material-ui/icons/Book';
import Card, {
    CardActions,
    CardActionButtons,
  } from "@material/react-card";
import ReactCardFlip from 'react-card-flip';
import ReactImageFallback from "react-image-fallback";

import StoryViewer from "./StoryViewer.jsx";
import StoryTile from "./StoryTile.jsx";
import Rating from "./Rate.jsx";
import Views from "./Views.jsx";

import '@material/react-card/dist/card.css';
import "../stylesheets/StoryCard.css";


class StoryCard extends React.Component{
    constructor(){
        super();
        this.state={};
    }
    render(){
        const {flipped} = this.state;
        const {
            photo:{
                url,
                deleteStory,
                publishStory,
                backgroundColor,
                text,
                title,
                author,
                authorUrl,
                views,
                rating,
                isOwned,
            }
        } = this.props;

        const deleteIconButton = <IconButton aria-label="Delete">
            <DeleteIcon onClick={deleteStory}/>
        </IconButton>;
        const editIconButton = <IconButton aria-label="Edit">
            <EditIcon onClick={()=>{window.location.href = url.split(".json")[0]+"/edit";}}/>
        </IconButton>;
        const publishIconButton = <IconButton aria-label="Publish">
            <PublishIcon onClick={publishStory}/>
        </IconButton>;
        const readIconButton = <IconButton aria-label="Read">
            <BookIcon onClick={()=>this.setState({flipped:!flipped})}/>
        </IconButton>;
        const viewsComponent = <Views views={views}/>
        const ratingComponent = <Rating editable={false} rating={rating} key={rating}/>;

        const cardActionButtons = isOwned ? <CardActionButtons>
            {deleteIconButton}
            {editIconButton}
            {publishIconButton}
            {readIconButton}
            {viewsComponent}
            {ratingComponent}
        </CardActionButtons> :<CardActionButtons>
            {readIconButton}
            {viewsComponent}
            {ratingComponent}
        </CardActionButtons>;

        //TODO I put this in just for the storybook
        const fullImageUrl = url.startsWith("http") ? url : `${window.location.origin}/${url}`;
        return <ReactCardFlip isFlipped = {flipped}>
        <Card className='mdc-card demo-card demo-basic-with-header storyCardDiv' key = "front" style={{
            backgroundColor,
        }}>
            <img src={fullImageUrl} className="storyCardImage" />
            <div className="cardRightRow">
                <div className='demo-card__primary'>
                    <h3 className='demo-card__title'>
                    {title}
                    </h3>
                    <h5 className='demo-card__subtitle'>
                    by <a href={authorUrl}>{author}</a>
                    </h5>
                </div>
                <StoryViewer story={text}/>
            <CardActions>
                {cardActionButtons}
            </CardActions>
            </div> 
        </Card>
        <StoryTile {...{...this.props,photo:{
            ...this.props.photo,
            imageHeight:"100%",
        }}} key="back">
            <button
                className='smallCloseStoryBtn'
                onClick={(e)=>{
                    e.stopPropagation();
                    this.setState({flipped:false})
                }}>
                ✖
            </button>
        </StoryTile>
        </ReactCardFlip>
    };
}

export default StoryCard;