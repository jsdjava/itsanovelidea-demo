# EC2 role, policy, and attachments

resource "aws_iam_role" "itsanovelidea-service-ec2" {
  name = "${var.itsanovelidea_service_name}-ec2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags = {
    project = "itsanovelidea"
  }
}

resource "aws_iam_policy" "itsanovelidea-service-ec2" {
  name        = "${var.itsanovelidea_service_name}-ec2"
  description = "Grants EC2 instances running the service access to Secrets"
# Technically, they don't all need access to s3...
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": ["arn:aws:s3:::itsanovelidea-photos/*"]
    },
    {
      "Effect": "Allow",
      "Action": [
          "secretsmanager:Describe*",
          "secretsmanager:Get*",
          "secretsmanager:List*" 
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "itsanovelidea-service-ec2-custom" {
  role       = "${aws_iam_role.itsanovelidea-service-ec2.name}"
  policy_arn = "${aws_iam_policy.itsanovelidea-service-ec2.arn}"
}

resource "aws_iam_role_policy_attachment" "itsanovelidea-service-ec2-ecs" {
  role       = "${aws_iam_role.itsanovelidea-service-ec2.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

#Task policy

resource "aws_iam_role" "itsanovelidea-service-task" {
  name = "${var.itsanovelidea_service_name}-task"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags = {
    project = "itsanovelidea"
  }
}

resource "aws_iam_policy" "itsanovelidea-service-task" {
  name        = "${var.itsanovelidea_service_name}-task"
  description = "Grants ECS task access to Secrets"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "secretsmanager:Describe*",
          "secretsmanager:Get*",
          "secretsmanager:List*" 
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "itsanovelidea-service-task-ecs" {
  role = "${aws_iam_role.itsanovelidea-service-task.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "itsanovelidea-service-task-custom" {
  role       = "${aws_iam_role.itsanovelidea-service-task.name}"
  policy_arn = "${aws_iam_policy.itsanovelidea-service-task.arn}"
}

#Service
resource "aws_ecs_service" "itsanovelidea-service" {
  name            = "${var.itsanovelidea_service_name}"
  cluster         = "${var.itsanovelidea_cluster_id}"
  task_definition = "${var.itsanovelidea_task_definition_arn}"
  desired_count   = 1
  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent = 100
}

resource "aws_iam_instance_profile" "itsanovelidea-service" {
  name = "${var.itsanovelidea_service_name}-iam"
  role = "${aws_iam_role.itsanovelidea-service-ec2.name}"
}

data "template_file" "itsanovelidea-service-user-data" {
  template =  "${file("./itsanovelidea-service/user-data.sh")}"

  vars = {
    subdomain="${var.itsanovelidea_subdomain_prefix}"
  }
}

# Had to shorten the name so the filepath wasn't too long. Would have
# preferred itsanovelidea-service-asg
module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "${var.itsanovelidea_service_name}-asg"

  # Launch configuration
  lc_name = "itsanovelidea-service"
  key_name = "itsanovelidea-two"

  image_id             = data.aws_ami.amazon_linux_ecs.id
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.itsanovelidea-service.id
  user_data            = data.template_file.itsanovelidea-service-user-data.rendered

  # Auto scaling group
  asg_name                  = "${var.itsanovelidea_service_name}"

  # Todo this might be totally wrong, definitely check this
  vpc_zone_identifier       = var.itsanovelidea_vpc_subnets
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
}

#AMI for the EC2s in the ECS clusters to use
#For now we only use the AWS ECS optimized ami <https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html>
data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}