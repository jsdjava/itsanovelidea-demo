import React from "react";
import {TextValidator} from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Account from "./components/Account.jsx";

class ForgotPassword extends React.Component{
    render(){
        const {forgotPasswordUrl} = this.props;
        return <Account url = {forgotPasswordUrl}  
            title="Forgot your password?"
            btnDataQaTag="passwordResetBtn"
            btnTitle="Send me reset password instructions"
            commit="Forgot my password"
            formKeys={["email"]}
            render={ (setFormValue,{email})=>
                <div>
                <div className="accountTextField">
                    <TextValidator
                        name="email"
                        label="Your email"
                        value={email}
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <MailOutlineIcon/>
                            </InputAdornment>
                        ),
                        }}
                        type="email"
                        onChange={(e)=>setFormValue("email",e)}
                        data-qa="emailInput"
                        validators={['required', 'isEmail']}
                        errorMessages={['*required', 'email is not valid']}
                    />
                    </div>
                </div>
            }/>
    };
}

export default ForgotPassword;