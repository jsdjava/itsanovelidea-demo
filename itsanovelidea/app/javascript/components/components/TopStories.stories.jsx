import React from 'react';
import TopStories from "./TopStories.jsx";

export default {
  component: TopStories,
  title: 'TopStories',
};

const buildStory=(overrides={})=>({
    photoUrl:"https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg/1024px-Snowy-bellied_hummingbird_%28Amazilia_edward_niveoventer%29_1.jpg",
    backgroundColor:"AAAAAA",
    text:"this is a good story huh",
    name:"title",
    owner:{
        email:"xd@yahoo.com"
    },
    impressions_count:5,
    rating:4.5,
    ...overrides,
})

export const topStories = () => <TopStories
    highestRatedStory={buildStory()}
    mostViewedStory={buildStory({
        text:"man, lots of people like this story",
        name:"popular",
        impressions_count:9000,
    })}
    mostRecentStory={buildStory({
        text:"just started working on this guy",
        name:"cool new story idea",
    })}
/>

