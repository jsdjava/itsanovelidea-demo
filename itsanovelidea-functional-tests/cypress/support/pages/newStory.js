module.exports = {
  createStoryDiv:".CreateStory",
  route:"stories/new?writing_prompt_id=1",
  storyTitleInput:".CreateStory .MuiGrid-root:nth-child(1) input",
  photoSearchInput:".CreateStory .MuiGrid-root:nth-child(2) input",
  storyTitleErrorMessage:".CreateStory .MuiGrid-root:nth-child(1) p.Mui-error",
  photoSearchErrorMessage:".CreateStory .MuiGrid-root:nth-child(2) p.Mui-error",
  photoSearchImages:".photoCarouselDiv .loadingOverlay img",
  photoSearchBackBtn:".arrowContainerDiv:nth-child(1)",
  photoSearchForwardBtn:".arrowContainerDiv:nth-child(3)",
  selectedPhotoImage:".photoCarouselDiv .loadingOverlay .selected",
  createStoryBtn:".CreateStory button",
}