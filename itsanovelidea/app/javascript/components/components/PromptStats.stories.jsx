import React from 'react';
import PromptsStats from "./PromptsStats.jsx";

export default {
  component: PromptsStats,
  title: 'PromptsStats',
};

export const promptsStats = () => <PromptsStats
    writingPromptsCount={1}
    rating={2}
    totalResponses={3}
    totalViews={4}
/>;
