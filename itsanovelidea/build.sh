#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Build Docker Image"
docker build -t itsanovelidea-main .
echo "Push to ECR Repository"
docker tag itsanovelidea-main:latest $AWS_ECR_REPOSITORY/itsanovelidea-main:latest
docker push $AWS_ECR_REPOSITORY/itsanovelidea-main:latest