describe('GET /swagger', () => {
    it('returns 401 without the api key', () => {
      cy.request({
          url:Cypress.config("itsanovelideaPhotosUrl")+"/swagger",
          failOnStatusCode:false,
      }).should(resp=>{
          expect(resp.status).to.equal(401);
      });
    });
    it('returns the swagger html page',()=>{
        cy.request({
            url:Cypress.config("itsanovelideaPhotosUrl")+"/swagger",
            headers:{
                apikey:Cypress.config("apiKey"),
            },
        }).should(resp=>{
            expect(resp.body).to.include("swagger");
        });
    })
  })