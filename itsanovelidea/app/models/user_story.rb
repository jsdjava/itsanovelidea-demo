class UserStory < ApplicationRecord
    belongs_to :user
    belongs_to :story
    validates :rating, :inclusion => 1..5
    validates :user, :story, presence: true
end
