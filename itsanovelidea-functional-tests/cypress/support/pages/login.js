module.exports = {
    route:"users/sign_in",
    emailInput: "[data-qa='emailInput']",
    passwordInput: "[data-qa='passwordInput']",
    loginBtn: "[data-qa='loginBtn']",
    facebookBtn: "[title='facebook']",
    googlePlusBtn: "[title='googlePlus']",
    redditBtn: "[title='reddit']",
};