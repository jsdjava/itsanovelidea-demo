#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Build Docker Image"
docker build -t itsanovelidea-photos .
echo "Push to ECR Repository"
docker tag itsanovelidea-photos:latest $AWS_ECR_REPOSITORY/itsanovelidea-photos:latest
docker push $AWS_ECR_REPOSITORY/itsanovelidea-photos:latest