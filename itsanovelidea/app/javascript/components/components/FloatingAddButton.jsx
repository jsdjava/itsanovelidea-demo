import React from "react";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

import "../stylesheets/FloatingAddButton.css";

class FloatingAddButton extends React.Component{
  render(){
    const {onClick=()=>{}} = this.props;
    return <Fab className = "floatingAddButton" color="primary" aria-label="add">
        <AddIcon onClick={onClick}/>
    </Fab>;
  }
}

export default FloatingAddButton;