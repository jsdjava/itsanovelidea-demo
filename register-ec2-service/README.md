# Overview
Extremely simple container thats used inside ECS running on an EC2 to register that EC2 in AWS Cloud Map.
Basically, binds a public ip address to a subdomain in backend.itsanovelidea.com 
(like 166.71.38.92 being bound to photos.backend.itsanovelidea.com)

# Sample Usage
In your ECS task definition json file, add
```json
    { 
        "image": "<url of docker registry containing this image>",
        "name": "register-ec2-service",
        "environment" : [
            {"name": "INSTANCE_ID", "value" : "<desired subdomain name>" },
            {"name":"SERVICE_ID","value":"service id in cloud map"},
            {"name":"AWS_DEFAULT_REGION","value":"us-east-1"}
        ],
        "secrets": [{
          "name": "AWS_ACCESS_KEY_ID",
          "valueFrom": "<secret containing your access key id>"
        },{
            "name": "AWS_SECRET_ACCESS_KEY",
            "valueFrom": "<secret containing your access key>"
        }],
        "essential":false,
        "memoryReservation": 200
    }
```
Obviously, configure your terraform role to have permission to pull secrets from the secrets manager.

# Links
See [itsanovelidea-terraform/ecs](https://gitlab.com/itsanovelidea/itsanovelidea-terraform/-/tree/master/ecs) for a list of task definitions using it.

See the itsanovelidea.com [cloudmap](https://console.aws.amazon.com/cloudmap/home?region=us-east-1#namespaces) for currently registered services and instances. 