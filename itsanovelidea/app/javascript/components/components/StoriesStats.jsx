import React from "react";
import {
  Table,TableBody,TableCell,TableRow,TableHead,
} from '@material-ui/core';

import Rate from "./Rate.jsx";

import "../stylesheets/StoriesStats.css"

class StoriesStats extends React.Component{
  render(){
    const {
      storiesCount,
      rating,
      totalViews,
    } = this.props;
    return     <div className="storiesStatsDiv">
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>
              Total Stories
            </TableCell>
            <TableCell>
              Average Rating
            </TableCell>
            <TableCell>
              Total Views
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell size="small">{storiesCount}</TableCell>
            <TableCell size="small" classes={{
              root:"ratingTableCell"
            }}><Rate rating={rating}/></TableCell>
            <TableCell size="small">{totalViews}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </div> /* 
    <div className="storiesStatsDiv">
    <p>Total Stories: {storiesCount}</p> 
      <p className="averageRatingParagraph">Average Rating: </p> <Rate rating={rating}/>
      <p>Total Views: {totalViews}</p>
    </div>*/
  }
}

export default StoriesStats;