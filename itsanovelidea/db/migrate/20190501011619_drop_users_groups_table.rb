class DropUsersGroupsTable < ActiveRecord::Migration[5.1]
  def up
    drop_join_table :groups, :users 
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
